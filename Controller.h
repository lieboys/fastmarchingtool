#pragma once

/** Includes - Qt */
#include <QApplication>
#include <QMainWindow>
#include <QIcon>
#include <QPixmap>
#include <QProgressDialog>

/** Includes - UI */
#include "ui_View.h"

/** Includes - VTK */

/** Includes - Main Headers */
#include "Model.h"
#include "Visualization.h"
#include "VisualizationObject.h"

#include "common/ColorMap.h"

#include "processing/CodeLinkManager.h"

/** Forward declarations */
class GeodesicViewController;
class RegionViewController;
class ReferenceViewController;
class DistanceMapViewController;

class Ui_View;
class Model;
class Visualization;

/**
Main controller object, containing the connection to the View (Qt UI).
Handles all Signal/Slot connections to the UI elements. The behaviour of certain UI elements are delegated to seperate ViewControllers.
*/
class Controller : public QMainWindow {
	Q_OBJECT

public:

	enum PanelType {
		PTYPE_NAVIGATION,
		PTYPE_GLYPHS,
		PTYPE_SCALARMAP,
		PTYPE_CONTOURPLOT,
		PTYPE_COLORBAR,
		PTYPE_OUTLINE,
		PTYPE_CONTRAST,
		PTYPE_REGION,
		PTYPE_GEODESICS
	};

	/** Variables */

	/** Constructor */
	Controller();
	~Controller();

	/**
	 * Set the model and visualization
	 * @param model The pointer to the model.
	 */
	void SetMVC(Model* model, Visualization* visualization);

	/**
	* Sets the default state for the UI elements.
	*/
	void Setup();

	/**
	* Updates the Navigation controls based on the available datasets.
	*/
	void UpdateVisualNavControls();

	/**
	* (Un)blocks the signals from all the UI elements.
	*/
	void BlockAllSignals(bool toggle);

	/**
	* Get the user selection from the given datasets, will open a filter dialog.
	*/
	QStringList GetDataSelection(QList<QPair<QString, QString>> data);

	/**
	* Handle event when the external code has finished
	*/
	void CodeFinished(bool succes);

	/**
	* Get the connection to the Qt UI, used for the delegate ViewControllers
	*/
	Ui_View * GetUIConnection();

	/**
	* Adds the visual object of the given VisualType and label to the UI. Removal takes place in the delegate ViewControllers
	*/
	void AddVisualObjectToUi(VisualType type, QString label);

	/**
	* Exports the current visuals as a .PNG screenshot to the given path,.
	*/
	void ExportScreenshots(QString filepath);

	/**
	* Exports the current visuals as a .PNG screenshot to the given path, with a magnification of 4.0x .
	*/
	void ExportScreenshotHighRes(QString filepath);

	/**
	* Global function for checking the locking of UI controls
	*/
	void CheckLockingOfControls();
	/**
	* Helper function for showing a default loading screen.
	*/
	QProgressDialog * LoadingStart(QWidget* parent = nullptr);

	/**
	* Helper function for closing a default loading screen.
	*/
	void LoadingDone(QProgressDialog * dialog);

public slots:


	void slotLoadBinaryTriggered();

	void slotSpeedMapSpecifyTriggered();

	// Connectivity
	void slotComputeConnectivityClicked();

	// CodeLink
	void slotCodeLinkAdvancedButton();
	void slotCodeLinkFolderButtonTriggered();

	void slotRunExternalCode();
	void slotAbortExternalCode();

	// Toolbar
	void slotToolbarNavigationClicked();

	// Nav
	void slotVisPlaneComboBoxChanged(const QString & text);
	void slotVisPlaneSpinBoxChanged(int value);
	void slotVisPlaneSliderChanged(int value);

	void slotButton2DTriggered();
	void slotButton3DTriggered();

	void slotChangeInterpolation(int order);
	
	// Menu
	void slotExportVisualizationTriggered(int intType);
	void slotExportScreenshot();
	void slotEditSettings();
	void slotAbout();

	// Other
	void ThrowError(QString text);

	void slotWatchedFileChange(QString);

private:

	/** Functions */

	/**
	* Connect the Qt GUI elements to slots
	*/
	void connectSignals();

	void slotNavigationChange();


	/**
	* Checks the provided paths, ask for user confirmation and deletes the files.
	*/
	bool checkFilepathsForOverwrite(QStringList check);

	/**
	* Runs the external FM code based on the setting in the user interface.
	*/
	void runFMCode();

	/** Variables */

	// Qt view object
	Ui_View *ui;

	/** Viewcontrollers */
	GeodesicViewController* m_geodesicController;
	RegionViewController* m_regionController;
	ReferenceViewController* m_referenceController;
	DistanceMapViewController* m_distanceMapController;

	// Model
	Model* m_model;

	// Visualization
	Visualization* m_visualization;

	QProgressDialog* m_pseudoDialog;

	CodeLinkManager::CodeRunSettings m_currentExpSettings;
};
