/** Includes */
#include "Model.h"

#include <QMap>
#include <QString>
#include <QDir>
#include <QListIterator>
#include <QMessageBox>
#include <QObject>

#include "Controller.h"

#include "DataObjFactory.h"
#include "DataIOManager.h"
#include "VisualObjFactory.h"
#include "Visualization.h"
#include "VisualizationObject.h"
#include "DataIOManager.h"

#include "processing/ConnectivityMeasure.h"

// TODO:move
#include "DistanceMapSource.h"
#include "RegionSource.h"
#include "ReferenceSource.h"

DataObject* Model::findDataObjectByLabel(QString label) {
    QListIterator<DataObject *> j(m_dataObjects);
    while (j.hasNext()) {
        DataObject *obj = j.next();
        if (obj->label == label) {
            return obj;
        }
    }
    return nullptr;
}

//---------------[ Constructor ]-------------------

Model::Model(QMap<QString, std::string> argumentList) {
	// create database
	this->m_dataFactory = new DataObjFactory();

	// create visualization
	this->m_visualization = new Visualization();

	this->filewatcher = new QFileSystemWatcher();

	this->m_controller = nullptr;
	this->m_visualFactory = nullptr;

	this->m_connectivity = new ConnectivityMeasure();

	// Create and link thread
	this->m_codeLink = new CodeLinkManager();
	this->m_thread = new QThread();

	//connect(this->m_codeLink, SIGNAL(valueChanged(int)), this->m_controller, SLOT(UpdateProgressBar(int)));
	connect(this->m_codeLink, SIGNAL(workRequested()), this->m_thread, SLOT(start()));
	connect(this->m_codeLink, SIGNAL(finished()), this, SLOT(codeRunComplete()));
}

//---------------[ Destructor ]-------------------

Model::~Model() {
}

void Model::Setup() {
}

bool Model::LoadDatasetInDatabase(QString path, DataType type) {

	// Check for existing items with same path
	QListIterator<DataObject*> i(this->m_dataObjects);
	while (i.hasNext()) {
		if (i.next()->getSourcePath() == path) {

			QMessageBox msgBox;
			msgBox.addButton(QMessageBox::No);
			msgBox.addButton(QMessageBox::Yes);
			msgBox.setDefaultButton(QMessageBox::No);
			msgBox.setText("This dataset has already been imported, do you want to import another copy?");

			if (msgBox.exec() == QMessageBox::No) {
				return false;
			}
		}
	}

	// load a distance map, create source object
	DataObject* dataObj = m_dataFactory->makeDataObj(path, type);
	if (!dataObj) {
		return false;
	}

	this->AddToDataObjects(dataObj);

	this->filewatcher->addPath(path);

	bool ok = this->createVisualsFor(dataObj);


	this->m_controller->UpdateVisualNavControls();

	this->m_visualization->UpdateNavigation();
	this->m_visualization->ResetCamera();

	return ok;
}

void Model::RequestNavigationExtent(std::vector<int>& extent) {

	QListIterator<DataObject*> i(this->m_dataObjects);
	while (i.hasNext()) {

		DataObject* obj = i.next();
		if (obj->getDataType() == DTYPE_DISTANCEMAP) {
			int* extentArray = dynamic_cast<DistanceMapSource*>(obj)->data->GetExtent();

			extent[0] = extentArray[0];
			extent[1] = extentArray[1];
			extent[2] = extentArray[2];
			extent[3] = extentArray[3];
			extent[4] = extentArray[4];
			extent[5] = extentArray[5];

			return;
		}
        else if(obj->getDataType() == DTYPE_REFERENCE_VOLUME)
        {
            int* extentArray = dynamic_cast<ReferenceSource*>(obj)->data->GetExtent();
            
            extent[0] = extentArray[0];
            extent[1] = extentArray[1];
            extent[2] = extentArray[2];
            extent[3] = extentArray[3];
            extent[4] = extentArray[4];
            extent[5] = extentArray[5];
            
            return;
        }
	}

	return;
}

void Model::RemoveObject(DataType type, QString label) {

	QList<DataObject *> found;
	DataObject * target = nullptr;

	// First search by type
	QListIterator<DataObject *> i(m_dataObjects);
	while (i.hasNext()) {
		DataObject *obj = i.next();
		if (obj->getDataType() == type) {
			found.append(obj);
		}
	}

	if (found.size() == 1) {
		target = found.first();
	}

	// Second search by label
	if (label != "") {
		QListIterator<DataObject *> j(m_dataObjects);
		while (j.hasNext()) {
			DataObject *obj = j.next();
			if (obj->label == label) {
				target = obj;
			}
		}
	}

	if (target == nullptr) {
		return;
	}

	this->m_dataObjects.removeAll(target);
	this->m_visualization->RemoveVisualObjectFrom(target);

	this->m_controller->CheckLockingOfControls();
}

void Model::ReimportDatasetAt(QString path) {

	// Find obj
	DataObject* obj = nullptr;
	for (int i = 0; i < this->m_dataObjects.size(); i++) {

		if (this->m_dataObjects.at(i)->getSourcePath() == path) {
			obj = this->m_dataObjects.at(i);
		}
	}

	if (obj == nullptr) {
		std::cout << "Object not found with path '" << path.toStdString().c_str() << "' " << std::endl;
		return;
	}

	obj->Reimport();

	// Update child visuals
	this->m_visualization->UpdateVisualObjectFrom(obj);
}

void Model::ExportVolumeData(int intType, QString filepath) {

	Projection::ProjectionType type = Projection::ProjectionType(intType);

	VisualizationObject* visuals = this->m_visualization->GetVisualObjectByType(VisualType::VTYPE_SCALARMAP);
	DistanceMapSource* source = dynamic_cast<DistanceMapSource*>(visuals->getDataObject());
	SP(vtkImageData) data = this->m_visualization->GetExportData(VTYPE_SCALARMAP, type);

	if (data == nullptr) {
		return;
	}

	QString description;
	if (type == Projection::ProjectionType::MINIMUM) {
		description = "DistanceMap: minimum projection";
	}
	else if (type == Projection::ProjectionType::COMPOSITE) {
		description = "DistanceMap: composite projection";
	}
	else if (type == Projection::ProjectionType::GEODESIC) {
		description = "DistanceMap: geodesic projection";
	}

	DataIOManager::WriteNifti3D(data, filepath, description, source->transform);
}

void Model::ShowInExplorer(QString path, bool highlight) {

	if (path == "") {
		return;
	}

#ifdef _WIN32

	QString cmd;
	if (highlight) {
		cmd = "explorer.exe /select," + QDir::toNativeSeparators(path);
	}
	else {
		cmd = "explorer.exe " + QDir::toNativeSeparators(path);
	}

	system(cmd.toStdString().c_str());

#else

	QString cmd = "open -R " + QDir::toNativeSeparators(path);
	system(cmd.toStdString().c_str());

#endif
}

void Model::ComputeConnectivity(QString regionALabel,
                                QString regionBLabel,
                                QString finslerMap,
                                QString euclideanMap) {
	// grab regions and distance maps and give it to compute function
//    DataObject* regionA = findDataObjectByLabel(regionALabel);
//    DataObject* regionB = findDataObjectByLabel(regionBLabel);
//    DataObject* distanceMap = findDataObjectByLabel(BinairDefinitions::DMAP_LABEL_FINSLER_4D);
//    DataObject* distanceMapEuclidean = findDataObjectByLabel(BinairDefinitions::DMAP_LABEL_EUCLIDEAN_4D);
	// todo: warning if 3D

//    assert(regionA && regionB && distanceMap && distanceMapEuclidean);

//    m_connectivity->Compute(regionA, regionB, distanceMap, distanceMapEuclidean);
}

void Model::AddToDataObjects(DataObject* dataObj) {

	QString newLabel = dataObj->label;
	int cnt = 1;
	while (this->findDataObjectByLabel(newLabel) != nullptr) {
		newLabel = dataObj->label + " " + QString::number(cnt);
		cnt++;
	}
	dataObj->label = newLabel;

	this->m_dataObjects.append(dataObj);

	this->m_controller->CheckLockingOfControls();
}

bool Model::LoadFromBinaryFile(QString headerPath, QString dataPath) {

	bool ok = true;

	QList<QPair<QString, QString>> data = DataIOManager::ReadBinaryHeaderOnly(headerPath);

	QStringList filter = this->m_controller->GetDataSelection(data);
	if (filter.empty()) {
		return false;
	}

	QList<DataObject*> objects =
		this->m_dataFactory->makeDataObjectsFromBinary(headerPath, dataPath, filter);

	QListIterator<DataObject*> i(objects);
	while (i.hasNext()) {
		DataObject* obj = i.next();

		if (obj == nullptr) {
			continue;
		}

		this->AddToDataObjects(obj);
		this->filewatcher->addPath(headerPath);

		if (!this->createVisualsFor(obj)) {
			ok = false;
		}
	}

	this->m_visualization->UpdateNavigation();
	this->m_visualization->ResetCamera();

	return ok;
}

bool Model::createVisualsFor(DataObject* dataObj) {

	bool ok = false;

	switch (dataObj->getDataType()) {
	case DTYPE_NONE:
		break;
	case DTYPE_DISTANCEMAP: {

		VisualizationObject* visualObj = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_SCALARMAP);
		m_visualization->AddVisualObject(visualObj);

		VisualizationObject* glyphs = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_GLYPHS);
		m_visualization->AddVisualObject(glyphs);

		VisualizationObject* contours = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_CONTOURPLOT);
		m_visualization->AddVisualObject(contours);

		if (visualObj && glyphs && contours) {
			ok = true;
		}

		break;
	}
	case DTYPE_ROI:
	{
		VisualizationObject* visualObj = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_ROI);
		m_visualization->AddVisualObject(visualObj);

		if (visualObj) {
			ok = true;
		}

		break;
	}
	case DTYPE_GEODESIC:
	{
		VisualizationObject* visualObj = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_GEODESIC);
		m_visualization->AddVisualObject(visualObj);

		if (visualObj) {
			ok = true;
		}

		break;
	}
	case DTYPE_REFERENCE_VOLUME:
	{
		VisualizationObject* visualObj = this->m_visualFactory->makeVisualObj(dataObj, VisualType::VTYPE_ANATOMICAL_VOLUME);
		m_visualization->AddVisualObject(visualObj);

		if (visualObj) {
			ok = true;
		}

		break;
	}
	}

	return ok;
}

void Model::RunCode(QProgressDialog* progressDialog) {

	// Move and connect to thread
	this->m_codeLink->moveToThread(this->m_thread);
	connect(this->m_thread, SIGNAL(started()), this->m_codeLink, SLOT(doWork()));
//    connect(this->m_codeLink, SIGNAL(valueChanged(int)), progressDialog, SLOT(setValue(int)));
    connect(this->m_codeLink, SIGNAL(stringValueChanged(QString)), progressDialog, SLOT(setLabelText(QString)));

	// Start computation on thread
	this->m_codeLink->Abort();
	this->m_thread->wait();
	this->m_codeLink->RequestWork();
}

void Model::SetupCode(QString runOption,
                      CodeLinkManager::CodeRunSettings m_settings,
					  QString command,
					  QString speedPath) {

	// Get speed map
	SP(vtkTransform) transform = SP(vtkTransform)::New();;
	SP(vtkImageData) speed = DataIOManager::ReadNifti3D(speedPath, transform);

	// Find regions
	SP(vtkImageData) seed = nullptr;
	if (m_settings.seedName != "") {
		RegionSource* obj = dynamic_cast<RegionSource*>(findDataObjectByLabel(m_settings.seedName));
		if (obj) {
			seed = obj->data;
		}
	}

	SP(vtkImageData) tip = nullptr;
	if (m_settings.tipName != "") {
		RegionSource* obj = dynamic_cast<RegionSource*>(findDataObjectByLabel(m_settings.tipName));
		if (obj) {
			tip = obj->data;
		}
	}

    if(runOption == "LOCAL")
        this->m_codeLink->SetCommandPath(command);
    else if(runOption == "REMOTE")
        this->m_codeLink->SetRemoteSSHCommand(command);
	this->m_codeLink->SetupCodeRun(runOption, m_settings, speed, seed, tip);
}

void Model::GenerateFMFilesOnly(
                                // path settings
                                QString experimentName,
                                QString workFolder,
                                
                                // code parameters
                                double xi,
                                double epsilon,
                                double sigma,
                                double p,
                                double gridScale,
                                QString bundleType,
                                QString bundleMesh,
                                bool exportValuesLift,
                                bool exportValuesEucl,
                                bool applyFODTransformation,
                                bool transposeCoordinates,
                                bool stopWhenTipsReached,
                                
                                QString speedPath,
                                QString seedPath,
                                QString tipPath)
{
    CodeLinkManager::CodeRunSettings m_settings;
    m_settings.params.epsilon = epsilon;
    m_settings.params.xi = xi;
    m_settings.params.sigma = sigma;
    m_settings.params.p = p;
    m_settings.params.bundleMesh = bundleMesh;
    m_settings.params.bundleType = bundleType;
    m_settings.params.gridScale = gridScale;
    m_settings.params.exportValuesEucl = exportValuesEucl;
    m_settings.params.exportValuesLift = exportValuesLift;
    m_settings.params.applyFODTransformation = applyFODTransformation;
    m_settings.params.transposeCoordinates = transposeCoordinates;
    m_settings.params.stopWhenTipsReached = stopWhenTipsReached;
    m_settings.name = experimentName;
    m_settings.workfolder = workFolder;
    
    m_settings.inputBinaryPath = QDir(workFolder).absoluteFilePath(experimentName + "_input_Data.dat");
    m_settings.inputHeaderPath = QDir(workFolder).absoluteFilePath(experimentName + "_input_Format.txt");
    m_settings.outputBinaryPath = QDir(workFolder).absoluteFilePath(experimentName + "_output_Data.dat");
    m_settings.outputHeaderPath = QDir(workFolder).absoluteFilePath(experimentName + "_output_Format.txt");
    
    SP(vtkTransform) transform = SP(vtkTransform)::New();;
    
    // Get speed map
    SP(vtkImageData) speed = DataIOManager::ReadNifti3D(speedPath, transform);
    
    // Get seed
    SP(vtkImageData) seed = DataIOManager::ReadNifti3D(speedPath, transform);
    
    // Get tip
    SP(vtkImageData) tip = DataIOManager::ReadNifti3D(tipPath, transform);
    
    // Generate files
    this->m_codeLink->SetupCodeRun("LOCAL", m_settings, speed, seed, tip);
}

void Model::codeRunComplete() {

	this->m_thread->quit();
	disconnect(this->m_thread, SIGNAL(started()), this->m_codeLink, SLOT(doWork()));

	bool succes = true;
	if (!QFile(this->m_codeLink->GetSettings().outputBinaryPath).exists()) {
		succes = false;
	}
	if (!QFile(this->m_codeLink->GetSettings().outputHeaderPath).exists()) {
		succes = false;
	}

	QString output = this->m_codeLink->GetOutputMsg();
	if (output.contains("----- ! Warning ! -----")) {
		succes = false;
	}

	if (!succes) {
		this->m_controller->ThrowError(output);
	}

	this->m_controller->CodeFinished(succes);
}

void Model::AbortCode() {

	this->m_codeLink->Abort();
	this->m_thread->quit();
}

void Model::OpenITKSnap(QString cmd, QString dataLabelRoi) {

	// Lookup region object
	QString roiPath;
	RegionSource* obj = dynamic_cast<RegionSource*>(this->findDataObjectByLabel(dataLabelRoi));
	if (obj != nullptr) {
		roiPath = obj->getSourcePath();;
	}

	// Lookup volume object
	QString volPath;
	VisualizationObject* vObj = this->m_visualization->GetVisualObjectByType(VisualType::VTYPE_ANATOMICAL_VOLUME);
	if (vObj != nullptr) {
		ReferenceSource* vol = dynamic_cast<ReferenceSource*>(this->findDataObjectByLabel(vObj->GetSettings()->label));
		if (vol != nullptr) {
			volPath = vol->getSourcePath();
		}
	}

	// Format: itksnap -g grey.img -s segmentation.img
	QString exec;
	if (volPath != "" && roiPath == "") {
		exec = "\"" + cmd + "\" -g \"" + volPath + "\"";
	}
	else if (volPath == "" && roiPath != "") {
		exec = "\"" + cmd + "\" \"" + roiPath + "\"";
	}
	else if (volPath != "" && roiPath != "") {
		exec = "\"" + cmd + "\" -s \"" + roiPath + "\" -g \"" + volPath + "\"";
	}

	QProcess* process = new QProcess();
	process->startDetached(exec);
}

bool Model::CheckLocking(Model::LockType type) {

	switch (type) {
	case Model::GeodesicProjectionOfDistanceMap: {

		QListIterator<DataObject*> i(this->m_dataObjects);
		while (i.hasNext()) {
			if (i.next()->getDataType() == DataType::DTYPE_GEODESIC) {
				return true;
			}
		}
		break; }
	case Model::NewManualRegionFromReference: {

		QListIterator<DataObject*> i(this->m_dataObjects);
		while (i.hasNext()) {
			if (i.next()->getDataType() == DataType::DTYPE_REFERENCE_VOLUME) {
				return true;
			}
		}
		break;
	}
	case Model::AnyDatasetImported: {
		return (this->m_dataObjects.size() != 0);
		break;
	}
	case Model::DistanceMap: {

		QListIterator<DataObject*> i(this->m_dataObjects);
		while (i.hasNext()) {
			if (i.next()->getDataType() == DataType::DTYPE_DISTANCEMAP) {
				return true;
			}
		}
		break;
	}
	default:
		return false;
		break;
	}

	return false;
}
