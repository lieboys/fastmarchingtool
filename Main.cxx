/*
 * Main.cxx
 *
 * 2017-03-03	Stephan Meesters & Stefan Marien
 * - First version
 *
 */

 /** Includes - Qt */
#include <QApplication>
#include <QMainWindow>
#include <QMap>
#include <QPalette>
#include <QPixmap>
#include <QSplashScreen>
#include <QFile>

/** Includes - TCLAP */
#include "tclap/CmdLine.h"

/** Includes - Main Headers */
#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include <vtkObject.h>

int main(int argc, char* argv[]) {
	try {
		// Qt setup
		QApplication app(argc, argv);
		app.setApplicationName("Fast Marching Tool");
		app.setOrganizationName("Eindhoven University of Technology & Kempenhaeghe");
		app.setOrganizationDomain("tue.nl");

		//		QPixmap pixmap(":/splash");
		//		QSplashScreen splash(pixmap);
		//		splash.show();
		app.processEvents();

		// Load style sheet
		QFile file(":/stylesheet");
		file.open(QFile::ReadOnly);
		QString styleSheet = QLatin1String(file.readAll());
		app.setStyleSheet(styleSheet);

		// Create argument list dictionary
		QMap<QString, std::string> argumentList;

		// Disable vtk warning window
		vtkObject::GlobalWarningDisplayOff();

		// Create model and controller and visualization
		Controller* controller = new Controller();
		Model* model = new Model(argumentList);
		Visualization* visualization = new Visualization();
		controller->SetMVC(model, visualization);
		model->SetMVC(controller, visualization);
		visualization->setMVC(controller, model);

        // Run from command line only
        if(argc > 1)
        {
            // Command line arguments setup
            
            // mandatory
            TCLAP::CmdLine cmd("Command description message", ' ', "0.9");
            TCLAP::ValueArg<std::string> experiment_name("a", "experimentName", "experimentName", true, "", "experimentName");
            TCLAP::ValueArg<std::string> work_folder("b", "workFolder", "workFolder", true, "", "workFolder");
            TCLAP::ValueArg<double> xi("c", "xi", "xi", true, 1.0, "xi");
            TCLAP::ValueArg<double> epsilon("d", "epsilon", "epsilon", true, 1.0, "epsilon");
            TCLAP::ValueArg<std::string> speed_path("o", "fodPath", "fodPath", true, "", "fodPath");
            TCLAP::ValueArg<std::string> seed_path("p", "seedPath", "seedPath", true, "", "seedPath");
            TCLAP::ValueArg<std::string> tip_path("q", "tipPath", "tipPath", true, "", "tipPath");
            
            // optional
            TCLAP::ValueArg<double> sigma("e", "sigma", "sigma", false, 1.0, "sigma");
            TCLAP::ValueArg<double> p("f", "p", "p", false, 1.0, "p");
            TCLAP::ValueArg<double> grid_scale("g", "gridScale", "gridScale", false, 1.0, "gridScale");
            TCLAP::ValueArg<std::string> bundle_mesh("w", "bundleMesh", "bundleMesh", false, "icosahedron4", "bundleMesh");
            TCLAP::ValueArg<std::string> bundle_type("i", "bundleType", "bundleType", false, "ReedSheppCar", "bundleType");
            TCLAP::ValueArg<bool> export_values_lifted("j", "exportValuesLift", "exportValuesLift", false, true, "exportValuesLift");
            TCLAP::ValueArg<bool> export_values_euclidean("k", "exportValuesEucl", "exportValuesEucl", false, true, "exportValuesEucl");
            TCLAP::ValueArg<bool> apply_fod_transformation("l", "applyFODTransformation", "applyFODTransformation", false, true, "applyFODTransformation");
            TCLAP::ValueArg<bool> transpose_coordinates("m", "transposeCoordinates", "transposeCoordinates", false, true, "transposeCoordinates");
            TCLAP::ValueArg<bool> stop_when_tips_reached("n", "stopWhenTipsReached", "stopWhenTipsReached", false, false, "stopWhenTipsReached");
            
            cmd.add(experiment_name);
            cmd.add(work_folder);
            cmd.add(xi);
            cmd.add(epsilon);
            cmd.add(speed_path);
            cmd.add(seed_path);
            cmd.add(tip_path);
            cmd.add(sigma);
            cmd.add(p);
            cmd.add(grid_scale);
            cmd.add(bundle_mesh);
            cmd.add(bundle_type);
            cmd.add(export_values_lifted);
            cmd.add(export_values_euclidean);
            cmd.add(transpose_coordinates);
            cmd.add(stop_when_tips_reached);
            cmd.add(apply_fod_transformation);
            
            // Parse the args.
            cmd.parse( argc, argv );
            
            model->GenerateFMFilesOnly(QString::fromStdString(experiment_name.getValue()),
                                       QString::fromStdString(work_folder.getValue()),
                                       xi.getValue(),
                                       epsilon.getValue(),
                                       sigma.getValue(),
                                       p.getValue(),
                                       grid_scale.getValue(),
                                       QString::fromStdString(bundle_type.getValue()),
                                       QString::fromStdString(bundle_mesh.getValue()),
                                       export_values_lifted.getValue(),
                                       export_values_euclidean.getValue(),
                                       apply_fod_transformation.getValue(),
                                       transpose_coordinates.getValue(),
                                       stop_when_tips_reached.getValue(),
                                       QString::fromStdString(speed_path.getValue()),
                                       QString::fromStdString(seed_path.getValue()),
                                       QString::fromStdString(tip_path.getValue()));
        }
        
        // Run full interface
        else
        {
            // Show the app
            controller->show();

            // Setup
            controller->Setup();
            
            return app.exec();
        }
	}
	catch (TCLAP::ArgException &e)  // catch any exceptions
	{
		cerr << "error: " << e.error() << " for arg " << e.argId() << endl;
		return EXIT_FAILURE;
	}

	return 0;
}
