
#include "ColorMap.h"

#include <vtkColorTransferFunction.h>
#include <vtkSmartPointer.h>
#include <vtkLookupTable.h>

ColorMap* ColorMap::fromString(QString str) {
	if (str == "Greyscale") {
		return new ColorMap(ColorMapValue::Greyscale);
	}
	else if (str == "Hotmetal") {
		return new ColorMap(ColorMapValue::Hotmetal);
	}
	else if (str == "Rainbow") {
		return new ColorMap(ColorMapValue::Rainbow);
	}
	else if (str == "FlowDark") {
		return new ColorMap(ColorMapValue::FlowDark);
	}
	else if (str == "FlowLight") {
		return new ColorMap(ColorMapValue::FlowLight);
	}
	else if (str == "Temperature") {
		return new ColorMap(ColorMapValue::Temperature);
	}
	else if (str == "Red") {
		return new ColorMap(ColorMapValue::Red);
	}
	else if (str == "Blue") {
		return new ColorMap(ColorMapValue::Blue);
	}
	else if (str == "Green") {
		return new ColorMap(ColorMapValue::Green);
	}
	else if (str == "Inferno") {
		return new ColorMap(ColorMapValue::Inferno);
	}
	else if (str == "Ocean") {
		return new ColorMap(ColorMapValue::Ocean);
	}
	else if (str == "Cortex") {
		return new ColorMap(ColorMapValue::Cortex);
	}
    return nullptr;
}
ColorMap::ColorMap(ColorMapValue val) {
	this->value = val;
}

QString ColorMap::toString() {
	if (this->value == ColorMapValue::Greyscale) {
		return "Greyscale";
	}
	else if (this->value == ColorMapValue::Hotmetal) {
		return "Hotmetal";
	}
	else if (this->value == ColorMapValue::Rainbow) {
		return "Rainbow";
	}
	else if (this->value == ColorMapValue::FlowDark) {
		return "FlowDark";
	}
	else if (this->value == ColorMapValue::FlowLight) {
		return "FlowLight";
	}
	else if (this->value == ColorMapValue::Temperature) {
		return "Temperature";
	}
	else if (this->value == ColorMapValue::Red) {
		return "Red";
	}
	else if (this->value == ColorMapValue::Blue) {
		return "Blue";
	}
	else if (this->value == ColorMapValue::Green) {
		return "Green";
	}
	else if (this->value == ColorMapValue::Inferno) {
		return "Inferno";
	}
	else if (this->value == ColorMapValue::Ocean) {
		return "Ocean";
	}
	else if (this->value == ColorMapValue::Cortex) {
		return "Cortex";
	}
    return nullptr;
}

QList<QString> ColorMap::ListAllFunctional() {
	QList<QString> out;

	out \
		//<< "Greyscale"
		<< "Hotmetal"
		<< "Rainbow"
		//<< "Cortex"
		<< "FlowDark"
		<< "FlowLight"
		//<< "Temperature"
		//<< "Red"
		//<< "Blue"
		//<< "Green"
		<< "Inferno"
		<< "Ocean";

	return out;
}


QList<QString> ColorMap::ListAll() {
	QList<QString> out;

	out \
		<< "Greyscale"
		<< "Hotmetal"
		<< "Rainbow"
		<< "Cortex"
		<< "FlowDark"
		<< "FlowLight"
		<< "Temperature"
		<< "Red"
		<< "Blue"
		<< "Green"
		<< "Inferno"
		<< "Ocean";

	return out;
}

SP(vtkLookupTable) ColorMap::CreateLookupTable(double min, double max) {
	VTK_CREATE(vtkColorTransferFunction, transFunc);
	transFunc->SetColorSpaceToRGB();

	double range = max - min;

	switch (this->value) {
	case ColorMap::Greyscale:

		transFunc->AddRGBPoint(min + 0.0, 0, 0, 0); //black
		transFunc->AddRGBPoint(min + range, 1, 1, 1); //white
		break;

	case ColorMap::Hotmetal:

		transFunc->AddRGBPoint(min + 0.000 * range, 010.0 / 255, 000.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 0.375 * range, 255.0 / 255, 000.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 0.750 * range, 255.0 / 255, 255.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 1.000 * range, 255.0 / 255, 255.0 / 255, 255.0 / 255);
		break;

	case ColorMap::Rainbow:

		transFunc->AddRGBPoint(min + 0.0000 * range, 000.0 / 255, 000.0 / 255, 143.0 / 255);
		transFunc->AddRGBPoint(min + 0.1250 * range, 255.0 / 255, 000.0 / 255, 255.0 / 255);
		transFunc->AddRGBPoint(min + 0.3125 * range, 000.0 / 255, 255.0 / 255, 255.0 / 255);
		transFunc->AddRGBPoint(min + 0.6250 * range, 255.0 / 255, 255.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 0.8750 * range, 255.0 / 255, 000.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 1.0000 * range, 128.0 / 255, 000.0 / 255, 000.0 / 255);
		break;

	case ColorMap::FlowDark:

		//transFunc->SetColorSpaceToDiverging();
		transFunc->AddRGBPoint(min + 0.00 * range, 000.0 / 255, 255.0 / 255, 255.0 / 255); //Cyan
		transFunc->AddRGBPoint(min + 0.25 * range, 000.0 / 255, 000.0 / 255, 200.0 / 255); //Blue
		transFunc->AddRGBPoint(min + 0.50 * range, 000.0 / 255, 000.0 / 255, 000.0 / 255); //Black
		transFunc->AddRGBPoint(min + 0.75 * range, 200.0 / 255, 000.0 / 255, 000.0 / 255); //Red
		transFunc->AddRGBPoint(min + 1.00 * range, 255.0 / 255, 255.0 / 255, 000.0 / 255); //White
		break;

	case ColorMap::FlowLight:

		//transFunc->SetColorSpaceToDiverging();
		transFunc->AddRGBPoint(min + 0.000 * range, 000.0 / 255, 000.0 / 255, 128.0 / 255);
		transFunc->AddRGBPoint(min + 0.125 * range, 000.0 / 255, 000.0 / 255, 255.0 / 255);
		transFunc->AddRGBPoint(min + 0.375 * range, 000.0 / 255, 255.0 / 255, 255.0 / 255);
		transFunc->AddRGBPoint(min + 0.625 * range, 255.0 / 255, 255.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 0.875 * range, 255.0 / 255, 000.0 / 255, 000.0 / 255);
		transFunc->AddRGBPoint(min + 1.000 * range, 128.0 / 255, 000.0 / 255, 000.0 / 255);
		break;

	case ColorMap::Temperature:
		transFunc->SetColorSpaceToDiverging();

		transFunc->AddRGBPoint(min + 0.0 * range, 0, 0, 1);
		transFunc->AddRGBPoint(min + 0.5 * range, 1, 1, 1);
		transFunc->AddRGBPoint(min + 1.0 * range, 1, 0, 0);
		break;

	case ColorMap::Red:

		transFunc->AddRGBPoint(min + 0.0 * range, 0, 0, 0);
		transFunc->AddRGBPoint(min + 0.5 * range, 1, 0, 0);
		transFunc->AddRGBPoint(min + 1.0 * range, 1, 1, 1);
		break;

	case ColorMap::Green:

		transFunc->AddRGBPoint(min + 0.0 * range, 0, 0, 0);
		transFunc->AddRGBPoint(min + 0.5 * range, 0, 1, 0);
		transFunc->AddRGBPoint(min + 1.0 * range, 1, 1, 1);
		break;

	case ColorMap::Blue:

		transFunc->AddRGBPoint(min + 0.0 * range, 0, 0, 0);
		transFunc->AddRGBPoint(min + 0.5 * range, 0, 0, 1);
		transFunc->AddRGBPoint(min + 1.0 * range, 1, 1, 1);
		break;

	case ColorMap::Inferno:
		transFunc->AddRGBPoint(min + 0.0 / 255 * range, 0.00, 0.00, 0.01);
		transFunc->AddRGBPoint(min + 2.0 / 255 * range, 0.00, 0.00, 0.02);
		transFunc->AddRGBPoint(min + 4.0 / 255 * range, 0.01, 0.00, 0.04);
		transFunc->AddRGBPoint(min + 6.0 / 255 * range, 0.01, 0.01, 0.06);
		transFunc->AddRGBPoint(min + 8.0 / 255 * range, 0.01, 0.01, 0.07);
		transFunc->AddRGBPoint(min + 10.0 / 255 * range, 0.02, 0.02, 0.09);
		transFunc->AddRGBPoint(min + 12.0 / 255 * range, 0.03, 0.02, 0.11);
		transFunc->AddRGBPoint(min + 14.0 / 255 * range, 0.03, 0.02, 0.12);
		transFunc->AddRGBPoint(min + 16.0 / 255 * range, 0.04, 0.03, 0.14);
		transFunc->AddRGBPoint(min + 18.0 / 255 * range, 0.05, 0.03, 0.16);
		transFunc->AddRGBPoint(min + 20.0 / 255 * range, 0.06, 0.04, 0.18);
		transFunc->AddRGBPoint(min + 22.0 / 255 * range, 0.07, 0.04, 0.20);
		transFunc->AddRGBPoint(min + 24.0 / 255 * range, 0.08, 0.04, 0.22);
		transFunc->AddRGBPoint(min + 26.0 / 255 * range, 0.09, 0.05, 0.23);
		transFunc->AddRGBPoint(min + 28.0 / 255 * range, 0.10, 0.05, 0.25);
		transFunc->AddRGBPoint(min + 30.0 / 255 * range, 0.12, 0.05, 0.27);
		transFunc->AddRGBPoint(min + 32.0 / 255 * range, 0.13, 0.05, 0.29);
		transFunc->AddRGBPoint(min + 34.0 / 255 * range, 0.14, 0.05, 0.31);
		transFunc->AddRGBPoint(min + 36.0 / 255 * range, 0.16, 0.04, 0.33);
		transFunc->AddRGBPoint(min + 38.0 / 255 * range, 0.17, 0.04, 0.34);
		transFunc->AddRGBPoint(min + 40.0 / 255 * range, 0.18, 0.04, 0.35);
		transFunc->AddRGBPoint(min + 42.0 / 255 * range, 0.20, 0.04, 0.37);
		transFunc->AddRGBPoint(min + 44.0 / 255 * range, 0.21, 0.04, 0.38);
		transFunc->AddRGBPoint(min + 46.0 / 255 * range, 0.22, 0.04, 0.39);
		transFunc->AddRGBPoint(min + 48.0 / 255 * range, 0.24, 0.04, 0.40);
		transFunc->AddRGBPoint(min + 50.0 / 255 * range, 0.25, 0.04, 0.40);
		transFunc->AddRGBPoint(min + 52.0 / 255 * range, 0.26, 0.04, 0.41);
		transFunc->AddRGBPoint(min + 54.0 / 255 * range, 0.28, 0.04, 0.41);
		transFunc->AddRGBPoint(min + 56.0 / 255 * range, 0.29, 0.05, 0.42);
		transFunc->AddRGBPoint(min + 58.0 / 255 * range, 0.30, 0.05, 0.42);
		transFunc->AddRGBPoint(min + 60.0 / 255 * range, 0.32, 0.05, 0.43);
		transFunc->AddRGBPoint(min + 62.0 / 255 * range, 0.33, 0.06, 0.43);
		transFunc->AddRGBPoint(min + 64.0 / 255 * range, 0.34, 0.06, 0.43);
		transFunc->AddRGBPoint(min + 66.0 / 255 * range, 0.35, 0.07, 0.43);
		transFunc->AddRGBPoint(min + 68.0 / 255 * range, 0.37, 0.07, 0.43);
		transFunc->AddRGBPoint(min + 70.0 / 255 * range, 0.38, 0.08, 0.43);
		transFunc->AddRGBPoint(min + 72.0 / 255 * range, 0.39, 0.08, 0.43);
		transFunc->AddRGBPoint(min + 74.0 / 255 * range, 0.40, 0.09, 0.43);
		transFunc->AddRGBPoint(min + 76.0 / 255 * range, 0.42, 0.09, 0.43);
		transFunc->AddRGBPoint(min + 78.0 / 255 * range, 0.43, 0.09, 0.43);
		transFunc->AddRGBPoint(min + 80.0 / 255 * range, 0.44, 0.10, 0.43);
		transFunc->AddRGBPoint(min + 82.0 / 255 * range, 0.45, 0.10, 0.43);
		transFunc->AddRGBPoint(min + 84.0 / 255 * range, 0.47, 0.11, 0.43);
		transFunc->AddRGBPoint(min + 86.0 / 255 * range, 0.48, 0.11, 0.43);
		transFunc->AddRGBPoint(min + 88.0 / 255 * range, 0.49, 0.12, 0.43);
		transFunc->AddRGBPoint(min + 90.0 / 255 * range, 0.50, 0.12, 0.42);
		transFunc->AddRGBPoint(min + 92.0 / 255 * range, 0.52, 0.13, 0.42);
		transFunc->AddRGBPoint(min + 94.0 / 255 * range, 0.53, 0.13, 0.42);
		transFunc->AddRGBPoint(min + 96.0 / 255 * range, 0.54, 0.13, 0.42);
		transFunc->AddRGBPoint(min + 98.0 / 255 * range, 0.55, 0.14, 0.41);
		transFunc->AddRGBPoint(min + 100.0 / 255 * range, 0.57, 0.14, 0.41);
		transFunc->AddRGBPoint(min + 102.0 / 255 * range, 0.58, 0.15, 0.40);
		transFunc->AddRGBPoint(min + 104.0 / 255 * range, 0.59, 0.15, 0.40);
		transFunc->AddRGBPoint(min + 106.0 / 255 * range, 0.60, 0.16, 0.40);
		transFunc->AddRGBPoint(min + 108.0 / 255 * range, 0.62, 0.16, 0.39);
		transFunc->AddRGBPoint(min + 110.0 / 255 * range, 0.63, 0.17, 0.39);
		transFunc->AddRGBPoint(min + 112.0 / 255 * range, 0.64, 0.17, 0.38);
		transFunc->AddRGBPoint(min + 114.0 / 255 * range, 0.65, 0.18, 0.38);
		transFunc->AddRGBPoint(min + 116.0 / 255 * range, 0.66, 0.18, 0.37);
		transFunc->AddRGBPoint(min + 118.0 / 255 * range, 0.68, 0.19, 0.36);
		transFunc->AddRGBPoint(min + 120.0 / 255 * range, 0.69, 0.19, 0.36);
		transFunc->AddRGBPoint(min + 122.0 / 255 * range, 0.70, 0.20, 0.35);
		transFunc->AddRGBPoint(min + 124.0 / 255 * range, 0.71, 0.20, 0.34);
		transFunc->AddRGBPoint(min + 126.0 / 255 * range, 0.72, 0.21, 0.34);
		transFunc->AddRGBPoint(min + 128.0 / 255 * range, 0.74, 0.22, 0.33);
		transFunc->AddRGBPoint(min + 130.0 / 255 * range, 0.75, 0.22, 0.32);
		transFunc->AddRGBPoint(min + 132.0 / 255 * range, 0.76, 0.23, 0.32);
		transFunc->AddRGBPoint(min + 134.0 / 255 * range, 0.77, 0.24, 0.31);
		transFunc->AddRGBPoint(min + 136.0 / 255 * range, 0.78, 0.24, 0.30);
		transFunc->AddRGBPoint(min + 138.0 / 255 * range, 0.79, 0.25, 0.29);
		transFunc->AddRGBPoint(min + 140.0 / 255 * range, 0.80, 0.26, 0.28);
		transFunc->AddRGBPoint(min + 142.0 / 255 * range, 0.81, 0.27, 0.27);
		transFunc->AddRGBPoint(min + 144.0 / 255 * range, 0.82, 0.28, 0.27);
		transFunc->AddRGBPoint(min + 146.0 / 255 * range, 0.83, 0.28, 0.26);
		transFunc->AddRGBPoint(min + 148.0 / 255 * range, 0.84, 0.29, 0.25);
		transFunc->AddRGBPoint(min + 150.0 / 255 * range, 0.85, 0.30, 0.24);
		transFunc->AddRGBPoint(min + 152.0 / 255 * range, 0.86, 0.31, 0.23);
		transFunc->AddRGBPoint(min + 154.0 / 255 * range, 0.87, 0.32, 0.22);
		transFunc->AddRGBPoint(min + 156.0 / 255 * range, 0.88, 0.33, 0.21);
		transFunc->AddRGBPoint(min + 158.0 / 255 * range, 0.89, 0.34, 0.20);
		transFunc->AddRGBPoint(min + 160.0 / 255 * range, 0.89, 0.35, 0.19);
		transFunc->AddRGBPoint(min + 162.0 / 255 * range, 0.90, 0.36, 0.18);
		transFunc->AddRGBPoint(min + 164.0 / 255 * range, 0.91, 0.38, 0.17);
		transFunc->AddRGBPoint(min + 166.0 / 255 * range, 0.92, 0.39, 0.16);
		transFunc->AddRGBPoint(min + 168.0 / 255 * range, 0.92, 0.40, 0.16);
		transFunc->AddRGBPoint(min + 170.0 / 255 * range, 0.93, 0.41, 0.15);
		transFunc->AddRGBPoint(min + 172.0 / 255 * range, 0.94, 0.42, 0.14);
		transFunc->AddRGBPoint(min + 174.0 / 255 * range, 0.94, 0.44, 0.13);
		transFunc->AddRGBPoint(min + 176.0 / 255 * range, 0.95, 0.45, 0.12);
		transFunc->AddRGBPoint(min + 178.0 / 255 * range, 0.95, 0.46, 0.11);
		transFunc->AddRGBPoint(min + 180.0 / 255 * range, 0.96, 0.48, 0.09);
		transFunc->AddRGBPoint(min + 182.0 / 255 * range, 0.96, 0.49, 0.08);
		transFunc->AddRGBPoint(min + 184.0 / 255 * range, 0.97, 0.50, 0.07);
		transFunc->AddRGBPoint(min + 186.0 / 255 * range, 0.97, 0.52, 0.06);
		transFunc->AddRGBPoint(min + 188.0 / 255 * range, 0.97, 0.53, 0.05);
		transFunc->AddRGBPoint(min + 190.0 / 255 * range, 0.98, 0.54, 0.04);
		transFunc->AddRGBPoint(min + 192.0 / 255 * range, 0.98, 0.56, 0.03);
		transFunc->AddRGBPoint(min + 194.0 / 255 * range, 0.98, 0.57, 0.03);
		transFunc->AddRGBPoint(min + 196.0 / 255 * range, 0.98, 0.59, 0.02);
		transFunc->AddRGBPoint(min + 198.0 / 255 * range, 0.98, 0.60, 0.02);
		transFunc->AddRGBPoint(min + 200.0 / 255 * range, 0.99, 0.62, 0.03);
		transFunc->AddRGBPoint(min + 202.0 / 255 * range, 0.99, 0.63, 0.03);
		transFunc->AddRGBPoint(min + 204.0 / 255 * range, 0.99, 0.65, 0.04);
		transFunc->AddRGBPoint(min + 206.0 / 255 * range, 0.99, 0.66, 0.05);
		transFunc->AddRGBPoint(min + 208.0 / 255 * range, 0.99, 0.68, 0.07);
		transFunc->AddRGBPoint(min + 210.0 / 255 * range, 0.99, 0.69, 0.08);
		transFunc->AddRGBPoint(min + 212.0 / 255 * range, 0.99, 0.71, 0.10);
		transFunc->AddRGBPoint(min + 214.0 / 255 * range, 0.99, 0.72, 0.11);
		transFunc->AddRGBPoint(min + 216.0 / 255 * range, 0.98, 0.74, 0.13);
		transFunc->AddRGBPoint(min + 218.0 / 255 * range, 0.98, 0.75, 0.15);
		transFunc->AddRGBPoint(min + 220.0 / 255 * range, 0.98, 0.77, 0.17);
		transFunc->AddRGBPoint(min + 222.0 / 255 * range, 0.98, 0.78, 0.19);
		transFunc->AddRGBPoint(min + 224.0 / 255 * range, 0.97, 0.80, 0.21);
		transFunc->AddRGBPoint(min + 226.0 / 255 * range, 0.97, 0.81, 0.23);
		transFunc->AddRGBPoint(min + 228.0 / 255 * range, 0.97, 0.83, 0.25);
		transFunc->AddRGBPoint(min + 230.0 / 255 * range, 0.96, 0.84, 0.27);
		transFunc->AddRGBPoint(min + 232.0 / 255 * range, 0.96, 0.86, 0.30);
		transFunc->AddRGBPoint(min + 234.0 / 255 * range, 0.96, 0.87, 0.32);
		transFunc->AddRGBPoint(min + 236.0 / 255 * range, 0.95, 0.89, 0.35);
		transFunc->AddRGBPoint(min + 238.0 / 255 * range, 0.95, 0.90, 0.38);
		transFunc->AddRGBPoint(min + 240.0 / 255 * range, 0.95, 0.92, 0.41);
		transFunc->AddRGBPoint(min + 242.0 / 255 * range, 0.95, 0.93, 0.44);
		transFunc->AddRGBPoint(min + 244.0 / 255 * range, 0.95, 0.94, 0.47);
		transFunc->AddRGBPoint(min + 246.0 / 255 * range, 0.95, 0.96, 0.51);
		transFunc->AddRGBPoint(min + 248.0 / 255 * range, 0.95, 0.97, 0.54);
		transFunc->AddRGBPoint(min + 250.0 / 255 * range, 0.96, 0.98, 0.57);
		transFunc->AddRGBPoint(min + 252.0 / 255 * range, 0.97, 0.99, 0.60);
		transFunc->AddRGBPoint(min + 254.0 / 255 * range, 0.98, 0.99, 0.63);
		break;

	case ColorMap::Ocean:
		transFunc->AddRGBPoint(min + 0.0 / 255 * range, 0.27, 0.00, 0.33);
		transFunc->AddRGBPoint(min + 2.0 / 255 * range, 0.27, 0.01, 0.34);
		transFunc->AddRGBPoint(min + 4.0 / 255 * range, 0.27, 0.03, 0.35);
		transFunc->AddRGBPoint(min + 6.0 / 255 * range, 0.27, 0.04, 0.36);
		transFunc->AddRGBPoint(min + 8.0 / 255 * range, 0.28, 0.05, 0.38);
		transFunc->AddRGBPoint(min + 10.0 / 255 * range, 0.28, 0.06, 0.39);
		transFunc->AddRGBPoint(min + 12.0 / 255 * range, 0.28, 0.07, 0.40);
		transFunc->AddRGBPoint(min + 14.0 / 255 * range, 0.28, 0.08, 0.41);
		transFunc->AddRGBPoint(min + 16.0 / 255 * range, 0.28, 0.09, 0.42);
		transFunc->AddRGBPoint(min + 18.0 / 255 * range, 0.28, 0.11, 0.43);
		transFunc->AddRGBPoint(min + 20.0 / 255 * range, 0.28, 0.12, 0.44);
		transFunc->AddRGBPoint(min + 22.0 / 255 * range, 0.28, 0.13, 0.44);
		transFunc->AddRGBPoint(min + 24.0 / 255 * range, 0.28, 0.14, 0.45);
		transFunc->AddRGBPoint(min + 26.0 / 255 * range, 0.28, 0.15, 0.46);
		transFunc->AddRGBPoint(min + 28.0 / 255 * range, 0.28, 0.16, 0.47);
		transFunc->AddRGBPoint(min + 30.0 / 255 * range, 0.28, 0.17, 0.48);
		transFunc->AddRGBPoint(min + 32.0 / 255 * range, 0.28, 0.18, 0.48);
		transFunc->AddRGBPoint(min + 34.0 / 255 * range, 0.28, 0.19, 0.49);
		transFunc->AddRGBPoint(min + 36.0 / 255 * range, 0.28, 0.19, 0.50);
		transFunc->AddRGBPoint(min + 38.0 / 255 * range, 0.27, 0.20, 0.50);
		transFunc->AddRGBPoint(min + 40.0 / 255 * range, 0.27, 0.21, 0.51);
		transFunc->AddRGBPoint(min + 42.0 / 255 * range, 0.27, 0.22, 0.51);
		transFunc->AddRGBPoint(min + 44.0 / 255 * range, 0.27, 0.23, 0.52);
		transFunc->AddRGBPoint(min + 46.0 / 255 * range, 0.26, 0.24, 0.52);
		transFunc->AddRGBPoint(min + 48.0 / 255 * range, 0.26, 0.25, 0.52);
		transFunc->AddRGBPoint(min + 50.0 / 255 * range, 0.26, 0.26, 0.53);
		transFunc->AddRGBPoint(min + 52.0 / 255 * range, 0.25, 0.27, 0.53);
		transFunc->AddRGBPoint(min + 54.0 / 255 * range, 0.25, 0.28, 0.53);
		transFunc->AddRGBPoint(min + 56.0 / 255 * range, 0.24, 0.29, 0.54);
		transFunc->AddRGBPoint(min + 58.0 / 255 * range, 0.24, 0.30, 0.54);
		transFunc->AddRGBPoint(min + 60.0 / 255 * range, 0.24, 0.31, 0.54);
		transFunc->AddRGBPoint(min + 62.0 / 255 * range, 0.23, 0.31, 0.54);
		transFunc->AddRGBPoint(min + 64.0 / 255 * range, 0.23, 0.32, 0.55);
		transFunc->AddRGBPoint(min + 66.0 / 255 * range, 0.23, 0.33, 0.55);
		transFunc->AddRGBPoint(min + 68.0 / 255 * range, 0.22, 0.34, 0.55);
		transFunc->AddRGBPoint(min + 70.0 / 255 * range, 0.22, 0.35, 0.55);
		transFunc->AddRGBPoint(min + 72.0 / 255 * range, 0.21, 0.36, 0.55);
		transFunc->AddRGBPoint(min + 74.0 / 255 * range, 0.21, 0.36, 0.55);
		transFunc->AddRGBPoint(min + 76.0 / 255 * range, 0.21, 0.37, 0.55);
		transFunc->AddRGBPoint(min + 78.0 / 255 * range, 0.20, 0.38, 0.55);
		transFunc->AddRGBPoint(min + 80.0 / 255 * range, 0.20, 0.39, 0.55);
		transFunc->AddRGBPoint(min + 82.0 / 255 * range, 0.20, 0.40, 0.56);
		transFunc->AddRGBPoint(min + 84.0 / 255 * range, 0.19, 0.40, 0.56);
		transFunc->AddRGBPoint(min + 86.0 / 255 * range, 0.19, 0.41, 0.56);
		transFunc->AddRGBPoint(min + 88.0 / 255 * range, 0.19, 0.42, 0.56);
		transFunc->AddRGBPoint(min + 90.0 / 255 * range, 0.18, 0.43, 0.56);
		transFunc->AddRGBPoint(min + 92.0 / 255 * range, 0.18, 0.43, 0.56);
		transFunc->AddRGBPoint(min + 94.0 / 255 * range, 0.18, 0.44, 0.56);
		transFunc->AddRGBPoint(min + 96.0 / 255 * range, 0.17, 0.45, 0.56);
		transFunc->AddRGBPoint(min + 98.0 / 255 * range, 0.17, 0.46, 0.56);
		transFunc->AddRGBPoint(min + 100.0 / 255 * range, 0.17, 0.46, 0.56);
		transFunc->AddRGBPoint(min + 102.0 / 255 * range, 0.16, 0.47, 0.56);
		transFunc->AddRGBPoint(min + 104.0 / 255 * range, 0.16, 0.48, 0.56);
		transFunc->AddRGBPoint(min + 106.0 / 255 * range, 0.16, 0.49, 0.56);
		transFunc->AddRGBPoint(min + 108.0 / 255 * range, 0.15, 0.49, 0.56);
		transFunc->AddRGBPoint(min + 110.0 / 255 * range, 0.15, 0.50, 0.56);
		transFunc->AddRGBPoint(min + 112.0 / 255 * range, 0.15, 0.51, 0.56);
		transFunc->AddRGBPoint(min + 114.0 / 255 * range, 0.15, 0.52, 0.56);
		transFunc->AddRGBPoint(min + 116.0 / 255 * range, 0.14, 0.52, 0.56);
		transFunc->AddRGBPoint(min + 118.0 / 255 * range, 0.14, 0.53, 0.56);
		transFunc->AddRGBPoint(min + 120.0 / 255 * range, 0.14, 0.54, 0.55);
		transFunc->AddRGBPoint(min + 122.0 / 255 * range, 0.14, 0.54, 0.55);
		transFunc->AddRGBPoint(min + 124.0 / 255 * range, 0.13, 0.55, 0.55);
		transFunc->AddRGBPoint(min + 126.0 / 255 * range, 0.13, 0.56, 0.55);
		transFunc->AddRGBPoint(min + 128.0 / 255 * range, 0.13, 0.57, 0.55);
		transFunc->AddRGBPoint(min + 130.0 / 255 * range, 0.13, 0.57, 0.55);
		transFunc->AddRGBPoint(min + 132.0 / 255 * range, 0.12, 0.58, 0.55);
		transFunc->AddRGBPoint(min + 134.0 / 255 * range, 0.12, 0.59, 0.55);
		transFunc->AddRGBPoint(min + 136.0 / 255 * range, 0.12, 0.60, 0.54);
		transFunc->AddRGBPoint(min + 138.0 / 255 * range, 0.12, 0.60, 0.54);
		transFunc->AddRGBPoint(min + 140.0 / 255 * range, 0.12, 0.61, 0.54);
		transFunc->AddRGBPoint(min + 142.0 / 255 * range, 0.12, 0.62, 0.54);
		transFunc->AddRGBPoint(min + 144.0 / 255 * range, 0.12, 0.63, 0.53);
		transFunc->AddRGBPoint(min + 146.0 / 255 * range, 0.12, 0.63, 0.53);
		transFunc->AddRGBPoint(min + 148.0 / 255 * range, 0.12, 0.64, 0.53);
		transFunc->AddRGBPoint(min + 150.0 / 255 * range, 0.13, 0.65, 0.52);
		transFunc->AddRGBPoint(min + 152.0 / 255 * range, 0.13, 0.66, 0.52);
		transFunc->AddRGBPoint(min + 154.0 / 255 * range, 0.14, 0.66, 0.52);
		transFunc->AddRGBPoint(min + 156.0 / 255 * range, 0.14, 0.67, 0.51);
		transFunc->AddRGBPoint(min + 158.0 / 255 * range, 0.15, 0.68, 0.51);
		transFunc->AddRGBPoint(min + 160.0 / 255 * range, 0.16, 0.68, 0.50);
		transFunc->AddRGBPoint(min + 162.0 / 255 * range, 0.17, 0.69, 0.50);
		transFunc->AddRGBPoint(min + 164.0 / 255 * range, 0.18, 0.70, 0.49);
		transFunc->AddRGBPoint(min + 166.0 / 255 * range, 0.19, 0.70, 0.49);
		transFunc->AddRGBPoint(min + 168.0 / 255 * range, 0.20, 0.71, 0.48);
		transFunc->AddRGBPoint(min + 170.0 / 255 * range, 0.21, 0.72, 0.47);
		transFunc->AddRGBPoint(min + 172.0 / 255 * range, 0.22, 0.73, 0.47);
		transFunc->AddRGBPoint(min + 174.0 / 255 * range, 0.23, 0.73, 0.46);
		transFunc->AddRGBPoint(min + 176.0 / 255 * range, 0.25, 0.74, 0.45);
		transFunc->AddRGBPoint(min + 178.0 / 255 * range, 0.26, 0.75, 0.44);
		transFunc->AddRGBPoint(min + 180.0 / 255 * range, 0.27, 0.75, 0.44);
		transFunc->AddRGBPoint(min + 182.0 / 255 * range, 0.29, 0.76, 0.43);
		transFunc->AddRGBPoint(min + 184.0 / 255 * range, 0.30, 0.76, 0.42);
		transFunc->AddRGBPoint(min + 186.0 / 255 * range, 0.32, 0.77, 0.41);
		transFunc->AddRGBPoint(min + 188.0 / 255 * range, 0.34, 0.78, 0.40);
		transFunc->AddRGBPoint(min + 190.0 / 255 * range, 0.35, 0.78, 0.39);
		transFunc->AddRGBPoint(min + 192.0 / 255 * range, 0.37, 0.79, 0.38);
		transFunc->AddRGBPoint(min + 194.0 / 255 * range, 0.39, 0.79, 0.37);
		transFunc->AddRGBPoint(min + 196.0 / 255 * range, 0.40, 0.80, 0.36);
		transFunc->AddRGBPoint(min + 198.0 / 255 * range, 0.42, 0.81, 0.35);
		transFunc->AddRGBPoint(min + 200.0 / 255 * range, 0.44, 0.81, 0.34);
		transFunc->AddRGBPoint(min + 202.0 / 255 * range, 0.46, 0.82, 0.33);
		transFunc->AddRGBPoint(min + 204.0 / 255 * range, 0.48, 0.82, 0.32);
		transFunc->AddRGBPoint(min + 206.0 / 255 * range, 0.50, 0.83, 0.31);
		transFunc->AddRGBPoint(min + 208.0 / 255 * range, 0.52, 0.83, 0.29);
		transFunc->AddRGBPoint(min + 210.0 / 255 * range, 0.54, 0.84, 0.28);
		transFunc->AddRGBPoint(min + 212.0 / 255 * range, 0.56, 0.84, 0.27);
		transFunc->AddRGBPoint(min + 214.0 / 255 * range, 0.58, 0.84, 0.26);
		transFunc->AddRGBPoint(min + 216.0 / 255 * range, 0.60, 0.85, 0.24);
		transFunc->AddRGBPoint(min + 218.0 / 255 * range, 0.62, 0.85, 0.23);
		transFunc->AddRGBPoint(min + 220.0 / 255 * range, 0.64, 0.86, 0.22);
		transFunc->AddRGBPoint(min + 222.0 / 255 * range, 0.66, 0.86, 0.20);
		transFunc->AddRGBPoint(min + 224.0 / 255 * range, 0.68, 0.86, 0.19);
		transFunc->AddRGBPoint(min + 226.0 / 255 * range, 0.70, 0.87, 0.18);
		transFunc->AddRGBPoint(min + 228.0 / 255 * range, 0.72, 0.87, 0.16);
		transFunc->AddRGBPoint(min + 230.0 / 255 * range, 0.74, 0.87, 0.15);
		transFunc->AddRGBPoint(min + 232.0 / 255 * range, 0.76, 0.88, 0.14);
		transFunc->AddRGBPoint(min + 234.0 / 255 * range, 0.78, 0.88, 0.13);
		transFunc->AddRGBPoint(min + 236.0 / 255 * range, 0.80, 0.88, 0.11);
		transFunc->AddRGBPoint(min + 238.0 / 255 * range, 0.82, 0.88, 0.11);
		transFunc->AddRGBPoint(min + 240.0 / 255 * range, 0.85, 0.89, 0.10);
		transFunc->AddRGBPoint(min + 242.0 / 255 * range, 0.87, 0.89, 0.10);
		transFunc->AddRGBPoint(min + 244.0 / 255 * range, 0.89, 0.89, 0.10);
		transFunc->AddRGBPoint(min + 246.0 / 255 * range, 0.91, 0.89, 0.10);
		transFunc->AddRGBPoint(min + 248.0 / 255 * range, 0.93, 0.90, 0.10);
		transFunc->AddRGBPoint(min + 250.0 / 255 * range, 0.95, 0.90, 0.11);
		transFunc->AddRGBPoint(min + 252.0 / 255 * range, 0.96, 0.90, 0.12);
		transFunc->AddRGBPoint(min + 254.0 / 255 * range, 0.98, 0.90, 0.14);
		transFunc->AddRGBPoint(min + 256.0 / 255 * range, 0.99, 0.91, 0.14);
		break;

	case ColorMap::Cortex:
		transFunc->AddRGBPoint(min + 000.0 / 255 * range, 0.00, 0.00, 0.00);
		transFunc->AddRGBPoint(min + 010.0 / 255 * range, 0.06, 0.04, 0.03);
		transFunc->AddRGBPoint(min + 020.0 / 255 * range, 0.11, 0.07, 0.05);
		transFunc->AddRGBPoint(min + 030.0 / 255 * range, 0.17, 0.11, 0.08);
		transFunc->AddRGBPoint(min + 040.0 / 255 * range, 0.22, 0.14, 0.11);
		transFunc->AddRGBPoint(min + 050.0 / 255 * range, 0.27, 0.18, 0.14);
		transFunc->AddRGBPoint(min + 060.0 / 255 * range, 0.33, 0.21, 0.16);
		transFunc->AddRGBPoint(min + 070.0 / 255 * range, 0.38, 0.25, 0.19);
		transFunc->AddRGBPoint(min + 080.0 / 255 * range, 0.44, 0.28, 0.22);
		transFunc->AddRGBPoint(min + 090.0 / 255 * range, 0.49, 0.32, 0.24);
		transFunc->AddRGBPoint(min + 100.0 / 255 * range, 0.55, 0.35, 0.27);
		transFunc->AddRGBPoint(min + 110.0 / 255 * range, 0.60, 0.39, 0.30);
		transFunc->AddRGBPoint(min + 120.0 / 255 * range, 0.65, 0.42, 0.33);
		transFunc->AddRGBPoint(min + 130.0 / 255 * range, 0.71, 0.46, 0.35);
		transFunc->AddRGBPoint(min + 140.0 / 255 * range, 0.76, 0.49, 0.38);
		transFunc->AddRGBPoint(min + 150.0 / 255 * range, 0.82, 0.53, 0.40);
		transFunc->AddRGBPoint(min + 160.0 / 255 * range, 0.87, 0.56, 0.43);
		transFunc->AddRGBPoint(min + 170.0 / 255 * range, 0.93, 0.60, 0.46);
		transFunc->AddRGBPoint(min + 180.0 / 255 * range, 0.98, 0.63, 0.49);
		transFunc->AddRGBPoint(min + 190.0 / 255 * range, 1.00, 0.67, 0.51);
		transFunc->AddRGBPoint(min + 200.0 / 255 * range, 1.00, 0.70, 0.54);
		transFunc->AddRGBPoint(min + 210.0 / 255 * range, 1.00, 0.74, 0.57);
		transFunc->AddRGBPoint(min + 220.0 / 255 * range, 1.00, 0.77, 0.59);
		transFunc->AddRGBPoint(min + 230.0 / 255 * range, 1.00, 0.81, 0.62);
		transFunc->AddRGBPoint(min + 240.0 / 255 * range, 1.00, 0.84, 0.71);
		transFunc->AddRGBPoint(min + 250.0 / 255 * range, 1.00, 0.90, 0.88);
		transFunc->AddRGBPoint(min + 254.0 / 255 * range, 1.00, 0.98, 0.96);

		break;
	default:

		transFunc->AddRGBPoint(min + 0.0, 0, 0, 0); //black
		transFunc->AddRGBPoint(min + range, 1, 1, 1); //white
		break;
	}

	transFunc->SetClamping(true);
	transFunc->SetScaleToLinear();
	transFunc->SetAlpha(1.0);

	VTK_CREATE(vtkLookupTable, lut);
	lut->SetNumberOfTableValues(256);

	// Iterpolate range with 256 steps
	for (int i = 0; i < 256; ++i) {
		// Default value: transparent
		double* rgb = new double[4];
		double value = min + i*1.0 / 255 * (max - min);

		// Skip first entry
		if (i == 0 && this->hasTransparantBackground()) {
			rgb[0] = 0;
			rgb[1] = 0;
			rgb[2] = 0;
			rgb[3] = 0;
		}
		else {
            delete[] rgb;
			rgb = transFunc->GetColor(value);
			rgb[3] = 1;
		}

		lut->SetTableValue(i, rgb);
	}

	lut->SetNanColor(0, 0, 0, 0);

	lut->SetBelowRangeColor(0, 0, 0, 0);
	lut->SetUseBelowRangeColor(1);

	lut->SetAboveRangeColor(transFunc->GetColor(max));
	lut->SetUseAboveRangeColor(1);

	lut->SetTableRange(min, max);
	lut->SetRampToLinear();

	return lut;
}

bool ColorMap::hasTransparantBackground() {
	if (this->value == ColorMapValue::Rainbow) {
		return false;
	}
	else if (this->value == ColorMapValue::FlowDark) {
		return false;
	}
	else if (this->value == ColorMapValue::FlowLight) {
		return false;
	}
	else if (this->value == ColorMapValue::Temperature) {
		return false;
	}
	else if (this->value == ColorMapValue::Ocean) {
		return false;
	}

	return true;
}
