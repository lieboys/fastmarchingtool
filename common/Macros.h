#pragma once

/** Macro's */

// Create function
#define CREATE_FUNC(__TYPE__) \
static __TYPE__* create() \
{ \
__TYPE__ *pRet = new(std::nothrow) __TYPE__(); \
if (pRet && pRet->init()) \
{ \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = nullptr; \
return nullptr; \
} \
}

// Create function for VTK smart pointer
#define VTK_CREATE(type, name) \
vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

// Short for VTK smart pointer
#define SP(type) \
vtkSmartPointer<type>

/* Anatomical orientation options */
enum Orientation {
	AXIAL,
	CORONAL,
	SAGITTAL
};

/* Options for VisualObject types */
enum VisualType {
	VTYPE_GLYPHS,
	VTYPE_SCALARMAP,
	VTYPE_CONTOURPLOT,
	VTYPE_ROI,
	VTYPE_GEODESIC,
	VTYPE_ANATOMICAL_VOLUME,
};

/* Options for DataObject types */
enum DataType {
	DTYPE_NONE,
	DTYPE_DISTANCEMAP,
	DTYPE_ROI,
	DTYPE_GEODESIC,
	DTYPE_REFERENCE_VOLUME,
};
