
#include "Interactor2D.h"

#include <vtkObjectFactory.h>

vtkStandardNewMacro(Interactor2D);

void Interactor2D::OnLeftButtonDown() {
	vtkInteractorStyleImage::OnLeftButtonUp();
};

void Interactor2D::OnLeftButtonUp() {
	vtkInteractorStyleImage::OnLeftButtonUp();
};

void Interactor2D::OnRightButtonDown() {
	// Disabled
};

void Interactor2D::OnRightButtonUp() {
	// Disabled
};

void Interactor2D::OnMouseMove() {
	vtkInteractorStyleImage::OnMouseMove();
};

void Interactor2D::OnLeave() {
	vtkInteractorStyleImage::OnLeave();
};

void Interactor2D::OnMiddleButtonDown() {
	vtkInteractorStyleImage::OnMiddleButtonDown();
};

void Interactor2D::OnMiddleButtonUp() {
	vtkInteractorStyleImage::OnMiddleButtonUp();
};

void Interactor2D::OnMouseWheelForward() {
	vtkInteractorStyleImage::OnMouseWheelForward();
};

void Interactor2D::OnMouseWheelBackward() {
	vtkInteractorStyleImage::OnMouseWheelBackward();
};