#pragma once

#include "Macros.h"

#include <vtkSmartPointer.h>
#include <vtkLookupTable.h>

#include <QString>
#include <QList>
/**
ColorMap, enum based object for handling the colormappings.
*/
class ColorMap {
public:

	/** Possible options for the ColorMap */
	enum ColorMapValue : short {
		Greyscale,
		Cortex,
		Hotmetal,
		Rainbow,
		FlowLight,
		FlowDark,
		Temperature,
		Red,
		Blue,
		Green,
		Inferno,
		Ocean,
	};

	/* Constructor, string-based  */
	static ColorMap* fromString(QString str);

	/* Constructor, enum-based */
	ColorMap(ColorMapValue val);

	/* Conversion to string */
	QString toString();

	/* Global function for generating a list of possible options, for functional datasts only.*/
	static QList<QString> ListAllFunctional();

	/* Global function for generating a list of possible options, for all datasts.*/
	static QList<QString> ListAll();

	/* Create a lookup table usable by the VisualObjects, from the current option*/
	SP(vtkLookupTable) CreateLookupTable(double min, double max);

	/* Current option */
	ColorMapValue value;

private:

	/* Checks if the current option has a transparant background*/
	bool hasTransparantBackground();
};
