#pragma once

#include "vtkInteractorStyleImage.h"

/**
Custom adaption of an Interactor for 2D, enables the swapping of functionality to different mouse buttons.
*/
class Interactor2D : public vtkInteractorStyleImage {
public:
	static Interactor2D* New();

	vtkTypeMacro(Interactor2D, vtkInteractorStyleImage);

	Interactor2D() {
		this->SetInteractionModeToImageSlicing();
	}

	virtual void OnLeftButtonDown();
	virtual void OnLeftButtonUp();

	virtual void OnRightButtonDown();
	virtual void OnRightButtonUp();

	virtual void OnMouseMove();
	virtual void OnLeave();

	virtual void OnMiddleButtonDown();
	virtual void OnMiddleButtonUp();

	virtual void OnMouseWheelForward();
	virtual void OnMouseWheelBackward();
};
