#include "Controller.h"

#include <QFileDialog>
#include <QSettings>
#include <QDir>
#include <QSignalMapper>
#include <QColorDialog>
#include <QListWidget>
#include <QMessageBox>
#include <QFileSystemWatcher>
#include <QPainter>

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>

#include "viewcontrollers/GeodesicViewController.h"
#include "viewcontrollers/RegionViewController.h"
#include "viewcontrollers/ReferenceViewController.h"
#include "viewcontrollers/DistanceMapViewController.h"

#include "gui/FilterDialog.h"	
#include "gui/SettingsDialog.h"

Controller::Controller() {
	this->m_model = nullptr;
	this->m_visualization = nullptr;

	// Setup UI
	this->ui = new Ui_View;
	this->ui->setupUi(this);

	this->m_currentExpSettings = SettingsDialog::GetExpFromSettings();

	this->m_pseudoDialog = nullptr;
    
//    codeLinkWorkFolderEdit

    const QString CODELINK_WORKFOLDER("codelink_workfolder");
    const QString CODELINK_EXPERIMENTNAME("codelink_experimentname");
    const QString CODELINK_SPEEDMAP("codelink_speedmap");
    const QString CODELINK_IMPORTAFTER("codelink_importafter");
    QSettings MySettings;
    if(MySettings.contains(CODELINK_WORKFOLDER))
        this->ui->codeLinkWorkFolderEdit->setText(MySettings.value(CODELINK_WORKFOLDER).toString());
    if(MySettings.contains(CODELINK_EXPERIMENTNAME))
        this->ui->codeLinkExpNameEdit->setText(MySettings.value(CODELINK_EXPERIMENTNAME).toString());
    if(MySettings.contains(CODELINK_SPEEDMAP))
        this->ui->speedMapEdit->setText(MySettings.value(CODELINK_SPEEDMAP).toString());
    if(MySettings.contains(CODELINK_IMPORTAFTER))
        this->ui->codeLinkImportAfterCheckbox->setChecked(MySettings.value(CODELINK_IMPORTAFTER).toBool());

	qApp->processEvents();
}

Controller::~Controller() {
	delete this->ui;
	this->ui = NULL;
}

void Controller::connectSignals() {
	
	QObject::connect(this->m_model->filewatcher, SIGNAL(fileChanged(QString)), 
					 this, SLOT(slotWatchedFileChange(QString)));
	
	// Toolbar
	connect(this->ui->navButtonReference, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));
	connect(this->ui->navButtonRegions, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));
	connect(this->ui->navButtonCodeLink, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));
	connect(this->ui->navButtonDistanceMaps, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));
	connect(this->ui->navButtonGeodesics, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));
//    connect(this->ui->navButtonConnectivity, SIGNAL(clicked()), this, SLOT(slotToolbarNavigationClicked()));

	// Export
	QSignalMapper* exportMapper = new QSignalMapper(this);

	connect(this->ui->actionExportProjectionMinimum, SIGNAL(triggered()),exportMapper, SLOT(map()));
	connect(this->ui->actionExportProjectionComposite, SIGNAL(triggered()),exportMapper, SLOT(map()));
	connect(this->ui->actionExportProjectionGeodesic, SIGNAL(triggered()),exportMapper, SLOT(map()));

	exportMapper->setMapping(this->ui->actionExportProjectionMinimum, (int)Projection::ProjectionType::MINIMUM);
	exportMapper->setMapping(this->ui->actionExportProjectionComposite, (int)Projection::ProjectionType::COMPOSITE);
	exportMapper->setMapping(this->ui->actionExportProjectionGeodesic, (int)Projection::ProjectionType::GEODESIC);

	connect(exportMapper, SIGNAL(mapped(int)), this, SLOT(slotExportVisualizationTriggered(int)));

	connect(this->ui->actionExportScreenshot, SIGNAL(triggered()), this, SLOT(slotExportScreenshot()));
	
	// Connectivity
    connect(this->ui->connectivityCompute, SIGNAL(clicked()), this, SLOT(slotComputeConnectivityClicked()));

	// Navigation
	connect(this->ui->visPlaneComboBox, SIGNAL(currentIndexChanged(const QString &)), 
			this, SLOT(slotVisPlaneComboBoxChanged(const QString &)));
	connect(this->ui->visPlaneSpinBox, SIGNAL(valueChanged(int)),
			this, SLOT(slotVisPlaneSpinBoxChanged(int)));
	connect(this->ui->visPlaneSlider, SIGNAL(valueChanged(int)), 
			this, SLOT(slotVisPlaneSliderChanged(int)));

	connect(this->ui->pushButton2D, SIGNAL(clicked()), this, SLOT(slotButton2DTriggered()));
	connect(this->ui->pushButton3D, SIGNAL(clicked()), this, SLOT(slotButton3DTriggered()));
	
	// Menu
	QSignalMapper* interpolationMapper = new QSignalMapper(this);

	connect(this->ui->actionInterpolationNone, SIGNAL(triggered()),
			interpolationMapper, SLOT(map()));
	connect(this->ui->actionInterpolationLineair, SIGNAL(triggered()),
			interpolationMapper, SLOT(map()));
	connect(this->ui->actionInterpolationQuadratic, SIGNAL(triggered()),
			interpolationMapper, SLOT(map()));

	interpolationMapper->setMapping(this->ui->actionInterpolationNone, 0);
	interpolationMapper->setMapping(this->ui->actionInterpolationLineair, 1);
	interpolationMapper->setMapping(this->ui->actionInterpolationQuadratic, 2);
	
	connect(interpolationMapper, SIGNAL(mapped(int)), this, SLOT(slotChangeInterpolation(int)));

	connect(this->ui->actionSettings, SIGNAL(triggered()), this, SLOT(slotEditSettings()));
	connect(this->ui->actionAbout, SIGNAL(triggered()), this, SLOT(slotAbout()));
		
	// CodeLink
	connect(this->ui->codeLinkSpeedButton, SIGNAL(clicked()), 
			this, SLOT(slotSpeedMapSpecifyTriggered()));
	connect(this->ui->codeLinkStartButton, SIGNAL(clicked()), 
			this, SLOT(slotRunExternalCode()));
	connect(this->ui->codeLinkAdvancedButton, SIGNAL(clicked()),
			this, SLOT(slotCodeLinkAdvancedButton()));
	connect(this->ui->codeLinkFolderButton, SIGNAL(clicked()),
			this, SLOT(slotCodeLinkFolderButtonTriggered()));

}

void Controller::SetMVC(Model * model, Visualization * visualization) {

	this->m_model = model;
	this->m_visualization = visualization;

	// Connect Qt controls
	connectSignals();

	this->m_geodesicController = new GeodesicViewController(this, m_model,
															m_visualization);

	this->m_regionController = new RegionViewController(this, m_model,
														m_visualization);

	this->m_referenceController = new ReferenceViewController(this, m_model,
															  m_visualization);

	this->m_distanceMapController = new DistanceMapViewController(this, m_model,
																  m_visualization);
}

void Controller::Setup() {

	// bind render window to Visualization
	m_visualization->BindRenderWindow(ui->mainScreen->GetRenderWindow());

	m_visualization->Setup();
	m_model->Setup();

	this->ui->pushButton2D->setChecked(false);
	this->ui->pushButton3D->setChecked(true);

	// Hide side bar 
	QTabBar *tabBar = this->ui->tabWidget->findChild<QTabBar *>();
	tabBar->hide();
}

void Controller::slotVisPlaneComboBoxChanged(const QString & text) {
	UpdateVisualNavControls();

	// Update visuals
	slotNavigationChange();
}

void Controller::slotVisPlaneSpinBoxChanged(int value) {
	// check min/max
	if (value > this->ui->visPlaneSlider->maximum()) {
		this->ui->visPlaneSpinBox->setValue(this->ui->visPlaneSlider->maximum());
		return;
	}
	if (value < this->ui->visPlaneSlider->minimum()) {
		this->ui->visPlaneSpinBox->setValue(this->ui->visPlaneSlider->minimum());
		return;
	}

	this->ui->visPlaneSlider->blockSignals(true);
	this->ui->visPlaneSlider->setValue(value);
	this->ui->visPlaneSlider->blockSignals(false);

	// Update navigation
	slotNavigationChange();
}

void Controller::slotVisPlaneSliderChanged(int value) {
	this->ui->visPlaneSpinBox->blockSignals(true);
	this->ui->visPlaneSpinBox->setValue(value);
	this->ui->visPlaneSpinBox->blockSignals(false);

	// Update navigation
	slotNavigationChange();
}

void Controller::slotLoadBinaryTriggered() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir_load_binary");
	QSettings MySettings;

	QString filepath = QFileDialog::getOpenFileName(this, tr("Load distance map"),
													MySettings.value(DEFAULT_DIR_KEY).toString(),
													tr("Binary/Textual file (*.txt* *.dat);;Any files (*)"));
	if (filepath.isEmpty()) {
		return;
	}

	// Parse filepaths
	QString dataPath = filepath.replace("_Format.txt","_Data.dat");
	QString headerPath = filepath.replace("_Data.dat", "_Format.txt");

	if (!QFile(dataPath).exists() || !QFile(headerPath).exists()) {
		ThrowError("Incorrect binary format, files not found.");
		return;
	}

	QFileInfo fi(filepath);
	MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	filepath = QDir::toNativeSeparators(filepath);

	QProgressDialog* dialog = LoadingStart();

	bool ok = this->m_model->LoadFromBinaryFile(headerPath, dataPath);
	if (!ok) {
		ThrowError("Import failed");
	}

	this->LoadingDone(dialog);
}

void Controller::slotComputeConnectivityClicked()
{
    QString roiA = this->ui->connectivityRoiACombobox->currentText();
    QString roiB = this->ui->connectivityRoiBCombobox->currentText();
    QString finslerMap = this->ui->connectivityFinslerMapCombobox->currentText();
    QString euclideanMap = this->ui->connectivityEuclideanMapCombobox->currentText();
    
    // check if regions A and B are selected
    if(roiA == "none" || roiB == "none")
    {
        ThrowError("Both ROIs are required.");
        return;
    }
    
    // check if region A != region B
    if(roiA == roiB)
    {
        ThrowError("ROIs cannot be the same.");
        return;
    }
    
    // check if there are Finsler and Euclidean distance maps loaded
    if(finslerMap == "none" || euclideanMap == "none")
    {
        ThrowError("Both Finsler and Eucldidean maps are required.");
        return;
    }
    
    // call connectivity measures with the relevant regions
    m_model->ComputeConnectivity(roiA, roiB, finslerMap, euclideanMap);
}

void Controller::UpdateVisualNavControls() {

	this->ui->navPanel->setEnabled(this->m_model->CheckLocking(Model::LockType::AnyDatasetImported));

	// Get new range
	int range[2];
	std::vector<int> extent(6, 0);
	this->m_model->RequestNavigationExtent(extent);

	if (this->ui->visPlaneComboBox->currentText() == "Axial") {
		range[0] = extent[4];
		range[1] = extent[5];
		m_visualization->renderProperties.positionSettings.orient = Orientation::AXIAL;
	}
	else if (this->ui->visPlaneComboBox->currentText() == "Coronal") {
		range[0] = extent[2];
		range[1] = extent[3];
		m_visualization->renderProperties.positionSettings.orient = Orientation::CORONAL;
	}
	else if (this->ui->visPlaneComboBox->currentText() == "Sagittal") {
		range[0] = extent[0];
		range[1] = extent[1];
		m_visualization->renderProperties.positionSettings.orient = Orientation::SAGITTAL;
	}

	m_visualization->renderProperties.isDirty = true;
	m_visualization->UpdateNavigation();

	// Update Position controls with new range
	this->ui->visPlaneSlider->blockSignals(true);
	this->ui->visPlaneSlider->setMinimum(range[0]);
	this->ui->visPlaneSlider->setMaximum(range[1]);
	this->ui->visPlaneSlider->blockSignals(false);

	this->ui->visPlaneSpinBox->blockSignals(true);
	this->ui->visPlaneSpinBox->setMinimum(range[0]);
	this->ui->visPlaneSpinBox->setMaximum(range[1]);
	this->ui->visPlaneSpinBox->blockSignals(false);

	// set camera angle
}

void Controller::BlockAllSignals(bool toggle) {
	QList<QWidget *> widgetList = this->findChildren<QWidget *>();
	QList<QWidget *>::const_iterator widgetIter(widgetList.begin());
	QList<QWidget *>::const_iterator lastWidget(widgetList.end());

	while (widgetIter != lastWidget) {
		(*widgetIter)->blockSignals(toggle);
		++widgetIter;
	}
}

void Controller::slotNavigationChange() {

	Orientation orientation = Orientation::AXIAL;
	QString orientationLabel = this->ui->visPlaneComboBox->currentText();
	if (this->ui->visPlaneComboBox->currentText() == "Axial") {
		orientation = Orientation::AXIAL;
	}
	else if (this->ui->visPlaneComboBox->currentText() == "Coronal") {
		orientation = Orientation::CORONAL;
	}
	else if (this->ui->visPlaneComboBox->currentText() == "Sagittal") {
		orientation = Orientation::SAGITTAL;
	}
	this->m_visualization->renderProperties.positionSettings.orient = orientation;
	this->m_visualization->renderProperties.positionSettings.position = this->ui->visPlaneSpinBox->value();
	this->m_visualization->renderProperties.isDirty = true;
	this->m_visualization->UpdateNavigation();
}

void Controller::ThrowError(QString text) {

	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Icon::Critical);
	msgBox.setText(text);
	msgBox.setWindowTitle("Error");
	msgBox.exec();
}

void Controller::slotButton2DTriggered() {

	this->m_visualization->SetObjectDimensionality(2);
	
	if (this->ui->pushButton2D->isChecked()) {
		this->ui->pushButton2D->setChecked(true);
		this->ui->pushButton3D->setChecked(false);
	}
}

void Controller::slotButton3DTriggered() {

	this->m_visualization->SetObjectDimensionality(3);

	if (this->ui->pushButton3D->isChecked()) {
		this->ui->pushButton2D->setChecked(false);
		this->ui->pushButton3D->setChecked(true);
	}
}

void Controller::slotChangeInterpolation(int order) {

	this->m_visualization->SetInterpolation(order);

	this->ui->actionInterpolationNone->setIcon(QIcon(":/CustomIcons/MenuEmpty"));
	this->ui->actionInterpolationLineair->setIcon(QIcon(":/CustomIcons/MenuEmpty"));
	this->ui->actionInterpolationQuadratic->setIcon(QIcon(":/CustomIcons/MenuEmpty"));

	if (order == 0) {
		this->ui->actionInterpolationNone->setIcon(QIcon(":/CustomIcons/MenuCheck"));
	}
	else if (order == 1) {
		this->ui->actionInterpolationLineair->setIcon(QIcon(":/CustomIcons/MenuCheck"));
	}
	else if (order == 2) {
		this->ui->actionInterpolationQuadratic->setIcon(QIcon(":/CustomIcons/MenuCheck"));
	}
}

void Controller::slotWatchedFileChange(QString path) {

	std::cout << "change in file: '" << path.toStdString().c_str() << std::endl;

	// Reimport
	this->m_model->ReimportDatasetAt(path);
	
	this->m_regionController->UpdateVisualsFromUi();
}

QProgressDialog* Controller::LoadingStart(QWidget* parent) {

	if (parent == nullptr) {
		parent = this;
	}

	this->blockSignals(true);
	qApp->processEvents(QEventLoop::ExcludeUserInputEvents);

	QProgressDialog* dialog = new QProgressDialog(parent);
	dialog->setAttribute(Qt::WA_DeleteOnClose);
	dialog->setWindowModality(Qt::WindowModality::NonModal);
	dialog->setLabelText("Loading...");
	dialog->setWindowTitle("Loading...");
	dialog->setFont(QFont("Segoe UI", 10));
	dialog->setCancelButton(0);
	dialog->setRange(0, 0);
	dialog->setMinimumDuration(0);
	dialog->show();
	dialog->raise();

	qApp->processEvents(QEventLoop::ExcludeUserInputEvents);

	return dialog;
}

void Controller::LoadingDone(QProgressDialog* dialog) {

	dialog->close();
	delete dialog;

	this->blockSignals(false);
	this->setEnabled(true);

	qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
}

void Controller::slotExportVisualizationTriggered(int intType) {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir_export_viz");
	QSettings MySettings;

	// Multiple file selection
	QString filepath = QFileDialog::getSaveFileName(this, tr("Save as volumetric file"),
														 MySettings.value(DEFAULT_DIR_KEY).toString(),
														 tr("NIfTI (*.nii* *.nii.gz *.hdr)"));

	if (filepath.isEmpty()) {
		return;
	}

	this->m_model->ExportVolumeData(intType, filepath);

	if (!QFile(filepath).exists()) {
		this->ThrowError("Export failed!");
	}
	else {
		Model::ShowInExplorer(filepath, true);
	}
}

void Controller::slotExportScreenshot() {
	
	// Get file
	const QString DEFAULT_DIR_KEY("default_dir_export_screen");
	QSettings MySettings;

	QString suggestion = QDir(MySettings.value(DEFAULT_DIR_KEY).toString()).absoluteFilePath("screenshot.png");

	// Multiple file selection
	QString filepath = QFileDialog::getSaveFileName(this, tr("Save as volumetric file"),
													suggestion,
													tr("PNG images (*.png)"));

	QProgressDialog* dialog = LoadingStart();

	this->ExportScreenshotHighRes(filepath);

	LoadingDone(dialog);

	if (!QFile(filepath).exists()) {
		this->ThrowError("Export failed!");
	}
	else {
		Model::ShowInExplorer(filepath, true);
	}
}

QStringList Controller::GetDataSelection(QList<QPair<QString, QString>> data) {

	QStringList out;

	FilterDialog* dialog = new FilterDialog(data);
	int result = dialog->exec();

	if (result != QDialog::Accepted) {
		delete dialog;
		return out;
	}
	
	out = dialog->selection;
	delete dialog;

	return out;
}

void Controller::slotEditSettings() {

	SettingsDialog* dialog = new SettingsDialog();
    dialog->selectTab(0);
	dialog->exec();
	delete dialog;

}

void Controller::slotCodeLinkAdvancedButton() {
	
    SettingsDialog* dialog = new SettingsDialog();
    dialog->selectTab(0);
    dialog->exec();
    this->m_currentExpSettings.params = SettingsDialog::GetParameters();
    delete dialog;
}

void Controller::slotToolbarNavigationClicked() {
		
	this->ui->navButtonReference->setChecked(false);
	this->ui->navButtonRegions->setChecked(false);
	this->ui->navButtonCodeLink->setChecked(false);
	this->ui->navButtonDistanceMaps->setChecked(false);
	this->ui->navButtonGeodesics->setChecked(false);
//    this->ui->navButtonConnectivity->setChecked(false);

	QToolButton* button = dynamic_cast<QToolButton*> (sender());
	button->setChecked(true);

	if (button == this->ui->navButtonReference) {
		this->ui->tabWidget->setCurrentIndex(0);
	}
	else if (button == this->ui->navButtonRegions) {
		this->ui->tabWidget->setCurrentIndex(1);
	}
	else if (button == this->ui->navButtonCodeLink) {
		this->ui->tabWidget->setCurrentIndex(2);
	}
	else if (button == this->ui->navButtonDistanceMaps) {
		this->ui->tabWidget->setCurrentIndex(3);
	}
	else if (button == this->ui->navButtonGeodesics) {
		this->ui->tabWidget->setCurrentIndex(4);
	}
//    else if (button == this->ui->navButtonConnectivity) {
//        this->ui->tabWidget->setCurrentIndex(5);
//    }

}

void Controller::slotCodeLinkFolderButtonTriggered() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir_codelinkfolder");
	QSettings MySettings;

	QString folder = QFileDialog::getExistingDirectory(this, tr("Specify work folder"),
												MySettings.value(DEFAULT_DIR_KEY).toString());
	if (folder.isEmpty()) {
		return;
	}

	this->ui->codeLinkWorkFolderEdit->setText(QDir::toNativeSeparators(folder));
}

void Controller::runFMCode() {
    
	QSettings regSettings;
    QString option = regSettings.value(SettingsDialog::SETTINGS_CODE_RUN_OPTION).toString();
    
    /*** save ui settings of main window in qsettings ***/
    
    const QString CODELINK_WORKFOLDER("codelink_workfolder");
    const QString CODELINK_EXPERIMENTNAME("codelink_experimentname");
    const QString CODELINK_SPEEDMAP("codelink_speedmap");
    const QString CODELINK_IMPORTAFTER("codelink_importafter");
    
    regSettings.setValue(CODELINK_WORKFOLDER, this->ui->codeLinkWorkFolderEdit->text());
    regSettings.setValue(CODELINK_EXPERIMENTNAME, this->ui->codeLinkExpNameEdit->text());
    regSettings.setValue(CODELINK_SPEEDMAP, this->ui->speedMapEdit->text());
    regSettings.setValue(CODELINK_IMPORTAFTER, this->ui->codeLinkImportAfterCheckbox->isChecked());
    
    /*** common code ***/
    
    this->m_currentExpSettings.workfolder = this->ui->codeLinkWorkFolderEdit->text();
    this->m_currentExpSettings.name = this->ui->codeLinkExpNameEdit->text();
    
    this->m_currentExpSettings.importAfterwards = this->ui->codeLinkImportAfterCheckbox->isChecked();
    
    this->m_currentExpSettings.inputBinaryPath = QDir(this->m_currentExpSettings.workfolder).absoluteFilePath(this->m_currentExpSettings.name + "_input_Data.dat");
    this->m_currentExpSettings.inputHeaderPath = QDir(this->m_currentExpSettings.workfolder).absoluteFilePath(this->m_currentExpSettings.name + "_input_Format.txt");
    this->m_currentExpSettings.outputBinaryPath = QDir(this->m_currentExpSettings.workfolder).absoluteFilePath(this->m_currentExpSettings.name + "_output_Data.dat");
    this->m_currentExpSettings.outputHeaderPath = QDir(this->m_currentExpSettings.workfolder).absoluteFilePath(this->m_currentExpSettings.name + "_output_Format.txt");
    
    this->m_currentExpSettings.remoteSettings.inputBinaryPath = QString("%1/%2_input_Data.dat").arg(
        this->m_currentExpSettings.remoteSettings.dataDirectory,
        this->m_currentExpSettings.name
        );
    this->m_currentExpSettings.remoteSettings.inputHeaderPath = QString("%1/%2_input_Format.txt").arg(
        this->m_currentExpSettings.remoteSettings.dataDirectory,
        this->m_currentExpSettings.name
        );
    this->m_currentExpSettings.remoteSettings.outputBinaryPath = QString("%1/%2_output_Data.dat").arg(
        this->m_currentExpSettings.remoteSettings.dataDirectory,
        this->m_currentExpSettings.name
        );
    this->m_currentExpSettings.remoteSettings.outputHeaderPath = QString("%1/%2_output_Format.txt").arg(
        this->m_currentExpSettings.remoteSettings.dataDirectory,
        this->m_currentExpSettings.name
        );
    
    
    QString inputHeaderPath;
    QString inputBinaryPath;
    QString outputHeaderPath;
    QString outputBinaryPath;
    
    if (this->ui->codeLinkSeedCombobox->currentText() != "none") {
        this->m_currentExpSettings.seedName = this->ui->codeLinkSeedCombobox->currentText();
    }
    if (this->ui->codeLinkTipCombobox->currentText() != "none") {
        this->m_currentExpSettings.tipName = this->ui->codeLinkTipCombobox->currentText();
    }
    
    QStringList toCheck = QStringList()
    << this->m_currentExpSettings.inputBinaryPath
    << this->m_currentExpSettings.inputHeaderPath
    << this->m_currentExpSettings.outputBinaryPath
    << this->m_currentExpSettings.outputHeaderPath;
    
    
    
    // check if speedmap exists
    QString speedMapPath = this->ui->speedMapEdit->text();
    if (!QFile().exists(speedMapPath))
    {
        ThrowError("FOD file was not found. The specified path may be incorrect.");
        return;
    }
    
    // check if working directory exists, otherwise create it
    QString workingDirectory = this->ui->codeLinkWorkFolderEdit->text();
    if(!QDir(workingDirectory).exists())
    {
        QDir().mkdir(workingDirectory);
        ThrowError("Working directory did not exist yet and was created. Continuing..");
    }
    
    // check if a seed roi is given
    if(this->ui->codeLinkSeedCombobox->currentText() == "none")
    {
        ThrowError("No seed ROI was specified.");
        return;
    }
    
    bool continuing = checkFilepathsForOverwrite(toCheck);
    if (!continuing) {
        return;
    }
    
    /*** run FM on the local machine ***/
    
    if (option == "LOCAL") {
        
        QString executablePath = regSettings.value(
                                                   SettingsDialog::SETTINGS_CODE_PATH_LABEL).toString();
        
        // check if local FM executable exists
        if (!QFile().exists(executablePath))
        {
            ThrowError("Path to local FM executable is incorrect or has yet not been configured.");
            SettingsDialog* dialog = new SettingsDialog();
            dialog->selectTab(0);
            dialog->exec();
            delete dialog;
            return;
        }
        
        this->m_model->SetupCode(option,
                                 this->m_currentExpSettings,
                                 executablePath,
                                 this->ui->speedMapEdit->text());
        
        this->m_pseudoDialog = new QProgressDialog("Running local FM code..", "Abort", 0, 0, this);
        this->m_pseudoDialog->setFont(QFont("Segoe UI", 10));
        this->m_pseudoDialog->setWindowModality(Qt::WindowModal);
        this->m_pseudoDialog->show();
        this->m_pseudoDialog->raise();
        qApp->processEvents();
        
        this->m_model->RunCode(m_pseudoDialog);
        
        connect(this->m_pseudoDialog, SIGNAL(canceled()), this, SLOT(slotAbortExternalCode()));
        
        this->ui->statusbar->showMessage("Running external code...");
        
    }
    
    /*** run FM on a remote Linux machine ***/
    
    else if (option == "REMOTE") {
        
        QString SSHCommand = QString("\"%1\" \"%2/%3_input\" \"%2/%3_output\"").arg(                                                                                                    regSettings.value(SettingsDialog::SETTINGS_CODE_SSH_LABEL).toString(),                                                                                                                                  regSettings.value(SettingsDialog::SETTINGS_CODE_SERVER_DATA_DIRECTORY).toString(),                                                                                                                                  this->m_currentExpSettings.name);
        
        this->m_model->SetupCode(option,
                                 this->m_currentExpSettings,
                                 SSHCommand,
                                 this->ui->speedMapEdit->text());
        
        this->m_pseudoDialog = new QProgressDialog("Running remote FM code", "Abort", 0, 0, this);
        this->m_pseudoDialog->setFont(QFont("Segoe UI", 10));
        this->m_pseudoDialog->setMinimumWidth(400);
        this->m_pseudoDialog->setWindowModality(Qt::WindowModal);
        this->m_pseudoDialog->setMinimumDuration(0);
        this->m_pseudoDialog->setValue(0);
        this->m_pseudoDialog->show();
        this->m_pseudoDialog->raise();
        qApp->processEvents();
        
        this->m_model->RunCode(this->m_pseudoDialog);
        
        connect(this->m_pseudoDialog, SIGNAL(canceled()), this, SLOT(slotAbortExternalCode()));
        
        this->ui->statusbar->showMessage("Running remote FM code...");
        
    }
}

void Controller::CodeFinished(bool succes) {

	if (succes) {
		this->ui->statusbar->showMessage("Finished running external code!");
	}

	disconnect(this->m_pseudoDialog, SIGNAL(canceled()), this, SLOT(slotAbortExternalCode()));

	this->m_pseudoDialog->hide();
	delete this->m_pseudoDialog;
	this->m_pseudoDialog = nullptr;
	
	if (this->m_currentExpSettings.importAfterwards && succes) {

		QProgressDialog* dialog = LoadingStart();

		bool ok = this->m_model->LoadFromBinaryFile(
			this->m_currentExpSettings.outputHeaderPath,
			this->m_currentExpSettings.outputBinaryPath);

		LoadingDone(dialog);

		if (!ok) {
			ThrowError("Import failed");
		}
	}
}

void Controller::slotAbortExternalCode() {

	QMessageBox msg{};
	msg.setText("Do you want to abort the external code?");
	msg.addButton(QMessageBox::StandardButton::Yes);
	msg.addButton(QMessageBox::StandardButton::No);
	msg.setDefaultButton(QMessageBox::StandardButton::No);
	if (msg.exec() == 0) {
		return;
	}

	this->m_model->AbortCode();
	this->ui->statusbar->showMessage("Aborted external code.");
}

void Controller::slotSpeedMapSpecifyTriggered() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir_speedmap");
	QSettings MySettings;

	QString filepath = QFileDialog::getOpenFileName(this, tr("Load distance map"),
													MySettings.value(DEFAULT_DIR_KEY).toString(),
													tr("NIfTI (*.nii* *.nii.gz *.hdr);;Any files (*)"));
	if (filepath.isEmpty()) {
		return;
	}

	QFileInfo fi(filepath);
	MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	filepath = QDir::toNativeSeparators(filepath);

	this->ui->speedMapEdit->setText(filepath);
}

void Controller::slotRunExternalCode() {
	
	runFMCode();
}

bool Controller::checkFilepathsForOverwrite(QStringList check) {

	QStringList existing = {};

	for (int i = 0; i < check.size(); i++) {
		if (QFile(check.at(i)).exists()) {
			existing << check.at(i);
		}
	}

	if (!existing.isEmpty()) {
		QString text = "";
		text += "Overwrite these files ? \n\n";

		for (int i = 0; i < existing.size(); i++) {
			text += existing[i] + "\n";
		}

		QMessageBox msg{};
		msg.setText(text);
		msg.addButton(QMessageBox::StandardButton::Yes);
		msg.addButton(QMessageBox::StandardButton::No);
		msg.setDefaultButton(QMessageBox::StandardButton::Yes);
		int result = msg.exec();

		if (result == 0) {
			return false;
		}
		else {
			// Delete files
			for (int j = 0; j < existing.size(); j++) {
				QFile(existing.at(j)).remove();
			}
		}
	}

	return true;
}

Ui_View* Controller::GetUIConnection() {
	return this->ui;
}


void Controller::AddVisualObjectToUi(VisualType type, QString label) {

	this->BlockAllSignals(true);

	switch (type) {
	case VTYPE_GLYPHS:
		break;
	case VTYPE_SCALARMAP:

		this->m_distanceMapController->AddObject(label, true);
		break;
	case VTYPE_CONTOURPLOT:
		break;
	case VTYPE_ROI:

		this->m_regionController->AddObject(label, true);
		break;

	case VTYPE_GEODESIC:

		this->m_geodesicController->AddObject(label, true);
		break;

	case VTYPE_ANATOMICAL_VOLUME:

		this->m_referenceController->AddObject(label, true);
		break;

	default:
		break;
	}

	this->BlockAllSignals(false);
}
void Controller::ExportScreenshots(QString filepath) {

	// Get main screen
	QPixmap main = this->grab();

	// Get visual data
	vtkImageData* imageData = this->ui->mainScreen->cachedImage();
	const int width = imageData->GetDimensions()[0];
	const int height = imageData->GetDimensions()[1];

	QImage image(width, height, QImage::Format_RGB32);
	QRgb* rgbPtr = reinterpret_cast<QRgb*>(image.bits()) + width * (height - 1);
	unsigned char* colorsPtr = reinterpret_cast<unsigned char*>(imageData->GetScalarPointer());

	// Mirror vertically
	for (int row = 0; row < height; ++row) {
		for (int col = 0; col < width; ++col) {
			// Swap rgb
			*(rgbPtr++) = QColor(colorsPtr[0], colorsPtr[1], colorsPtr[2]).rgb();
			colorsPtr += 3;
		}
		rgbPtr -= width * 2;
	}

	// Add to main pixmap
	QPixmap pixmapPart = QPixmap::fromImage(image);
	QPoint paintPos = this->ui->mainScreen->mapTo(this, this->ui->mainScreen->rect().topLeft());

	QPainter painter(&main);
	painter.drawPixmap(paintPos, pixmapPart);

	// Export
	const int MARGIN = 6;
	QImage copy;

	QPoint cropTopLeft = QPoint(
		this->ui->mainScreen->mapTo(this, this->ui->mainScreen->rect().topLeft()).x() - MARGIN,
		this->ui->mainScreen->mapTo(this, this->ui->mainScreen->rect().topLeft()).y() - MARGIN);

	QPoint cropBottomRight = QPoint(
		this->ui->mainScreen->mapTo(this, this->ui->mainScreen->rect().bottomRight()).x() + MARGIN,
		this->ui->mainScreen->mapTo(this, this->ui->mainScreen->rect().bottomRight()).y() + MARGIN);

	QFile file(filepath.toStdString().c_str());
	file.open(QIODevice::WriteOnly);

	copy = main.toImage().copy(QRect(cropTopLeft, cropBottomRight));
	copy.save(&file, "PNG");

}

void Controller::ExportScreenshotHighRes(QString filepath) {

	// Content
	vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter =
		vtkSmartPointer<vtkWindowToImageFilter>::New();
	windowToImageFilter->SetInput(this->m_visualization->GetRenderWindow());
	windowToImageFilter->ShouldRerenderOff();

	// Magnification
	windowToImageFilter->SetMagnification(4.0);
	windowToImageFilter->SetInputBufferTypeToRGBA();
	windowToImageFilter->Update();

	vtkSmartPointer<vtkPNGWriter> writer =
		vtkSmartPointer<vtkPNGWriter>::New();
	writer->SetFileName(filepath.toLocal8Bit().data());
	writer->SetInputConnection(windowToImageFilter->GetOutputPort());
	writer->Write();

}

void Controller::CheckLockingOfControls() {

	this->BlockAllSignals(true);

	// Geodesics
	QString text = this->ui->scalarProjectionCombobox->currentText();

	this->ui->scalarProjectionCombobox->clear();

	this->ui->scalarProjectionCombobox->addItem("Minimum");
	this->ui->scalarProjectionCombobox->addItem("Composite");

	if (this->m_model->CheckLocking(Model::LockType::GeodesicProjectionOfDistanceMap)) {
		this->ui->scalarProjectionCombobox->addItem("Geodesic");
		this->ui->scalarProjectionCombobox->addItem("Geodesic_2");
	}
	else if (text == "Geodesic" || text == "Geodesic_2") {
		text = "Minimum";
	}

	this->ui->scalarProjectionCombobox->setCurrentText(text);


	// Import distance map
	bool dist = this->m_model->CheckLocking(Model::LockType::DistanceMap);
	this->ui->glyphRenderCheckBox->setEnabled(dist);
	this->ui->scalarRenderCheckBox->setEnabled(dist);
	this->ui->contourRenderCheckBox->setEnabled(dist);
	this->ui->colorbarCheckBox->setEnabled(dist);
	this->ui->outlineCheckBox->setEnabled(dist);
	this->ui->contrastBox->setEnabled(dist);
	this->ui->distanceMapVisOtherLabel->setEnabled(dist);
	this->ui->distanceMapVisRenderLabel->setEnabled(dist);
	this->ui->navPanel->setEnabled(dist);

	if (!dist) {
		this->ui->scalarBox->setVisible(false);
		this->ui->glyphBox->setVisible(false);
		this->ui->contourBox->setVisible(false);
	}

	this->ui->scalarRenderCheckBox->setChecked(dist);
	
	// Manual regions
	bool reg = this->m_model->CheckLocking(Model::LockType::NewManualRegionFromReference);
	this->ui->newRegionButtonCustom->setEnabled(reg);

	// Update Region comboboxes
	QStringList regions;
	for (int i = 0; i < this->ui->regionListWidget->count(); i++) {
		regions << this->ui->regionListWidget->item(i)->text();
	}

//    this->ui->roiACombobox->clear();
//    this->ui->roiBCombobox->clear();
//    this->ui->codeLinkSeedCombobox->clear();
//    this->ui->codeLinkTipCombobox->clear();
//    this->ui->codeLinkSeedCombobox->addItem("none");
//    this->ui->codeLinkTipCombobox->addItem("none");
//
//    for (int i = 0; i < regions.size(); i++) {
//        this->ui->roiACombobox->addItem(regions.at(i));
//        this->ui->roiBCombobox->addItem(regions.at(i));
//        this->ui->codeLinkSeedCombobox->addItem(regions.at(i));
//        this->ui->codeLinkTipCombobox->addItem(regions.at(i));
//    }

	this->BlockAllSignals(false);
}

void Controller::slotAbout() {

	QString about =
		"This software has been developed at the Eindhoven University of Technology, in collaboration with Kempenhaeghe.\n"
		"This software is presented as is, with absolutely no warranty or garantuees at all.\n"
		"\n"
		"\n"
		"Stefan Mari�n, s.marien127@gmail.com"
		"Stephan Meesters, stephan.meesters@gmail.com";

	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Icon::Information);
	msgBox.setText(about);
	msgBox.setWindowTitle("About");
	msgBox.exec();

}

