/** Includes */
#include "DataObjFactory.h"

#include "DistanceMapSource.h"
#include "RegionSource.h"
#include "GeodesicSource.h"
#include "ReferenceSource.h"

#include "DataIOManager.h"

#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>

#include <QList>
#include <QPair>

//---------------[ Constructor ]-------------------

DataObjFactory::DataObjFactory() {
}

//---------------[ Destructor ]-------------------

DataObjFactory::~DataObjFactory() {
}

DataObject* DataObjFactory::makeDataObj(QString path, DataType dtype) {

	switch (dtype) {
        case DTYPE_DISTANCEMAP:
        {

            DistanceMapSource* distancemap = DistanceMapSource::createWithNiftiData(path);
            if (distancemap) {
                return distancemap;
            }
            break;
        }
        case DTYPE_ROI:
        {
            RegionSource* region = RegionSource::createWithNiftiData(path);
            if (region) {
                return region;
            }
            break;
        }
		case DTYPE_GEODESIC:
		{
			GeodesicSource* geo = GeodesicSource::createWithVtkDataset(path);
			if (geo) {
				return geo;
			}

			break;
		}
		case DTYPE_REFERENCE_VOLUME:
		{
			ReferenceSource* vol = ReferenceSource::createWithNiftiData(path);
			if (vol) {
				return vol;
			}

			break;
		}
        case DTYPE_NONE:
            break;

	}
	return nullptr;
}

QList<DataObject*> DataObjFactory::makeDataObjectsFromBinary(QString headerPath, 
															 QString dataPath,
															 QStringList filter) {


	SP(vtkImageData) templateImage;

	QList<DataObject*> out;

	QList<QPair<QString, SP(vtkImageData)>> volumes;
	QList<QPair<QString, QString>> textData;
	QList<QPair<QString, double>> scalarData;
	QList<QPair<QString, SP(vtkPolyData)>> geoData;
	QList<QPair<QString, SP(vtkPolyData)>> regionData;

	DataIOManager::FullReadBinary(headerPath, dataPath, textData,
								  scalarData, geoData, regionData, volumes);

    // extract experiment name
    QString experimentName;
    bool hasExperimentName = false;
    QMutableListIterator<DataIOManager::TextualData> j(textData);
    while (j.hasNext()) {
        DataIOManager::TextualData entry = j.next();
        QString name = entry.first;
        if(name == "ExperimentName")
        {
            experimentName = entry.second;
            hasExperimentName = true;
            break;
        }
    }
    
	// Create volumes
	for (int i = 0; i < volumes.size(); i++) {

		if (filter.contains(volumes.at(i).first) || filter.isEmpty()) {
            
			DistanceMapSource* distancemap = DistanceMapSource::createWithBinaryData(volumes.at(i).second);
			if (distancemap) {
                if(hasExperimentName)
                    distancemap->label = QString("%1 -- %2").arg(experimentName,volumes.at(i).first);
                else
                    distancemap->label = volumes.at(i).first;
				out.append(distancemap);

				templateImage = volumes.at(i).second;
			}
		}		
	}

	// Create log data


	// Create scalar data


	// Create geodesics
	for (int i = 0; i < geoData.size(); i++) {

		if (filter.contains(geoData.at(i).first) || filter.isEmpty()) {

			GeodesicSource* geodesic = GeodesicSource::createWithBinaryData(geoData.at(i).second);
			if (geodesic) {
                if(hasExperimentName)
                    geodesic->label = QString("%1 -- %2").arg(experimentName,geoData.at(i).first);
                else
                    geodesic->label = geoData.at(i).first;
				out.append(geodesic);
			}
		}
	}

	// Create regions
	for (int i = 0; i < regionData.size(); i++) {

		if (filter.contains(regionData.at(i).first) || filter.isEmpty()) {

			RegionSource* geodesic = RegionSource::createWithBinaryData(regionData.at(i).second, templateImage);
			if (geodesic) {
                if(hasExperimentName)
                    geodesic->label = QString("%1 -- %2").arg(experimentName,regionData.at(i).first);
                else
                    geodesic->label = regionData.at(i).first;
				out.append(geodesic);
			}
		}
	}
	

	return out;
}
