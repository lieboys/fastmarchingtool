#pragma once

/** Includes - Main Headers */
#include "DataObject.h"

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>

/* DataObject for Geodesics: line data, with vectors on each point*/
class GeodesicSource : public DataObject {
public:

	/* Construct from a VTK polydata file */
	static GeodesicSource* createWithVtkDataset(QString filename);

	/* Construct from binary data */
	static GeodesicSource* createWithBinaryData(SP(vtkPolyData) input);

	// Function for reimporting the dataobject, when the file has changed.
	void Reimport();

	/** Mesh data */
	SP(vtkPolyData) data;

private:

	bool initWithFile(QString filename);
	bool initWithBinary(SP(vtkPolyData) data);
};
