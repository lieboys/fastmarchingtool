#pragma once

/** Includes - Main Headers */
#include "DataObject.h"

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkTransform.h>

enum DistanceMapType {
    DISTANCEMAP_FINSLER,
    DISTANCEMAP_EUCLIDEAN,
    DISTANCEMAP_UNKNOWN
};

/* DataObject for Distance maps: 3-5D volumes*/
class DistanceMapSource : public DataObject {
public:

	/* Construct from a Nifti dataset */
	static DistanceMapSource* createWithNiftiData(QString filename);

	/* Construct from a binary dataset */
	static DistanceMapSource* createWithBinaryData(SP(vtkImageData) input);

	/* Voxel data */
	SP(vtkImageData) data;

	/* Nifti transformation info*/
	SP(vtkTransform) transform;

	/*Flag for if the data is based on projective (81) or full(162) orientations. */
	bool projective;

	/* Function for reimporting the dataobject, when the file has changed. */
	void Reimport();
    
    /* Euclidean or Finsler map */
    DistanceMapType distanceMapType;

private:

	bool initWithFile(QString filename);
	bool initWithBinary(SP(vtkImageData) input);
};
