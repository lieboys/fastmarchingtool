/** Includes */
#include "ReferenceSource.h"

#include "DataIOManager.h"

#include <QDir>

ReferenceSource* ReferenceSource::createWithNiftiData(QString filename) {
	ReferenceSource *pRet = new (std::nothrow) ReferenceSource();
	if (pRet && pRet->initWithFile(filename)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void ReferenceSource::Reimport() {

	this->transform = nullptr;
	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(this->sourcePath, this->transform);
	if (!niftiData) {
		return;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() != 1) {
		std::cout << "Region : '" + this->sourcePath.toStdString() + "' cannot be imported: volume is not 3D" << std::endl;
		return;
	}

	this->data = nullptr;
	this->data = niftiData;

	return;
}

bool ReferenceSource::initWithFile(QString filename) {
	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(filename, this->transform);
	if (!niftiData) {
		return false;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() != 1) {
		std::cout << "Region : '" + filename.toStdString() + "' cannot be imported: volume is not 3D" << std::endl;
		return false;
	}

	this->data = niftiData;
	this->sourcePath = filename;
	this->dataType = DTYPE_REFERENCE_VOLUME;
	this->label = QDir(filename).dirName();

	return true;
}