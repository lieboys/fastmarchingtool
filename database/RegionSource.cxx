/** Includes */
#include "RegionSource.h"

#include "DataIOManager.h"

#include <QDir>

RegionSource* RegionSource::createWithNiftiData(QString filename) {
	RegionSource *pRet = new (std::nothrow) RegionSource();
	if (pRet && pRet->initWithFile(filename)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

RegionSource* RegionSource::createWithBinaryData(SP(vtkPolyData) data, SP(vtkImageData) source) {
	RegionSource *pRet = new (std::nothrow) RegionSource();
	if (pRet && pRet->initWithBinary(data, source)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void RegionSource::Reimport() {

	this->transform = nullptr;
	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(this->sourcePath, this->transform);
	if (!niftiData) {
		return;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() != 1) {
		std::cout << "Region : '" + this->sourcePath.toStdString() + "' cannot be imported: volume is not 3D" << std::endl;
		return;
	}

	this->data = nullptr;
	this->data = niftiData;

	return;
}

bool RegionSource::initWithFile(QString filename) {

	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(filename, this->transform);
	if (!niftiData) {
		return false;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() != 1) {
		std::cout << "Region : '" + filename.toStdString() + "' cannot be imported: volume is not 3D" << std::endl;
		return false;
	}

	// TODO: Check if volume is binary?

	this->data = niftiData;
	this->sourcePath = filename;
	this->dataType = DTYPE_ROI;
	this->label = QDir(filename).dirName();

	return true;
}


bool RegionSource::initWithBinary(SP(vtkPolyData) data, SP(vtkImageData) source) {
	
	VTK_CREATE(vtkImageData, regionData);
	regionData->SetDimensions(source->GetDimensions());
	regionData->SetExtent(source->GetExtent());
	regionData->SetOrigin(source->GetOrigin());
	regionData->AllocateScalars(VTK_DOUBLE, 1);

	for (vtkIdType n = 0; n < data->GetNumberOfPoints(); n++) {

		int x = data->GetPoint(n)[0];
		int y = data->GetPoint(n)[1];
		int z = data->GetPoint(n)[2];

		double* pointer = static_cast<double*>(regionData->GetScalarPointer(x, y, z));
		pointer[0] = 1.0;
	}


	this->data = regionData;
	this->sourcePath = "BIN";
	this->dataType = DTYPE_ROI;

	return true;
}
