/** Includes */
#include "VisualObjFactory.h"

#include "VTKScalarProjection.h"
#include "VTKGlyphs.h"
#include "VTKRegion.h"
#include "VTKContourPlot.h"
#include "VTKGeodesics.h"
#include "VTKAnatomicalVolume.h"

//---------------[ Constructor ]-------------------

VisualObjFactory::VisualObjFactory() {
}

//---------------[ Destructor ]-------------------

VisualObjFactory::~VisualObjFactory() {
}

VisualizationObject* VisualObjFactory::makeVisualObj(DataObject* obj, VisualType vtype) {
	switch (vtype) {
	case VTYPE_GLYPHS: {
		VTKGlyphs* glyphs = VTKGlyphs::CreateWithDataObject(obj);
		if (glyphs) {
			return glyphs;
		}
		else {
			// notify user of error
		}
		break;
	}
	case VTYPE_SCALARMAP: {
		VTKScalarProjection* projection = VTKScalarProjection::CreateWithDataObject(obj);
		if (projection) {
			return projection;
		}
		else {
			// notify user of error
		}
		break;
	}
	case VTYPE_ROI: {
		VTKRegion* region = VTKRegion::CreateWithDataObject(obj);
		if (region) {
			return region;
		}
		else {
			// notify user of error
		}
		break;
	}
	case VTYPE_CONTOURPLOT: {
		VTKContourPlot* contours = VTKContourPlot::CreateWithDataObject(obj);
		if (contours) {
			return contours;
		}
		else {
			// notify user of error
		}
		break;
	}
	case VTYPE_GEODESIC: {
		VTKGeodesics* geo = VTKGeodesics::CreateWithDataObject(obj);
		if (geo) {
			return geo;
		}
		else {
			// notify user of error
		}
		break;
	}
	case VTYPE_ANATOMICAL_VOLUME: {
		VTKAnatomicalVolume* vol = VTKAnatomicalVolume::CreateWithDataObject(obj);
		if (vol) {
			return vol;
		}
		else {
			// notify user of error
		}
		break;
	}
	}
	return nullptr;
}