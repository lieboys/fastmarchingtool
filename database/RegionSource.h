#pragma once

/** Includes - Main Headers */
#include "DataObject.h"

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkTransform.h>

/* DataObject for regions maps: either volume or point based */
class RegionSource : public DataObject {
public:

	/* Construct from a Nifti dataset */
	static RegionSource* createWithNiftiData(QString filename);

	/* Construct from a point-based, binary dataset */
	static RegionSource* createWithBinaryData(SP(vtkPolyData), SP(vtkImageData) source);

	/* Function for reimporting the dataobject, when the file has changed. */
	void Reimport();

	/* Voxel data */
	SP(vtkImageData) data;

	/* Nifti transformation info*/
	SP(vtkTransform) transform;

private:

	bool initWithFile(QString filename);
	bool initWithBinary(SP(vtkPolyData) data, SP(vtkImageData) source);
};
