#pragma once

/** Includes - Main Headers */
#include <QString>
#include "common/Macros.h"

/* Virtual base object for all DataObjects*/
class DataObject {
public:

	virtual bool init() {
		return true;
	};

	DataObject() {
		sourcePath = nullptr;
		dataType = DTYPE_NONE;
	};
	virtual ~DataObject() {
	};

	QString getSourcePath() {
		return sourcePath;
	}
	DataType getDataType() {
		return dataType;
	}

	QString label;

	virtual void Reimport() = 0;

protected:

	/** Variables */
	QString sourcePath;
	DataType dataType;
};
