#include "DataIOManager.h"

#include <vtkTransform.h>

#include <vtkNIFTIHeader.h>
#include <vtkNIFTIWriter.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkLine.h>
#include <vtkImageMathematics.h>
#include <vtkErrorCode.h>

#include <QRegularExpression>
#include <QVector>
#include <QDataStream>

SP(vtkImageData) DataIOManager::ReadNifti3D(QString fileName, SP(vtkTransform) transform) {
	
	// Convert QString to char
	QFileInfo fileInfo(fileName);
	QByteArray ba = fileInfo.filePath().toLocal8Bit();
	char* fileName_c = ba.data();

	// Reader
	VTK_CREATE(vtkNIFTIReader, reader);
	if (!reader->CanReadFile(fileName_c)) {
		cerr << "CanReadFile failed for " << fileName_c << "\n";
		return NULL;
	}
    reader->TimeAsVectorOn();
	reader->SetFileName(fileName_c);
	reader->Update();
    if (reader->GetErrorCode() != vtkErrorCode::NoError)
    {
        return NULL;
    }
    
    // get volume, set spacing to 1 mm / voxel
    SP(vtkImageData) volume = reader->GetOutput();
    volume->SetSpacing(1.0, 1.0, 1.0);
	
	return volume;
}

void DataIOManager::WriteNifti3D(SP(vtkImageData) data, QString filepath, 
								QString description, SP(vtkTransform) transform) {

	VTK_CREATE(vtkNIFTIHeader, hdr);

	VTK_CREATE(vtkNIFTIWriter, writer);
	writer->SetDescription(description.toStdString().c_str());
	writer->SetFileName(filepath.toStdString().c_str());
	writer->SetNIFTIHeader(hdr);
	if (transform) {
		writer->SetSFormMatrix(transform->GetMatrix());
	}
	writer->SetInputData(data);
	writer->Write();

}

QString DataIOManager::ReadTextFile(QString fileName) {

	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly)) {
		std::cout << " Error: reading header file: " << file.errorString().toStdString() << std::endl;
		return "";
	}

	QString total = file.readAll();

	return total;
}

void DataIOManager::writeBinaryFile(double* data, int size, QString filepath) {

	QFile file(filepath);
	if (!file.open(QIODevice::WriteOnly)) {
		std::cout << " Error: writing binary file: " << file.errorString().toStdString() << std::endl;
		return;
	}
	
	file.write(reinterpret_cast<char*>(data), size * sizeof(double));
	
}

void DataIOManager::writeTextFile(QString text, QString filepath) {

	QFile file(filepath);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		std::cout << " Error: writing header file: " << file.errorString().toStdString() << std::endl;
		return;
	}

	file.write(text.toStdString().c_str());

}

QList<QPair<QString, QString>> DataIOManager::ReadBinaryHeaderOnly(QString headerFile) {
	
	QList<QPair<QString, QString>> out;

	QString header = ReadTextFile(headerFile);
	header.replace('\r', "");

	QList<DataIOManager::TextualData> textualData;
	QList<DataIOManager::BinaryData> binaryList;
	DataIOManager::parseFromBinaryHeader(header, binaryList, textualData);

	for (int i = 0; i < binaryList.size(); i++) {

		QString name = std::get<QString>(binaryList.at(i));
        DataIOManager::BinaryDataType dType = getBinaryDataTypeFromName(name);
        
        if(dType == DataIOManager::BinaryDataType::Undefined)
            continue; // type not recognized, skip
        
		QString type = dataTypeToString(dType);
		out << QPair<QString, QString>(type, name);
	}

	return out;
}

void DataIOManager::FullReadBinary(QString headerFile, QString dataFile,
								   QList<QPair<QString, QString>> &textualData,
								   QList<QPair<QString, double>> &scalarData,
								   QList<QPair<QString, SP(vtkPolyData)>> &geoData,
								   QList<QPair<QString, SP(vtkPolyData)>> &regionData,
								   QList<QPair<QString, SP(vtkImageData)>> &volumeData) {

	// Read header
	QString header = ReadTextFile(headerFile);
	header.replace('\r', "");

	// Read binary data and convert to doubles
	QFile file(dataFile);
	if (!file.open(QIODevice::ReadOnly)) return;
	QByteArray binary = file.readAll();
	double* sourceData = reinterpret_cast<double*>(binary.data());

	QList<DataIOManager::BinaryData> binaryList;
	DataIOManager::parseFromBinaryHeader(header, binaryList, textualData);
    
    QList<int> geodesicLengths;

	// Do something with the data    
	QMutableListIterator<DataIOManager::BinaryData> i(binaryList);
	while (i.hasNext()) {

		DataIOManager::BinaryData entry = i.next();

		QString name = std::get<0>(entry);
//        double size = std::get<1>(entry);
//        printf(name.toStdString().c_str());
		int dim = std::get<2>(entry);
		int* m_dims = std::get<3>(entry);
        int pos = std::get<4>(entry);

		DataIOManager::BinaryDataType type = DataIOManager::getBinaryDataTypeFromName(name);
		switch (type) {
            case DataIOManager::BinaryDataType::Parameter:
            {
                QPair<QString, double> container(name, 0.0);
                scalarData.append(container);
                i.remove();
                break;
            }
            case DataIOManager::BinaryDataType::Geodesic:
            {
                if(geodesicLengths.empty())
                    continue;
                
                SP(vtkPolyData) poly = buildGeoData(sourceData, pos, dim, m_dims, geodesicLengths);
                if (poly == nullptr) {
                    continue;
                }

                QPair<QString, SP(vtkPolyData)> container(name, poly);
                geoData.append(container);
                i.remove();
                break;
            }
            case DataIOManager::BinaryDataType::GeodesicLength:
            {
                QList<int> geoLengths = buildGeoLengths(sourceData, pos, dim, m_dims);
                if (geoLengths.empty()) {
                    continue;
                }
                geodesicLengths = geoLengths;
                i.remove();
                break;
            }
            case DataIOManager::BinaryDataType::Region:
            {
                SP(vtkPolyData) data = buildGeoData(sourceData, pos, dim, m_dims);
                if (data == nullptr) {
                    continue;
                }

                QPair<QString, SP(vtkPolyData)> container(name, data);
                regionData.append(container);
                i.remove();
                break;
            }
            case DataIOManager::BinaryDataType::ScalarVolume:
            {
                SP(vtkImageData) volume = buildDataScalarVolume(sourceData, pos, dim, m_dims);
                if (volume == nullptr) {
                    continue;
                }

                QPair<QString, SP(vtkImageData)> container(name, volume);
                volumeData.append(container);
                i.remove();
                break;
            }
            case DataIOManager::BinaryDataType::Undefined:
                i.remove();
                break;
		}
        
        if(!i.hasNext()) // keep looping until all data objects processed
            i.toFront();
	}
    
    // Remove the data binaries from memory
    binaryList.clear();
    sourceData = NULL;
    binary.clear();
    file.close();
}

void DataIOManager::parseFromBinaryHeader(QString header,
								 QList<DataIOManager::BinaryData> &binaryList,
								 QList<DataIOManager::TextualData> &textData) {

	QStringList entries = header.split(QRegularExpression("\n\n"));
    
    int pos = 0;

	// For each header entry: (split by \n\n)
	for (int i = 0; i < entries.size(); i++) {

		QStringList entry = entries.at(i).split("\n");
		int length = entry.length();
		if (length < 2) {
			continue;
		}

		QString name = entry[0];

		// Calculate new size: 	
		int* dataDimensions = new int[length - 2];
		int size = 1;
		for (int i = 0; i < length - 2; i++) {

			// Swap Y and X
			if (i == 0 && length > 3) {
				dataDimensions[i] = entry[3].toInt();
				size *= dataDimensions[i];
			}
			else if (i == 1 && length > 3) {
				dataDimensions[i] = entry[2].toInt();
				size *= dataDimensions[i];
			}
			else {
				dataDimensions[i] = entry[2 + i].toInt();
				size *= dataDimensions[i];
			}
		}

		// Check dimensions: 
		int m_dims = entry[1].toInt();
		if (m_dims == -1) {

			// Parse text data
			DataIOManager::TextualData data(name, entry[2]);
			textData.append(data);
		}
		else{/* if (dims == 0) {*/

			BinaryData binData = BinaryData(name, size, m_dims, dataDimensions, pos);
			
            pos += size;
			binaryList.append(binData);
		}
	}
}

DataIOManager::BinaryDataType DataIOManager::getBinaryDataTypeFromName(QString name) {
	
	if (BinairDefinitions::BIN_NAMES_ScalarVolumes.contains(name, Qt::CaseInsensitive)) {
		return DataIOManager::BinaryDataType::ScalarVolume;
	}

	if (BinairDefinitions::BIN_NAMES_Regions.contains(name, Qt::CaseInsensitive)) {
		return DataIOManager::BinaryDataType::Region;
	}

	if (BinairDefinitions::BIN_NAMES_Geodesics.contains(name, Qt::CaseInsensitive)) {
		return DataIOManager::BinaryDataType::Geodesic;
	}
    
    if (BinairDefinitions::BIN_NAMES_GeodesicLength.contains(name, Qt::CaseInsensitive)) {
        return DataIOManager::BinaryDataType::GeodesicLength;
    }

	if (BinairDefinitions::BIN_NAMES_Parameters.contains(name, Qt::CaseInsensitive)) {
		return DataIOManager::BinaryDataType::Parameter;
	}
    
    return DataIOManager::BinaryDataType::Undefined;
}

QString DataIOManager::dataTypeToString(BinaryDataType type) {

	switch (type) {
        case DataIOManager::ScalarVolume:
            return "ScalarVolume";
            break;
        case DataIOManager::Geodesic:
            return "Geodesic";
            break;
        case DataIOManager::Region:
            return "Region";
            break;
        case DataIOManager::Parameter:
            return "Parameter";
            break;
        case DataIOManager::Undefined:
            // throw error
            throw std::runtime_error("Undefined DataIOManager object");
            return nullptr;
            break;
	}

	return nullptr;
}

SP(vtkImageData) DataIOManager::buildDataScalarVolume(double* source, int startPos, int ndim, int* m_dims) {

	int pos = startPos;

	if (ndim == 4) {

		// Reshape array, based on dimensions:  {n, z, x, y} = {4, 3, 1, 2}
		VTK_CREATE(vtkImageData, volume);
		volume->SetDimensions(m_dims);
		volume->AllocateScalars(VTK_DOUBLE, m_dims[3]);
        volume->SetSpacing(1.0, 1.0, 1.0);
        
        printf("Allocating %f MB of memory\n",(double)(m_dims[0]*m_dims[1]*m_dims[2]*m_dims[3]*sizeof(double)/(1024.0*1024.0)));
		
		for (int n = 0; n < m_dims[3]; n++) {
			for (int z = 0; z < m_dims[2]; z++) {
				for (int x = 0; x < m_dims[0]; x++) {
					for (int y = 0; y < m_dims[1]; y++) {

						double* targetVoxel = static_cast<double*>(volume->GetScalarPointer(x, y, z));

						targetVoxel[n] = (source)[pos];
						pos++;
					}
				}
			}
		}

		return volume;
	}



	return nullptr;
}


SP(vtkPolyData) DataIOManager::buildGeoData(double* source, int startPos, int ndim, int* m_dims) {

	int pos = startPos;

	VTK_CREATE(vtkPoints, points);

	if (m_dims[1] < 3 ) {
		return nullptr;
	}

	VTK_CREATE(vtkDoubleArray, orientData);
	if (m_dims[1] > 3) {		
		orientData->SetNumberOfComponents(m_dims[1] - 3);
	}
		
	for (int n = 0; n < m_dims[0]; n++) {

		double point[3] = {
			(source)[pos++],
			(source)[pos++],
			(source)[pos++]
		};
		vtkIdType i = points->InsertNextPoint(point);

		if (m_dims[1] == 4) {
			orientData->InsertTuple1(i, (source)[pos++]);
		}
		else if (m_dims[1] == 5) {
			orientData->InsertTuple2(i, (source)[pos++], (source)[pos++]);
		}
		else if (m_dims[1] == 6) {
			orientData->InsertTuple3(i, (source)[pos++], (source)[pos++], (source)[pos++]);
		}
	}

	// Parse orientations
	if (m_dims[1] == 6) {
		VTK_CREATE(vtkPolyData, output);
		output->SetPoints(points);
		output->GetPointData()->SetVectors(orientData);

		// Build lines
		VTK_CREATE(vtkCellArray, lines);
		for (unsigned int i = 0; i < output->GetNumberOfPoints(); i++) {
			vtkSmartPointer<vtkLine> line =
				vtkSmartPointer<vtkLine>::New();
			line->GetPointIds()->SetId(0, i);
			line->GetPointIds()->SetId(1, i + 1);
			lines->InsertNextCell(line);
		}
		output->SetLines(lines);
		
		return output;
	}

	return nullptr;
}

SP(vtkPolyData) DataIOManager::buildGeoData(double* source, int startPos, int ndim, int* m_dims, QList<int> partitioning) {
    
    int pos = startPos;
    
    VTK_CREATE(vtkPoints, points);
    
    if (m_dims[1] < 3 ) {
        return nullptr;
    }
    
    VTK_CREATE(vtkDoubleArray, orientData);
    if (m_dims[1] > 3) {
        orientData->SetNumberOfComponents(m_dims[1] - 3);
    }
    orientData->SetName("orientations");
    
    for (int n = 0; n < m_dims[0]; n++) {
        
        double point[3] = {
            (source)[pos++],
            (source)[pos++],
            (source)[pos++]
        };
        vtkIdType i = points->InsertNextPoint(point);
        
        if (m_dims[1] == 4) {
            orientData->InsertTuple1(i, (source)[pos++]);
        }
        else if (m_dims[1] == 5) {
            orientData->InsertTuple2(i, (source)[pos++], (source)[pos++]);
        }
        else if (m_dims[1] == 6) {
            orientData->InsertTuple3(i, (source)[pos++], (source)[pos++], (source)[pos++]);
        }
    }
    
    // Parse orientations
    if (m_dims[1] == 6) {
        VTK_CREATE(vtkPolyData, output);
        output->SetPoints(points);
        output->GetPointData()->SetVectors(orientData);
        
        // Build lines
        VTK_CREATE(vtkCellArray, lines);
        int count = 0;
        for(int j = 0; j < partitioning.size(); j++) {
            for (int i = 0; i < partitioning.at(j)-1; i++) {
                vtkSmartPointer<vtkLine> line =
                vtkSmartPointer<vtkLine>::New();
                line->GetPointIds()->SetId(0, count + i);
                line->GetPointIds()->SetId(1, count + i + 1);
                lines->InsertNextCell(line);
            }
            count += partitioning.at(j);
        }
        output->SetLines(lines);
        
        return output;
    }
    
    return nullptr;
}

QList<int> DataIOManager::buildGeoLengths(double* source, int startPos, int ndim, int* m_dims) {

    QList<int> lengths;
    if(ndim != 1)
        return lengths;
    
    for(int i = startPos; i<startPos + m_dims[0]; i++)
    {
        lengths.append((source)[i]);
    }
    return lengths;
}

void DataIOManager::FullWriteBinary(QString headerFile, QString dataFile,
									QList<QPair<QString, QString>> &textualData,
									QList<QPair<QString, double>> &valueData,
									QList<QPair<QString, std::vector<double>>> &scalarData,
									QList<QPair<QString, SP(vtkPolyData)>> &polyData,
									QList<QPair<QString, SP(vtkImageData)>> &volumeData) {

	double xi = 0;

	QString headerText;
	std::vector<double> binary;

	// PARSE TEXT DATA
	for (int i = 0; i < textualData.size(); i++) {

		// Generate header entry
		QString entry = textualData.at(i).first + "\n";
		entry += "-1\n";
		entry += textualData.at(i).second + "\n\n";
		headerText += entry;

	}

	// PARSE VALUE DATA
	for (int i = 0; i < valueData.size(); i++) {

		// Generate header entry
		QString entry = valueData.at(i).first + "\n";
		entry += "0\n";
		entry += "\n";

		DataIOManager::writeScalarVector(std::vector<double>{valueData.at(i).second}, &binary);
		headerText += entry;

		if (valueData.at(i).first == "xi") {
			xi = valueData.at(i).second;
		}
	}

	// PARSE 1D SCALAR DATA
	for (int i = 0; i < scalarData.size(); i++) {

		// Generate header entry
		QString entry = scalarData.at(i).first + "\n";
		entry += "1\n";
		entry += QString::number(scalarData.at(i).second.size()) + "\n";
		entry += "\n";

		DataIOManager::writeScalarVector(scalarData.at(i).second, &binary);
		headerText += entry;

	}
	// PARSE REGION DATA
	for (int i = 0; i < polyData.size(); i++) {

		// Generate header entry
		QString entry = polyData.at(i).first + "\n";
		entry += "2\n";
		entry += "3\n";
		entry += QString::number(polyData.at(i).second->GetNumberOfPoints()) + "\n";
		entry += "\n";

		DataIOManager::writeRegion(polyData.at(i).second, &binary);
		headerText += entry;
	}

	// PARSE VOLUME DATA
	for (int i = 0; i < volumeData.size(); i++) {

		QString name = volumeData.at(i).first;

		if (name == "speed") {
			writeSpeedMap(volumeData.at(i).second, &binary, xi);

			QString entry = "speed\n5\n2\n";
			entry += QString::number(volumeData.at(i).second->GetDimensions()[1]) + "\n";
			entry += QString::number(volumeData.at(i).second->GetDimensions()[0]) + "\n";
			entry += QString::number(volumeData.at(i).second->GetDimensions()[2]) + "\n";

			int numComp = volumeData.at(i).second->GetNumberOfScalarComponents();
			entry += QString::number(numComp) + "\n";

			headerText += entry;
			continue;
		}

		int numComp = volumeData.at(i).second->GetNumberOfScalarComponents();

		int dim = (numComp == 1) ? 3 : 4;

		// Generate header entry
		QString entry = name + "\n";
		entry += QString::number(dim)+ "\n";
		
		entry += QString::number(volumeData.at(i).second->GetDimensions()[1]) + "\n";
		entry += QString::number(volumeData.at(i).second->GetDimensions()[0]) + "\n";
		entry += QString::number(volumeData.at(i).second->GetDimensions()[2]) + "\n";
	
		if (dim == 4) {
			entry += QString::number(numComp) + "\n";
		}
		entry += "\n";

		DataIOManager::writeVolume(volumeData.at(i).second, &binary);
		headerText += entry;
	}
	

	double* bin = &binary[0];

	DataIOManager::writeBinaryFile(bin, (int) binary.size(), dataFile);
	DataIOManager::writeTextFile(headerText, headerFile);

}


void DataIOManager::writeVolume(SP(vtkImageData) data, 
								std::vector<double>* target) {

	int numComponents = data->GetNumberOfScalarComponents();
	int writeSize = 1;
	if (numComponents != 1) {

		int m_dims[4] = {
			data->GetDimensions()[0],
			data->GetDimensions()[1],
			data->GetDimensions()[2],
			numComponents
		};

		for (int i = 0; i < 4; i++) {
			writeSize *= m_dims[i];
		}

		int pos = (int) target->size();
		target->resize(target->size() + writeSize, NAN);

		// 4D
		// Reshape array, based on dimensions:  {n, z, x, y} = {4, 3, 1, 2}
		for (int n = 0; n < m_dims[3]; n++) {
			for (int z = 0; z < m_dims[2]; z++) {
				for (int x = 0; x < m_dims[0]; x++) {
					for (int y = 0; y < m_dims[1]; y++) {

						double* targetVoxel = static_cast<double*>(data->GetScalarPointer(x, y, z));

						(*target)[pos] = targetVoxel[n];
						pos++;
					}
				}
			}
		}
	}

}


void DataIOManager::writeScalarVector(std::vector<double> data,
									  std::vector<double>* target) {

	int pos = (int)target->size();
	target->resize(target->size() + data.size(), NAN);

	for (int n = 0; n < data.size(); n++) {
		(*target)[pos] = data[n];
		pos++;
	}
}



void DataIOManager::writeSpeedMap(SP(vtkImageData) data,
								  std::vector<double>* target,
								  double xi) {


	int numComponents = data->GetNumberOfScalarComponents();
	int writeSize = 1;
	if (numComponents != 1) {

		int m_dims[5] = {
			data->GetDimensions()[0],
			data->GetDimensions()[1],
			data->GetDimensions()[2],
			numComponents,
			2
		};

		for (int i = 0; i < 5; i++) {
			writeSize *= m_dims[i];
		}

		int pos = (int)target->size();
		target->resize(target->size() + writeSize, NAN);

		// 5D
		// Reshape array, based on dimensions:  {n, z, x, y} = {4, 3, 1, 2, 5}
		// In mathematica:  {n, z, x, y, m} = {4, 3, 1, 2, 5}
		for (int n = 0; n < m_dims[3]; n++) {
			for (int z = 0; z < m_dims[2]; z++) {
				for (int x = 0; x < m_dims[0]; x++) {
					for (int y = 0; y < m_dims[1]; y++) {
						for (int m = 0; m < m_dims[4]; m++) {

							double* targetVoxel = static_cast<double*>(data->GetScalarPointer(x, y, z));

                            if(m == 0)
                                (*target)[pos] = 1 / (xi * targetVoxel[n]);
                            else if (m == 1)
                                (*target)[pos] = 1 / targetVoxel[n];
							pos++;
						}
					}
				}
			}
		}

	}	
}

void DataIOManager::writeRegion(SP(vtkPolyData) data, std::vector<double>* target) {


	int writeSize = 3 * data->GetNumberOfPoints();

	int pos = (int)target->size();
	target->resize(target->size() + writeSize, NAN);

	for (int n = 0; n < data->GetNumberOfPoints(); n++) {
		double* point = data->GetPoint(n);

		(*target)[pos++] = point[0];
		(*target)[pos++] = point[1];
		(*target)[pos++] = point[2];
	}
}

SP(vtkPolyData) DataIOManager::MaskToPointData(SP(vtkImageData) data) {

	VTK_CREATE(vtkPoints, points);

	int* m_dims = data->GetDimensions();

	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {

				double* targetVoxel = static_cast<double*>(data->GetScalarPointer(x, y, z));

				if (targetVoxel[0] == 0 || isnan(targetVoxel[0])) {
					continue;
				}

				points->InsertNextPoint(x + .5, y + .5, z + .5);
				
			}
		}
	}

	VTK_CREATE(vtkPolyData, output);
	output->SetPoints(points);

	return output;
}
