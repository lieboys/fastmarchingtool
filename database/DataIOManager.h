#pragma once

/** Includes - Main Headers */
#include "common/Macros.h"

/** Includes - Qt */
#include <QString>
#include <QList>
#include <QPair>
#include <QFileInfo>

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkPolyData.h>
#include <vtkNIFTIReader.h>
#include <vtkMatrix4x4.h>
#include <vtkImageReslice.h>
#include <vtkTransform.h>


/* Manager for all read/write action to files. Contains the general functions for IO operations on both standard and binary data. */
class DataIOManager {
public:
	
	/* Default function for reading a 3D Nifti file, that will save the transformation in the given vtkTransform. */
	static SP(vtkImageData) ReadNifti3D(QString fileName, SP(vtkTransform) transform);

	/* Default function for writing a 3D Nifti file. */
	static void WriteNifti3D(SP(vtkImageData) data, QString filepath,
							QString description, SP(vtkTransform) transform = nullptr);

	/* Default function for a standard text file. */
	static QString ReadTextFile(QString fileName);

	/* Reads the header file of the binary data format. */
	static QList<QPair<QString, QString>> ReadBinaryHeaderOnly(QString headerFile);

	/* Reads the all data of the binary data format. The function will save all data in the given lists for different dataobjects*/
	static void FullReadBinary(QString headerFile, QString dataFile, 
							   QList<QPair<QString, QString>>& textualData, 
							   QList<QPair<QString, double>>& scalarData, 
							   QList<QPair<QString, SP(vtkPolyData)>>& geoData, 
							   QList<QPair<QString, SP(vtkPolyData)>>& regionData,
							   QList<QPair<QString, SP(vtkImageData)>>& volumeData);

	/* Writes all data in the given datalists to a paired binary format. */
	static void FullWriteBinary(QString headerFile, QString dataFile, 
								QList<QPair<QString, QString>>& textualData, 
								QList<QPair<QString, double>>& valueData, 
								QList<QPair<QString, std::vector<double>>>& scalarData, 
								QList<QPair<QString, SP(vtkPolyData)>>& polyData, 
								QList<QPair<QString, SP(vtkImageData)>>& volumeData);


	/* Converts a binary volumetric mask to a point-based format */
	static SP(vtkPolyData) MaskToPointData(SP(vtkImageData) data);

    typedef std::tuple<QString, double, int, int*, int> BinaryData; // name of object, size, dim, dims, pos
    typedef QPair<QString, QString> TextualData;
    
private:

	enum BinaryDataType {
		ScalarVolume,
		Geodesic,
        GeodesicLength,
		Region,
		Parameter,
        Undefined
	};

	static QString dataTypeToString(BinaryDataType type);

	static void parseFromBinaryHeader(QString header, QList<DataIOManager::BinaryData>& binaryList, QList<DataIOManager::TextualData>& textData);

	static BinaryDataType getBinaryDataTypeFromName(QString name);

	static SP(vtkImageData) buildDataScalarVolume(double * source, int startPos, int ndim, int * m_dims);
	static SP(vtkPolyData) buildGeoData(double * source, int startPos, int ndim, int * m_dims);
    static SP(vtkPolyData) buildGeoData(double* source, int startPos, int ndim, int* m_dims, QList<int> partitioning);
    static QList<int> buildGeoLengths(double* source, int startPos, int ndim, int* m_dims);

	static void writeBinaryFile(double * data, int size, QString filepath);

	static void writeTextFile(QString text, QString filepath);

	static void writeVolume(SP(vtkImageData) data, std::vector<double>* target);

	static void writeScalarVector(std::vector<double> data, std::vector<double>* target);

	static void writeSpeedMap(SP(vtkImageData) data, std::vector<double>* target, double xi);

	static void writeRegion(SP(vtkPolyData) data, std::vector<double>* target);

}; 

/* Collection of alll definitions needed for read/writing in the paired binary format. */
namespace BinairDefinitions {


	/** Distance map types */
	const QString DMAP_LABEL_FINSLER_3D = "values";
	const QString DMAP_LABEL_EUCLIDEAN_3D = "valuesEuclidean";

	const QString DMAP_LABEL_FINSLER_4D = "valuesLifted";
	const QString DMAP_LABEL_EUCLIDEAN_4D = "valuesLiftedEuclidean";

	const QString INPUT_LABEL_SPEED_MAP = "speed";

	/** Parameters */
    const QString PARAM_EXPERIMENT_NAME = "ExperimentName";
    
	const QString PARAM_TYPE = "BundleType"; // this has to be with a capital first letter for some reason
	const QString PARAM_MESH = "bundleMesh";

	const QString PARAM_GRID_SCALE = "gridScale";
	const QString PARAM_EPS = "epsilon";
	const QString PARAM_XI = "xi";
	const QString PARAM_SIGMA = "sigma";
	const QString PARAM_P = "p";

	const QString PARAM_TRANSPOSE = "transposeFirstTwoCoordinates";
    const QString PARAM_STOPTIPSREACHED = "stopWhenTipsReached";
	const QString PARAM_EXPORT_VAL_LIFT = "exportValuesLifted";
	const QString PARAM_EXPORT_VAL_EUCL = "exportValuesEuclidean";
	const QString PARAM_OFFWAY_PEN = "offwayPenalization";
    const QString PARAM_APPLY_FOD_TRANSFORMATION = "applyFODTransformation";
    
	//berekend uit epilson
	
	/** Scalar list (n) */
	const QString LIST_LABEL_PHYSDIMS = "physicalDims";
	const QString GEO_LABEL_LIFTED_LENGTHS = "geodesicLengthsLifted";

	/** Point list (x,y,z) */
	const QString REGION_LABEL_TIPS = "seeds";
	const QString REGION_LABEL_SEEDS = "tips";

	//
	const QString REGION_LABEL_TIPS_LIFTED = "seedsLifted";
	const QString REGION_LABEL_SEEDS_LIFTED = "tipsLifted";

	/** Geodesics list (x,y,z, rz, ry, rz) */
	const QString GEO_LABEL_LIFTED = "geodesicsLifted";
	const QString GEO_LABEL_EUCL = "geodesics";
    
    /** Geodesic length list (n) */
    const QString GEOLENGTH_LABEL = "geodesicLengths";
		
	const QStringList BIN_NAMES_Parameters = {
		PARAM_TYPE,
        PARAM_EXPERIMENT_NAME,
		PARAM_EPS,
		PARAM_XI,
		PARAM_SIGMA,
		PARAM_P,

		PARAM_TRANSPOSE,
		PARAM_EXPORT_VAL_LIFT,
		PARAM_EXPORT_VAL_EUCL,
		PARAM_OFFWAY_PEN,
	};

	const QStringList BIN_NAMES_Geodesics = {
		GEO_LABEL_LIFTED,
		GEO_LABEL_EUCL,
	};
    
    const QStringList BIN_NAMES_GeodesicLength = {
        GEOLENGTH_LABEL
    };

	const QStringList BIN_NAMES_Regions = {
		REGION_LABEL_TIPS,
		REGION_LABEL_SEEDS
	};

	const QStringList BIN_NAMES_ScalarVolumes = {
		DMAP_LABEL_FINSLER_3D,
		DMAP_LABEL_FINSLER_4D,
		DMAP_LABEL_EUCLIDEAN_3D,
		DMAP_LABEL_EUCLIDEAN_4D,

		INPUT_LABEL_SPEED_MAP
	};

}
