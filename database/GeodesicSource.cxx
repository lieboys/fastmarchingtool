/** Includes */
#include "GeodesicSource.h"

#include "DataIOManager.h"
#include <vtkPolyDataReader.h>
#include <vtkPointData.h>

#include <vtkPolyDataWriter.h>
#include <vtkDoubleArray.h>
#include <vtkPointData.h>

GeodesicSource* GeodesicSource::createWithVtkDataset(QString filename) {
	GeodesicSource *pRet = new (std::nothrow) GeodesicSource();
	if (pRet && pRet->initWithFile(filename)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

GeodesicSource* GeodesicSource::createWithBinaryData(SP(vtkPolyData) input) {

	GeodesicSource *pRet = new (std::nothrow) GeodesicSource();
	if (pRet && pRet->initWithBinary(input)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void GeodesicSource::Reimport() {

	return;
}

bool GeodesicSource::initWithFile(QString filename) {

	VTK_CREATE(vtkPolyDataReader, reader);
	reader->SetFileName(filename.toStdString().c_str());
	reader->Update();

	vtkPolyData* polydata = reader->GetOutput();
	if (!polydata) {
		return false;
	}

	vtkDataArray* numOfVectors = polydata->GetPointData()->GetVectors();
	if (numOfVectors == nullptr) {

		std::cout << "Geodesic : '" << filename.toStdString()
			<< "' cannot be imported: no vectors found." << std::endl;
		return false;
	}

	int numOfVectorComps = polydata->GetPointData()->GetVectors()->GetNumberOfComponents();
	if (numOfVectorComps != 3) {

		std::cout << "Geodesic : '" << filename.toStdString()
			<< "' cannot be imported: incorrect vector dimensions." << std::endl;
		return false;
	}

	this->label = QFileInfo(filename).fileName();
	this->data = polydata;
	this->sourcePath = filename;
	this->dataType = DTYPE_GEODESIC;

	return true;
}

bool GeodesicSource::initWithBinary(SP(vtkPolyData) data) {

	vtkDataArray* numOfVectors = data->GetPointData()->GetVectors();
	if (numOfVectors == nullptr) {

		std::cout << "Geodesic : cannot be imported: no vectors found." << std::endl;
		return false;
	}

	int numOfVectorComps = data->GetPointData()->GetVectors()->GetNumberOfComponents();
	if (numOfVectorComps != 3) {

		std::cout << "Geodesic : cannot be imported: incorrect vector dimensions." << std::endl;
		return false;
	}

	this->data = data;
	this->sourcePath = "BIN";
	this->dataType = DTYPE_GEODESIC;

	return true;
}