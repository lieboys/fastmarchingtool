#pragma once

/** Includes - Main Headers */
#include "DataObject.h"

/** Includes - Qt */
#include <QString>
#include <QStringList>

/* Factory class for building DataObjects*/
class DataObjFactory {
public:

	/**
	 * Constructor
	 * @param
	 */
	DataObjFactory();

	/**
	 * Destructor
	 */
	~DataObjFactory();

	/* Build a DataObject based on the given dataset */
	DataObject* makeDataObj(QString path, DataType dtype);

	/* Build all DataObjects found in the binary dataset */
	QList<DataObject*> makeDataObjectsFromBinary(QString headerPath, QString dataPath, QStringList filter = QStringList());

private:

};
