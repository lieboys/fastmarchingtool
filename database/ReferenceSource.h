#pragma once

/** Includes - Main Headers */
#include "DataObject.h"

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkTransform.h>

/* DataObject for Reference Volume maps: 3D volumes*/
class ReferenceSource : public DataObject {
public:

	/* Construct from a Nifti dataset */
	static ReferenceSource* createWithNiftiData(QString filename);

	// Function for reimporting the dataobject, when the file has changed.
	void Reimport();

	/* Voxel data */
	SP(vtkImageData) data;

	/* Nifti transformation info*/
	SP(vtkTransform) transform;

private:

	bool initWithFile(QString filename);
};
