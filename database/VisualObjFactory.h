#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"

/** Includes - Qt */
#include <QString>

class VisualObjFactory {
public:

	/**
	 * Constructor
	 * @param
	 */
	VisualObjFactory();

	/**
	 * Destructor
	 */
	~VisualObjFactory();

	VisualizationObject * makeVisualObj(DataObject * obj, VisualType vtype);

private:
};
