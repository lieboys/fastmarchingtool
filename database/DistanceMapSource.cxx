/** Includes */
#include "DistanceMapSource.h"

#include "DataIOManager.h"


DistanceMapSource* DistanceMapSource::createWithNiftiData(QString filename) {
	DistanceMapSource *pRet = new (std::nothrow) DistanceMapSource();
	if (pRet && pRet->initWithFile(filename)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

DistanceMapSource* DistanceMapSource::createWithBinaryData(SP(vtkImageData) input) {

	DistanceMapSource *pRet = new (std::nothrow) DistanceMapSource();
	if (pRet && pRet->initWithBinary(input)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

bool DistanceMapSource::initWithFile(QString filename) {
	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(filename, this->transform);
	if (!niftiData) {
		return false;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() < 2) {
		std::cout << "Distance map: '" + filename.toStdString() + "' cannot be imported: volume is 3D instead of 4D" << std::endl;
		return false;
	}

	if (niftiData->GetNumberOfScalarComponents() == 162) {
		this->projective = false;
	}
	else if (niftiData->GetNumberOfScalarComponents() == 81) {
		this->projective = true;
	}
	else {
		this->projective = false;
	}

	this->data = niftiData;
	this->sourcePath = filename;
	this->dataType = DTYPE_DISTANCEMAP;
	this->label = QFileInfo(filename).fileName();
    this->distanceMapType = DISTANCEMAP_UNKNOWN;
    
	return true;
}

bool DistanceMapSource::initWithBinary(SP(vtkImageData) input) {

	this->transform = SP(vtkTransform)::New();

	if (!input) {
		return false;
	}

	// Check dimensions
	if (input->GetNumberOfScalarComponents() < 2) {
		return false;
	}

	if (input->GetNumberOfScalarComponents() == 162) {
		this->projective = false;
	}
	else if (input->GetNumberOfScalarComponents() == 81) {
		this->projective = true;
	}
	else {
		this->projective = false;
	}

	this->data = input;
	this->sourcePath = "BIN";
	this->dataType = DTYPE_DISTANCEMAP;

	return true;
}

void DistanceMapSource::Reimport() {

	this->transform = SP(vtkTransform)::New();

	SP(vtkImageData) niftiData = DataIOManager::ReadNifti3D(this->sourcePath, this->transform);
	if (!niftiData) {
		return;
	}

	// Check dimensions
	if (niftiData->GetNumberOfScalarComponents() != 1) {
		std::cout << "Distance map: '" + this->sourcePath.toStdString() + "' cannot be imported: volume is not 3D" << std::endl;
		return;
	}

	this->data = niftiData;

	return;
}
