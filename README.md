# README #

### What is this repository for? ###

This repository contains a tool to use the Lifted Fast Marching Code (developed by Jean-Marie Mireabeau). 
It allows to compute shortest paths by linking it with the Lifted Fast Marching executable.
Note that some of the features (e.g. orientation visualization of the geodesics or a remote executable)
are not implemented, even though the button is enabled in the GUI.

### Build ###

#### Preliminairies  ####
You need to have some software installed before we begin:

* [git](https://git-scm.com/) 
* [CMake](https://cmake.org/download/)
* [Visual Studio 2017 Community](https://www.visualstudio.com/downloads/)

#### Qt ####
Qt is required by both VTK and our FastMarchingTool, we simply need to download and install it. 
Somehow they made it difficult to find the free version containing all the libraries on their website.
Mae sure you select the download containing all the libraries.

* [Qt Offline Installer](https://www1.qt.io/download-open-source/#section-2)

#### VTK ####
Before we can build the Fast Marching Tool we need to build VTK. Pre-build versions are around although we want to
make some specific setting and therefor I recommend building it yourself.

1. Clone the VTK Repository 
2. Checkout Tag v8.0.1 (!)
2. Configure your Build using CMake
3. x64 Build VTK (if you want to build the Release version of the Fast Marching Tool, also build VTK in release mode)

* [VTK Repositories](https://github.com/Kitware/VTK)

*CMAKE Configuration*

* Set 'BUILD_EXAMPLES' to false
* Set 'VTK_QT_VERSION' to 5
* Set 'VTK_RENDERING_BACKEND' to 'OpenGL2'
* Set 'VTK_Group_Qt' to true
* Set 'Module_vtkGUISupportQt' to true
* Set 'Module_vtkGUISupportOpenGL' to true
* Set 'Module_vtkDICOM' to true

#### Fast Marching Tool ####
Now that all our dependencies are set up we can build our tool.

1. Clone the Repository
2. Configure Build using CMake
3. x64 Build FastMarchingTool

Update: you need https://www.libssh.org/ installed

*CMAKE Configuration*
Make sure CMAKE can find the Qt directories are found and that the 'VTK_DIR' is set.

### Create Installer ###
Creating an installer is usefull when distributing the tool over multiple machines. 
In this way they do not have to repeat the steps mentioned above.
Basically we need to do the following:

1. Create a folder containing the .exe file
2. Include dependant VTK DLL's 
3. Include dependant QMAKE files

The most easy way to achieve this is using the tool created for Visual Studio.

* [Create Installer Tool](https://marketplace.visualstudio.com/items?itemName=VisualStudioProductTeam.MicrosoftVisualStudio2017InstallerProjects)

You can follow this tutorial, which is somewhat whacky but will explain how it is done [YouTube](https://www.youtube.com/watch?v=HeDBYc3ybxc).
Just make sure you include all DLL's (that belong to your compiler) to the application folder, in my case these folders would be:

```
C:\_Qt\Qt5.9.3\5.9.3\msvc2017_64\bin
C:\_Qt\Qt5.9.3\5.9.3\msvc2017_64\plugins\
D:\Builds\VTK\bin\Release
```

Note that you have to select either the Release or Debug DLL from VTK depending if you are building a Debug or Release version of the Fast Marching Tool.

### Who do I talk to? ###

* This repository is maintained by Stephan Meesters <s.p.l.meesters@tue.nl>







