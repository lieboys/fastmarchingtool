#pragma once

/** Includes - Main Headers */
#include "common/Macros.h"

/** Includes - Qt */

/** Includes - VTK */
#include <vtkSmartPointer.h>

class DataObject;

/* Class for all the connectivity info, is able to calculate the connectivity between regions. */
class ConnectivityMeasure {
public:

	/**
	 * Constructor
	 * @param
	 */
	ConnectivityMeasure();

	/**
	 * Destructor
	 */
	~ConnectivityMeasure();

	double Compute(DataObject* regionA,
				   DataObject* regionB,
				   DataObject* distancemap,
				   DataObject* distancemapEuclidean);

private:
};
