#include "CodeLinkManager.h"

#include "database/DataIOManager.h"

#include <QSettings>
#include <QDebug>
#include <QThread>
#include <QDir>
#include <QApplication>

#include <vtkImageMathematics.h>
#include <vtkImageThreshold.h>

#include "gui/SettingsDialog.h"

//---------------[ LibSSH functions ]-------------------

#ifndef _MSC_VER
namespace SSHMODULE
{
    int authenticate_kbdint(ssh_session session, const char *password) {
        int err;

        err = ssh_userauth_kbdint(session, NULL, NULL);
        while (err == SSH_AUTH_INFO) {
            const char *instruction;
            const char *name;
            char buffer[128];
            int i, n;

            name = ssh_userauth_kbdint_getname(session);
            instruction = ssh_userauth_kbdint_getinstruction(session);
            n = ssh_userauth_kbdint_getnprompts(session);

            if (name && strlen(name) > 0) {
                printf("%s\n", name);
            }

            if (instruction && strlen(instruction) > 0) {
                printf("%s\n", instruction);
            }

            for (i = 0; i < n; i++) {
                const char *answer;
                const char *prompt;
                char echo;

                prompt = ssh_userauth_kbdint_getprompt(session, i, &echo);
                if (prompt == NULL) {
                    break;
                }

                if (echo) {
                    char *p;

                    printf("%s", prompt);

                    if (fgets(buffer, sizeof(buffer), stdin) == NULL) {
                        return SSH_AUTH_ERROR;
                    }

                    buffer[sizeof(buffer) - 1] = '\0';
                    if ((p = strchr(buffer, '\n'))) {
                        *p = '\0';
                    }

                    if (ssh_userauth_kbdint_setanswer(session, i, buffer) < 0) {
                        return SSH_AUTH_ERROR;
                    }

                    memset(buffer, 0, strlen(buffer));
                } else {
                    if (password && strstr(prompt, "Password:")) {
                        answer = password;
                    } else {
                        buffer[0] = '\0';

                        if (ssh_getpass(prompt, buffer, sizeof(buffer), 0, 0) < 0) {
                            return SSH_AUTH_ERROR;
                        }
                        answer = buffer;
                    }
                    if (ssh_userauth_kbdint_setanswer(session, i, answer) < 0) {
                        return SSH_AUTH_ERROR;
                    }
                }
            }
            err=ssh_userauth_kbdint(session,NULL,NULL);
        }

        return err;
    }

    static void error(ssh_session session){
        fprintf(stderr,"Authentication failed: %s\n",ssh_get_error(session));
    }

    int authenticate_console(ssh_session session, char *pass){
        int rc;
        int method;
        char password[128] = {0};
        char *banner;

        // Try to authenticate
        rc = ssh_userauth_none(session, NULL);
        if (rc == SSH_AUTH_ERROR) {
            error(session);
            return rc;
        }

        method = ssh_auth_list(session);
        while (rc != SSH_AUTH_SUCCESS) {
            // Try to authenticate with public key first
//            if (method & SSH_AUTH_METHOD_PUBLICKEY) {
//                rc = ssh_userauth_autopubkey(session, NULL);
//                if (rc == SSH_AUTH_ERROR) {
//                    error(session);
//                    return rc;
//                } else if (rc == SSH_AUTH_SUCCESS) {
//                    break;
//                }
//            }
//
//            // Try to authenticate with keyboard interactive";
//            if (method & SSH_AUTH_METHOD_INTERACTIVE) {
//                rc = authenticate_kbdint(session, NULL);
//                if (rc == SSH_AUTH_ERROR) {
//                    error(session);
//                    return rc;
//                } else if (rc == SSH_AUTH_SUCCESS) {
//                    break;
//                }
//            }
//
//            if (ssh_getpass("Password: ", password, sizeof(password), 0, 0) < 0) {
//                return SSH_AUTH_ERROR;
//            }

            // Try to authenticate with password
//            if (method & SSH_AUTH_METHOD_PASSWORD) {
                rc = ssh_userauth_password(session, NULL, pass);
                if (rc == SSH_AUTH_ERROR) {
                    error(session);
                    return rc;
                } else if (rc == SSH_AUTH_SUCCESS) {
                    break;
                }
//            }
        }

        banner = ssh_get_issue_banner(session);
        if (banner) {
            printf("%s\n",banner);
            free(banner);
        }

        return rc;
    }

    int verify_knownhost(ssh_session session)
    {
        int state, hlen;
        unsigned char *hash = NULL;
        char *hexa;
        char buf[10];
        state = ssh_is_server_known(session);
        hlen = ssh_get_pubkey_hash(session, &hash);
        if (hlen < 0)
            return -1;
        switch (state)
        {
            case SSH_SERVER_KNOWN_OK:
                break; /* ok */
            case SSH_SERVER_KNOWN_CHANGED:
                fprintf(stderr, "Host key for server changed: it is now:\n");
                ssh_print_hexa("Public key hash", hash, hlen);
                fprintf(stderr, "For security reasons, connection will be stopped\n");
                free(hash);
                return -1;
            case SSH_SERVER_FOUND_OTHER:
                fprintf(stderr, "The host key for this server was not found but an other"
                        "type of key exists.\n");
                fprintf(stderr, "An attacker might change the default server key to"
                        "confuse your client into thinking the key does not exist\n");
                free(hash);
                return -1;
            case SSH_SERVER_FILE_NOT_FOUND:
                fprintf(stderr, "Could not find known host file.\n");
                fprintf(stderr, "If you accept the host key here, the file will be"
                        "automatically created.\n");
                /* fallback to SSH_SERVER_NOT_KNOWN behavior */
            case SSH_SERVER_NOT_KNOWN:
                hexa = ssh_get_hexa(hash, hlen);
                fprintf(stderr,"The server is unknown. Do you trust the host key?\n");
                fprintf(stderr, "Public key hash: %s\n", hexa);
                free(hexa);
                if (fgets(buf, sizeof(buf), stdin) == NULL)
                {
                    free(hash);
                    return -1;
                }
                if (strncasecmp(buf, "yes", 3) != 0)
                {
                    free(hash);
                    return -1;
                }
                if (ssh_write_knownhost(session) < 0)
                {
                    fprintf(stderr, "Error %s\n", strerror(errno));
                    free(hash);
                    return -1;
                }
                break;
            case SSH_SERVER_ERROR:
                fprintf(stderr, "Error %s", ssh_get_error(session));
                free(hash);
                return -1;
        }
        free(hash);
        return 0;
    }

    ssh_session connect_ssh(const char *host, const char *user, int verbosity, char* pass){
        ssh_session session;
        int auth=0;

        session=ssh_new();
        if (session == NULL) {
            return NULL;
        }

        if(user != NULL){
            if (ssh_options_set(session, SSH_OPTIONS_USER, user) < 0) {
                ssh_disconnect(session);
                return NULL;
            }
        }

        if (ssh_options_set(session, SSH_OPTIONS_HOST, host) < 0) {
            return NULL;
        }
        ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
        if(ssh_connect(session)){
            fprintf(stderr,"Connection failed : %s\n",ssh_get_error(session));
            ssh_disconnect(session);
            return NULL;
        }
        if(verify_knownhost(session)<0){
            ssh_disconnect(session);
            return NULL;
        }
        auth=authenticate_console(session, pass);
        if(auth==SSH_AUTH_SUCCESS){
            return session;
        } else if(auth==SSH_AUTH_DENIED){
            fprintf(stderr,"Authentication failed\n");
        } else {
            fprintf(stderr,"Error while authenticating : %s\n",ssh_get_error(session));
        }
        ssh_disconnect(session);
        return NULL;
    }

    static struct location *parse_location(char *loc){
        struct location *location= static_cast<struct location*>(malloc(sizeof(struct location)));
        char *ptr;

        location->host=location->user=NULL;
        ptr=strchr(loc,':');
        if(ptr != NULL){
            location->is_ssh=1;
            location->path=strdup(ptr+1);
            *ptr='\0';
            ptr=strchr(loc,'@');
            if(ptr != NULL){
                location->host=strdup(ptr+1);
                *ptr='\0';
                location->user=strdup(loc);
            } else {
                location->host=strdup(loc);
            }
        } else {
            location->is_ssh=0;
            location->path=strdup(loc);
        }
        return location;
    }

    static int open_location(struct location *loc, int flag, int verbosity)
    {
        if(loc->is_ssh && flag==WRITE){
            loc->session=connect_ssh(loc->host,loc->user,verbosity, loc->pass);
            if(!loc->session){
                fprintf(stderr,"Couldn't connect to %s\n",loc->host);
                return -1;
            }
            loc->scp=ssh_scp_new(loc->session,SSH_SCP_WRITE,loc->path);
            if(!loc->scp){
                fprintf(stderr,"error 1 : %s\n",ssh_get_error(loc->session));
                return -1;
            }
            if(ssh_scp_init(loc->scp)==SSH_ERROR){
                fprintf(stderr,"error 2: %s\n",ssh_get_error(loc->session));
                ssh_scp_free(loc->scp);
                return -1;
            }
            return 0;
        } else if(loc->is_ssh && flag==READ){
            loc->session=connect_ssh(loc->host, loc->user, verbosity, loc->pass);
            if(!loc->session){
                fprintf(stderr,"Couldn't connect to %s\n",loc->host);
                return -1;
            }
            loc->scp=ssh_scp_new(loc->session,SSH_SCP_READ,loc->path);
            if(!loc->scp){
                fprintf(stderr,"error 3: %s\n",ssh_get_error(loc->session));
                return -1;
            }
            if(ssh_scp_init(loc->scp)==SSH_ERROR){
                fprintf(stderr,"error 4: %s\n",ssh_get_error(loc->session));
                ssh_scp_free(loc->scp);
                return -1;
            }
            return 0;
        } else {
            loc->file=fopen(loc->path,flag==READ ? "r":"w");
            if(!loc->file){
                if(errno==EISDIR){
                    if(chdir(loc->path)){
                        fprintf(stderr,"Error changing directory to %s: %s\n",loc->path,strerror(errno));
                        return -1;
                    }
                    return 0;
                }
                fprintf(stderr,"Error opening %s: %s\n",loc->path,strerror(errno));
                return -1;
            }
            return 0;
        }
        return -1;
    }

    /** @brief copies files from source location to destination
     * @param src source location
     * @param dest destination location
     * @param recursive Copy also directories
     */
    int do_copy(struct location *src, struct location *dest, int recursive){
        int size;
        socket_t fd;
        struct stat s;
        int w,r;
        char buffer[16384];
        int total=0;
        int mode;
        char *filename;
        /* recursive mode doesn't work yet */
        (void)recursive;
        /* Get the file name and size*/
        if(!src->is_ssh){
            fd=fileno(src->file);
            fstat(fd,&s);
            size=s.st_size;
            mode = s.st_mode & ~S_IFMT;
            filename=ssh_basename(src->path);
        } else {
            size=0;
            do {
                r=ssh_scp_pull_request(src->scp);
                if(r==SSH_SCP_REQUEST_NEWDIR){
                    ssh_scp_deny_request(src->scp,"Not in recursive mode");
                    continue;
                }
                if(r==SSH_SCP_REQUEST_NEWFILE){
                    size=ssh_scp_request_get_size(src->scp);
                    filename=strdup(ssh_scp_request_get_filename(src->scp));
                    mode=ssh_scp_request_get_permissions(src->scp);
                    //ssh_scp_accept_request(src->scp);
                    break;
                }
                if(r==SSH_ERROR){
                    fprintf(stderr,"Error: %s\n",ssh_get_error(src->session));
                    return -1;
                }
            } while(r != SSH_SCP_REQUEST_NEWFILE);
        }

        if(dest->is_ssh){
            r=ssh_scp_push_file(dest->scp,src->path, size, mode);
            snprintf(buffer,sizeof(buffer),"C0644 %d %s\n",size,src->path);
            if(r==SSH_ERROR){
                fprintf(stderr,"error 5: %s\n",ssh_get_error(dest->session));
                ssh_scp_free(dest->scp);
                return -1;
            }
        } else {
            if(!dest->file){
                dest->file=fopen(filename,"w");
                if(!dest->file){
                    fprintf(stderr,"Cannot open %s for writing: %s\n",filename,strerror(errno));
                    if(src->is_ssh)
                        ssh_scp_deny_request(src->scp,"Cannot open local file");
                    return -1;
                }
            }
            if(src->is_ssh){
                ssh_scp_accept_request(src->scp);
            }
        }
        do {
            if(src->is_ssh){
                r=ssh_scp_read(src->scp,buffer,sizeof(buffer));
                if(r==SSH_ERROR){
                    fprintf(stderr,"Error reading scp: %s\n",ssh_get_error(src->session));
                    return -1;
                }
                if(r==0)
                    break;
            } else {
                r=fread(buffer,1,sizeof(buffer),src->file);
                if(r==0)
                    break;
                if(r<0){
                    fprintf(stderr,"Error reading file: %s\n",strerror(errno));
                    return -1;
                }
            }
            if(dest->is_ssh){
                w=ssh_scp_write(dest->scp,buffer,r);
                if(w == SSH_ERROR){
                    fprintf(stderr,"Error writing in scp: %s\n",ssh_get_error(dest->session));
                    ssh_scp_free(dest->scp);
                    dest->scp=NULL;
                    return -1;
                }
            } else {
                w=fwrite(buffer,r,1,dest->file);
                if(w<=0){
                    fprintf(stderr,"Error writing in local file: %s\n",strerror(errno));
                    return -1;
                }
            }
            total+=r;

        } while(total < size);
        printf("wrote %d bytes\n",total);
        return 0;
    }

}
#endif

/*** MAIN SSH ****/
#ifndef _MSC_VER
int CodeLinkManager::run_ssh()
{
    using namespace SSHMODULE;
    
    /*** COPY FM FILES TO THE REMOTE SERVER ***/
    
    emit valueChanged(0);
    emit stringValueChanged(QString("Copying files to remote server"));
    
    // prepare paths to input source files (binary file and header)
    char **sources;
    int nsources = 2;
    sources=static_cast<char**>(malloc((nsources + 1) * sizeof(char *)));
    if(sources == NULL)
        return SSH_ERROR;
    
    // path to header input file, formatted user@server:path
    sources[0] = strdup(this->m_settings.inputHeaderPath.toStdString().c_str());
    
    // path to binary input file, formatted user@server:path
    sources[1] = strdup(this->m_settings.inputBinaryPath.toStdString().c_str());
    
    // destination of file to transfer, either remote of local
    char *destination = strdup(QString("%1@%2:%3").arg(
                      this->m_settings.remoteSettings.username,
                      this->m_settings.remoteSettings.serverPath,
                      this->m_settings.remoteSettings.dataDirectory).toStdString().c_str());
    
    // verbose mode
    int verbosity = 0;
    
    struct location *dest, *src;
    int i;
    int r;
    dest=parse_location(destination);
    dest->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
    if(open_location(dest,WRITE,verbosity)<0)
        return SSH_ERROR;
    
    // connect to ssh, copy files with scp
    for(i=0;i<nsources;++i)
    {
        src=parse_location(sources[i]);
//        src->user = user;
        src->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
//        src->host = host;
        if(open_location(src,READ,verbosity)<0)
            return SSH_ERROR;
        if(do_copy(src,dest,0) < 0)
            break;
        
        if(i==1)
            emit valueChanged(50);
    }
    
    // close ssh
    if(dest->is_ssh){
        r=ssh_scp_close(dest->scp);
        if(r == SSH_ERROR){
            fprintf(stderr,"Error closing scp: %s\n",ssh_get_error(dest->session));
            ssh_scp_free(dest->scp);
            dest->scp=NULL;
            return SSH_ERROR;
        }
    } else {
        fclose(dest->file);
        dest->file=NULL;
    }
    ssh_disconnect(dest->session);
    ssh_finalize();
    
    emit valueChanged(0);
    emit stringValueChanged(QString("Running FM on remote server"));
    
    /*** RUN FM CODE WITH SSH COMMAND ***/
    
    ssh_session my_ssh_session;
    int rc;
    // Open session and set options
    my_ssh_session = ssh_new();
    if (my_ssh_session == NULL)
        return SSH_ERROR;
    ssh_options_set(my_ssh_session,
                    SSH_OPTIONS_HOST,
                    this->m_settings.remoteSettings.serverPath.toStdString().c_str());
    // Connect to server
    rc = ssh_connect(my_ssh_session);
    if (rc != SSH_OK)
    {
        fprintf(stderr, "Error connecting to localhost: %s\n",
                ssh_get_error(my_ssh_session));
        ssh_free(my_ssh_session);
        return SSH_ERROR;
    }
    // Verify the server's identity
    // For the source code of verify_knowhost(), check previous example
    if (verify_knownhost(my_ssh_session) < 0)
    {
        ssh_disconnect(my_ssh_session);
        ssh_free(my_ssh_session);
        return SSH_ERROR;
    }
    // Authenticate ourselves
    rc = ssh_userauth_password(my_ssh_session,
                               this->m_settings.remoteSettings.username.toStdString().c_str(),
                               this->m_settings.remoteSettings.password.toStdString().c_str());
    if (rc != SSH_AUTH_SUCCESS)
    {
        fprintf(stderr, "Error authenticating with password: %s\n",
                ssh_get_error(my_ssh_session));
        ssh_disconnect(my_ssh_session);
        ssh_free(my_ssh_session);
        return SSH_ERROR;
    }
    
    // run FM code

    ssh_channel channel;
    char buffer[64];
    int nbytes;

    channel = ssh_channel_new(my_ssh_session);
    if (channel == NULL)
        return SSH_ERROR;
    rc = ssh_channel_open_session(channel);
    if (rc != SSH_OK)
    {
        ssh_channel_free(channel);
        return SSH_ERROR;
    }
    
//    rc = ssh_channel_request_shell(channel);
//    if (rc != SSH_OK)
//    {
//        ssh_channel_free(channel);
//        return SSH_ERROR;
//    }
    
    rc = ssh_channel_request_exec(channel, m_sshCommand.toStdString().c_str());
    if (rc != SSH_OK)
    {
        fprintf(stderr, "error: %s\n",
                ssh_get_error(my_ssh_session));
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return SSH_ERROR;
    }
    nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    while (nbytes > 0)
    {
        if (write(1, buffer, nbytes) != (unsigned int) nbytes)
        {
            fprintf(stderr, "error: %s\n",
                    ssh_get_error(my_ssh_session));
            ssh_channel_close(channel);
            ssh_channel_free(channel);
            return SSH_ERROR;
        }
        
        // search for last percentage sign in buffer
        char * pch;
        char percentage[2];
        pch=strrchr(buffer,'%');
        if(pch != nullptr)
        {
            memcpy(percentage, pch-2, 2*sizeof(char));
            emit valueChanged((int)atoi(percentage));
        }
        
        nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    }

    if (nbytes < 0)
    {
        fprintf(stderr, "error: %s\n",
                ssh_get_error(my_ssh_session));
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return SSH_ERROR;
    }

    ssh_disconnect(my_ssh_session);
    ssh_free(my_ssh_session);
    
    
    
    
    /*** COPY THE RESULT FILES FROM THE REMOTE SERVER TO LOCAL ***/
    
    emit valueChanged(0);
    emit stringValueChanged(QString("Copy files from remote to local"));
    
    nsources = 1;
    
    // path to header input file, formatted user@server:path
    sources[0] = strdup(QString("%1@%2:%3").arg(
                    this->m_settings.remoteSettings.username,
                    this->m_settings.remoteSettings.serverPath,
                    this->m_settings.remoteSettings.outputHeaderPath).toStdString().c_str());

    // destination of file to transfer, either remote of local
    destination = strdup(this->m_settings.workfolder.toStdString().c_str());
    
    verbosity = 0;
    
    dest=parse_location(destination);
    dest->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
    if(open_location(dest,WRITE,verbosity)<0)
        return SSH_ERROR;
    
    // connect to ssh, copy files with scp
    for(i=0;i<nsources;++i)
    {
        src=parse_location(sources[i]);
        //        src->user = user;
        src->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
        //        src->host = host;
        if(open_location(src,READ,verbosity)<0)
            return SSH_ERROR;
        if(do_copy(src,dest,0) < 0)
            break;
    }
    
    // close ssh
    if(dest->is_ssh){
        r=ssh_scp_close(dest->scp);
        if(r == SSH_ERROR){
            fprintf(stderr,"Error closing scp: %s\n",ssh_get_error(dest->session));
            ssh_scp_free(dest->scp);
            dest->scp=NULL;
            return SSH_ERROR;
        }
        ssh_disconnect(dest->session);
    } else {
        fclose(dest->file);
        dest->file=NULL;
        ssh_disconnect(src->session);
    }
    ssh_finalize();
    
    
    emit valueChanged(50);
    
    
    
    
    
    // path to binary input file, formatted user@server:path
    sources[0] = strdup(QString("%1@%2:%3").arg(
                                                this->m_settings.remoteSettings.username,
                                                this->m_settings.remoteSettings.serverPath,
                                                this->m_settings.remoteSettings.outputBinaryPath).toStdString().c_str());
    
    // destination of file to transfer, either remote of local
    destination = strdup(this->m_settings.workfolder.toStdString().c_str());
    
    verbosity = 0;
    
    dest=parse_location(destination);
    dest->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
    if(open_location(dest,WRITE,verbosity)<0)
        return SSH_ERROR;
    
    // connect to ssh, copy files with scp
    for(i=0;i<nsources;++i)
    {
        src=parse_location(sources[i]);
        //        src->user = user;
        src->pass = strdup(this->m_settings.remoteSettings.password.toStdString().c_str());
        //        src->host = host;
        if(open_location(src,READ,verbosity)<0)
            return SSH_ERROR;
        if(do_copy(src,dest,0) < 0)
            break;
    }
    
    // close ssh
    if(dest->is_ssh){
        r=ssh_scp_close(dest->scp);
        if(r == SSH_ERROR){
            fprintf(stderr,"Error closing scp: %s\n",ssh_get_error(dest->session));
            ssh_scp_free(dest->scp);
            dest->scp=NULL;
            return SSH_ERROR;
        }
        ssh_disconnect(dest->session);
    } else {
        fclose(dest->file);
        dest->file=NULL;
        ssh_disconnect(src->session);
    }
    ssh_finalize();
    
    emit valueChanged(100);
    
    
    
    return SSH_OK;
}
#else
int CodeLinkManager::run_ssh()
{
	emit valueChanged(10.0);

	QSettings m_settings;
	QString puttyPath = m_settings.value(SettingsDialog::SETTINGS_CODE_PUTTY_DIRECTORY).toString();

	// COPY INPUT FILES TO REMOTE

	QString headerPath = this->m_settings.inputHeaderPath;
	QString dataPath = this->m_settings.inputBinaryPath;

	QString scpCommand = QString("%1@%2:\"%3\"").arg(
		this->m_settings.remoteSettings.username,
		this->m_settings.remoteSettings.serverPath,
		this->m_settings.remoteSettings.dataDirectory);

	QString cmd = "";
	cmd += "\"" + puttyPath +
		+ "/pscp.exe" + "\""
		+ " -scp" 
		+ " -pw \"" + this->m_settings.remoteSettings.password + "\""
		+ " \"" + headerPath + "\""
		+ " " + scpCommand;

	QString cmd2 = "";
	cmd2 += "\"" + puttyPath +
		+"/pscp.exe" + "\""
		+ " -scp"
		+ " -pw \"" + this->m_settings.remoteSettings.password + "\""
		+ " \"" + dataPath + "\""
		+ " " + scpCommand;

	std::cout << cmd.toStdString() << std::endl;

	if (this->m_process != nullptr) {
		delete this->m_process;
		this->m_process = nullptr;
	}
	this->m_process = new QProcess(this);

	connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
	connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

	QDir::setCurrent(QDir::toNativeSeparators(this->m_settings.workfolder));
	this->m_process->start(cmd);
	this->m_process->waitForFinished(-1);
	
	delete this->m_process;
	this->m_process = nullptr;
	this->m_process = new QProcess(this);
	connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
	connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

	this->m_process->start(cmd2);
	this->m_process->waitForFinished(-1);

	// RUN SSH COMMAND

	QString loginCommand = QString("%1@%2").arg(
		this->m_settings.remoteSettings.username,
		this->m_settings.remoteSettings.serverPath);

	QString cmd3 = "";
	cmd3 += "\"" + puttyPath +
		+ "/plink.exe" + "\""
		+ " " + loginCommand
		+ " -pw \"" + this->m_settings.remoteSettings.password + "\""
		+ " " + m_sshCommand;

	delete this->m_process;
	this->m_process = nullptr;
	this->m_process = new QProcess(this);
	connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
	connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

	this->m_process->start(cmd3);
	this->m_process->waitForFinished(-1);

	// COPY FILES FROM REMOTE TO LOCAL

	QString scpCommand2 = QString("%1@%2:").arg(
		this->m_settings.remoteSettings.username,
		this->m_settings.remoteSettings.serverPath);

	QString cmd4 = "";
	cmd4 += "\"" + puttyPath +
		+"/pscp.exe" + "\""
		+ " -scp"
		+ " -pw \"" + this->m_settings.remoteSettings.password + "\""
		+ " " + scpCommand2
		+ "\"" + this->m_settings.remoteSettings.outputHeaderPath + "\""
		+ " \"" + this->m_settings.workfolder + "\"";

	QString cmd5 = "";
	cmd5 += "\"" + puttyPath +
		+"/pscp.exe" + "\""
		+ " -scp"
		+ " -pw \"" + this->m_settings.remoteSettings.password + "\""
		+ " " + scpCommand2
		+ "\"" + this->m_settings.remoteSettings.outputBinaryPath + "\""
		+ " \"" + this->m_settings.workfolder + "\"";

	delete this->m_process;
	this->m_process = nullptr;
	this->m_process = nullptr;
	this->m_process = new QProcess(this);
	connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
	connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

	this->m_process->start(cmd4);
	this->m_process->waitForFinished(-1);

	delete this->m_process;
	this->m_process = nullptr;
	this->m_process = new QProcess(this);
	connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
	connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

	this->m_process->start(cmd5);
	this->m_process->waitForFinished(-1);

	// finish

	QDir::setCurrent(QApplication::applicationDirPath());

	emit valueChanged(80);
	if (this->m_abort) {
		return 0;
	}

	return 1;
}
#endif


//---------------[ Constructor ]-------------------
CodeLinkManager::CodeLinkManager(QObject *parent) : QObject(parent){

	this->m_ready = false;
	this->m_working = false;
	this->m_abort = false;
	this->m_forced = false;

	this->m_error = "";

	this->m_process = nullptr;
}

bool CodeLinkManager::SetupCodeRun(QString runOption,
                                   CodeRunSettings m_settings,
								   SP(vtkImageData) speedSource,
								   SP(vtkImageData) seed,
								   SP(vtkImageData) tip) {

	this->m_settings = m_settings;
	this->m_error = "";
    this->runOption = runOption;

	QList<QPair<QString, QString>> textualData;
	QList<QPair<QString, std::vector<double>>> scalarData;
	QList<QPair<QString, double>> valueData;
	QList<QPair<QString, SP(vtkPolyData)>> polyData;
	QList<QPair<QString, SP(vtkImageData)>> volumeData;
    
    
    // apply transformation to FOD (speed source)
    vtkSmartPointer<vtkImageMathematics> imageMath8;
    if(m_settings.params.applyFODTransformation)
    {
        // get maximum value
        double* valuesRange = speedSource->GetScalarRange();
        if(valuesRange[1] <= 0.)
        {
            std::cout << "CodeLinkManager error: maximum of speed dataset is negative." << std::endl;
            //            return data;
        }
        
        // clip negative values
        vtkSmartPointer<vtkImageThreshold> imageThreshold =
        vtkSmartPointer<vtkImageThreshold>::New();
        imageThreshold->SetInputData(speedSource);
        imageThreshold->ThresholdByLower(0.0);
        imageThreshold->ReplaceInOn();
        imageThreshold->SetInValue(0.0);
        imageThreshold->Update();
        
        // W / ||W+||_inf
        vtkSmartPointer<vtkImageMathematics> imageMath1 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath1->SetOperationToMultiplyByK();
        imageMath1->SetConstantK(1/valuesRange[1]);
        imageMath1->SetInputData(imageThreshold->GetOutput());
        
        // | W / ||W+||_inf |
        vtkSmartPointer<vtkImageMathematics> imageMath2 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath2->SetOperationToAbsoluteValue();
        imageMath2->SetInputConnection(imageMath1->GetOutputPort());
        
        // | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath3 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath3->SetOperationToLog();
        imageMath3->SetInputConnection(imageMath2->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath4 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath4->SetOperationToMultiplyByK();
        imageMath4->SetConstantK(m_settings.params.p);
        imageMath4->SetInputConnection(imageMath3->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath5 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath5->SetOperationToExp();
        imageMath5->SetInputConnection(imageMath4->GetOutputPort());
        
        // sigma | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath6 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath6->SetOperationToMultiplyByK();
        imageMath6->SetConstantK(m_settings.params.sigma);
        imageMath6->SetInputConnection(imageMath5->GetOutputPort());
        
        // 1 + sigma | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath7 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath7->SetOperationToAddConstant();
        imageMath7->SetConstantC(1.0);
        imageMath7->SetInputConnection(imageMath6->GetOutputPort());
        
        // 1 / (1 + sigma | W / ||W+||_inf |^p)
        imageMath8 = vtkSmartPointer<vtkImageMathematics>::New();
        imageMath8->SetOperationToInvert();
        imageMath8->SetInputConnection(imageMath7->GetOutputPort());
        imageMath8->Update();
    }
    
	// Default values
    textualData << QPair<QString, QString>(BinairDefinitions::PARAM_EXPERIMENT_NAME, m_settings.name);
	textualData << QPair<QString, QString>(BinairDefinitions::PARAM_TYPE, m_settings.params.bundleType);
	textualData << QPair<QString, QString>(BinairDefinitions::PARAM_MESH, m_settings.params.bundleMesh);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_GRID_SCALE,  m_settings.params.gridScale);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_EXPORT_VAL_LIFT,  m_settings.params.exportValuesLift);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_EXPORT_VAL_EUCL,  m_settings.params.exportValuesEucl);
    valueData << QPair<QString, double>(BinairDefinitions::PARAM_APPLY_FOD_TRANSFORMATION,  m_settings.params.applyFODTransformation);
    valueData << QPair<QString, double>(BinairDefinitions::PARAM_TRANSPOSE,  m_settings.params.transposeCoordinates);
    valueData << QPair<QString, double>(BinairDefinitions::PARAM_STOPTIPSREACHED,  m_settings.params.stopWhenTipsReached==true?1:0);

	//// Add parameters
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_EPS, m_settings.params.epsilon);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_XI, m_settings.params.xi);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_SIGMA, m_settings.params.sigma);
	valueData << QPair<QString, double>(BinairDefinitions::PARAM_P, m_settings.params.p);

	valueData << QPair<QString, double>(BinairDefinitions::PARAM_OFFWAY_PEN, pow(m_settings.params.epsilon, -1.0));

	// Save physical dimensions
	std::vector<double> physDims = {
		(double) speedSource->GetDimensions()[0],
		(double) speedSource->GetDimensions()[1],
		(double) speedSource->GetDimensions()[2]
    };

	scalarData << QPair<QString, std::vector<double>>(BinairDefinitions::LIST_LABEL_PHYSDIMS, physDims);
	
	// Save speed map (requires DWI data, p and sigma)
    if(m_settings.params.applyFODTransformation)
        volumeData << QPair<QString, SP(vtkImageData)>("speed", imageMath8->GetOutput());
    else
        volumeData << QPair<QString, SP(vtkImageData)>("speed", speedSource);

	// Optional: save tips and seeds (as list of 6D points)
	if (tip) {
		polyData << QPair<QString, SP(vtkPolyData)>("tips", DataIOManager::MaskToPointData(tip));
	}

	if (seed) {
		polyData << QPair<QString, SP(vtkPolyData)>("seeds", DataIOManager::MaskToPointData(seed));
	}

	// Write binary and header
	DataIOManager::FullWriteBinary(m_settings.inputHeaderPath, 
								   m_settings.inputBinaryPath,
								   textualData,
								   valueData,
								   scalarData,
								   polyData,
								   volumeData);

	if (!QFile(m_settings.inputHeaderPath).exists()) {
		return false;
	}
	if (!QFile(m_settings.inputBinaryPath).exists()) {
		return false;
	}

	this->m_ready = true;

	return true;
}

void CodeLinkManager::SetCommandPath(QString codeCommand) {

	if (QFile(codeCommand).exists()) {
		this->m_codeCommand = codeCommand;
	}
	else {
		std::cout << "Executable not found: '" + codeCommand.toStdString() + "'" << std::endl;
	}
}

void CodeLinkManager::SetRemoteSSHCommand(QString codeCommand) {
    
    this->m_sshCommand = codeCommand;
}

void CodeLinkManager::RequestWork() {

	this->m_mutex.lock();
	this->m_working = true;
	this->m_abort = false;
	qDebug() << "Request worker start in Thread " << thread()->currentThreadId();
	this->m_mutex.unlock();

	emit workRequested();
}

void CodeLinkManager::Abort() {

	this->m_mutex.lock();
	if (this->m_working) {
		this->m_abort = true;
		qDebug() << "Request worker aborting in Thread " << thread()->currentThreadId();
	}

	this->m_mutex.unlock();

	if (!this->m_working) {
		return;
	}

	if (!this->m_forced && this->m_process != nullptr) {

		this->m_process->kill();
		this->m_error = "";
	}
}

void CodeLinkManager::doWork() {

    if(this->runOption == "LOCAL")
    {
        emit valueChanged(10.0);
        
        QString cmd = "";
        cmd += "\"" + m_codeCommand + "\""
            + " \"" + this->m_settings.name + "_input\""
            + " \"" + this->m_settings.name + "_output\"";

        std::cout << cmd.toStdString() << std::endl;

        if (this->m_process != nullptr) {
            delete this->m_process;
            this->m_process = nullptr;
        }
        this->m_process = new QProcess(this);

        connect(this->m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(slotPrintLog()));
        connect(this->m_process, SIGNAL(readyReadStandardError()), this, SLOT(slotPrintError()));

        QDir::setCurrent(QDir::toNativeSeparators(this->m_settings.workfolder));
        this->m_process->start(cmd);
        this->m_process->waitForFinished(-1);

        QDir::setCurrent(QApplication::applicationDirPath());

        emit valueChanged(80);
        if (this->m_abort) {
            return;
        }

        emit finished();
    }
    
    else if(runOption == "REMOTE")
    {
        int ssh_output = run_ssh();

#ifndef _MSC_VER
        if(ssh_output == SSH_ERROR)
            return; // show error message
        else if(ssh_output == SSH_OK)
            emit finished();
#else
		if (ssh_output == 0)
			return; // show error message
		else if (ssh_output == 1)
			emit finished();
#endif
    }
    
    else
        emit finished();
}

void CodeLinkManager::slotPrintLog() {

	QString output;

	// Parse output text
	QByteArray byteArray = this->m_process->readAllStandardOutput();
	QStringList lines = QString(byteArray).split("\n");
	for (int i = 0; i < lines.size(); i++) {
		output += lines.at(i);
	}
	std::cout << output.toStdString() << std::endl;
	this->m_error += output + "\n";
}

void CodeLinkManager::slotPrintError() {

	QString output;

	QByteArray byteArray = this->m_process->readAllStandardError();
	QStringList lines = QString(byteArray).split("\n");

	for (int i = 0; i < lines.size(); i++) {
		output += lines.at(i) + "\n";
	}

	std::cout << output.toStdString() << std::endl;
	this->m_error += output + "\n";
}

QString CodeLinkManager::GetOutputMsg() {

	return this->m_error;
}
