#pragma once

/** Includes - Main Headers */
#include "common/Macros.h"

/** Includes - Qt */
#include <QProcess>
#include <QObject>
#include <QMutex>

/** Includes - VTK */
#include <vtkSmartPointer.h>
#include <vtkImageData.h>

/** Includes - LibSSH */
#ifndef _MSC_VER // not for windows!
#include <libssh/libssh.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#endif

class DataObject;

/*** LibSSH types ***/

#ifndef _MSC_VER
namespace SSHMODULE
{
    struct location {
        int is_ssh;
        char *user;
        char *pass;
        char *host;
        char *path;
        ssh_session session;
        ssh_scp scp;
        FILE *file;
    };

    enum {
        READ,
        WRITE
    };
}
#endif

/* Manager of the external LFM executable that runs in ints own thread. Contains the settings and function for running experiments.*/
class CodeLinkManager : public QObject {
	Q_OBJECT

public:
	/* Constructor */
	explicit CodeLinkManager(QObject *parent = 0);

	/* Parameters used during the CodeLink experiment. */
	struct CodeParameters {

		double epsilon;
		double xi;
		double sigma;
		double p;
		
		QString bundleType = "ReedSheppCar";
		QString bundleMesh = "icosahedron4";
		
		double gridScale = 1.;
		bool exportValuesLift = true;
		bool exportValuesEucl = true;
        
        bool applyFODTransformation = true;
        bool transposeCoordinates = true;
        bool stopWhenTipsReached = false;
	};
    
    /* Remote settings */
    struct RemoteSettings {
        
        QString serverPath;
        QString dataDirectory;
        QString username;
        QString password;
        
        QString inputHeaderPath;
        QString inputBinaryPath;
        QString outputHeaderPath;
        QString outputBinaryPath;
    };

	/* General settings for running the experiments. */
	struct CodeRunSettings {

		CodeParameters params;
        RemoteSettings remoteSettings;

		QString inputHeaderPath;
		QString inputBinaryPath;
		QString outputHeaderPath;
		QString outputBinaryPath;

		QString name;
		QString workfolder;

		QString seedName;
		QString tipName;

		bool importAfterwards;
	};
    
	/* Setup everything of the given experiment. */
	bool SetupCodeRun(QString runOption,
                      CodeRunSettings m_settings,
					  SP(vtkImageData) speedSource,
					  SP(vtkImageData) tip = nullptr,
					  SP(vtkImageData) seed = nullptr);

	/* Setup the command path for the LFM executable. */
	void SetCommandPath(QString codeCommand);
    
    /* Setup the remote SSH command. */
    void SetRemoteSSHCommand(QString codeCommand);

	/* Gets the currently set CodeSettings. */
	CodeLinkManager::CodeRunSettings GetSettings() {
		return this->m_settings;
	}

	/* Gets the error output from running the LFM code. */
	QString GetOutputMsg();

	/* Actual execute function for the LFM code */
	void RequestWork();

	/* Abort the exection of the currently running LFM code. */
	void Abort();

private:
    
    /*** LibSSH functions ***/
    int run_ssh();
    
    //////////////////////////////////

	CodeRunSettings m_settings;
	QString m_error;
    QString runOption;

	bool m_ready;
	bool m_working;		
	bool m_abort;		
	bool m_forced;		

	QMutex m_mutex;		
	QProcess* m_process;

	QString m_codeCommand;
    QString m_sshCommand;

signals:
	void workRequested();
	void valueChanged(const int &value);
    void stringValueChanged(QString);
	void finished();

public slots:

	void doWork();

	void slotPrintLog();
	void slotPrintError();


};

