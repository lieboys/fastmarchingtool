#include "ConnectivityMeasure.h"

//#include "DataObject.h"
#include "DistanceMapSource.h"
#include "RegionSource.h"

#include <vtkImageIterator.h>

//---------------[ Constructor ]-------------------

ConnectivityMeasure::ConnectivityMeasure() {
}

//---------------[ Destructor ]-------------------

ConnectivityMeasure::~ConnectivityMeasure() {
}

double ConnectivityMeasure::Compute(DataObject* regionA,
									DataObject* regionB,
									DataObject* distancemap,
									DataObject* distancemapEuclidean) {
	RegionSource* regionSourceA = dynamic_cast<RegionSource*>(regionA);
	RegionSource* regionSourceB = dynamic_cast<RegionSource*>(regionB);
	DistanceMapSource* distancemapSource = dynamic_cast<DistanceMapSource*>(distancemap);
	DistanceMapSource* distancemapSourceEuclidean = dynamic_cast<DistanceMapSource*>(distancemapEuclidean);

	SP(vtkImageData) regionSourceAdata = regionSourceA->data;

	//    regionSourceAdata->GetScalarPointer(x, y, z)[0] ==1)

	vtkImageIterator<double> it(regionSourceAdata, regionSourceAdata->GetExtent());
	while (!it.IsAtEnd()) {
		double* valIt = it.BeginSpan();
		double *valEnd = it.EndSpan();
		while (valIt != valEnd) {
			// Increment for each component
			double x = *valIt++;
			double y = *valIt++;
			double z = *valIt++;
			std::cout << "("
				<< x << ","
				<< y << ","
				<< z << ") ";
		}
		std::cout << std::endl;
		it.NextSpan();
	}

	return 0.;
}