#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

#include "viewcontrollers/GlyphViewController.h"
#include "viewcontrollers/ContourViewController.h"
#include "viewcontrollers/ScalarMapViewController.h"

#include "common/Macros.h"

#include "database/DataObject.h"
#include "database/DistanceMapSource.h"

class Controller;
class Model;
class Visualization;
class DataObject;

/* ViewController for the Reference visualization controls. */
class DistanceMapViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	DistanceMapViewController(Controller* control,
						 Model* model,
						 Visualization* vis);
	
	// Virtual from base
	void UpdateVisualsFromUi();
	void AddObject(QString label, bool visible);

		
	ContourViewController* contourController;
	GlyphViewController* glyphController;
	ScalarMapViewController* scalarController;

private:

	void setupControls();

	void connectSignals();


private slots:

	void slotDeleteDistanceMapButtonTriggered();
	void slotDistanceMapLoadClicked();
	void slotContrastChange();
	void slotOutlineChange();
	void slotColorbarChange();
	void slotSliderTextChanged(const QString & text);
	void slotSliderValueChanged(int value);
	void slotMapSelectionChanged(const QString & text);


};
