#pragma once

#include <QString>
#include <QObject>

#include "vtkobjects/VisualizationObject.h"

class Ui_View;

class Controller;
class Model;
class Visualization;

/* Virtual base class for all Viewontrollers */
class ViewControllerBase : public QObject{
	Q_OBJECT
	
public:

	/**
	Constructor
	*/
	ViewControllerBase(Controller* control, Model* model, Visualization* vis);

	/**
	Update the visuals of the current object based on the current values in the UI.
	*/
	virtual void UpdateVisualsFromUi() = 0;

protected:
	
	/**
	Checks whether signals are blocked or not.
	*/
	virtual void setupControls() = 0;

	/**
	Helper function to connect all Qt signals and slots.
	*/
	virtual void connectSignals() = 0;


	Controller* m_controller;				///< connection to main UI
	Model* m_model;							///< connection to main Model
	Visualization* m_visualization;			///< connection to main Visualization

	Ui_View* m_ui;			///< connection Qt UI


};

