#include "GlyphViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include <QSignalMapper>

GlyphViewController::GlyphViewController(
	Controller* control, Model* model, Visualization* vis) :
	ViewControllerBase(control, model, vis) {

	setupControls();
	connectSignals();
}

void GlyphViewController::setupControls() {

	this->m_ui->glyphRenderCheckBox->setEnabled(false);
	this->m_ui->glyphBox->setVisible(false);

	// Populate comboboxes
	this->m_ui->glyphColormapCombobox->clear();
	QListIterator<QString> i(ColorMap::ListAllFunctional());
	while (i.hasNext()) {
		QString map = i.next();
		this->m_ui->glyphColormapCombobox->addItem(QIcon(":/ColorMap/" + map), map);
	}
}

void GlyphViewController::connectSignals() {

	QSignalMapper* signalMapper = new QSignalMapper();

	// Glyphs
	connect(this->m_ui->glyphColormapCombobox, static_cast<void (QComboBox::*)(int index)>(&QComboBox::currentIndexChanged),
			signalMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));

	connect(this->m_ui->glyphRenderCheckBox, SIGNAL(clicked()),
			signalMapper, SLOT(map()));

	signalMapper->setMapping(this->m_ui->glyphColormapCombobox, 0);
	signalMapper->setMapping(this->m_ui->glyphRenderCheckBox, 0);

	// Main panel mapper 
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(slotGlobalChange(int)));
}

void GlyphViewController::slotGlobalChange(int) {
	UpdateVisualsFromUi();
}

void GlyphViewController::UpdateVisualsFromUi() {

	// Manage controls
	bool enable = this->m_ui->glyphRenderCheckBox->isChecked();
	this->m_ui->glyphBox->setVisible(enable);

	// Pass changes in visuals
	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(
		VisualType::VTYPE_GLYPHS, this->m_ui->distanceMapCombobox->currentText());

	if (visualObj == nullptr) {
		return;
	}

	QProgressDialog* dialog = this->m_controller->LoadingStart();

	VisualSettingsGlyphs* m_settings = dynamic_cast<VisualSettingsGlyphs*>(visualObj->GetSettings());
	VisualSettingsGlyphs* oldSettings = new VisualSettingsGlyphs(*m_settings);

	// Contrast
	if (this->m_ui->contrastNoneRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::None;
		this->m_ui->contrastAdvancedPanel->setEnabled(false);
	}
	else if (this->m_ui->contrastPolyRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Polynomal;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
	else if (this->m_ui->contrastExpRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Exponential;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
    else if (this->m_ui->contrastCostFuncRadiobutton->isChecked()) {
        m_settings->contrastFunction.type = ContrastFunctionType::CostFunction;
        this->m_ui->contrastAdvancedPanel->setEnabled(true);
    }
    
	m_settings->contrastFunction.sigma = this->m_ui->contrastSigmaLineEdit->text().toDouble();
	m_settings->contrastFunction.p = this->m_ui->contrastSharpnessLineEdit->text().toDouble();
	m_settings->contrastFunction.flipSign = this->m_ui->contrastFlipSignCheckbox->isChecked();

	m_settings->visible = this->m_ui->glyphRenderCheckBox->isChecked();
	m_settings->colormap = ColorMap::fromString(this->m_ui->glyphColormapCombobox->currentText());
	m_settings->showColorbar = this->m_ui->colorbarCheckBox->isChecked();

	m_settings->isDirty = !(*m_settings == *oldSettings);
	m_settings->contrastFunction.isDirty = !(m_settings->contrastFunction == oldSettings->contrastFunction);

	if (m_settings->visible && m_settings->visible != oldSettings->visible) {
		m_settings->contrastFunction.isDirty = true;
		m_settings->isDirty = true;
	}

	visualObj->SetSettings(m_settings);

	this->m_controller->LoadingDone(dialog);

	this->m_visualization->Render();
}

void GlyphViewController::AddObject(QString label, bool visible) {

	// Checkbox + label
	QListWidgetItem* item = new QListWidgetItem(label);
	item->setToolTip(label);
	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);

	if (visible) {
		item->setCheckState(Qt::CheckState::Checked);
	}
	else {
		item->setCheckState(Qt::CheckState::Unchecked);
	}

	// Add
	this->m_ui->referenceListWidget->insertItem(0, item);
	this->m_ui->referenceListWidget->setCurrentRow(0);
}

void GlyphViewController::UpdateUiFromSettings(QString label) {

	this->m_controller->BlockAllSignals(true);

	VisualizationObject* glyphs = this->m_visualization->GetVisualObject(VisualType::VTYPE_GLYPHS, label);
	VisualSettingsGlyphs* glyphSettings = dynamic_cast<VisualSettingsGlyphs*>(glyphs->GetSettings());

	this->m_ui->glyphRenderCheckBox->setChecked(glyphSettings->visible);
	this->m_ui->glyphBox->setVisible(glyphSettings->visible);
	this->m_ui->glyphColormapCombobox->setCurrentText(glyphSettings->colormap->toString());
	
	this->m_controller->BlockAllSignals(false);
}
