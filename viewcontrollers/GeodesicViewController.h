#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

class Controller;
class Model;
class Visualization;

/* ViewController for the Geodesic visualization controls. */
class GeodesicViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	GeodesicViewController(Controller* control,
						   Model* model,
						   Visualization* vis);

	// Virtual from base
	void UpdateVisualsFromUi();
	void AddObject(QString label, bool visible);

private:

	void setupControls();

	void connectSignals();

private slots:

	void slotGeoEditColor();
	void slotGeoListSelectionChanged();
	void slotGeoListItemChanged(QListWidgetItem * item);
	void slotDeleteButtonTriggered();
	void slotSliderTextChanged(const QString & text);
	void slotSliderValueChanged(int value);

	void slotGeodesicsLoadClicked();
	
	void slotGlobalPanelChange();


};