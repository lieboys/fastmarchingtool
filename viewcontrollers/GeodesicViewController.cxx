#include "GeodesicViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include <QColorDialog>
#include <QSettings>
#include <QFileDialog>

GeodesicViewController::GeodesicViewController(
	Controller* control, Model* model, Visualization* vis) : 
	ViewControllerBase(control, model, vis) {
	
	setupControls();
	connectSignals();
}

void GeodesicViewController::setupControls() {

	this->m_ui->geodesicBox->setEnabled(false);
}

void GeodesicViewController::connectSignals(){

	connect(this->m_ui->newGeodesicsButton, SIGNAL(clicked()),
			this, SLOT(slotGeodesicsLoadClicked()));
	connect(this->m_ui->newGeodesicsBinaryButton, SIGNAL(clicked()),
			this->m_controller, SLOT(slotLoadBinaryTriggered()));
	connect(this->m_ui->deleteGeodesicsButton, SIGNAL(clicked()), 
			this, SLOT(slotDeleteButtonTriggered()));

	connect(this->m_ui->geodesicsListWidget, SIGNAL(itemChanged(QListWidgetItem*)), this,
			SLOT(slotGeoListItemChanged(QListWidgetItem*)));
	connect(this->m_ui->geodesicsListWidget, SIGNAL(itemSelectionChanged()), this,
			SLOT(slotGeoListSelectionChanged()));

	connect(this->m_ui->geoOpacityTextbox, SIGNAL(textChanged(const QString &)), this,
			SLOT(slotSliderTextChanged(const QString &)));
	connect(this->m_ui->geoOpacitySlider, SIGNAL(valueChanged(int)), this,
			SLOT(slotSliderValueChanged(int)));
	connect(this->m_ui->geoColorEdit, SIGNAL(clicked()), this, SLOT(slotGeoEditColor()));
		connect(this->m_ui->geoArrowSamplingSlider, SIGNAL(valueChanged(int)),
			this, SLOT(slotSliderValueChanged(int)));
	connect(this->m_ui->geoArrowSizeSlider, SIGNAL(valueChanged(int)),
			this, SLOT(slotSliderValueChanged(int)));

	connect(this->m_ui->geoShowArrowsCheck, SIGNAL(clicked()), this, SLOT(slotGlobalPanelChange()));
	connect(this->m_ui->geoSingleColorCheck, SIGNAL(clicked()), this, SLOT(slotGlobalPanelChange()));
	connect(this->m_ui->geoOrientationCheck, SIGNAL(clicked()), this, SLOT(slotGlobalPanelChange()));

}

void GeodesicViewController::slotGeoEditColor() {

	QColor oldColor = QColor(
		this->m_ui->geoColorbox->property("red").toInt(),
		this->m_ui->geoColorbox->property("green").toInt(),
		this->m_ui->geoColorbox->property("blue").toInt());

	// Get new color
	QColor color = QColorDialog::getColor(oldColor);
	if (!color.isValid()) {
		return;
	}

	this->m_ui->geoColorbox->setProperty("red", color.red());
	this->m_ui->geoColorbox->setProperty("green", color.green());
	this->m_ui->geoColorbox->setProperty("blue", color.blue());

	this->m_ui->geoColorbox->setAutoFillBackground(true);
	this->m_ui->geoColorbox->setStyleSheet("QLabel { background-color :" +
										 QVariant(color).toString() +
										 ";border: 1px solid #7a7a7a }");
	UpdateVisualsFromUi();
}

void GeodesicViewController::slotGeoListSelectionChanged() {

	QListWidgetItem* item = this->m_ui->geodesicsListWidget->currentItem();
	if (item == nullptr) {
		this->m_ui->geodesicBox->setEnabled(false);
		return;
	}
	else {
		this->m_ui->geodesicBox->setEnabled(true);
	}

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(VisualType::VTYPE_GEODESIC, item->text());
	VisualSettingsGeodesic* m_settings = dynamic_cast<VisualSettingsGeodesic*>(visualObj->GetSettings());

	this->m_controller->BlockAllSignals(true);

	// Opacity
	this->m_ui->geoOpacitySlider->setValue(qRound(m_settings->opacity * 100.0));
	this->m_ui->geoOpacityTextbox->setText(QString::number(qRound(m_settings->opacity * 100.0)));

	// Color
	this->m_ui->geoColorbox->setAutoFillBackground(true);
	this->m_ui->geoColorbox->setStyleSheet("QLabel { background-color :" +
										 QVariant(m_settings->color).toString() +
										 ";border: 1px solid #7a7a7a }");

	// Rendering
	if (m_settings->rendering == GeodesicRendering::COLOR) {
		this->m_ui->geoSingleColorCheck->setChecked(true);
		this->m_ui->geoOrientationCheck->setChecked(false);
	}
	else if (m_settings->rendering == GeodesicRendering::ORIENTATION) {
		this->m_ui->geoSingleColorCheck->setChecked(false);
		this->m_ui->geoOrientationCheck->setChecked(true);
	}

	// Set Color properties
	this->m_ui->geoColorbox->setProperty("red", m_settings->color.red());
	this->m_ui->geoColorbox->setProperty("green", m_settings->color.green());
	this->m_ui->geoColorbox->setProperty("blue", m_settings->color.blue());

	// Arrow
	this->m_ui->geoShowArrowsCheck->setChecked(m_settings->showArrows);

	this->m_ui->geoArrowSamplingSlider->setValue(qRound(m_settings->arrowDistance * 1000.0));
	this->m_ui->geoArrowSamplingText->setText(QString::number(qRound(m_settings->arrowDistance * 1000.0)));

	this->m_ui->geoArrowSizeSlider->setValue(qRound(m_settings->arrowSize * 100.0));
	this->m_ui->geoArrowSizeText->setText(QString::number(qRound(m_settings->arrowSize * 100.0)));

	this->m_controller->BlockAllSignals(false);
}
void GeodesicViewController::UpdateVisualsFromUi() {


	QListWidgetItem* item = this->m_ui->geodesicsListWidget->currentItem();
	if (item == nullptr) {
		return;
	}
	QString label = item->text();

	bool color = this->m_ui->geoSingleColorCheck->isChecked();
	this->m_ui->geoColorbox->setEnabled(color);
	this->m_ui->geoColorEdit->setEnabled(color);

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(VisualType::VTYPE_GEODESIC, label);
	VisualSettingsGeodesic* m_settings = dynamic_cast<VisualSettingsGeodesic*>(visualObj->GetSettings());
	VisualSettingsGeodesic* oldSettings = new VisualSettingsGeodesic(*m_settings);

	m_settings->visible = this->m_ui->geodesicsListWidget->currentItem()->checkState() == Qt::CheckState::Checked;
	m_settings->opacity = this->m_ui->geoOpacitySlider->value() / 100.0;

	m_settings->color = QColor(this->m_ui->geoColorbox->property("red").toInt(),
							 this->m_ui->geoColorbox->property("green").toInt(),
							 this->m_ui->geoColorbox->property("blue").toInt());

	if (this->m_ui->geoSingleColorCheck->isChecked()) {
		m_settings->rendering = GeodesicRendering::COLOR;
	}
	else if (this->m_ui->geoOrientationCheck->isChecked()) {
		m_settings->rendering = GeodesicRendering::ORIENTATION;
	}

	// Arrows
	m_settings->showArrows = this->m_ui->geoShowArrowsCheck->isChecked();
	m_settings->arrowSize = this->m_ui->geoArrowSizeSlider->value() / 100.0;
	m_settings->arrowDistance = this->m_ui->geoArrowSamplingSlider->value() / 1000.0;

	m_settings->isDirty = !(*m_settings == *oldSettings);
	m_settings->arrowIsDirty = !(m_settings->arrowSize == oldSettings->arrowSize &&
							   m_settings->arrowDistance == oldSettings->arrowDistance);

	visualObj->SetSettings(m_settings);

	this->m_visualization->Render();
}


void GeodesicViewController::slotGeoListItemChanged(QListWidgetItem* item) {

	this->m_controller->BlockAllSignals(true);

	this->m_ui->geodesicsListWidget->clearSelection();
	this->m_ui->geodesicsListWidget->setCurrentItem(item);


	QString oldLabel = item->toolTip();
	QString newLabel = item->text();

	if (oldLabel != newLabel) {
		this->m_visualization->RenameObj(oldLabel, newLabel);
		item->setToolTip(newLabel);
	}

	this->m_controller->BlockAllSignals(false);

	slotGeoListSelectionChanged(); 

	UpdateVisualsFromUi();
}


void GeodesicViewController::slotDeleteButtonTriggered() {

	QString label = this->m_ui->geodesicsListWidget->currentItem()->text();

	// Remove from UI
	for (int i = 0; i < this->m_ui->geodesicsListWidget->count(); i++) {
		if (this->m_ui->geodesicsListWidget->item(i)->text() == label) {
			delete this->m_ui->geodesicsListWidget->takeItem(i);
		}
	}
	this->m_model->RemoveObject(DataType::DTYPE_GEODESIC, label);
}


void GeodesicViewController::slotSliderTextChanged(const QString & text) {

	this->m_controller->BlockAllSignals(true);

	int value = text.toInt();;
	QSlider* slider = nullptr;
	QLineEdit* textbox = dynamic_cast<QLineEdit*> (sender());;

	// Control specific
	if (textbox == this->m_ui->geoArrowSizeText) {
		slider = this->m_ui->geoArrowSizeSlider;
	}
	else if (textbox == this->m_ui->geoArrowSamplingText) {
		slider = this->m_ui->geoArrowSamplingSlider;
	}
	else if (textbox == this->m_ui->geoOpacityTextbox) {
		slider = this->m_ui->geoOpacitySlider;
	}
	slider->setValue(value);

	this->m_controller->BlockAllSignals(false);

	// Check every range
	if (text.toInt() > slider->maximum()) {
		textbox->setText(QString::number(slider->maximum()));
		return;
	}
	if (text.toInt() < slider->minimum()) {
		textbox->setText(QString::number(slider->minimum()));
		return;
	}

	UpdateVisualsFromUi();
}

void GeodesicViewController::slotSliderValueChanged(int value) {

	this->m_controller->BlockAllSignals(true);

	QString text = QString::number(value);
	QSlider* slider = dynamic_cast<QSlider*> (sender());
	QLineEdit* textbox = nullptr;

	// Control specific
	if (slider == this->m_ui->geoArrowSizeSlider) {
		textbox = this->m_ui->geoArrowSizeText;
	}
	else if (slider == this->m_ui->geoArrowSamplingSlider) {
		textbox = this->m_ui->geoArrowSamplingText;
	}
	else if (slider == this->m_ui->geoOpacitySlider) {
		textbox = this->m_ui->geoOpacityTextbox;
	}

	textbox->setText(text);

	this->m_controller->BlockAllSignals(false);

	UpdateVisualsFromUi();
}

void GeodesicViewController::slotGeodesicsLoadClicked() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;

	// Multiple file selection
	QStringList filepaths = QFileDialog::getOpenFileNames(this->m_controller, tr("Load region file"),
														  MySettings.value(DEFAULT_DIR_KEY).toString(),
														  tr("VTK file (*.vtk);;Any files (*)"));

	if (filepaths.isEmpty()) {
		return;
	}

	for (int i = 0; i < filepaths.size(); i++) {

		QFileInfo fi(filepaths.at(i));
		MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

		this->m_controller->BlockAllSignals(true);

		bool ok = this->m_model->LoadDatasetInDatabase(QDir::toNativeSeparators(filepaths.at(i)),
													   DataType::DTYPE_GEODESIC);

		if (!ok) {
			this->m_controller->BlockAllSignals(false);
			this->m_controller->ThrowError("Import failed");
			return;
		}

		this->m_ui->geodesicBox->setEnabled(true);

		this->m_controller->BlockAllSignals(false);

		this->m_controller->UpdateVisualNavControls();

		UpdateVisualsFromUi();
	}

}

void GeodesicViewController::slotGlobalPanelChange() {
	this->UpdateVisualsFromUi();
}

void GeodesicViewController::AddObject(QString label, bool visible){

	// Checkbox + label
	QListWidgetItem* item = new QListWidgetItem(label);
	item->setToolTip(label);
	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);

	if (visible) {
		item->setCheckState(Qt::CheckState::Checked);
	}
	else {
		item->setCheckState(Qt::CheckState::Unchecked);
	}

	// Add
	this->m_ui->geodesicsListWidget->insertItem(0, item);
	this->m_ui->geodesicsListWidget->setCurrentRow(0);
    
    slotGeoListItemChanged(item);
}
