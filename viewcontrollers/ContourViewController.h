#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

class Controller;
class Model;
class Visualization;

/* ViewController for the Reference visualization controls. */
class ContourViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	ContourViewController(Controller* control,
						 Model* model,
						 Visualization* vis);
	
	// Virtual from base
	void UpdateVisualsFromUi();

	void UpdateUiFromSettings(QString label);

			
private:

	void setupControls();

	void connectSignals();



private slots:

	void slotSliderTextChanged(const QString & text);
	void slotSliderValueChanged(int value);

	void slotGlobalChange(int);


};