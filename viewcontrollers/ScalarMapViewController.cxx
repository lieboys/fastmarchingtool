#include "ScalarMapViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include <QSignalmapper>

ScalarMapViewController::ScalarMapViewController(
	Controller* control, Model* model, Visualization* vis) :
	ViewControllerBase(control, model, vis) {

	setupControls();
	connectSignals();
}

void ScalarMapViewController::setupControls() {

	// Populate comboboxes

	this->m_ui->scalarColormapCombobox->clear();
	QListIterator<QString> i(ColorMap::ListAllFunctional());
	while (i.hasNext()) {
		QString map = i.next();
		this->m_ui->scalarColormapCombobox->addItem(QIcon(":/ColorMap/" + map), map);
	}

	this->m_ui->scalarRenderCheckBox->setEnabled(false);
	this->m_ui->scalarBox->setVisible(false);
	
}

void ScalarMapViewController::connectSignals() {

	QSignalMapper* signalMapper = new QSignalMapper();

	// Scalarmap
	connect(this->m_ui->scalarColormapCombobox, static_cast<void (QComboBox::*)(int index)>(&QComboBox::currentIndexChanged),
			signalMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(this->m_ui->scalarRenderCheckBox, SIGNAL(clicked()),
			signalMapper, SLOT(map()));
	connect(this->m_ui->scalarProjectionCombobox, static_cast<void (QComboBox::*)(int index)>(&QComboBox::currentIndexChanged),
			signalMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	
	signalMapper->setMapping(this->m_ui->scalarColormapCombobox, 0);
	signalMapper->setMapping(this->m_ui->scalarRenderCheckBox, 0);
	signalMapper->setMapping(this->m_ui->scalarProjectionCombobox, 0);

	// Main panel mapper 
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(slotGlobalChange(int)));
}

void ScalarMapViewController::UpdateVisualsFromUi() {

	// Manage controls
	bool enable = this->m_ui->scalarRenderCheckBox->isChecked();
	this->m_ui->scalarBox->setVisible(enable);

	// Pass changes in visuals
	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(
		VisualType::VTYPE_SCALARMAP, this->m_ui->distanceMapCombobox->currentText());

	if (visualObj == nullptr) {
		return;
	}
	QProgressDialog* dialog = this->m_controller->LoadingStart();

	VisualSettingsScalarProjection* m_settings =
		dynamic_cast<VisualSettingsScalarProjection*>(visualObj->GetSettings());

	VisualSettingsScalarProjection* oldSettings = new VisualSettingsScalarProjection(*m_settings);

	m_settings->visible = this->m_ui->scalarRenderCheckBox->isChecked();
	m_settings->colormap = ColorMap::fromString(this->m_ui->scalarColormapCombobox->currentText());

	// Colorbar
	m_settings->showColorbar =
		this->m_ui->colorbarCheckBox->isChecked() &&
		this->m_ui->colorbarCheckBox->isEnabled();

	m_settings->showOutline = this->m_ui->outlineCheckBox->isChecked();

	// Contrast
	if (this->m_ui->contrastNoneRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::None;
		this->m_ui->contrastAdvancedPanel->setEnabled(false);
	}
	else if (this->m_ui->contrastPolyRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Polynomal;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
	else if (this->m_ui->contrastExpRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Exponential;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
    else if (this->m_ui->contrastCostFuncRadiobutton->isChecked()) {
        m_settings->contrastFunction.type = ContrastFunctionType::CostFunction;
        this->m_ui->contrastAdvancedPanel->setEnabled(true);
    }
    
	m_settings->contrastFunction.sigma = this->m_ui->contrastSigmaLineEdit->text().toDouble();
	m_settings->contrastFunction.p = this->m_ui->contrastSharpnessLineEdit->text().toDouble();
	m_settings->contrastFunction.flipSign = this->m_ui->contrastFlipSignCheckbox->isChecked();

	// Projection
	m_settings->projection = Projection::fromString(this->m_ui->scalarProjectionCombobox->currentText());

	// Save
	m_settings->isDirty = !(*m_settings == *oldSettings);
	m_settings->contrastFunction.isDirty = !(m_settings->contrastFunction == oldSettings->contrastFunction);
	m_settings->projectionIsDirty = !(m_settings->projection == oldSettings->projection);

	if (m_settings->visible && m_settings->visible != oldSettings->visible) {
		m_settings->contrastFunction.isDirty = true;
		m_settings->isDirty = true;
	}

	visualObj->SetSettings(m_settings);
	delete oldSettings;

	this->m_controller->LoadingDone(dialog);

	this->m_visualization->Render();
}

void ScalarMapViewController::UpdateUiFromSettings(QString label) {

	VisualizationObject* scalar = this->m_visualization->GetVisualObject(VisualType::VTYPE_SCALARMAP, label);
	VisualSettingsScalarProjection* scalarSettings = dynamic_cast<VisualSettingsScalarProjection*>(scalar->GetSettings());
	
	this->m_controller->BlockAllSignals(true);

	this->m_ui->scalarRenderCheckBox->setChecked(scalarSettings->visible);
	this->m_ui->scalarBox->setVisible(scalarSettings->visible);

	this->m_ui->scalarColormapCombobox->setCurrentText(scalarSettings->colormap->toString());
	this->m_ui->scalarProjectionCombobox->setCurrentText(scalarSettings->projection.toString());

	// Outline
	this->m_ui->outlineCheckBox->setChecked(scalarSettings->showOutline);

	// Colorbar
	this->m_ui->colorbarCheckBox->setChecked(scalarSettings->showColorbar);


	this->m_controller->BlockAllSignals(false);

}

void ScalarMapViewController::slotGlobalChange(int ) {
	this->UpdateVisualsFromUi();
}
