#include "ContourViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include <QSignalMapper>

ContourViewController::ContourViewController(
	Controller* control, Model* model, Visualization* vis) :
	ViewControllerBase(control, model, vis) {

	setupControls();
	connectSignals();
}

void ContourViewController::setupControls() {

	this->m_ui->contourBox->setVisible(false);

	// Populate comboboxes
	this->m_ui->contourColorMapCombobox->clear();
	QListIterator<QString> i(ColorMap::ListAllFunctional());
	while (i.hasNext()) {
		QString map = i.next();
		this->m_ui->contourColorMapCombobox->addItem(QIcon(":/ColorMap/" + map), map);
	}
	this->m_ui->contourRenderCheckBox->setEnabled(false);
}

void ContourViewController::connectSignals() {

	QSignalMapper* signalMapper = new QSignalMapper();

	// Contours
	connect(this->m_ui->contourOpacityTextbox, SIGNAL(textChanged(const QString &)), this,
			SLOT(slotSliderTextChanged(const QString &)));
	connect(this->m_ui->contourOpacitySlider, SIGNAL(valueChanged(int)), this,
			SLOT(slotSliderValueChanged(int)));


	connect(this->m_ui->contourColorMapCombobox, static_cast<void (QComboBox::*)(int index)>(&QComboBox::currentIndexChanged),
			signalMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));
	connect(this->m_ui->contourRenderCheckBox, SIGNAL(clicked()),
			signalMapper, SLOT(map()));
	connect(this->m_ui->contoursSmoothCheckbox, SIGNAL(clicked()),
			signalMapper, SLOT(map()));
	connect(this->m_ui->contourAmountEditBox, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
			signalMapper, static_cast<void (QSignalMapper::*)()>(&QSignalMapper::map));

	
	signalMapper->setMapping(this->m_ui->contourColorMapCombobox, 0);
	signalMapper->setMapping(this->m_ui->contourRenderCheckBox, 0);
	signalMapper->setMapping(this->m_ui->contoursSmoothCheckbox, 0);
	signalMapper->setMapping(this->m_ui->contourAmountEditBox, 0);

	// Main panel mapper 
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(slotGlobalChange(int)));
}

void ContourViewController::slotGlobalChange(int) {
	this->UpdateVisualsFromUi();
}

void ContourViewController::UpdateUiFromSettings(QString label) {

	this->m_controller->BlockAllSignals(true);

	VisualizationObject* contours = this->m_visualization->GetVisualObject(VisualType::VTYPE_CONTOURPLOT, label);
	VisualSettingsContourPlot* contourSettings = dynamic_cast<VisualSettingsContourPlot*>(contours->GetSettings());

	this->m_ui->contourRenderCheckBox->setChecked(contourSettings->visible);
	this->m_ui->contourBox->setVisible(contourSettings->visible);
	this->m_ui->contourColorMapCombobox->setCurrentText(contourSettings->colormap->toString());
	this->m_ui->contourAmountEditBox->setValue(contourSettings->numContours - 1);
	this->m_ui->contourOpacitySlider->setValue((int)contourSettings->opacity * 100);
	this->m_ui->contoursSmoothCheckbox->setChecked(contourSettings->renderSmooth);

	this->m_controller->BlockAllSignals(false);
}

void ContourViewController::UpdateVisualsFromUi() {

	// Manage controls
	bool enable = this->m_ui->contourRenderCheckBox->isChecked();
	this->m_ui->contourBox->setVisible(enable);

	// Pass changes in visuals
	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(
		VisualType::VTYPE_CONTOURPLOT, this->m_ui->distanceMapCombobox->currentText());

	if (visualObj == nullptr) {
		return;
	}

	VisualSettingsContourPlot* m_settings =
		dynamic_cast<VisualSettingsContourPlot*>(visualObj->GetSettings());

	VisualSettingsContourPlot* oldSettings = new VisualSettingsContourPlot(*m_settings);

	m_settings->visible = this->m_ui->contourRenderCheckBox->isChecked();
	m_settings->colormap = ColorMap::fromString(this->m_ui->contourColorMapCombobox->currentText());
	m_settings->showColorbar = this->m_ui->colorbarCheckBox->isChecked();
	m_settings->renderSmooth = this->m_ui->contoursSmoothCheckbox->isChecked();
	m_settings->opacity = this->m_ui->contourOpacitySlider->value() / 100.0;
	m_settings->numContours = QString(this->m_ui->contourAmountEditBox->text()).toInt() + 1;
	m_settings->numContoursIsDirty = !(m_settings->numContours == oldSettings->numContours);

	// Contrast
	if (this->m_ui->contrastNoneRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::None;
		this->m_ui->contrastAdvancedPanel->setEnabled(false);
	}
	else if (this->m_ui->contrastPolyRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Polynomal;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
	else if (this->m_ui->contrastExpRadiobutton->isChecked()) {
		m_settings->contrastFunction.type = ContrastFunctionType::Exponential;
		this->m_ui->contrastAdvancedPanel->setEnabled(true);
	}
    else if (this->m_ui->contrastCostFuncRadiobutton->isChecked()) {
        m_settings->contrastFunction.type = ContrastFunctionType::CostFunction;
        this->m_ui->contrastAdvancedPanel->setEnabled(true);
    }
	m_settings->contrastFunction.sigma = this->m_ui->contrastSigmaLineEdit->text().toDouble();
	m_settings->contrastFunction.p = this->m_ui->contrastSharpnessLineEdit->text().toDouble();
	m_settings->contrastFunction.flipSign = this->m_ui->contrastFlipSignCheckbox->isChecked();
	m_settings->contrastFunction.isDirty = true;
	m_settings->projection = Projection::ProjectionType::MINIMUM;//TODO: check this

	m_settings->isDirty = !(*m_settings == *oldSettings);
	m_settings->contrastFunction.isDirty = !(m_settings->contrastFunction == oldSettings->contrastFunction);

	visualObj->SetSettings(m_settings);

	this->m_visualization->Render();
}

void ContourViewController::slotSliderTextChanged(const QString & text) {

	this->m_controller->BlockAllSignals(true);

	int value = text.toInt();;
	QSlider* slider = this->m_ui->contourOpacitySlider;
	QLineEdit* textbox = dynamic_cast<QLineEdit*> (sender());;

	slider->setValue(value);
	this->m_controller->BlockAllSignals(false);

	// Check every range
	if (text.toInt() > slider->maximum()) {
		textbox->setText(QString::number(slider->maximum()));
		return;
	}
	if (text.toInt() < slider->minimum()) {
		textbox->setText(QString::number(slider->minimum()));
		return;
	}

	UpdateVisualsFromUi();
}

void ContourViewController::slotSliderValueChanged(int value) {

	this->m_controller->BlockAllSignals(true);

	QString text = QString::number(value);
	QLineEdit* textbox = this->m_ui->contourOpacityTextbox;
	QSlider* slider = dynamic_cast<QSlider*> (sender());

	textbox->setText(text);

	this->m_controller->BlockAllSignals(false);

	UpdateVisualsFromUi();
}
