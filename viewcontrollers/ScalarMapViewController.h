#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

class Controller;
class Model;
class Visualization;

/* ViewController for the Reference visualization controls. */
class ScalarMapViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	ScalarMapViewController(Controller* control,
						 Model* model,
						 Visualization* vis);
	
	// Virtual from base
	void UpdateVisualsFromUi();

	void UpdateUiFromSettings(QString label);


		
private:

	void setupControls();

	void connectSignals();


private slots:

void slotGlobalChange(int);


};