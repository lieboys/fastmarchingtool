#include "RegionViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include "gui/SettingsDialog.h"

#include <QColorDialog>
#include <QSettings>
#include <QFileDialog>
#include <QSettings>

RegionViewController::RegionViewController(
	Controller* control, Model* model, Visualization* vis) : 
	ViewControllerBase(control, model, vis) {
	
	setupControls();
	connectSignals();
}

void RegionViewController::setupControls() {

	this->m_ui->regionBox->setEnabled(false);
}

void RegionViewController::connectSignals(){

	connect(this->m_ui->deleteRegionButton, SIGNAL(clicked()), this, SLOT(slotDeleteButtonTriggered()));

	connect(this->m_ui->newRegionButtonNii, SIGNAL(clicked()), this, SLOT(slotRegionLoadClicked()));

	connect(this->m_ui->editRegionButton, SIGNAL(clicked()), this, SLOT(slotEditRegionClicked()));

	connect(this->m_ui->regionRenderSmoothCheck, SIGNAL(clicked()), this, SLOT(slotGlobalPanelChange()));

	connect(this->m_ui->regionListWidget, SIGNAL(itemChanged(QListWidgetItem*)), this,
			SLOT(slotRegionListItemChanged(QListWidgetItem*)));

	connect(this->m_ui->regionListWidget, SIGNAL(itemSelectionChanged()), this,
			SLOT(slotRegionListSelectionChanged()));

	connect(this->m_ui->regionOpacityTextbox, SIGNAL(textChanged(const QString &)), this,
			SLOT(slotSliderTextChanged(const QString &)));

	connect(this->m_ui->regionOpacitySlider, SIGNAL(valueChanged(int)), this,
			SLOT(slotSliderValueChanged(int)));

	connect(this->m_ui->regionColorEdit, SIGNAL(clicked()), this, SLOT(slotRegionEditColor()));

	connect(this->m_ui->newRegionButtonCustom, SIGNAL(clicked()), this,
			SLOT(slotNewManualRegionClicked()));
}

void RegionViewController::AddObject(QString label, bool visible){

	// Checkbox + label
	QListWidgetItem* item = new QListWidgetItem(label);
	item->setToolTip(label);
	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);

	if (visible) {
		item->setCheckState(Qt::CheckState::Checked);
	}
	else {
		item->setCheckState(Qt::CheckState::Unchecked);
	}

	// Add
	this->m_ui->regionListWidget->insertItem(999, item);
	this->m_ui->regionListWidget->setCurrentRow(0);

	this->m_ui->connectivityRoiACombobox->insertItem(999, label);
	this->m_ui->connectivityRoiBCombobox->insertItem(999, label);
	this->m_ui->codeLinkSeedCombobox->insertItem(999, label);
	this->m_ui->codeLinkTipCombobox->insertItem(999, label);
	
    slotRegionListItemChanged(item);
}

void RegionViewController::slotRegionLoadClicked() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;

	// Multiple file selection
	QStringList filepaths = QFileDialog::getOpenFileNames(this->m_controller, tr("Load region file"),
														  MySettings.value(DEFAULT_DIR_KEY).toString(),
														  tr("NIfTI (*.nii* *.nii.gz *.hdr);;Any files (*)"));

	if (filepaths.isEmpty()) {
		return;
	}

	for (int i = 0; i < filepaths.size(); i++) {

		QFileInfo fi(filepaths.at(i));
		MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

		this->m_controller->BlockAllSignals(true);

		bool ok = this->m_model->LoadDatasetInDatabase(QDir::toNativeSeparators(filepaths.at(i)),
													   DataType::DTYPE_ROI);
		slotRegionListSelectionChanged();

		if (!ok) {
			this->m_controller->BlockAllSignals(false);
			this->m_controller->ThrowError("Import failed");
			return;
		}

		this->m_ui->navPanel->setEnabled(ok);
		this->m_ui->regionBox->setEnabled(ok);

		this->m_controller->BlockAllSignals(false);

		this->m_controller->UpdateVisualNavControls();
		
		UpdateVisualsFromUi();
	}
}


void RegionViewController::UpdateVisualsFromUi() {

	QListWidgetItem* item = this->m_ui->regionListWidget->currentItem();
	if (item == nullptr) {
		return;
	}
	QString label = item->text();

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(
		VisualType::VTYPE_ROI, label);

	if (visualObj == nullptr) {
		return;
	}
	VisualSettingsRegion* newSettings = dynamic_cast<VisualSettingsRegion*>(visualObj->GetSettings());
	newSettings->isDirty = true;

	newSettings->visible = this->m_ui->regionListWidget->currentItem()->checkState() == Qt::CheckState::Checked;
	newSettings->opacity = this->m_ui->regionOpacitySlider->value() / 100.0;

	newSettings->color = QColor(this->m_ui->regionColorbox->property("red").toInt(),
								this->m_ui->regionColorbox->property("green").toInt(),
								this->m_ui->regionColorbox->property("blue").toInt());

	newSettings->renderSmooth = this->m_ui->regionRenderSmoothCheck->isChecked();

	visualObj->SetSettings(newSettings);

	this->m_visualization->Render();
}


void RegionViewController::slotRegionListSelectionChanged() {

	QListWidgetItem* item = this->m_ui->regionListWidget->currentItem();
	if (item == nullptr) {
		this->m_ui->regionBox->setEnabled(false);
		return;
	}
	else {
		this->m_ui->regionBox->setEnabled(true);
	}

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(VisualType::VTYPE_ROI, item->text());
	VisualSettingsRegion* m_settings = dynamic_cast<VisualSettingsRegion*>(visualObj->GetSettings());

	this->m_controller->BlockAllSignals(true);

	// Opacity
	this->m_ui->regionOpacitySlider->setValue(qRound(m_settings->opacity * 100.0));
	this->m_ui->regionOpacityTextbox->setText(QString::number(qRound(m_settings->opacity * 100.0)));

	// Color
	this->m_ui->regionColorbox->setAutoFillBackground(true);
	this->m_ui->regionColorbox->setStyleSheet("QLabel { background-color :" +
											QVariant(m_settings->color).toString() +
											";border: 1px solid #7a7a7a }");

	// Set Color properties
	this->m_ui->regionColorbox->setProperty("red", m_settings->color.red());
	this->m_ui->regionColorbox->setProperty("green", m_settings->color.green());
	this->m_ui->regionColorbox->setProperty("blue", m_settings->color.blue());

	// Render 3D
	this->m_ui->regionRenderSmoothCheck->setChecked(m_settings->renderSmooth);

	this->m_controller->BlockAllSignals(false);
}


void RegionViewController::slotRegionEditColor() {

	QColor oldColor = QColor(
		this->m_ui->regionColorbox->property("red").toInt(),
		this->m_ui->regionColorbox->property("green").toInt(),
		this->m_ui->regionColorbox->property("blue").toInt());

	// Get new color
	QColor color = QColorDialog::getColor(oldColor);
	if (!color.isValid()) {
		return;
	}

	this->m_ui->regionColorbox->setProperty("red", color.red());
	this->m_ui->regionColorbox->setProperty("green", color.green());
	this->m_ui->regionColorbox->setProperty("blue", color.blue());

	this->m_ui->regionColorbox->setAutoFillBackground(true);
	this->m_ui->regionColorbox->setStyleSheet("QLabel { background-color :" +
											QVariant(color).toString() +
											";border: 1px solid #7a7a7a }");

	UpdateVisualsFromUi();
}

void RegionViewController::slotRegionListItemChanged(QListWidgetItem* item) {

	this->m_controller->BlockAllSignals(true);

	this->m_ui->regionListWidget->clearSelection();
	this->m_ui->regionListWidget->setCurrentItem(item);


	QString oldLabel = item->toolTip();
	QString newLabel = item->text();

	if (oldLabel != newLabel) {
		this->m_visualization->RenameObj(oldLabel, newLabel);
		item->setToolTip(newLabel);
	}

	this->m_controller->BlockAllSignals(false);

	slotRegionListSelectionChanged();
	UpdateVisualsFromUi();
}


void RegionViewController::slotSliderTextChanged(const QString & text) {

	this->m_controller->BlockAllSignals(true);

	int value = text.toInt();;
	QSlider* slider = nullptr;
	QLineEdit* textbox = dynamic_cast<QLineEdit*> (sender());;

	// Control specific
	if (textbox == this->m_ui->regionOpacityTextbox) {
		slider = this->m_ui->regionOpacitySlider;
	}

	slider->setValue(value);
	this->m_controller->BlockAllSignals(false);

	// Check every range
	if (text.toInt() > slider->maximum()) {
		textbox->setText(QString::number(slider->maximum()));
		return;
	}
	if (text.toInt() < slider->minimum()) {
		textbox->setText(QString::number(slider->minimum()));
		return;
	}

	UpdateVisualsFromUi();
}

void RegionViewController::slotSliderValueChanged(int value) {

	this->m_controller->BlockAllSignals(true);

	QString text = QString::number(value);
	QSlider* slider = dynamic_cast<QSlider*> (sender());
	QLineEdit* textbox = nullptr;

	// Control specific
	if (slider == this->m_ui->regionOpacitySlider) {
		textbox = this->m_ui->regionOpacityTextbox;
	}

	textbox->setText(text);

	this->m_controller->BlockAllSignals(false);

	UpdateVisualsFromUi();
}


void RegionViewController::slotDeleteButtonTriggered() {

	QString label = this->m_ui->regionListWidget->currentItem()->text();

	// Remove from UI
	for (int i = 0; i < this->m_ui->regionListWidget->count(); i++) {
		if (this->m_ui->regionListWidget->item(i)->text() == label) {
			delete this->m_ui->regionListWidget->takeItem(i);
		}
	}

	this->m_model->RemoveObject(DataType::DTYPE_ROI, label);
}

void RegionViewController::slotGlobalPanelChange() {

	UpdateVisualsFromUi();
}

void RegionViewController::slotEditRegionClicked() {

	QListWidgetItem* item = this->m_ui->regionListWidget->currentItem();
	if (item == nullptr) {
		return;
	}

	QSettings m_settings;
	QString cmd = m_settings.value(SettingsDialog::SETTINGS_ROI_ITKSNAP_COMMAND).toString();


	this->m_model->OpenITKSnap(cmd, item->text());

}

void RegionViewController::slotNewManualRegionClicked() {

	QSettings m_settings;
	QString cmd = m_settings.value(SettingsDialog::SETTINGS_ROI_ITKSNAP_COMMAND).toString();

	this->m_model->OpenITKSnap(cmd, "");
}
