#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

class Controller;
class Model;
class Visualization;

/* ViewController for the Region visualization controls. */
class RegionViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	RegionViewController(Controller* control,
						 Model* model,
						 Visualization* vis);

	// Virtual from base
	void UpdateVisualsFromUi();
	void AddObject(QString label, bool visible);

private:

	void setupControls();

	void connectSignals();

private slots:

	void slotRegionLoadClicked();
	void slotRegionListSelectionChanged();
	void slotRegionEditColor();
	void slotRegionListItemChanged(QListWidgetItem * item);
	void slotSliderTextChanged(const QString & text);
	void slotSliderValueChanged(int value);
	void slotGlobalPanelChange();

	void slotEditRegionClicked();
	void slotNewManualRegionClicked();
	void slotDeleteButtonTriggered();

};