#include "ViewControllerBase.h"

#include "ui_View.h"

#include "Controller.h"
#include "Visualization.h"
#include "Model.h"

ViewControllerBase::ViewControllerBase(Controller* control,
									   Model* model,
									   Visualization* vis) {

	this->m_controller = control;
	this->m_model = model;
	this->m_visualization = vis;

	this->m_ui = control->GetUIConnection();

	//this->dirtyRendering = true;
	//this->m_legalChange = true;
}