#include "ReferenceViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include "gui/SettingsDialog.h"

#include <QColorDialog>
#include <QSettings>
#include <QFileDialog>
#include <QSettings>

ReferenceViewController::ReferenceViewController(
	Controller* control, Model* model, Visualization* vis) :
	ViewControllerBase(control, model, vis) {

	setupControls();
	connectSignals();
}

void ReferenceViewController::setupControls() {

	this->m_ui->referenceBox->setEnabled(false);

	// Populate comboboxes
	this->m_ui->referenceColormapCombobox->clear();
	QListIterator<QString> i(ColorMap::ListAll());
	while (i.hasNext()) {
		QString map = i.next();
		this->m_ui->referenceColormapCombobox->addItem(QIcon(":/ColorMap/" + map), map);
	}
}

void ReferenceViewController::connectSignals() {

	connect(this->m_ui->deleteReferenceButton, SIGNAL(clicked()), this, SLOT(slotDeleteButtonTriggered()));

	connect(this->m_ui->newReferenceButton, SIGNAL(clicked()), this, SLOT(slotReferenceLoadClicked()));

	connect(this->m_ui->referenceListWidget, SIGNAL(itemChanged(QListWidgetItem*)), this,
			SLOT(slotReferenceListItemChanged(QListWidgetItem*)));

	connect(this->m_ui->referenceListWidget, SIGNAL(itemSelectionChanged()), this,
			SLOT(slotReferenceListSelectionChanged()));

	connect(this->m_ui->referenceOpacityTextbox, SIGNAL(textChanged(const QString &)), this,
			SLOT(slotSliderTextChanged(const QString &)));

	connect(this->m_ui->referenceOpacitySlider, SIGNAL(valueChanged(int)), this,
			SLOT(slotSliderValueChanged(int)));

	connect(this->m_ui->referenceColormapCombobox, SIGNAL(currentTextChanged(const QString &)), this,
			SLOT(slotColormapChanged(const QString &)));
}

void ReferenceViewController::AddObject(QString label, bool visible) {

	// Checkbox + label
	QListWidgetItem* item = new QListWidgetItem(label);
	item->setToolTip(label);
	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);

	if (visible) {
		item->setCheckState(Qt::CheckState::Checked);
	}
	else {
		item->setCheckState(Qt::CheckState::Unchecked);
	}

	// Add
	this->m_ui->referenceListWidget->insertItem(0, item);
	this->m_ui->referenceListWidget->setCurrentRow(0);
    
    slotReferenceListItemChanged(item);
}

void ReferenceViewController::slotReferenceLoadClicked() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;

	// Multiple file selection
	QStringList filepaths = QFileDialog::getOpenFileNames(this->m_controller, tr("Load reference file"),
														  MySettings.value(DEFAULT_DIR_KEY).toString(),
														  tr("NIfTI (*.nii* *.nii.gz *.hdr);;Any files (*)"));

	if (filepaths.isEmpty()) {
		return;
	}

	for (int i = 0; i < filepaths.size(); i++) {

		QFileInfo fi(filepaths.at(i));
		MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

		this->m_controller->BlockAllSignals(true);

		bool ok = this->m_model->LoadDatasetInDatabase(QDir::toNativeSeparators(filepaths.at(i)),
													   DataType::DTYPE_REFERENCE_VOLUME);
		slotReferenceListSelectionChanged();

		if (!ok) {
			this->m_controller->BlockAllSignals(false);
			this->m_controller->ThrowError("Import failed");
			return;
		}

		this->m_ui->navPanel->setEnabled(ok);
		this->m_ui->referenceBox->setEnabled(ok);

		this->m_controller->BlockAllSignals(false);

		this->m_controller->UpdateVisualNavControls();

		UpdateVisualsFromUi();

		this->m_visualization->ResetCamera();
		this->m_visualization->Render();
	}
}

void ReferenceViewController::UpdateVisualsFromUi() {

	QListWidgetItem* item = this->m_ui->referenceListWidget->currentItem();
	if (item == nullptr) {
		return;
	}
	QString label = item->text();

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(
		VisualType::VTYPE_ANATOMICAL_VOLUME, label);

	if (visualObj == nullptr) {
		return;
	}
	VisualSettingsVolume* newSettings = dynamic_cast<VisualSettingsVolume*>(visualObj->GetSettings());
	newSettings->isDirty = true;

	newSettings->visible = this->m_ui->referenceListWidget->currentItem()->checkState() == Qt::CheckState::Checked;
	newSettings->opacity = this->m_ui->referenceOpacitySlider->value() / 100.0;
	newSettings->colormap = ColorMap::fromString(this->m_ui->referenceColormapCombobox->currentText());

	visualObj->SetSettings(newSettings);

	this->m_visualization->Render();
}

void ReferenceViewController::slotReferenceListSelectionChanged() {

	QListWidgetItem* item = this->m_ui->referenceListWidget->currentItem();
	if (item == nullptr) {
		this->m_ui->referenceBox->setEnabled(false);
		return;
	}
	else {
		this->m_ui->referenceBox->setEnabled(true);
	}

	VisualizationObject* visualObj = this->m_visualization->GetVisualObject(VisualType::VTYPE_ANATOMICAL_VOLUME, item->text());
	VisualSettingsVolume* m_settings = dynamic_cast<VisualSettingsVolume*>(visualObj->GetSettings());

	this->m_controller->BlockAllSignals(true);

	// Opacity
	this->m_ui->referenceOpacitySlider->setValue(qRound(m_settings->opacity * 100.0));
	this->m_ui->referenceOpacityTextbox->setText(QString::number(qRound(m_settings->opacity * 100.0)));

	// Colormap
	this->m_ui->referenceColormapCombobox->setCurrentText(m_settings->colormap->toString());

	this->m_controller->BlockAllSignals(false);
}

void ReferenceViewController::slotReferenceListItemChanged(QListWidgetItem* item) {

	this->m_controller->BlockAllSignals(true);

	this->m_ui->referenceListWidget->clearSelection();
	this->m_ui->referenceListWidget->setCurrentItem(item);

	QString oldLabel = item->toolTip();
	QString newLabel = item->text();

	if (oldLabel != newLabel) {
		this->m_visualization->RenameObj(oldLabel, newLabel);
		item->setToolTip(newLabel);
	}

	this->m_controller->BlockAllSignals(false);

	slotReferenceListSelectionChanged();
	UpdateVisualsFromUi();
}

void ReferenceViewController::slotSliderTextChanged(const QString & text) {

	this->m_controller->BlockAllSignals(true);

	int value = text.toInt();;
	QSlider* slider = nullptr;
	QLineEdit* textbox = dynamic_cast<QLineEdit*> (sender());;

	// Control specific
	if (textbox == this->m_ui->referenceOpacityTextbox) {
		slider = this->m_ui->referenceOpacitySlider;
	}

	slider->setValue(value);
	this->m_controller->BlockAllSignals(false);

	// Check every range
	if (text.toInt() > slider->maximum()) {
		textbox->setText(QString::number(slider->maximum()));
		return;
	}
	if (text.toInt() < slider->minimum()) {
		textbox->setText(QString::number(slider->minimum()));
		return;
	}

	UpdateVisualsFromUi();
}

void ReferenceViewController::slotSliderValueChanged(int value) {

	this->m_controller->BlockAllSignals(true);

	QString text = QString::number(value);
	QSlider* slider = dynamic_cast<QSlider*> (sender());
	QLineEdit* textbox = nullptr;

	// Control specific
	if (slider == this->m_ui->referenceOpacitySlider) {
		textbox = this->m_ui->referenceOpacityTextbox;
	}

	textbox->setText(text);

	this->m_controller->BlockAllSignals(false);

	UpdateVisualsFromUi();
}

void ReferenceViewController::slotDeleteButtonTriggered() {

	QString label = this->m_ui->referenceListWidget->currentItem()->text();

	// Remove from UI
	for (int i = 0; i < this->m_ui->referenceListWidget->count(); i++) {
		if (this->m_ui->referenceListWidget->item(i)->text() == label) {
			delete this->m_ui->referenceListWidget->takeItem(i);
		}
	}

	this->m_model->RemoveObject(DataType::DTYPE_REFERENCE_VOLUME, label);
}

void ReferenceViewController::slotGlobalPanelChange() {

	UpdateVisualsFromUi();
}

void ReferenceViewController::slotColormapChanged(const QString & text) {

	UpdateVisualsFromUi();
}
