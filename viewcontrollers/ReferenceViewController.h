#pragma once

#include "viewcontrollers/ViewControllerBase.h"

#include "ui_View.h"

class Controller;
class Model;
class Visualization;

/* ViewController for the Reference visualization controls. */
class ReferenceViewController : public ViewControllerBase {
	Q_OBJECT

public:

	/**
	Constructor
	@param	controller			connection to main window UI
	*/
	ReferenceViewController(Controller* control,
						 Model* model,
						 Visualization* vis);
	
	// Virtual from base
	void UpdateVisualsFromUi();
	void AddObject(QString label, bool visible);
		
private:

	void setupControls();

	void connectSignals();

private slots:

	void slotReferenceListSelectionChanged();
	void slotReferenceLoadClicked();
	void slotReferenceListItemChanged(QListWidgetItem * item);
	void slotSliderTextChanged(const QString & text);
	void slotSliderValueChanged(int value);
	void slotDeleteButtonTriggered();
	void slotGlobalPanelChange();
	void slotColormapChanged(const QString & text);

};