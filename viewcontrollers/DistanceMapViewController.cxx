#include "DistanceMapViewController.h"

#include "Controller.h"
#include "Model.h"
#include "Visualization.h"

#include "viewcontrollers/ContourViewController.h"
#include "viewcontrollers/GlyphViewController.h"
#include "viewcontrollers/ScalarMapViewController.h"

#include "DataIOManager.h" // refractor

#include <QSettings>
#include <QFileInfo>
#include <QFileDialog>
#include <QDir>

DistanceMapViewController::DistanceMapViewController(
	Controller* control, Model* model, Visualization* vis) :
	ViewControllerBase(control, model, vis) {

	setupControls();
	connectSignals();
	
	this->contourController = new ContourViewController(control, model, vis);
	this->glyphController = new GlyphViewController(control, model, vis);
	this->scalarController = new ScalarMapViewController(control, model, vis);

}

void DistanceMapViewController::setupControls() {
	
	// Initial lock of all controls	
	this->m_ui->colorbarCheckBox->setEnabled(false);
	this->m_ui->outlineCheckBox->setEnabled(false);

	this->m_ui->contrastBox->setEnabled(false);
	this->m_ui->distanceMapVisOtherLabel->setEnabled(false);
	this->m_ui->distanceMapVisRenderLabel->setEnabled(false);

}

void DistanceMapViewController::connectSignals() {

	// IO
	connect(this->m_ui->deleteDistanceMapButton, SIGNAL(clicked()), 
			this, SLOT(slotDeleteDistanceMapButtonTriggered()));
	connect(this->m_ui->distanceMapLoadButton, SIGNAL(clicked()), 
			this, SLOT(slotDistanceMapLoadClicked()));
	connect(this->m_ui->distanceMapLoadBinaryButton, SIGNAL(clicked()), 
			this->m_controller, SLOT(slotLoadBinaryTriggered()));

	connect(this->m_ui->distanceMapCombobox, SIGNAL(currentIndexChanged(const QString &)),
			this, SLOT(slotMapSelectionChanged(const QString &)));
	
	// Colorbar
	connect(this->m_ui->colorbarCheckBox, SIGNAL(clicked()),
			this, SLOT(slotColorbarChange()));
	connect(this->m_ui->outlineCheckBox, SIGNAL(clicked()),
			this, SLOT(slotOutlineChange()));

	// Contrast
	connect(this->m_ui->contrastNoneRadiobutton, SIGNAL(clicked()),
			this, SLOT(slotContrastChange()));
	connect(this->m_ui->contrastPolyRadiobutton, SIGNAL(clicked()),
			this, SLOT(slotContrastChange()));
	connect(this->m_ui->contrastExpRadiobutton, SIGNAL(clicked()),
			this, SLOT(slotContrastChange()));
    connect(this->m_ui->contrastCostFuncRadiobutton, SIGNAL(clicked()),
            this, SLOT(slotContrastChange()));

	connect(this->m_ui->contrastFlipSignCheckbox, SIGNAL(clicked()),
			this, SLOT(slotContrastChange()));

	connect(this->m_ui->contrastSigmaLineEdit, SIGNAL(textChanged(const QString &)), 
			this, SLOT(slotSliderTextChanged(const QString &)));

	connect(this->m_ui->contrastSigmaSlider, SIGNAL(valueChanged(int)), 
			this, SLOT(slotSliderValueChanged(int)));

	connect(this->m_ui->contrastSharpnessLineEdit, SIGNAL(textChanged(const QString &)), 
			this, SLOT(slotSliderTextChanged(const QString &)));

	connect(this->m_ui->contrastSharpnessSlider, SIGNAL(valueChanged(int)), 
			this, SLOT(slotSliderValueChanged(int)));
}

void DistanceMapViewController::slotMapSelectionChanged(const QString & text) {

	if (text == "") {
		this->m_ui->distanceMapCombobox->setEnabled(false);
		return;
	}
	else {
		this->m_ui->distanceMapCombobox->setEnabled(true);
	}

	//// All others
	//for (int i = 0; i < this->m_ui->distanceMapCombobox->count(); i++) {

	//	QString itemText = this->m_ui->distanceMapCombobox->itemText(i);
	//	if (itemText == item) {
	//		continue;
	//	}

	//	VisualizationObject* visuals = this->m_visualization->GetVisualObject(VisualType::VTYPE_SCALARMAP, itemText);
	//	visuals->GetSettings()->visible = false;
	//	visuals->Update();

	//	VisualizationObject* visuals1 = this->m_visualization->GetVisualObject(VisualType::VTYPE_GLYPHS, itemText);
	//	visuals1->GetSettings()->visible = false;
	//	visuals1->Update();

	//	VisualizationObject* visuals2 = this->m_visualization->GetVisualObject(VisualType::VTYPE_CONTOURPLOT, itemText);
	//	visuals2->GetSettings()->visible = false;
	//	visuals2->Update();
	//}

	this->contourController->UpdateUiFromSettings(text);
	this->glyphController->UpdateUiFromSettings(text);
	this->scalarController->UpdateUiFromSettings(text);
	
}

void DistanceMapViewController::UpdateVisualsFromUi() {

	this->contourController->UpdateVisualsFromUi();
	this->glyphController->UpdateVisualsFromUi();
	this->scalarController->UpdateVisualsFromUi();

}

void DistanceMapViewController::AddObject(QString label, bool visible) {

	// Add
	this->m_ui->distanceMapCombobox->addItem(label);
    
    // Add to connectivity tab
    DistanceMapSource* dmap = static_cast<DistanceMapSource*>(this->m_model->findDataObjectByLabel(label));
    if(dmap != nullptr)
    {
        if(dmap->distanceMapType == DISTANCEMAP_FINSLER)
            this->m_ui->connectivityFinslerMapCombobox->addItem(label);
        else if(dmap->distanceMapType == DISTANCEMAP_EUCLIDEAN)
            this->m_ui->connectivityEuclideanMapCombobox->addItem(label);
    }
    
    this->m_visualization->ResetCamera();
}

void DistanceMapViewController::slotDeleteDistanceMapButtonTriggered() {

	DataType type = DataType::DTYPE_DISTANCEMAP;
	QString label = this->m_ui->distanceMapCombobox->currentText();

	// Remove from UI
	for (int i = 0; i < this->m_ui->distanceMapCombobox->count(); i++) {
		if (this->m_ui->distanceMapCombobox->itemText(i) == label) {
			this->m_ui->distanceMapCombobox->removeItem(i);
		}
	}

	this->m_model->RemoveObject(type, label);
}
void DistanceMapViewController::slotDistanceMapLoadClicked() {

	// Get file
	const QString DEFAULT_DIR_KEY("default_dir");
	QSettings MySettings;

	QString filepath = QFileDialog::getOpenFileName(this->m_controller, tr("Load distance map"),
													MySettings.value(DEFAULT_DIR_KEY).toString(),
													tr("NIfTI (*.nii* *.nii.gz *.hdr);;Any files (*)"));
	if (filepath.isEmpty()) {
		return;
	}

	QProgressDialog* dialog = this->m_controller->LoadingStart();

	QFileInfo fi(filepath);
	MySettings.setValue(DEFAULT_DIR_KEY, fi.absolutePath());

	filepath = QDir::toNativeSeparators(filepath);

	this->m_controller->BlockAllSignals(true);
	bool ok = this->m_model->LoadDatasetInDatabase(filepath, DataType::DTYPE_DISTANCEMAP);
	this->m_controller->BlockAllSignals(false);

	if (!ok) {
		this->m_controller->ThrowError("Import failed");
	}
	this->slotContrastChange();
	this->m_visualization->ResetCamera();

	this->m_controller->LoadingDone(dialog);
}

void DistanceMapViewController::slotContrastChange() {

	this->scalarController->UpdateVisualsFromUi();
	this->glyphController->UpdateVisualsFromUi();
	this->contourController->UpdateVisualsFromUi();

	this->m_visualization->Render();
}

void DistanceMapViewController::slotOutlineChange() {
	this->scalarController->UpdateVisualsFromUi();
}

void DistanceMapViewController::slotColorbarChange() {

	this->scalarController->UpdateVisualsFromUi();
	this->glyphController->UpdateVisualsFromUi();
	this->contourController->UpdateVisualsFromUi();
}

void DistanceMapViewController::slotSliderTextChanged(const QString & text) {

	this->m_controller->BlockAllSignals(true);

	int value = text.toInt();;
	QSlider* slider = nullptr;
	QLineEdit* textbox = dynamic_cast<QLineEdit*> (sender());;

	// Control specific
	if (textbox == this->m_ui->contrastSharpnessLineEdit) {
		slider = this->m_ui->contrastSharpnessSlider;
		value = log10(text.toDouble()) * 10.0;
	}
	else if (textbox == this->m_ui->contrastSigmaLineEdit) {
		slider = this->m_ui->contrastSigmaSlider;
		value = log10(text.toDouble()) * 10.0;
	}

	slider->setValue(value);
	this->m_controller->BlockAllSignals(false);

	// Check every range
	if (text.toInt() > slider->maximum()) {
		textbox->setText(QString::number(slider->maximum()));
		return;
	}
	if (text.toInt() < slider->minimum()) {
		textbox->setText(QString::number(slider->minimum()));
		return;
	}

	slotContrastChange();
}

void DistanceMapViewController::slotSliderValueChanged(int value) {

	this->m_controller->BlockAllSignals(true);

	QString text = QString::number(value);
	QSlider* slider = dynamic_cast<QSlider*> (sender());
	QLineEdit* textbox = nullptr;

	// Control specific
	if (slider == this->m_ui->contrastSharpnessSlider) {
		textbox = this->m_ui->contrastSharpnessLineEdit;
		text = QString::number(pow(10.0, value / 10.0), 10, 2);
	}
	else if (slider == this->m_ui->contrastSigmaSlider) {
		textbox = this->m_ui->contrastSigmaLineEdit;
		text = QString::number(pow(10.0, value / 10.0), 10, 2);
	}

	textbox->setText(text);

	this->m_controller->BlockAllSignals(false);

	slotContrastChange();
}
