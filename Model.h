#pragma once

/** Includes - Main Headers */
#include "common/Macros.h"

/** Includes - QT */
#include <QThread>
#include <QDebug>
#include <QFileSystemWatcher>
#include <QProgressDialog>

#include "processing/CodeLinkManager.h"

/** Forward declarations */
class Controller;
class Visualization;
class DataObjFactory;
class VisualObjFactory;
class FastMarchingLinker;
class ConnectivityMeasure;

class DataObject;

/**
Main data controller, that manages all data-related functionality.
This object manages the database of data objects, as well as the connect to the Data- and VisualFactories and 
the link to the external LFM code.
*/
class Model : public QObject {
	Q_OBJECT

public:

	enum LockType {
		GeodesicProjectionOfDistanceMap,
		NewManualRegionFromReference,
		AnyDatasetImported,
		DistanceMap,

	};

	QFileSystemWatcher* filewatcher;

	/** Functions */

	/**
	 * Model constructor
	 * @param    argumentList   List of command-line arguments
	 */
	Model(QMap<QString, std::string> argumentList);

	/**
	 * Model destructor
	 */
	~Model();

	void SetMVC(Controller* controller, Visualization* visualization) {
		this->m_controller = controller;
		this->m_visualization = visualization;
	}

	/**
	* Sets the proper initial state.
	*/
	void Setup();

	/**
	* Imports an indiviudal dataset of the given type at the given filepath.
	*/
	bool LoadDatasetInDatabase(QString path, DataType type);

	/**
	* Imports multiple datasets from the binary dataset at the given filepath.
	*/
	bool LoadFromBinaryFile(QString headerPath, QString dataPath);

	/**
	* Gets the extent of the current datasets, for use in the navigation controls
	*/
	void RequestNavigationExtent(std::vector<int>& extent);

	/**
	* Exports the current projectional volume to the given filepath.
	* The projection type is based on the Projection::ProjectionType.
	*/
	void ExportVolumeData(int projectiontype, QString filepath);

	/**
	* Shows the given file path in the OS native folder explorer, with optional selection of the file.
	*/
	static void ShowInExplorer(QString path, bool highlight = true);

	/** CodeLink **/

	/**
	* Sets up the CodeLink for an experiment with the given settings, command and speed map
	*/
	void SetupCode(QString runOption,
                   CodeLinkManager::CodeRunSettings m_settings,
                   QString command,
                   QString speedPath);
	
	/**
	* Runs the currently prepared experiment.
	*/
	void RunCode(QProgressDialog* progressDialog);
	
	/**
	* Abort the current execution of the external code.
	*/
	void AbortCode();

	/**
	* Opens the ITK-SNAP app with the existing data, based on the given command.
	*/
	void OpenITKSnap(QString cmd, QString dataLabelRoi);

	/**
	* Check if the given locking has to be enforced, based in the current datasets.
	*/
	bool CheckLocking(LockType type);
	
	/**
	* Compute the connectivity between the given regions
	*/
    void ComputeConnectivity(QString regionA,
                             QString regionB,
                             QString finslerMap,
                             QString euclideanMap);

	/** Data objects **/	

	/**
	* Adds the given object to the database, while checking the label for duplicates.
	*/
	void AddToDataObjects(DataObject * dataObj);

	/**
	* Remove the dataobject from the database, including all the related visual objects
	*/
	void RemoveObject(DataType type, QString label = "");

	/**
	* Reimports the dataset at the given path.
	*/
	void ReimportDatasetAt(QString path);
    
    // Lookup function for data objects
    DataObject* findDataObjectByLabel(QString label);
    
    /**
     *     Minimal function to generate Lifted Fast Marching datafiles
     */
    void GenerateFMFilesOnly(
                             // path settings
                             QString experimentName,
                             QString workFolder,
                             
                             // code parameters
                             double xi,
                             double epsilon,
                             double sigma,
                             double p,
                             double gridScale,
                             QString bundleType,
                             QString bundleMesh,
                             bool exportValuesLift,
                             bool exportValuesEucl,
                             bool applyFODTransformation,
                             bool transposeCoordinates,
                             bool stopWhenTipsReached,
                             
                             QString speedPath,
                             QString seedPath,
                             QString tipPath);

private:

	// Create VisualObjects for the given DataObject.
	bool createVisualsFor(DataObject* obj);
	
	/** Variables */

	// Controller
	Controller* m_controller;

	// Database
	DataObjFactory* m_dataFactory;
	VisualObjFactory* m_visualFactory;
	FastMarchingLinker* m_linker;

	// Visualization
	Visualization* m_visualization;
    
    // Processing
    ConnectivityMeasure* m_connectivity;

	// Command line parameter list
	QMap<QString, std::string> argumentList;

	// Processing threads
	QThread* m_thread;
	CodeLinkManager* m_codeLink;

	// DataObj db
	QList<DataObject*> m_dataObjects;


private slots:

	void codeRunComplete();

};
