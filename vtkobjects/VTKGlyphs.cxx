/** Includes */
#include "VTKGlyphs.h"

#include <QDir>

#include <vtkDelaunay3D.h>
#include <vtkGeometryFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPointData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkProp3DCollection.h>
#include <vtkProp.h>
#include <vtkOutlineFilter.h>
#include <vtkTextProperty.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>

#include <vtkPolyDataNormals.h>
#include <vtkOpenGLPolyDataMapper.h>
#include <vtkCommand.h>
#include "vtkOpenGLVertexBufferObject.h"
#include "vtkOpenGLIndexBufferObject.h"
#include "vtkOpenGLVertexArrayObject.h"
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkLookupTable.h>
#include <vtkShaderProgram.h>
#include <vtkDepthSortPolyData.h>
#include <vtkCamera.h>

#include "DistanceMapSource.h"

#include "common/ColorMap.h"

#include <cmath>

//const double GYLPH_SPACING = 1.0;
//const double GLYPH_MARGIN = 0.1;
const double GLYPH_MAX_RADIUS = 0.4;// GYLPH_SPACING / 2 - GLYPH_MARGIN / 2;
const double GLYPH_MIN_RADIUS = 0.1;

class vtkShaderCallback : public vtkCommand {
public:
	static vtkShaderCallback *New() {
		return new vtkShaderCallback;
	}
	vtkRenderer *Renderer;
	bool init;
	SP(vtkLookupTable) m_colormapping;

	VTKGlyphs* parent;

	int x;
	int y;
	int z;

	virtual void Execute(vtkObject *, unsigned long, void*cbo) {
		vtkOpenGLHelper *cellBO = reinterpret_cast<vtkOpenGLHelper*>(cbo);
		BuildActor(cellBO);
	}

	void BuildActor(vtkOpenGLHelper* cellBO) {

		float maxRadius = GLYPH_MAX_RADIUS;
		cellBO->Program->SetUniformf("maxRadiusUniform", maxRadius);
		float minRadius = GLYPH_MIN_RADIUS;
		cellBO->Program->SetUniformf("minRadiusUniform", minRadius);

		if (init) {
			return;
		}

		VTK_CREATE(vtkFloatArray, dataArray);
		dataArray->SetNumberOfComponents(1);
		VTK_CREATE(vtkUnsignedCharArray, colorArray);
		colorArray->SetNumberOfComponents(3);

		for (size_t i = 0; i < parent->contrastData->GetNumberOfScalarComponents(); i++) {

			// This has to become a value between 0 and 1
			double* value = static_cast<double*>(parent->contrastData->GetScalarPointer(x, y, z));

			double scaled = (value[i] - this->parent->min) /
				(this->parent->max - this->parent->min);

			dataArray->InsertValue(i, static_cast<float>(scaled));

			unsigned char* color = m_colormapping->MapValue(value[i]);
			colorArray->InsertTypedTuple(i, color);
		}

		// Scaling
		vtkOpenGLVertexBufferObject* vbo1 = vtkOpenGLVertexBufferObject::New();
        vbo1->SetDataType(VTK_FLOAT);
//        vbo1->UploadDataArray(dataArray);
		//vbo1->InitVBO(dataArray, VTK_FLOAT);

		vbo1->UploadDataArray(dataArray);
		vbo1->Bind();

		cellBO->VAO->AddAttributeArray(cellBO->Program, vbo1, "value", 0, false);
		cellBO->VAO->Bind();

		// Color
		vtkOpenGLVertexBufferObject* vbo2 = vtkOpenGLVertexBufferObject::New();
        vbo2->SetDataType(VTK_UNSIGNED_CHAR);
//        vbo2->InitVBO(colorArray, VTK_UNSIGNED_CHAR);

		vbo2->UploadDataArray(colorArray);
		vbo2->Bind();

		cellBO->VAO->AddAttributeArray(cellBO->Program, vbo2, "color", 0, true);
		cellBO->VAO->Bind();

		init = true;
	}

	vtkShaderCallback() {
		this->Renderer = 0;
		this->init = false;

		this->m_colormapping = nullptr;
		this->parent = nullptr;

		this->x = 0;
		this->y = 0;
		this->z = 0;
	}
};

VTKGlyphs* VTKGlyphs::CreateWithDataObject(DataObject* DataObject) {
	VTKGlyphs *pRet = new (std::nothrow) VTKGlyphs();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

bool VTKGlyphs::initWithDataObject(DataObject* dataObject) {
	this->m_dataObject = dataObject;

	this->type = VisualType::VTYPE_GLYPHS;

	this->colorbar = SP(vtkScalarBarActor)::New();
	this->colorbar->SetVisibility(false);
	this->colorbar->SetTitle("Glyphs");

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->SetVisibility(false);

	this->m_visuals3D = SP(vtkAssembly)::New();
	this->m_visuals3D->SetVisibility(false);

	this->m_colormapping = SP(vtkLookupTable)::New();

	// Default values
	this->m_settings = new VisualSettingsGlyphs();
	this->m_settings->label = dataObject->label;;

	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);
	this->m_dims[0] = mapping->data->GetDimensions()[0];
	this->m_dims[1] = mapping->data->GetDimensions()[1];
	this->m_dims[2] = mapping->data->GetDimensions()[2];

	this->contrastData = CalculateContrast(dynamic_cast <DistanceMapSource*>(this->m_dataObject)->data,
										   this->m_settings->contrastFunction);
	this->updateCustomRange();

	this->buildAllActors();

	this->Update();

	this->setPlane(this->m_settings->positionSettings);

	this->hideAllGlyphs();

	//TODO: add check on correct import
	return true;
}

void VTKGlyphs::buildAllActors() {

	this->hideAllGlyphs();

	// Load template
	VTK_CREATE(vtkPolyDataReader, loader);
#ifdef __APPLE__
	loader->SetFileName("/Users/stephan/Research/Compiled/FastMarchingTool/resources/icosahedron3.vtk");
#else
	loader->SetFileName("C:\\Users\\admin_stefan\\Desktop\\KH\\_data\\fast marching\\icosahedron3.vtk");
#endif
	loader->Update();

	VTK_CREATE(vtkPolyDataNormals, norms);
	norms->SetInputConnection(loader->GetOutputPort());
	norms->Update();

	VTK_CREATE(vtkOpenGLPolyDataMapper, templateMapper);
	templateMapper->SetInputConnection(norms->GetOutputPort());

	// -- Shader stuff --
	templateMapper->SetVertexShaderCode(

		// --- Example ---
		"//VTK::System::Dec\n"  // always start with this line
		"//VTK::Normal::Dec\n"

		"attribute vec4 vertexMC;\n"
		"attribute float value;\n"
		"attribute vec3 color;\n"
		"varying vec3 finalColor;\n"

		"uniform mat4 MCDCMatrix;\n"
		"uniform float minRadiusUniform;\n"
		"uniform float maxRadiusUniform;\n"

		"void main () {\n"
		"	finalColor = color;\n"
		"	normalVCVSOutput = normalMatrix * normalMC;\n"
		"	float scaling = minRadiusUniform + value * (maxRadiusUniform - minRadiusUniform);\n"
		" gl_Position = MCDCMatrix * vec4(vertexMC[0] * scaling, vertexMC[1]*scaling, vertexMC[2]*scaling, vertexMC[3]);\n"
		"}\n"
	);

	templateMapper->SetFragmentShaderCode(

		// --- Example ---
		"//VTK::System::Dec\n"  // always start with this line
		"//VTK::Output::Dec\n"  // always have this line in your FS
		"varying vec3 finalColor;\n"
		"varying vec3 normalVCVSOutput;\n"

		"void main () {\n"
		"  float df = max(0.0, normalVCVSOutput.z);\n"
		"  float sf = pow(df, 20.0);\n"
		"  vec3 diffuse = df * finalColor;\n"
		"  vec3 specular = sf * finalColor;\n"
		"  gl_FragData[0] = vec4(0.3*finalColor + 0.7*diffuse + 0.0*specular, 1.0);\n"
		"}\n"
	);

	std::vector<std::vector<std::vector<SP(vtkActor)>>>
		temp(m_dims[0], std::vector<std::vector<SP(vtkActor)> >(m_dims[1], std::vector<SP(vtkActor)>(m_dims[2], 0)));

	this->m_actors.clear();
	this->m_actors = temp;

	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {

				this->m_actors[x][y][z] = buildGlyphActor(x, y, z, templateMapper);
			}
		}
	}

	this->setPlane(this->m_settings->positionSettings);
}

void VTKGlyphs::ReloadFromSource() {
}

SP(vtkAssembly) VTKGlyphs::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

SP(vtkActor) VTKGlyphs::buildGlyphActor(int x, int y, int z,
										vtkOpenGLPolyDataMapper* templateMapper) {

	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);
	int numComponents = contrastData->GetNumberOfScalarComponents();

	std::vector<double> values(numComponents, NAN);

	for (int i = 0; i < numComponents; i++) {
		double* pixel = static_cast<double*>(contrastData->GetScalarPointer(x, y, z));
		values[i] = pixel[i];
	}

	float imageCoord[3] = { static_cast<float>(x),static_cast<float>(y),static_cast<float>(z) };
	float* worldCoord = mapping->transform->TransformFloatPoint(imageCoord);

	VTK_CREATE(vtkOpenGLPolyDataMapper, newMapper);
	newMapper->ShallowCopy(templateMapper);

	// Setup a callback to change some uniforms
	VTK_CREATE(vtkShaderCallback, myCallback);
	myCallback->parent = this;
	myCallback->x = x;
	myCallback->y = y;
	myCallback->z = z;
	myCallback->m_colormapping = this->m_colormapping;

	VTK_CREATE(vtkActor, actor);
	actor->SetMapper(newMapper);
	actor->SetPosition(worldCoord[0], worldCoord[1], worldCoord[2]);

	newMapper->AddObserver(vtkCommand::UpdateShaderEvent, myCallback);

	return actor;
}

void VTKGlyphs::setPlane(PositionSettings posSettings) {

	hideAllGlyphs();

	switch (posSettings.orient) {
	case SAGITTAL:
		for (size_t z = 0; z < m_dims[2]; z++) {
			for (size_t y = 0; y < m_dims[1]; y++) {
				this->m_visuals3D->AddPart(this->m_actors[posSettings.position][y][z]);
			}
		}
		break;
	case CORONAL:
		for (size_t z = 0; z < m_dims[2]; z++) {
			for (size_t x = 0; x < m_dims[0]; x++) {
				this->m_visuals3D->AddPart(this->m_actors[x][posSettings.position][z]);
			}
		}
		break;
	case AXIAL:
		for (size_t y = 0; y < m_dims[1]; y++) {
			for (size_t x = 0; x < m_dims[0]; x++) {
				this->m_visuals3D->AddPart(this->m_actors[x][y][posSettings.position]);
			}
		}
		break;
	default:
		break;
	}
}

void VTKGlyphs::ShowPoint(int x, int y, int z) {

	this->m_visuals3D->AddPart(this->m_actors[x][y][z]);
}

void VTKGlyphs::setColorMap(ColorMap* map) {

	this->m_colormapping = map->CreateLookupTable(this->min, this->max);

	buildAllActors();

	// Colorbar
	this->colorbar->SetNumberOfLabels(3);
	this->colorbar->SetLabelFormat("%4.1f");
	this->colorbar->SetLookupTable(this->m_colormapping);

	this->colorbar->SetAnnotationTextScaling(0);
	this->colorbar->GetLabelTextProperty()->SetFontSize(10);
}

void VTKGlyphs::Update() {

	bool rebuild = true;

	this->m_visuals3D->SetVisibility(m_settings->visible);

	if (!m_settings->visible) {
		return;
	}

	if (this->m_settings->contrastFunction.isDirty) {
		this->contrastData = CalculateContrast(dynamic_cast <DistanceMapSource*>(this->m_dataObject)->data,
											   this->m_settings->contrastFunction);
		this->updateCustomRange();

		this->setColorMap(m_settings->colormap);

		this->m_settings->contrastFunction.isDirty = false;
		rebuild = false;
		std::cout << "Update Glyphs: data" << std::endl;
	}

	if (this->m_settings->positionSettings.isDirty) {
		this->setPlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
		std::cout << "Update Glyphs: nav" << std::endl;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	std::cout << "Update Glyphs" << std::endl;

	// Update visuals
	if (rebuild) {
		this->setColorMap(m_settings->colormap);
		this->setPlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
	}

	this->colorbar->SetVisibility(m_settings->visible && m_settings->showColorbar);

	this->m_settings->isDirty = false;
}

void VTKGlyphs::hideAllGlyphs() {

	vtkProp3DCollection* collec = this->m_visuals3D->GetParts();
	while (collec->GetNumberOfItems() > 0) {
		this->m_visuals3D->RemovePart(collec->GetLastProp3D());
		collec = this->m_visuals3D->GetParts();;
	}
}

void VTKGlyphs::updateCustomRange() {

	// Calculate range:
	this->min = +1e99;
	this->max = -1e99;
	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {
				for (int i = 0; i < contrastData->GetNumberOfScalarComponents(); i++) {

					double* pixel = static_cast<double*>(contrastData->GetScalarPointer(x, y, z));
					if (pixel[i] < this->min) {
						this->min = pixel[i];
					}
					if (pixel[i] > this->max) {
						this->max = pixel[i];
					}
				}
			}
		}
	}
}
