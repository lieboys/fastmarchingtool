#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"

#include "common/Macros.h"

#include <vtkSmartPointer.h>

#include <vector>

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

class ColorMap;

/** Includes - VTK */
class vtkScalarBarActor;
class vtkImageSlice;
class vtkPlane;

class VTKScalarProjection : public VisualizationObject {
public:

	/** Functions */

	/** Constructor from data object. */
	static VTKScalarProjection* CreateWithDataObject(DataObject* dataObject);

	SP(vtkAssembly) GetVisuals(int dim);

	void Update();

	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsScalarProjection* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsScalarProjection*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsScalarProjection*>(newSettings));
		}
	}

	/* Sets the data responsible for the geodesic projection. */
	void SetOrientationData(vtkPolyData * data);

	/* Gets the data for export, based on the given projection option */
	SP(vtkImageData) GetExportData(Projection::ProjectionType type);

	/* Object of this VisualObject's colorbar */
	SP(vtkScalarBarActor) colorbar;

private:

	/** Functions */

	bool initWithDataObject(DataObject* dataObject);

	void ReloadFromSource();

	void setPlane(PositionSettings posSettings);

	void buildVisuals();
	void buildData();

	void setColorMap(ColorMap* map);

	/** Variables */

	int m_dims[3];

	SP(vtkImageData) m_projectionData;
	QList<QPair<double*, int>> m_geoOrientationData;
	SP(vtkImageData) m_finalData;

	SP(vtkImageSlice) m_imageSlice;
	SP(vtkPlane) m_plane;

	SP(vtkActor) m_outline;

	Orientation m_orientation;

	VisualSettingsScalarProjection* m_settings;
};
