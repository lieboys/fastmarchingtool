#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"
#include "common/Macros.h"

/** Includes - VTK */
#include <vtkPolyData.h>

#include <vtkActor.h>
#include <vtkAssembly.h>
#include <vtkScalarBarActor.h>
#include <vtkSmartPointer.h>

class ColorMap;
class vtkOpenGLPolyDataMapper;

class VTKGlyphs : public VisualizationObject {
public:

	/* Constructor from data object.*/
	static VTKGlyphs* CreateWithDataObject(DataObject* dataObject);

	void ReloadFromSource();

	SP(vtkAssembly) GetVisuals(int dim);

	void Update();

	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsGlyphs* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsGlyphs*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsGlyphs*>(newSettings));
		}
	}

	/* Shows the glyph on the given point. */
	void ShowPoint(int x, int y, int z);

	/* Minimum of the data range */
	double min;

	/* Maximum of the data range */
	double max;

	/* Object of this VisualObject's colorbar */
	SP(vtkScalarBarActor) colorbar;

	/* The image data after the ContrastFunction has been applied. */
	SP(vtkImageData) contrastData;

private:

	/** Functions */
	bool initWithDataObject(DataObject* dataObject);

	void buildAllActors();

	SP(vtkActor) buildGlyphActor(int x, int y, int z, vtkOpenGLPolyDataMapper* templateMapper);

	void setPlane(PositionSettings posSettings);

	void hideAllGlyphs();

	void updateCustomRange();

	void setColorMap(ColorMap* map);

	/** Variables */

	std::vector<std::vector<std::vector<SP(vtkActor)>>> m_actors;

	int m_dims[3];

	SP(vtkPolyData) m_meshTemplate;

	SP(vtkActor)  m_outline;

	VisualSettingsGlyphs* m_settings;

	SP(vtkLookupTable) m_colormapping;
};
