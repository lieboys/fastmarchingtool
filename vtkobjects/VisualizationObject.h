#pragma once

/** Includes - Main Headers */
#include <vector>

#include <QString>
#include <QPair>
#include <QColor>

#include <vtkAssembly.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include "common/Macros.h"
#include "common/ColorMap.h"

class DataObject;
class ColorMap;

class vtkImageData;

/* Default orientations for full orientations (162)*/
std::vector<std::vector<double>> const ORIENTATIONS_FULL = { { 0, 0, 1. },{ 0.07760890225389615, 0.2388556408050596, 0.9679487802288659 },{ -0.20318274393026847, 0.14762090442216314, 0.9679487802288661 },{ -0.1381966011250105, 0.42532540417601994, 0.8944271909999157 },{ -0.42532540417602, 0.3090169943749474, 0.8506508083520399 },{ -0.36180339887498947, 0.587785252292473, 0.7236067977499789 },{ -0.6095482317908053, 0.4428627132664893, 0.6575131712132815 },{ -0.531939329536909, 0.6817183540715489, 0.5022953667054891 },{ -0.7236067977499789, 0.5257311121191336, 0.4472135954999579 },{ 0.16245984811645317, 0.5, 0.8506508083520399 },{ -0.052786404500042065, 0.6881909602355867, 0.7236067977499789 },{ -0.2628655560595668, 0.8090169943749473, 0.5257311121191336 },{ 0.23282670676168846, 0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, 0.8641878268373419, 0.5022953667054891 },{ 0.27639320225002106, 0.8506508083520399, 0.4472135954999579 },{ -0.20318274393026847, -0.14762090442216314, 0.9679487802288661 },{ -0.4472135954999579, 0, 0.8944271909999157 },{ -0.42532540417602, -0.3090169943749474, 0.8506508083520399 },{ -0.6708203932499369, -0.16245984811645314, 0.7236067977499789 },{ -0.6095482317908053, -0.4428627132664893, 0.6575131712132815 },{ -0.8127309757210738, -0.2952418088443262, 0.5022953667054891 },{ -0.7236067977499789, -0.5257311121191336, 0.4472135954999579 },{ -0.6708203932499369, 0.16245984811645314, 0.7236067977499789 },{ -0.85065080835204, 0, 0.5257311121191336 },{ -0.8127309757210738, 0.2952418088443262, 0.5022953667054891 },{ 0.07760890225389615, -0.2388556408050596, 0.9679487802288659 },{ -0.1381966011250105, -0.42532540417601994, 0.8944271909999157 },{ 0.16245984811645317, -0.5, 0.8506508083520399 },{ -0.052786404500042065, -0.6881909602355867, 0.7236067977499789 },{ 0.23282670676168846, -0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, -0.8641878268373419, 0.5022953667054891 },{ 0.27639320225002106, -0.8506508083520399, 0.4472135954999579 },{ -0.36180339887498947, -0.587785252292473, 0.7236067977499789 },{ -0.2628655560595668, -0.8090169943749473, 0.5257311121191336 },{ -0.531939329536909, -0.6817183540715489, 0.5022953667054891 },{ 0.2511476833527446, 0, 0.9679487802288661 },{ 0.36180339887498947, -0.2628655560595668, 0.8944271909999157 },{ 0.5257311121191336, 0, 0.8506508083520399 },{ 0.6381966011250104, -0.2628655560595668, 0.7236067977499789 },{ 0.7534430500582336, 0, 0.6575131712132815 },{ 0.8310519523121299, -0.23885564080505955, 0.5022953667054891 },{ 0.8944271909999159, 0, 0.4472135954999579 },{ 0.44721359549995787, -0.5257311121191336, 0.7236067977499789 },{ 0.6881909602355868, -0.5, 0.5257311121191336 },{ 0.48397439011443294, -0.7165669224151788, 0.5022953667054891 },{ 0.36180339887498947, 0.2628655560595668, 0.8944271909999157 },{ 0.44721359549995787, 0.5257311121191336, 0.7236067977499789 },{ 0.48397439011443294, 0.7165669224151788, 0.5022953667054891 },{ 0.6381966011250104, 0.2628655560595668, 0.7236067977499789 },{ 0.6881909602355868, 0.5, 0.5257311121191336 },{ 0.8310519523121299, 0.23885564080505955, 0.5022953667054891 },{ 0.7236067977499789, -0.5257311121191336, -0.4472135954999579 },{ 0.531939329536909, -0.6817183540715489, -0.5022953667054891 },{ 0.6095482317908053, -0.4428627132664893, -0.6575131712132815 },{ 0.36180339887498947, -0.587785252292473, -0.7236067977499789 },{ 0.42532540417602, -0.3090169943749474, -0.8506508083520399 },{ 0.1381966011250105, -0.42532540417601994, -0.8944271909999157 },{ 0.20318274393026847, -0.14762090442216314, -0.9679487802288661 },{ -0.07760890225389615, -0.2388556408050596, -0.9679487802288659 },{ 0, 0, -1. },{ 0.2628655560595668, -0.8090169943749473, -0.5257311121191336 },{ 0.052786404500042065, -0.6881909602355867, -0.7236067977499789 },{ -0.16245984811645317, -0.5, -0.8506508083520399 },{ -0.02964396283142001, -0.8641878268373419, -0.5022953667054891 },{ -0.23282670676168846, -0.7165669224151787, -0.6575131712132815 },{ -0.27639320225002106, -0.8506508083520399, -0.4472135954999579 },{ 0.7236067977499789, 0.5257311121191336, -0.4472135954999579 },{ 0.8127309757210738, 0.2952418088443262, -0.5022953667054891 },{ 0.6095482317908053, 0.4428627132664893, -0.6575131712132815 },{ 0.6708203932499369, 0.16245984811645314, -0.7236067977499789 },{ 0.42532540417602, 0.3090169943749474, -0.8506508083520399 },{ 0.4472135954999579, 0, -0.8944271909999157 },{ 0.20318274393026847, 0.14762090442216314, -0.9679487802288661 },{ 0.85065080835204, 0, -0.5257311121191336 },{ 0.6708203932499369, -0.16245984811645314, -0.7236067977499789 },{ 0.8127309757210738, -0.2952418088443262, -0.5022953667054891 },{ -0.27639320225002106, 0.8506508083520399, -0.4472135954999579 },{ -0.02964396283142001, 0.8641878268373419, -0.5022953667054891 },{ -0.23282670676168846, 0.7165669224151787, -0.6575131712132815 },{ 0.052786404500042065, 0.6881909602355867, -0.7236067977499789 },{ -0.16245984811645317, 0.5, -0.8506508083520399 },{ 0.1381966011250105, 0.42532540417601994, -0.8944271909999157 },{ -0.07760890225389615, 0.2388556408050596, -0.9679487802288659 },{ 0.2628655560595668, 0.8090169943749473, -0.5257311121191336 },{ 0.36180339887498947, 0.587785252292473, -0.7236067977499789 },{ 0.531939329536909, 0.6817183540715489, -0.5022953667054891 },{ -0.8944271909999159, 0, -0.4472135954999579 },{ -0.8310519523121299, 0.23885564080505955, -0.5022953667054891 },{ -0.7534430500582336, 0, -0.6575131712132815 },{ -0.6381966011250104, 0.2628655560595668, -0.7236067977499789 },{ -0.5257311121191336, 0, -0.8506508083520399 },{ -0.36180339887498947, 0.2628655560595668, -0.8944271909999157 },{ -0.2511476833527446, 0, -0.9679487802288661 },{ -0.6881909602355868, 0.5, -0.5257311121191336 },{ -0.44721359549995787, 0.5257311121191336, -0.7236067977499789 },{ -0.48397439011443294, 0.7165669224151788, -0.5022953667054891 },{ -0.48397439011443294, -0.7165669224151788, -0.5022953667054891 },{ -0.44721359549995787, -0.5257311121191336, -0.7236067977499789 },{ -0.36180339887498947, -0.2628655560595668, -0.8944271909999157 },{ -0.6881909602355868, -0.5, -0.5257311121191336 },{ -0.6381966011250104, -0.2628655560595668, -0.7236067977499789 },{ -0.8310519523121299, -0.23885564080505955, -0.5022953667054891 },{ 0.1552178045077923, 0.9554225632202383, 0.25114768335274457 },{ -0.1381966011250105, 0.9510565162951535, 0.276393202250021 },{ -0.4472135954999579, 0.8506508083520399, 0.276393202250021 },{ -0.6871571340447014, 0.6817183540715489, 0.25114768335274457 },{ 0, 1., 0 },{ -0.3090169943749474, 0.9510565162951535, 0 },{ -0.5877852522924731, 0.8090169943749473, 0 },{ -0.1552178045077923, 0.9554225632202383, -0.25114768335274457 },{ -0.4360094506919568, 0.8641878268373419, -0.25114768335274457 },{ -0.8606959151435498, 0.4428627132664894, 0.2511476833527446 },{ -0.9472135954999581, 0.1624598481164532, 0.2763932022500211 },{ -0.9472135954999581, -0.1624598481164532, 0.2763932022500211 },{ -0.8606959151435498, -0.4428627132664894, 0.2511476833527446 },{ -0.9510565162951535, 0.3090169943749474, 0 },{ -0.9999999999999999, 0, 0 },{ -0.9510565162951535, -0.3090169943749474, 0 },{ -0.9566257939885021, 0.1476209044221631, -0.25114768335274457 },{ -0.9566257939885021, -0.1476209044221631, -0.25114768335274457 },{ -0.6871571340447014, -0.6817183540715489, 0.25114768335274457 },{ -0.4472135954999579, -0.8506508083520399, 0.276393202250021 },{ -0.1381966011250105, -0.9510565162951535, 0.276393202250021 },{ 0.1552178045077923, -0.9554225632202383, 0.25114768335274457 },{ -0.5877852522924731, -0.8090169943749473, 0 },{ -0.3090169943749474, -0.9510565162951535, 0 },{ 0, -1., 0 },{ -0.4360094506919568, -0.8641878268373419, -0.25114768335274457 },{ -0.1552178045077923, -0.9554225632202383, -0.25114768335274457 },{ 0.4360094506919568, -0.8641878268373419, 0.25114768335274457 },{ 0.6708203932499369, -0.6881909602355867, 0.276393202250021 },{ 0.8618033988749894, -0.42532540417601994, 0.276393202250021 },{ 0.9566257939885021, -0.1476209044221631, 0.25114768335274457 },{ 0.5877852522924731, -0.8090169943749473, 0 },{ 0.8090169943749475, -0.587785252292473, 0 },{ 0.9510565162951535, -0.3090169943749474, 0 },{ 0.6871571340447014, -0.6817183540715489, -0.25114768335274457 },{ 0.8606959151435498, -0.4428627132664894, -0.2511476833527446 },{ 0.9566257939885021, 0.1476209044221631, 0.25114768335274457 },{ 0.8618033988749894, 0.42532540417601994, 0.276393202250021 },{ 0.6708203932499369, 0.6881909602355867, 0.276393202250021 },{ 0.4360094506919568, 0.8641878268373419, 0.25114768335274457 },{ 0.9510565162951535, 0.3090169943749474, 0 },{ 0.8090169943749475, 0.587785252292473, 0 },{ 0.5877852522924731, 0.8090169943749473, 0 },{ 0.8606959151435498, 0.4428627132664894, -0.2511476833527446 },{ 0.6871571340447014, 0.6817183540715489, -0.25114768335274457 },{ 0.4472135954999579, -0.8506508083520399, -0.276393202250021 },{ 0.1381966011250105, -0.9510565162951535, -0.276393202250021 },{ 0.3090169943749474, -0.9510565162951535, 0 },{ 0.9472135954999581, 0.1624598481164532, -0.2763932022500211 },{ 0.9472135954999581, -0.1624598481164532, -0.2763932022500211 },{ 1., 0, 0 },{ 0.1381966011250105, 0.9510565162951535, -0.276393202250021 },{ 0.4472135954999579, 0.8506508083520399, -0.276393202250021 },{ 0.3090169943749474, 0.9510565162951535, 0 },{ -0.8618033988749894, 0.42532540417601994, -0.276393202250021 },{ -0.6708203932499369, 0.6881909602355867, -0.276393202250021 },{ -0.8090169943749475, 0.587785252292473, 0 },{ -0.6708203932499369, -0.6881909602355867, -0.276393202250021 },{ -0.8618033988749894, -0.42532540417601994, -0.276393202250021 },{ -0.8090169943749475, -0.587785252292473, 0 } };

/* Default orientations for projective orientations (81)*/
std::vector<std::vector<double>> const ORIENTATIONS_PROJECTIVE = { { 0., 0., 1. },{ 0.07760890225389615, 0.2388556408050596, 0.967948780228866 },{ 0.1624598481164532, 0.5, 0.85065080835204 },{ 0.2328267067616884, 0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, 0.864187826837342, 0.5022953667054892 },{ 0.2763932022500211, 0.85065080835204, 0.447213595499958 },{ 0.07760890225389615, -0.2388556408050596, 0.967948780228866 },{ 0.1624598481164532, -0.5, 0.85065080835204 },{ 0.2328267067616884, -0.7165669224151787, 0.6575131712132815 },{ 0.2763932022500211, -0.85065080835204, 0.447213595499958 },{ 0.02964396283142001, -0.864187826837342, 0.5022953667054892 },{ 0.2511476833527446, 0., 0.967948780228866 },{ 0.5257311121191336, 0., 0.85065080835204 },{ 0.7534430500582337, 0., 0.6575131712132815 },{ 0.894427190999916, 0., 0.447213595499958 },{ 0.3618033988749895, -0.2628655560595668, 0.894427190999916 },{ 0.6381966011250104, -0.2628655560595668, 0.723606797749979 },{ 0.83105195231213, -0.2388556408050595, 0.5022953667054892 },{ 0.4472135954999579, -0.5257311121191336, 0.723606797749979 },{ 0.6881909602355868, -0.5, 0.5257311121191336 },{ 0.4839743901144329, -0.7165669224151788, 0.5022953667054892 },{ 0.3618033988749895, 0.2628655560595668, 0.894427190999916 },{ 0.4472135954999579, 0.5257311121191336, 0.723606797749979 },{ 0.4839743901144329, 0.7165669224151788, 0.5022953667054892 },{ 0.6381966011250104, 0.2628655560595668, 0.723606797749979 },{ 0.6881909602355868, 0.5, 0.5257311121191336 },{ 0.83105195231213, 0.2388556408050595, 0.5022953667054892 },{ 0.723606797749979, -0.5257311121191336, -0.447213595499958 },{ 0.6095482317908053, -0.4428627132664893, -0.6575131712132815 },{ 0.42532540417602, -0.3090169943749474, -0.85065080835204 },{ 0.2031827439302685, -0.1476209044221631, -0.967948780228866 },{ 0.5319393295369091, -0.6817183540715489, -0.5022953667054892 },{ 0.3618033988749895, -0.587785252292473, -0.723606797749979 },{ 0.1381966011250105, -0.4253254041760199, -0.894427190999916 },{ 0.2628655560595668, -0.809016994374947, -0.5257311121191336 },{ 0.05278640450004206, -0.6881909602355867, -0.723606797749979 },{ 0.723606797749979, 0.5257311121191336, -0.447213595499958 },{ 0.6095482317908053, 0.4428627132664893, -0.6575131712132815 },{ 0.42532540417602, 0.3090169943749474, -0.85065080835204 },{ 0.2031827439302685, 0.1476209044221631, -0.967948780228866 },{ 0.812730975721074, 0.2952418088443262, -0.5022953667054892 },{ 0.6708203932499369, 0.1624598481164532, -0.723606797749979 },{ 0.447213595499958, 0., -0.894427190999916 },{ 0.85065080835204, 0., -0.5257311121191336 },{ 0.6708203932499369, -0.1624598481164532, -0.723606797749979 },{ 0.812730975721074, -0.2952418088443262, -0.5022953667054892 },{ 0.05278640450004206, 0.6881909602355867, -0.723606797749979 },{ 0.1381966011250105, 0.4253254041760199, -0.894427190999916 },{ 0.2628655560595668, 0.809016994374947, -0.5257311121191336 },{ 0.3618033988749895, 0.587785252292473, -0.723606797749979 },{ 0.5319393295369091, 0.6817183540715489, -0.5022953667054892 },{ 0.1552178045077923, 0.955422563220238, 0.2511476833527446 },{ 0., 1., 0. },{ 0.1552178045077923, -0.955422563220238, 0.2511476833527446 },{ 0.4360094506919568, -0.864187826837342, 0.2511476833527446 },{ 0.6708203932499369, -0.6881909602355867, 0.276393202250021 },{ 0.861803398874989, -0.4253254041760199, 0.276393202250021 },{ 0.956625793988502, -0.1476209044221631, 0.2511476833527446 },{ 0.5877852522924732, -0.809016994374947, 0. },{ 0.809016994374948, -0.587785252292473, 0. },{ 0.951056516295154, -0.3090169943749474, 0. },{ 0.6871571340447014, -0.6817183540715489, -0.2511476833527446 },{ 0.86069591514355, -0.4428627132664894, -0.2511476833527446 },{ 0.956625793988502, 0.1476209044221631, 0.2511476833527446 },{ 0.861803398874989, 0.4253254041760199, 0.276393202250021 },{ 0.6708203932499369, 0.6881909602355867, 0.276393202250021 },{ 0.4360094506919568, 0.864187826837342, 0.2511476833527446 },{ 0.951056516295154, 0.3090169943749474, 0. },{ 0.809016994374948, 0.587785252292473, 0. },{ 0.5877852522924732, 0.809016994374947, 0. },{ 0.86069591514355, 0.4428627132664894, -0.2511476833527446 },{ 0.6871571340447014, 0.6817183540715489, -0.2511476833527446 },{ 0.447213595499958, -0.85065080835204, -0.276393202250021 },{ 0.1381966011250105, -0.951056516295154, -0.276393202250021 },{ 0.3090169943749474, -0.951056516295154, 0. },{ 0.947213595499958, 0.1624598481164532, -0.2763932022500211 },{ 0.947213595499958, -0.1624598481164532, -0.2763932022500211 },{ 1., 0., 0. },{ 0.1381966011250105, 0.951056516295154, -0.276393202250021 },{ 0.447213595499958, 0.85065080835204, -0.276393202250021 },{ 0.3090169943749474, 0.951056516295154, 0. } };

/* Projection object , enum based object with string conversion.*/
struct Projection {

	/* Projection options */
	enum ProjectionType {
		MINIMUM,
		MAXIMUM,
		COMPOSITE,
		GEODESIC,
		GEODESIC_2,
	};

	/* Construct from type. */
	Projection(ProjectionType m_type) {
		this->type = m_type;
	}

	/* Construct from string. */
	static Projection fromString(QString input) {
		if (input == "Minimum") {
			return Projection(ProjectionType::MINIMUM);
		}
		else if (input == "Composite") {
			return Projection(ProjectionType::COMPOSITE);
		}
		else if (input == "Geodesic") {
			return Projection(ProjectionType::GEODESIC);
		}
		else if (input == "Geodesic_2") {
			return Projection(ProjectionType::GEODESIC_2);
		}
		return Projection(ProjectionType::MINIMUM);
	}

	/* Convert option to string. */
	QString toString() {
		switch (this->type) {
		case MINIMUM:
			return "Minimum";
		case MAXIMUM:
			return "Maximum";
		case COMPOSITE:
			return "Composite";
		case GEODESIC:
			return "Geodesic";
		case GEODESIC_2:
			return "Geodesic_2";
		}
		return "ERROR";
	}

	/* Comparision operator */
	bool operator==(const Projection other) const {
		return (this->type == other.type);
	}

	/* Current option*/
	ProjectionType type;

};

/* Options for renderign Geodesic visuals*/
enum GeodesicRendering {
	COLOR,
	ORIENTATION
};

/* Contast function options*/
enum ContrastFunctionType {
	None,
	Polynomal,
	Exponential,
    CostFunction
};

/* Navigation settings*/
struct PositionSettings {
	Orientation orient = Orientation::AXIAL;
	int position = 0;
	bool isDirty = true;

	bool operator==(const PositionSettings& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			this->orient == other.orient &&
			this->position == other.position
			);
	}
};

/* Contrast function with all parameters. */
struct ContrastFunction {
	ContrastFunctionType type = ContrastFunctionType::None;
	double sigma = 1.0;
	double p = 1.0;
	bool flipSign = true;

	bool isDirty = false;

	bool operator==(const ContrastFunction& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			this->type == other.type &&
			this->sigma == other.sigma &&
			this->p == other.p &&
			this->flipSign == other.flipSign
			);
	}
};

/* Base struct for all VisualSettings.*/
struct VisualSettings {

	// Global settings
	QString label = "";
	bool visible = false;
	bool isDirty = true;

	// Navigation settings
	PositionSettings positionSettings;

	// Something to make the struct castable
	virtual void Delete() {
	};

	// Comparison

	virtual bool operator==(const VisualSettings& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings
			);
	}
};

/* VisualSettings for the VTKRegion object.*/
struct VisualSettingsRegion : VisualSettings {

	// VTYPE_Region
	QColor color = QColor(255, 0, 0);
	double opacity = 0.5;
	bool renderSmooth = false;

	int interpolationOrder = 1;

	bool operator==(const VisualSettingsRegion& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&
			

			// Custom
			this->color == other.color &&
			this->opacity == other.opacity &&
			this->renderSmooth == other.renderSmooth &&
			this->interpolationOrder == other.interpolationOrder
			);
	}
};

/* VisualSettings for the VTKGlyphs object.*/
struct VisualSettingsGlyphs : VisualSettings {

	ColorMap* colormap = new ColorMap(ColorMap::ColorMapValue::FlowDark);
	ContrastFunction contrastFunction = ContrastFunction();
	bool showColorbar = false;

	bool operator==(const VisualSettingsGlyphs& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&

			// Custom
			this->colormap->value == other.colormap->value &&
			this->contrastFunction == other.contrastFunction &&
			this->showColorbar == other.showColorbar
			);
	}
};

/* VisualSettings for the VTKGeodesic object.*/
struct VisualSettingsGeodesic : VisualSettings {

	QColor color = QColor(Qt::red);
	bool showArrows = false;
	double opacity = 1.0;

	double arrowDistance = 0.05; //relative sample vs boundig box
	double arrowSize = 0.8; //relative 
	bool arrowIsDirty = true;

	GeodesicRendering rendering = GeodesicRendering::COLOR;
	GeodesicRendering arrowRendering = GeodesicRendering::COLOR;

	bool operator==(const VisualSettingsGeodesic& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&

			// Custom
			this->color == other.color &&
			this->showArrows == other.showArrows&&
			this->opacity == other.opacity &&
			this->rendering == other.rendering &&

			// Arrows
			this->arrowDistance == other.arrowDistance &&
			this->arrowSize == other.arrowSize &&
			this->arrowRendering == other.arrowRendering &&
			this->arrowIsDirty == other.arrowIsDirty

			);
	}
};

/* VisualSettings for the VTKScalarProjectionobject.*/
struct VisualSettingsScalarProjection : VisualSettings {

	ColorMap* colormap = new ColorMap(ColorMap::ColorMapValue::FlowDark);
	ContrastFunction contrastFunction = ContrastFunction();
	bool showColorbar = false;

	bool showOutline = false;
	Projection projection = Projection(Projection::ProjectionType::MINIMUM);
	bool projectionIsDirty = false;
	
	int interpolationOrder = 1;

	bool operator==(const VisualSettingsScalarProjection& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&

			// Custom
			this->colormap->value == other.colormap->value &&
			this->contrastFunction == other.contrastFunction &&
			this->showColorbar == other.showColorbar &&
			this->showOutline == other.showOutline &&
			this->projection == other.projection &&
			this->interpolationOrder == other.interpolationOrder 
			);
	}

	~VisualSettingsScalarProjection() {
		delete this->colormap;
	}
};

/* VisualSettings for the VTKVolume (reference).*/
struct VisualSettingsVolume : VisualSettings {

	ColorMap* colormap = new ColorMap(ColorMap::ColorMapValue::Greyscale);
	bool showColorbar = false;
	double opacity = 1.0;
	int interpolationOrder = 1;

	bool operator==(const VisualSettingsVolume& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&

			// Custom
			this->colormap->value == other.colormap->value &&
			this->opacity == other.opacity &&
			this->showColorbar == other.showColorbar &&
			this->interpolationOrder == other.interpolationOrder
			);
	}

	~VisualSettingsVolume() {
		delete this->colormap;
	}
};

/* VisualSettings struct for the VTKContourPlot .*/
struct VisualSettingsContourPlot : VisualSettings {

	ColorMap* colormap = new ColorMap(ColorMap::ColorMapValue::FlowDark);
	ContrastFunction contrastFunction = ContrastFunction();
	bool showColorbar = false;

	bool showOutline = false;
	Projection projection = Projection(Projection::ProjectionType::MINIMUM);
	bool projectionIsDirty = false;

	bool renderSmooth = false;
	double opacity = 1.0;
	int numContours = 3;
	bool numContoursIsDirty = false;

	bool operator==(const VisualSettingsContourPlot& other) const {
		if (typeid(*this) != typeid(other))
			return false;

		return (
			// Base class
			this->label == other.label &&
			this->visible == other.visible &&
			this->positionSettings == other.positionSettings &&

			// Custom
			this->colormap->value == other.colormap->value &&
			this->contrastFunction == other.contrastFunction &&
			this->showColorbar == other.showColorbar &&

			this->showOutline == other.showOutline &&
			this->projection == other.projection &&

			this->renderSmooth == other.renderSmooth &&
			this->opacity == other.opacity &&
			this->numContours == other.numContours
			);
	}
};

/* Virtual base class for all Visual objects. */
class VisualizationObject {
public:

	/* Type of visual object.*/
	VisualType type;

	/* Constructor */
	VisualizationObject() {
		m_dataObject = nullptr;
	};

	/* Destructor */
	virtual ~VisualizationObject() {
	};

	/* Get the source data object.*/
	DataObject* getDataObject() {
		return m_dataObject;
	}

	/* Update the visuals based on the current visual settings.*/
	virtual void Update() = 0;

	/* Gets current visual settings.*/
	virtual VisualSettings* GetSettings() = 0;

	/* Sets current visual settings.*/
	virtual void SetSettings(VisualSettings* newSettings) = 0;

	/* Gets the complete visuals for the given dimension(2/3).*/
	virtual SP(vtkAssembly) GetVisuals(int dim) = 0; 

	/* Re-build the object based on the dataobject */
	virtual void ReloadFromSource() = 0;

protected:

	virtual bool init() {
		return true;
	};

	/** Variables */
	DataObject* m_dataObject = NULL;

	SP(vtkAssembly) m_visuals2D;
	SP(vtkAssembly) m_visuals3D;

	// Helper function for general calculations
	static SP(vtkImageData) CalculateProjection(SP(vtkImageData) data, Projection::ProjectionType type);
	static SP(vtkImageData) CalculateProjectionComposite(SP(vtkImageData) data);
	static SP(vtkImageData) CalculateContrast(SP(vtkImageData) data, ContrastFunction contrast);
	static double  ApplyInverseDistanceWeighting(double * dataPos, double * dataValues, QList<QPair<double*, int>>* orientationSourceList, std::vector<std::vector<double>> const * orientationDataList);
	static SP(vtkImageData)  CalculateProjectionGeodesic(SP(vtkImageData) data, QList<QPair<double*, int>>* list);

	static SP(vtkImageData) CalculateProjectionGeodesic2(SP(vtkImageData) data, QList<QPair<double*, int>>* list);

	static double ApplyInverseDistanceWeighting2(double * dataPos, double * dataValues, QList<QPair<double*, int>>* orientationSourceList, std::vector<std::vector<double>> const * orientationDataList);

	static QList<QPair<double*, int>> AggregateOrientationData(SP(vtkPolyData) polydata, bool projective);

	// Helper function for general calculations
	static double getProjectionValue(std::vector<double>* list,
									 Projection::ProjectionType type);

	static void getProjectionValueComposite(double* values, int size,
											const std::vector<std::vector<double>>* orientations,
											unsigned char * target);

	static int SnapToExisitingOrientation(std::vector<std::vector<double>> const * orientations, double * orient);
	
};
