#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"
#include "common/Macros.h"

#include <vtkSmartPointer.h>

#include <vector>

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

class ColorMap;

/** Includes - VTK */
class vtkScalarBarActor;
class vtkImageSlice;
class vtkPlane;
class vtkImageMapToColors;

class VTKAnatomicalVolume : public VisualizationObject {
public:

	/** Functions */

	/* Constructor from data object */
	static VTKAnatomicalVolume* CreateWithDataObject(DataObject* dataObject);
	
	SP(vtkAssembly) GetVisuals(int dim);

	void Update();
	
	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsVolume* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsVolume*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsVolume*>(newSettings));
		}
	}

	void ReloadFromSource();

	/** Variables */

	/* Object of this VisualObject's colorbar */
	SP(vtkScalarBarActor) colorbar; 

private:

	/** Functions */

	bool initWithDataObject(DataObject* dataObject);

	void setPlane(PositionSettings posSettings);

	void buildVisuals();

	void setColorMap(ColorMap* map);


	/** Variables */

	int m_dims[3];

	SP(vtkImageData) m_data;

	SP(vtkImageSlice) m_imageSlice;

	SP(vtkPlane) m_plane;
	
	VisualSettingsVolume* m_settings;

};
