/** Includes */
#include "VTKAnatomicalVolume.h"

#include <QDir>

#include <vtkOutlineFilter.h>
#include <vtkTextProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageProperty.h>
#include <vtkProp.h>
#include <vtkActor2D.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkScalarBarActor.h>
#include <vtkImageSlice.h>
#include <vtkPlane.h>
#include <vtkImageMapToColors.h>
#include <vtkLookupTable.h>

#include "ReferenceSource.h"

#include "common/ColorMap.h"

VTKAnatomicalVolume* VTKAnatomicalVolume::CreateWithDataObject(DataObject* DataObject) {
	VTKAnatomicalVolume *pRet = new (std::nothrow) VTKAnatomicalVolume();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void VTKAnatomicalVolume::Update() {

	this->colorbar->SetVisibility(this->m_settings->visible && this->m_settings->showColorbar);

	this->m_imageSlice->SetVisibility(m_settings->visible);

	if (!m_settings->visible) {
		return;
	}

	if (this->m_settings->positionSettings.isDirty) {
		this->setPlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	// Update visuals
	this->setColorMap(m_settings->colormap);
	this->m_imageSlice->GetProperty()->SetInterpolationType(this->m_settings->interpolationOrder);
	this->m_imageSlice->GetProperty()->SetOpacity(this->m_settings->opacity);

	this->m_settings->isDirty = false;
}

bool VTKAnatomicalVolume::initWithDataObject(DataObject* dataObject) {

	this->m_dataObject = dataObject;
	this->type = VisualType::VTYPE_ANATOMICAL_VOLUME;

	this->colorbar = SP(vtkScalarBarActor)::New();
	this->colorbar->SetVisibility(false);
	this->colorbar->SetTitle("Scalars");
	this->colorbar->GetLabelTextProperty()->SetFontSize(8);
	this->colorbar->SetWidth(0.1);
	this->colorbar->SetHeight(0.8);
	this->colorbar->SetPosition(0.88, 0.15);

	this->m_imageSlice = SP(vtkImageSlice)::New();
	this->m_imageSlice->SetVisibility(false);

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->AddPart(m_imageSlice);

	this->m_visuals3D = SP(vtkAssembly)::New();
	this->m_visuals3D->AddPart(m_imageSlice);

	this->m_plane = SP(vtkPlane)::New();

	// Default values
	this->m_settings = new VisualSettingsVolume();
	this->m_settings->label = dataObject->label;;

	ReferenceSource* mapping = dynamic_cast <ReferenceSource*>(this->m_dataObject);
	this->m_data = mapping->data;

	this->m_dims[0] = this->m_data->GetDimensions()[0];
	this->m_dims[1] = this->m_data->GetDimensions()[1];
	this->m_dims[2] = this->m_data->GetDimensions()[2];

	// Build:
	this->buildVisuals();

	this->Update();

	return true;
}

void VTKAnatomicalVolume::ReloadFromSource() {
}

void VTKAnatomicalVolume::setPlane(PositionSettings posSettings) {

	ReferenceSource* source = dynamic_cast<ReferenceSource*> (m_dataObject);

	// Calculate world position from image coordinates
	double normal[3] = { 0, 0, 0 };
	double imagePos[3] = { 0, 0, 0 };
	imagePos[2 - posSettings.orient] += posSettings.position;
	normal[2 - posSettings.orient] += 1;

	double* world = source->transform->TransformDoublePoint(imagePos);

	// Update plane position and orientation
	this->m_imageSlice->SetOrigin(world);

	this->m_plane->SetOrigin(world);
	this->m_plane->SetNormal(normal);

	this->m_imageSlice->GetMapper()->Update();
	this->m_imageSlice->Update();
}

void VTKAnatomicalVolume::setColorMap(ColorMap* map) {

	buildVisuals();
	this->setPlane(this->m_settings->positionSettings);
}

SP(vtkAssembly) VTKAnatomicalVolume::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

void VTKAnatomicalVolume::buildVisuals() {

	// Create the three orthogonal planes for 2D image merging
	double normals[3] = { 0, 0, 0 };
	switch (this->m_settings->positionSettings.orient) {
	case Orientation::SAGITTAL:
		normals[0] = 1;
		break;

	case Orientation::CORONAL:
		normals[1] = 1;
		break;

	case Orientation::AXIAL:
		normals[2] = 1;
		break;
	}

	VTK_CREATE(vtkImageResliceMapper, imageMapper);

	double* center = this->m_data->GetCenter();

	this->m_plane->SetNormal(normals);
	this->m_plane->SetOrigin(center[0], center[1], center[2]);

	SP(vtkLookupTable) table;
	double* range = this->m_data->GetScalarRange();
	table = this->m_settings->colormap->CreateLookupTable(range[0], range[1]);

	VTK_CREATE(vtkImageMapToColors, scalarValuesToColors);
	scalarValuesToColors->SetLookupTable(table);
	scalarValuesToColors->PassAlphaToOutputOn();
#if VTK_MAJOR_VERSION <= 5
	scalarValuesToColors->SetInput(this->m_data);
#else
	scalarValuesToColors->SetInputData(this->m_data);
#endif
	scalarValuesToColors->Update();

#if VTK_MAJOR_VERSION <= 5
	imageMapper->SetInput(scalarValuesToColors->GetOutput());
#else
	imageMapper->SetInputData(scalarValuesToColors->GetOutput());
#endif

	imageMapper->SetSlicePlane(this->m_plane);
	imageMapper->SetBorder(true);

	this->m_imageSlice->SetMapper(imageMapper);
	this->m_imageSlice->Update();

	this->m_imageSlice->GetProperty()->BackingOff();
	this->m_imageSlice->SetOrigin(center[0], center[1], center[2]);

	this->colorbar->SetLookupTable(table);
}