/** Includes */
#include "VTKGeodesics.h"

#include "GeodesicSource.h"

#include <QDir>

#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkArrowSource.h>
#include <vtkGlyph3D.h>
#include <vtkPointData.h>
#include <vtkPolyDataPointSampler.h>
#include <vtkDoubleArray.h>
#include <vtkPolyDataWriter.h>
#include <vtkCleanPolyData.h>
#include <vtkLookupTable.h>

VTKGeodesics* VTKGeodesics::CreateWithDataObject(DataObject* DataObject) {
	VTKGeodesics *pRet = new (std::nothrow) VTKGeodesics();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

bool VTKGeodesics::initWithDataObject(DataObject* dataObject) {
	this->m_dataObject = dataObject;

	this->type = VisualType::VTYPE_GEODESIC;

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->SetVisibility(false);

	this->m_visuals3D = SP(vtkAssembly)::New();
	this->m_visuals3D->SetVisibility(false);

	this->m_actor = SP(vtkActor)::New();
	this->m_visuals3D->AddPart(this->m_actor);

	this->m_glyphActor = SP(vtkActor)::New();
	this->m_visuals3D->AddPart(this->m_glyphActor);

	// Default values
	this->m_settings = new VisualSettingsGeodesic();
	this->m_settings->label = dataObject->label;

	GeodesicSource* source = dynamic_cast <GeodesicSource*>(this->m_dataObject);
	this->m_mesh = SP(vtkPolyData)::New();
	this->m_mesh->DeepCopy(source->data);

	this->buildColorData();
	this->buildVisuals();

	this->Update();

	return true;
}

void VTKGeodesics::buildVisuals() {

	//    GeodesicSource* source = dynamic_cast <GeodesicSource*>(this->m_dataObject);

	VTK_CREATE(vtkPolyDataMapper, mapper);
	mapper->SetInputData(this->m_mesh);

	this->m_actor->SetMapper(mapper);
	this->m_actor->GetProperty()->SetLineWidth(2.0);
	this->m_actor->GetProperty()->SetColor(this->m_settings->color.red() / 255.0,
										   this->m_settings->color.green() / 255.0,
										   this->m_settings->color.blue() / 255.0);

	VTK_CREATE(vtkCleanPolyData, cleanPolyData);
	cleanPolyData->SetInputData(this->m_mesh);
	cleanPolyData->SetTolerance(this->m_settings->arrowDistance);
	cleanPolyData->Update();

	vtkPolyData* temp = cleanPolyData->GetOutput();
	temp->GetPointData()->SetActiveVectors("orientations");

	VTK_CREATE(vtkArrowSource, arrowSource);

	VTK_CREATE(vtkGlyph3D, glyph3D);
	glyph3D->SetSourceConnection(arrowSource->GetOutputPort());
#if VTK_MAJOR_VERSION <= 5
	glyph3D->SetInput(temp);
#else
	glyph3D->SetInputData(temp);
#endif
	glyph3D->SetVectorModeToUseVector();
	glyph3D->SetScaleFactor(this->m_settings->arrowSize);
	glyph3D->SetScaleModeToDataScalingOff();
	glyph3D->Update();

	VTK_CREATE(vtkLookupTable, table);
	table->SetVectorModeToRGBColors();
	table->Build();

	VTK_CREATE(vtkPolyDataMapper, glyphMapper);
	glyphMapper->SetInputConnection(glyph3D->GetOutputPort());
	glyphMapper->SetScalarVisibility(false);
	glyphMapper->Update();

	this->m_glyphActor->SetMapper(glyphMapper);
	this->m_glyphActor->GetProperty()->SetColor(1.0, 1.0, 1.0);
}

void VTKGeodesics::ReloadFromSource() {
}

SP(vtkAssembly) VTKGeodesics::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

void VTKGeodesics::Update() {

	if (this->m_settings->positionSettings.isDirty) {
		this->m_settings->positionSettings.isDirty = false;
	}

	if (this->m_settings->arrowIsDirty) {
		this->buildVisuals();
		this->m_settings->arrowIsDirty = false;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	this->m_actor->GetProperty()->SetColor(this->m_settings->color.red() / 255.0,
										   this->m_settings->color.green() / 255.0,
										   this->m_settings->color.blue() / 255.0);

	this->setRendering(this->m_settings->rendering);

	this->m_actor->GetProperty()->SetOpacity(this->m_settings->opacity);
	this->m_glyphActor->GetProperty()->SetOpacity(this->m_settings->opacity);

	this->m_visuals3D->SetVisibility(m_settings->visible);
	this->m_glyphActor->SetVisibility(m_settings->visible && m_settings->showArrows);

	this->m_settings->isDirty = false;
}

void VTKGeodesics::setRendering(GeodesicRendering rendering) {

	if (rendering == GeodesicRendering::COLOR) {
		this->setSingleColorMode();
	}
	else if (rendering == GeodesicRendering::ORIENTATION) {
		this->setOrientationMode();
	}

	this->m_actor->GetMapper()->Update();
}

void VTKGeodesics::setSingleColorMode() {

	GeodesicSource* source = dynamic_cast <GeodesicSource*>(this->m_dataObject);

	// Generate new data
	vtkSmartPointer<vtkUnsignedCharArray> newData =
		vtkSmartPointer<vtkUnsignedCharArray>::New();
	newData->SetName("SingleColors");
	newData->SetNumberOfComponents(3);

	// Loop over all points
	for (int i = 0; i < source->data->GetNumberOfPoints(); i++) {

		unsigned char color[3]{
			static_cast<unsigned char>(this->m_settings->color.red()),
			static_cast<unsigned char>(this->m_settings->color.green()),
			static_cast<unsigned char>(this->m_settings->color.blue()) };

		newData->InsertTypedTuple(i, color);
	}

	this->m_colorData = newData;

	this->m_mesh->GetPointData()->SetScalars(this->m_colorData);
	this->m_mesh->GetPointData()->SetActiveVectors("SingleColors");

	vtkSmartPointer<vtkLookupTable> table =
		vtkSmartPointer<vtkLookupTable>::New();
	table->Build();
	table->SetVectorModeToRGBColors();

	this->m_actor->GetMapper()->SetLookupTable(table);
	this->m_actor->GetMapper()->SetColorModeToDefault();
	this->m_actor->GetMapper()->ScalarVisibilityOn();
	this->m_actor->GetMapper()->SetScalarModeToUsePointData();
	this->m_actor->GetMapper()->SetUseLookupTableScalarRange(false);
}

void VTKGeodesics::setOrientationMode() {

	//    GeodesicSource* source = dynamic_cast <GeodesicSource*>(this->m_dataObject);

	this->m_mesh->GetPointData()->SetScalars(this->m_orientationData);
	this->m_mesh->GetPointData()->SetActiveVectors("OrientationColors");

	vtkSmartPointer<vtkLookupTable> table =
		vtkSmartPointer<vtkLookupTable>::New();
	table->SetVectorModeToRGBColors();
	table->Build();

	this->m_actor->GetMapper()->SetLookupTable(table);
	this->m_actor->GetMapper()->SetColorModeToDefault();
	this->m_actor->GetMapper()->ScalarVisibilityOn();
	this->m_actor->GetMapper()->SetScalarModeToUsePointData();
	this->m_actor->GetMapper()->SetUseLookupTableScalarRange(false);
}

void VTKGeodesics::buildColorData() {

	GeodesicSource* source = dynamic_cast <GeodesicSource*>(this->m_dataObject);

	vtkSmartPointer<vtkUnsignedCharArray> orientData =
		vtkSmartPointer<vtkUnsignedCharArray>::New();
	orientData->SetNumberOfComponents(3);
	orientData->SetName("OrientationColors");

	vtkSmartPointer<vtkUnsignedCharArray> newColorData =
		vtkSmartPointer<vtkUnsignedCharArray>::New();
	newColorData->SetNumberOfComponents(3);
	newColorData->SetName("SingleColors");

	for (vtkIdType i = 0; i < source->data->GetNumberOfPoints(); i++) {

		// Orientation
		double* vec = source->data->GetPointData()->GetVectors()->GetTuple(i);
		double length = sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);

		unsigned char orientation[3]{
			static_cast<unsigned char>(qAbs(vec[0] / length * 255.0)),
			static_cast<unsigned char>(qAbs(vec[1] / length * 255.0)),
			static_cast<unsigned char>(qAbs(vec[2] / length * 255.0)) };

		orientData->InsertTypedTuple(i, orientation);

		// Color

		unsigned char color[3]{
			static_cast<unsigned char>(this->m_settings->color.red()),
			static_cast<unsigned char>(this->m_settings->color.green()),
			static_cast<unsigned char>(this->m_settings->color.blue()) };

		newColorData->InsertTypedTuple(i, color);
	}

	this->m_orientationData = orientData;
	this->m_colorData = newColorData;
}