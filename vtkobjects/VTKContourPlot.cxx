/** Includes */
#include "VTKContourPlot.h"

#include <QDir>

#include <vtkOutlineFilter.h>
#include <vtkTextProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageProperty.h>
#include <vtkProp.h>
#include <vtkActor2D.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkScalarBarActor.h>
#include <vtkImageSlice.h>
#include <vtkPlane.h>
#include <vtkImageMapToColors.h>
#include <vtkLookupTable.h>
#include <vtkImageMathematics.h>
#include <vtkMarchingCubes.h>
#include <vtkPolyDataNormals.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkImageGaussianSmooth.h>
#include <vtkCleanPolyData.h>
#include <vtkCutter.h>
#include <vtkFlyingEdges3D.h>
#include <vtkDepthSortPolyData.h>
#include <vtkCamera.h>

#include "DistanceMapSource.h"

#include "common/ColorMap.h"

//DEBUG
#include "Controller.h"
#include "Visualization.h"

VTKContourPlot* VTKContourPlot::CreateWithDataObject(DataObject* DataObject) {
	VTKContourPlot *pRet = new (std::nothrow) VTKContourPlot();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void VTKContourPlot::Update() {

	if (this->m_settings->contrastFunction.isDirty) {
		this->buildData();
		this->m_settings->contrastFunction.isDirty = false;
		this->m_settings->numContoursIsDirty = true;
		std::cout << "Update Contours: data" << std::endl;
	}

	if (this->m_settings->numContoursIsDirty) {
		this->buildContours();
		this->buildContoursSmooth();
		this->m_settings->numContoursIsDirty = false;
		std::cout << "Update Contours: build" << std::endl;
	}

	if (this->m_settings->positionSettings.isDirty) {
		this->setSlicePlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
		std::cout << "Update Contours: nav" << std::endl;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	std::cout << "Update Contours" << std::endl;

	// Update visuals
	this->setColorMap(m_settings->colormap);
	this->colorbar->SetVisibility(m_settings->visible && m_settings->showColorbar);

	this->m_originalActor3D->SetVisibility(m_settings->visible && !m_settings->renderSmooth);
	this->m_originalActor3D->GetProperty()->SetOpacity(m_settings->opacity);

	this->m_smoothActor3D->SetVisibility(m_settings->visible && m_settings->renderSmooth);
	this->m_smoothActor3D->GetProperty()->SetOpacity(m_settings->opacity);

	this->m_actor2D->SetVisibility(m_settings->visible);
	this->m_actor2D->GetProperty()->SetOpacity(m_settings->opacity);

	this->m_settings->isDirty = false;
}

bool VTKContourPlot::initWithDataObject(DataObject* dataObject) {
	this->m_dataObject = dataObject;

	this->type = VisualType::VTYPE_CONTOURPLOT;

	this->colorbar = SP(vtkScalarBarActor)::New();
	this->colorbar->SetVisibility(false);
	this->colorbar->SetTitle("Contours");
	this->colorbar->GetLabelTextProperty()->SetFontSize(8);
	this->colorbar->SetWidth(0.1);
	this->colorbar->SetHeight(0.8);
	this->colorbar->SetPosition(0.02, 0.15);

	this->m_finalData = SP(vtkImageData)::New();

	this->m_smoothActor3D = SP(vtkActor)::New();
	this->m_smoothActor3D->SetVisibility(false);
	this->m_originalActor3D = SP(vtkActor)::New();
	this->m_originalActor3D->SetVisibility(false);

	this->m_actor2D = SP(vtkActor)::New();
	this->m_actor2D->SetVisibility(false);
	this->m_actor2D->GetProperty()->SetInterpolationToFlat();

	this->m_visuals3D = SP(vtkAssembly)::New();
	this->m_visuals3D->AddPart(this->m_smoothActor3D);
	this->m_visuals3D->AddPart(this->m_originalActor3D);

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->AddPart(this->m_actor2D);

	this->m_slicePlane = SP(vtkPlane)::New();

	this->camera = SP(vtkCamera)::New();

	// Default values
	this->m_settings = new VisualSettingsContourPlot();
	this->m_settings->label = dataObject->label;;

	this->buildData();
	this->buildContours();
	this->buildContoursSmooth();

	// Building pipeline:
	this->Update();

	return true;
}

void VTKContourPlot::ReloadFromSource() {
}

void VTKContourPlot::setSlicePlane(PositionSettings posSettings) {

	DistanceMapSource* source = dynamic_cast<DistanceMapSource*> (m_dataObject);
	//    int* extent = source->data->GetExtent();

		// Calculate world position from image coordinates
	double normal[3] = { 0, 0, 0 };
	double imagePos[3] = { 0, 0, 0 };
	imagePos[2 - posSettings.orient] += posSettings.position;
	normal[2 - posSettings.orient] += 1;

	double* world = source->transform->TransformDoublePoint(imagePos);

	// Update plane position and orientation
	this->m_slicePlane->SetOrigin(world);
	this->m_slicePlane->SetNormal(normal);

	this->m_actor2D->GetMapper()->Update();
}

void VTKContourPlot::setColorMap(ColorMap* map) {

	double* range = this->m_finalData->GetScalarRange();
	SP(vtkLookupTable) table = this->m_settings->colormap->CreateLookupTable(range[0], range[1]);

	this->m_originalActor3D->GetMapper()->SetLookupTable(table);
	this->m_smoothActor3D->GetMapper()->SetLookupTable(table);

	this->colorbar->SetLookupTable(table);
}

SP(vtkAssembly) VTKContourPlot::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

void VTKContourPlot::buildData() {

	// Input
	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);

	// -- Projection --
	if (this->m_settings->projectionIsDirty || this->m_projectionData == nullptr) {
		this->m_projectionData = VisualizationObject::CalculateProjection(mapping->data,
																		  this->m_settings->projection.type);
		this->m_settings->projectionIsDirty = false;
	}

	// -- Constrast --
	this->m_finalData = VisualizationObject::CalculateContrast(this->m_projectionData,
															   this->m_settings->contrastFunction);
}

void VTKContourPlot::buildContours() {

	double* range = this->m_finalData->GetScalarRange();
	SP(vtkLookupTable) table = this->m_settings->colormap->CreateLookupTable(range[0], range[1]);

	VTK_CREATE(vtkFlyingEdges3D, surface);
#if VTK_MAJOR_VERSION <= 5
	surface->SetInput(this->m_finalData);
#else
	surface->SetInputData(this->m_finalData);
#endif
	surface->ComputeNormalsOn();
	surface->ComputeScalarsOn();
	surface->SetNumberOfContours(this->m_settings->numContours);
	for (int i = 0; i < this->m_settings->numContours; i++) {
		double step = (range[1] - range[0]) / this->m_settings->numContours;
		surface->SetValue(i, range[0] + i * step);
	}
	surface->Update();

	VTK_CREATE(vtkPolyDataNormals, normalGenerator);
	normalGenerator->SetInputConnection(surface->GetOutputPort());
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->ComputeCellNormalsOn();
	normalGenerator->Update();

	VTK_CREATE(vtkDepthSortPolyData, depthSort);
	depthSort->SetInputConnection(normalGenerator->GetOutputPort());
	depthSort->SetDirectionToBackToFront();
	depthSort->SetVector(1, 1, 1);
	depthSort->SetCamera(this->camera);
	depthSort->SortScalarsOff(); // do not really need this here
								 // Bring it to the mapper's input

	depthSort->Update();

	VTK_CREATE(vtkPolyDataMapper, mapper);
	mapper->SetInputConnection(depthSort->GetOutputPort());
	mapper->SetLookupTable(table);
	mapper->ScalarVisibilityOn();
	mapper->SetUseLookupTableScalarRange(true);

	this->m_originalActor3D->SetMapper(mapper);
	this->m_originalActor3D->SetVisibility(false);
	this->m_originalActor3D->GetProperty()->SetOpacity(this->m_settings->opacity);
	this->m_originalActor3D->GetProperty()->SetOpacity(this->m_settings->opacity);

	this->m_visuals3D->AddPart(this->m_originalActor3D);

	// 2D
	vtkSmartPointer<vtkCutter> cutter =
		vtkSmartPointer<vtkCutter>::New();
	cutter->SetInputConnection(normalGenerator->GetOutputPort());
	cutter->SetCutFunction(this->m_slicePlane);
	cutter->Update();

	VTK_CREATE(vtkPolyDataMapper, mapper2D);
	mapper2D->SetInputConnection(cutter->GetOutputPort());
	mapper2D->ScalarVisibilityOff();

	this->m_actor2D->SetMapper(mapper2D);
	this->m_actor2D->SetVisibility(false);
	this->m_actor2D->GetProperty()->SetOpacity(this->m_settings->opacity);
	this->m_actor2D->GetProperty()->SetColor(1.0, 1.0, 1.0);
}

void VTKContourPlot::buildContoursSmooth() {

	double* range = this->m_finalData->GetScalarRange();
	SP(vtkLookupTable) table = this->m_settings->colormap->CreateLookupTable(range[0], range[1]);

	vtkSmartPointer<vtkImageGaussianSmooth> smoother =
		vtkSmartPointer<vtkImageGaussianSmooth>::New();
#if VTK_MAJOR_VERSION <= 5
	smoother->SetInput(this->m_finalData);
#else
	smoother->SetInputData(this->m_finalData);
#endif
	smoother->SetStandardDeviation(1.2, 1.2, 1.2);
	smoother->Update();

	double* cubeRange = range;

	VTK_CREATE(vtkFlyingEdges3D, surface);
#if VTK_MAJOR_VERSION <= 5
	surface->SetInput(smoother->GetOutput());
#else
	surface->SetInputData(smoother->GetOutput());
#endif
	surface->ComputeNormalsOn();
	surface->ComputeScalarsOn();
	surface->SetNumberOfContours(this->m_settings->numContours);
	for (int i = 0; i < this->m_settings->numContours; i++) {
		double step = (cubeRange[1] - cubeRange[0]) / (this->m_settings->numContours);
		surface->SetValue(i, cubeRange[0] + i * step);
	}
	surface->SetInterpolateAttributes(true);
	surface->Update();

	VTK_CREATE(vtkPolyDataNormals, normalGenerator);
	normalGenerator->SetInputConnection(surface->GetOutputPort());
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->ComputeCellNormalsOn();
	normalGenerator->Update();

	VTK_CREATE(vtkDepthSortPolyData, depthSort);
	depthSort->SetInputConnection(normalGenerator->GetOutputPort());
	depthSort->SetDirectionToBackToFront();
	depthSort->SetVector(1, 1, 1);
	depthSort->SetCamera(this->camera);
	depthSort->SortScalarsOff();

	depthSort->Update();

	VTK_CREATE(vtkPolyDataMapper, mapper);
	mapper->SetInputConnection(depthSort->GetOutputPort());
	mapper->ScalarVisibilityOn();
	mapper->SetUseLookupTableScalarRange(true);
	mapper->InterpolateScalarsBeforeMappingOn();
	mapper->SetLookupTable(table);
	mapper->SetResolveCoincidentTopologyToShiftZBuffer();

	this->m_smoothActor3D->SetMapper(mapper);
	this->m_smoothActor3D->SetVisibility(false);
	this->m_smoothActor3D->GetProperty()->SetOpacity(this->m_settings->opacity);
}