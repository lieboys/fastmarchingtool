#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"
#include "common/Macros.h"

#include <QColor>
#include <vector>

class ColorMap;

/** Includes - VTK */

class vtkImageSlice;
class vtkPlane;
class vtkImageMapToColors;
class vktPolyData;

class VTKRegion : public VisualizationObject {
public:

	/** Constructor from data object. */
	static VTKRegion* CreateWithDataObject(DataObject* dataObject);

	SP(vtkAssembly) GetVisuals(int dim);

	void Update();

	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsRegion* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsRegion*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsRegion*>(newSettings));
		}
	}
	void ReloadFromSource();

private:

	/** Functions */

	bool initWithDataObject(DataObject* dataObject);

	void setColor(QColor color);

	void setPlane(PositionSettings posSettings);

	void buildSlices();
	void buildVisualsNormal();
	void buildVisualsSmooth();

	/** Variables */

	int m_dims[3];

	SP(vtkActor) m_actor_original;
	SP(vtkActor) m_actor_smooth;

	SP(vtkImageSlice) m_imageSlice;
	SP(vtkPlane) m_plane;
	SP(vktPolyData) m_polydata;
	SP(vtkImageMapToColors) m_colormapper;

	VisualSettingsRegion* m_settings;

	Orientation m_orientation;
};
