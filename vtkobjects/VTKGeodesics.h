#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"
#include "common/Macros.h"

/** Includes - VTK */
#include <vtkPolyData.h>

#include <vtkActor.h>
#include <vtkAssembly.h>
#include <vtkScalarBarActor.h>

class ColorMap;

class VTKGeodesics : public VisualizationObject {
public:

	static VTKGeodesics* CreateWithDataObject(DataObject* dataObject);

	void ReloadFromSource();

	SP(vtkAssembly) GetVisuals(int dim);

	void Update();

	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsGeodesic* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsGeodesic*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsGeodesic*>(newSettings));
		}
	}

private:

	/** Functions */
	bool initWithDataObject(DataObject* dataObject);

	void setRendering(GeodesicRendering rendering);

	void setSingleColorMode();
	void setOrientationMode();

	void buildColorData();
	void buildVisuals();

	/** Variables */
	SP(vtkActor) m_actor;
	SP(vtkActor) m_glyphActor;

	SP(vtkPolyData) m_mesh;

	SP(vtkUnsignedCharArray) m_colorData;
	SP(vtkUnsignedCharArray) m_orientationData;

	VisualSettingsGeodesic* m_settings;
};
