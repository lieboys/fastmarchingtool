/** Includes */
#include "VTKScalarProjection.h"

#include <QDir>

#include <vtkOutlineFilter.h>
#include <vtkTextProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageProperty.h>
#include <vtkProp.h>
#include <vtkActor2D.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkScalarBarActor.h>
#include <vtkImageSlice.h>
#include <vtkPlane.h>
#include <vtkImageMapToColors.h>
#include <vtkLookupTable.h>

#include "DistanceMapSource.h"

#include "common/ColorMap.h"

VTKScalarProjection* VTKScalarProjection::CreateWithDataObject(DataObject* DataObject) {
	VTKScalarProjection *pRet = new (std::nothrow) VTKScalarProjection();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void VTKScalarProjection::Update() {

	this->colorbar->SetVisibility(this->m_settings->visible && this->m_settings->showColorbar);
	this->m_outline->SetVisibility(this->m_settings->showOutline);

	this->m_imageSlice->SetVisibility(m_settings->visible);

	if (!m_settings->visible) {
		return;
	}

	if (this->m_settings->positionSettings.isDirty) {
		this->setPlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
		std::cout << "Update ScalarMap: nav" << std::endl;
	}

	if (this->m_settings->contrastFunction.isDirty ||
		this->m_settings->projectionIsDirty) {

		this->buildData();
		this->m_settings->positionSettings.isDirty = false;
		this->m_settings->contrastFunction.isDirty = false;
		std::cout << "Update ScalarMap: data" << std::endl;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	std::cout << "Update ScalarMap" << std::endl;

	// Update visuals
	this->setColorMap(m_settings->colormap);
	this->m_imageSlice->GetProperty()->SetInterpolationType(this->m_settings->interpolationOrder);

	this->m_settings->isDirty = false;
}

bool VTKScalarProjection::initWithDataObject(DataObject* dataObject) {
	this->m_dataObject = dataObject;

	this->type = VisualType::VTYPE_SCALARMAP;

	this->colorbar = SP(vtkScalarBarActor)::New();
	this->colorbar->SetVisibility(false);
	this->colorbar->SetTitle("Scalars");
	this->colorbar->GetLabelTextProperty()->SetFontSize(8);
	this->colorbar->SetWidth(0.1);
	this->colorbar->SetHeight(0.8);
	this->colorbar->SetPosition(0.88, 0.15);

	this->m_finalData = SP(vtkImageData)::New();

	this->m_imageSlice = SP(vtkImageSlice)::New();
	this->m_imageSlice->SetVisibility(false);

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->AddPart(m_imageSlice);

	this->m_visuals3D = SP(vtkAssembly)::New();
	this->m_visuals3D->AddPart(m_imageSlice);

	this->m_plane = SP(vtkPlane)::New();

	this->m_outline = SP(vtkActor)::New();
	this->m_outline->SetVisibility(false);
	this->m_visuals3D->AddPart(m_outline);

	// Default values
	this->m_settings = new VisualSettingsScalarProjection();
	this->m_settings->label = dataObject->label;;

	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);
	this->m_dims[0] = mapping->data->GetDimensions()[0];
	this->m_dims[1] = mapping->data->GetDimensions()[1];
	this->m_dims[2] = mapping->data->GetDimensions()[2];

	// Build:
	buildData();
	this->Update();

	return true;
}

void VTKScalarProjection::ReloadFromSource() {
}

void VTKScalarProjection::setPlane(PositionSettings posSettings) {

	DistanceMapSource* source = dynamic_cast<DistanceMapSource*> (m_dataObject);
	//     int* extent = source->data->GetExtent();

		// Calculate world position from image coordinates
	double normal[3] = { 0, 0, 0 };
	double imagePos[3] = { 0, 0, 0 };
	imagePos[2 - posSettings.orient] += posSettings.position;
	normal[2 - posSettings.orient] += 1;

	double* world = source->transform->TransformDoublePoint(imagePos);

	// Update plane position and orientation
	this->m_imageSlice->SetOrigin(world);

	this->m_plane->SetOrigin(world);
	this->m_plane->SetNormal(normal);

	this->m_imageSlice->GetMapper()->Update();
	this->m_imageSlice->Update();
}

void VTKScalarProjection::setColorMap(ColorMap* map) {

	buildVisuals();
	this->setPlane(this->m_settings->positionSettings);
}

SP(vtkAssembly) VTKScalarProjection::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

void VTKScalarProjection::buildVisuals() {

	// Create the three orthogonal planes for 2D image merging
	double normals[3] = { 0, 0, 0 };
	switch (this->m_orientation) {
	case Orientation::SAGITTAL:
		normals[0] = 1;
		break;

	case Orientation::CORONAL:
		normals[1] = 1;
		break;

	case Orientation::AXIAL:
		normals[2] = 1;
		break;
	}

	VTK_CREATE(vtkImageResliceMapper, imageMapper);

	double* center = this->m_finalData->GetCenter();

	this->m_plane->SetNormal(normals);
	this->m_plane->SetOrigin(center[0], center[1], center[2]);

	SP(vtkLookupTable) table;
	if (this->m_settings->projection.type == Projection::ProjectionType::COMPOSITE) {
		imageMapper->SetInputData(this->m_finalData);
	}
	else {
		double* range = this->m_finalData->GetScalarRange();
		table = this->m_settings->colormap->CreateLookupTable(range[0], range[1]);

		VTK_CREATE(vtkImageMapToColors, scalarValuesToColors);
		scalarValuesToColors->SetLookupTable(table);
		scalarValuesToColors->PassAlphaToOutputOn();
#if VTK_MAJOR_VERSION <= 5
		scalarValuesToColors->SetInput(this->m_finalData);
#else
		scalarValuesToColors->SetInputData(this->m_finalData);
#endif
		scalarValuesToColors->Update();

#if VTK_MAJOR_VERSION <= 5
		imageMapper->SetInput(scalarValuesToColors->GetOutput());
#else
		imageMapper->SetInputData(scalarValuesToColors->GetOutput());
#endif
	}

	imageMapper->SetSlicePlane(this->m_plane);
	imageMapper->SetBorder(true);

	this->m_imageSlice->SetMapper(imageMapper);
	this->m_imageSlice->Update();

	this->m_imageSlice->GetProperty()->BackingOff();
	this->m_imageSlice->SetOrigin(center[0], center[1], center[2]);

	this->colorbar->SetLookupTable(table);

	// Create the outline
	vtkSmartPointer<vtkOutlineFilter> outline =
		vtkSmartPointer<vtkOutlineFilter>::New();
#if VTK_MAJOR_VERSION <= 5
	outline->SetInput(sphere);
#else
	outline->SetInputData(this->m_finalData);
#endif

	VTK_CREATE(vtkPolyDataMapper, mapper);
	mapper->SetInputConnection(outline->GetOutputPort());

	this->m_outline->SetMapper(mapper);
	this->m_outline->GetMapper()->Update();
}

void VTKScalarProjection::buildData() {

	// Input
	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);

	if (this->m_settings->projection.type == Projection::ProjectionType::COMPOSITE) {

		// -- Constrast --
		SP(vtkImageData) constrast = VisualizationObject::CalculateContrast(mapping->data,
																			this->m_settings->contrastFunction);
		this->m_settings->contrastFunction.isDirty = false;

		// -- Projection --
		this->m_projectionData = VisualizationObject::CalculateProjectionComposite(constrast);
		this->m_settings->projectionIsDirty = false;

		this->m_finalData = this->m_projectionData;
	}
	else if (this->m_settings->projection.type == Projection::ProjectionType::GEODESIC) {

		if (this->m_geoOrientationData.isEmpty()) {
			this->m_settings->projection = Projection::ProjectionType::MINIMUM;
			buildData();
			return;
		}

		// -- Projection --
		if (this->m_settings->projectionIsDirty || this->m_projectionData == nullptr) {

			this->m_projectionData = VisualizationObject::CalculateProjectionGeodesic(mapping->data,
																					  &this->m_geoOrientationData);
			this->m_settings->projectionIsDirty = false;
		}

		// -- Constrast --
		this->m_finalData = VisualizationObject::CalculateContrast(this->m_projectionData,
																   this->m_settings->contrastFunction);
		this->m_settings->contrastFunction.isDirty = false;
	}
	else if (this->m_settings->projection.type == Projection::ProjectionType::GEODESIC_2) {

		if (this->m_geoOrientationData.isEmpty()) {
			this->m_settings->projection = Projection::ProjectionType::MINIMUM;
			buildData();
			return;
		}

		// -- Projection --
		if (this->m_settings->projectionIsDirty || this->m_projectionData == nullptr) {

			this->m_projectionData = VisualizationObject::CalculateProjectionGeodesic2(mapping->data,
																					   &this->m_geoOrientationData);
			this->m_settings->projectionIsDirty = false;
		}

		// -- Constrast --
		this->m_finalData = VisualizationObject::CalculateContrast(this->m_projectionData,
																   this->m_settings->contrastFunction);
		this->m_settings->contrastFunction.isDirty = false;
	}
	else {

		// -- Projection --
		if (this->m_settings->projectionIsDirty || this->m_projectionData == nullptr) {
			this->m_projectionData = VisualizationObject::CalculateProjection(mapping->data,
																			  this->m_settings->projection.type);
			this->m_settings->projectionIsDirty = false;
		}

		// -- Constrast --
		this->m_finalData = VisualizationObject::CalculateContrast(this->m_projectionData,
																   this->m_settings->contrastFunction);
		this->m_settings->contrastFunction.isDirty = false;
	}

	// -- Visual properties --
	buildVisuals();
}

void VTKScalarProjection::SetOrientationData(vtkPolyData* data) {

	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);

	this->m_geoOrientationData = VisualizationObject::AggregateOrientationData(data, mapping->projective);
}

SP(vtkImageData) VTKScalarProjection::GetExportData(Projection::ProjectionType type) {

	DistanceMapSource* mapping = dynamic_cast <DistanceMapSource*>(this->m_dataObject);

	if (type == Projection::ProjectionType::COMPOSITE) {

		// -- Constrast --
		SP(vtkImageData) constrast = VisualizationObject::CalculateContrast(mapping->data,
																			this->m_settings->contrastFunction);
		// -- Projection --
		SP(vtkImageData) projection = VisualizationObject::CalculateProjectionComposite(constrast);

		return projection;
	}
	else if (type == Projection::ProjectionType::GEODESIC) {

		if (this->m_geoOrientationData.isEmpty()) {
			return nullptr;
		}

		// -- Projection --
		SP(vtkImageData) projection = VisualizationObject::CalculateProjectionGeodesic(mapping->data,
																					   &this->m_geoOrientationData);
		// -- Constrast --
		SP(vtkImageData) constrast = VisualizationObject::CalculateContrast(this->m_projectionData,
																			this->m_settings->contrastFunction);
		return constrast;
	}
	else {

		// -- Projection --
		SP(vtkImageData) projection = VisualizationObject::CalculateProjection(mapping->data,
																			   this->m_settings->projection.type);

		// -- Constrast --
		SP(vtkImageData) constrast = VisualizationObject::CalculateContrast(this->m_projectionData,
																			this->m_settings->contrastFunction);
		return constrast;
	}
}
