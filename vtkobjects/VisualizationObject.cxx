#include "VisualizationObject.h"

#include <QList>

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkImageData.h>
#include <vtkImageMathematics.h>
#include <vtkDoubleArray.h>
#include <vtkImageThreshold.h>

SP(vtkImageData) VisualizationObject::CalculateProjection(SP(vtkImageData) data, Projection::ProjectionType type) {

	std::cout << "CalculateProjection!" << std::endl;

	if (type == Projection::ProjectionType::COMPOSITE) {
		std::cout << "Incorrect function call." << std::endl;
		return nullptr;
	}

	int* m_dims = data->GetDimensions();

	// Create an image data
	VTK_CREATE(vtkImageData, imageData);
	imageData->SetDimensions(m_dims);
#if VTK_MAJOR_VERSION <= 5
	imageData->SetNumberOfScalarComponents(1);
	imageData->SetScalarTypeToDouble();
#else
	imageData->AllocateScalars(VTK_DOUBLE, 1);
#endif

	int numComponents = data->GetNumberOfScalarComponents();

	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {
				std::vector<double> values(numComponents, NAN);

				double* oldVoxel = static_cast<double*>(data->GetScalarPointer(x, y, z));
				for (size_t i = 0; i < numComponents; i++) {
					values.push_back(oldVoxel[i]);
				}

				double* newVoxel = static_cast<double*>(imageData->GetScalarPointer(x, y, z));
				newVoxel[0] = getProjectionValue(&values, type);;
			}
		}
	}

	return imageData;
}

SP(vtkImageData) VisualizationObject::CalculateProjectionComposite(SP(vtkImageData) data) {

	std::cout << "CalculateProjectionComposite!" << std::endl;

	std::vector<std::vector<double>> const orientations = { { 0, 0, 1. },{ 0.07760890225389615, 0.2388556408050596, 0.9679487802288659 },{ -0.20318274393026847, 0.14762090442216314, 0.9679487802288661 },{ -0.1381966011250105, 0.42532540417601994, 0.8944271909999157 },{ -0.42532540417602, 0.3090169943749474, 0.8506508083520399 },{ -0.36180339887498947, 0.587785252292473, 0.7236067977499789 },{ -0.6095482317908053, 0.4428627132664893, 0.6575131712132815 },{ -0.531939329536909, 0.6817183540715489, 0.5022953667054891 },{ -0.7236067977499789, 0.5257311121191336, 0.4472135954999579 },{ 0.16245984811645317, 0.5, 0.8506508083520399 },{ -0.052786404500042065, 0.6881909602355867, 0.7236067977499789 },{ -0.2628655560595668, 0.8090169943749473, 0.5257311121191336 },{ 0.23282670676168846, 0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, 0.8641878268373419, 0.5022953667054891 },{ 0.27639320225002106, 0.8506508083520399, 0.4472135954999579 },{ -0.20318274393026847, -0.14762090442216314, 0.9679487802288661 },{ -0.4472135954999579, 0, 0.8944271909999157 },{ -0.42532540417602, -0.3090169943749474, 0.8506508083520399 },{ -0.6708203932499369, -0.16245984811645314, 0.7236067977499789 },{ -0.6095482317908053, -0.4428627132664893, 0.6575131712132815 },{ -0.8127309757210738, -0.2952418088443262, 0.5022953667054891 },{ -0.7236067977499789, -0.5257311121191336, 0.4472135954999579 },{ -0.6708203932499369, 0.16245984811645314, 0.7236067977499789 },{ -0.85065080835204, 0, 0.5257311121191336 },{ -0.8127309757210738, 0.2952418088443262, 0.5022953667054891 },{ 0.07760890225389615, -0.2388556408050596, 0.9679487802288659 },{ -0.1381966011250105, -0.42532540417601994, 0.8944271909999157 },{ 0.16245984811645317, -0.5, 0.8506508083520399 },{ -0.052786404500042065, -0.6881909602355867, 0.7236067977499789 },{ 0.23282670676168846, -0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, -0.8641878268373419, 0.5022953667054891 },{ 0.27639320225002106, -0.8506508083520399, 0.4472135954999579 },{ -0.36180339887498947, -0.587785252292473, 0.7236067977499789 },{ -0.2628655560595668, -0.8090169943749473, 0.5257311121191336 },{ -0.531939329536909, -0.6817183540715489, 0.5022953667054891 },{ 0.2511476833527446, 0, 0.9679487802288661 },{ 0.36180339887498947, -0.2628655560595668, 0.8944271909999157 },{ 0.5257311121191336, 0, 0.8506508083520399 },{ 0.6381966011250104, -0.2628655560595668, 0.7236067977499789 },{ 0.7534430500582336, 0, 0.6575131712132815 },{ 0.8310519523121299, -0.23885564080505955, 0.5022953667054891 },{ 0.8944271909999159, 0, 0.4472135954999579 },{ 0.44721359549995787, -0.5257311121191336, 0.7236067977499789 },{ 0.6881909602355868, -0.5, 0.5257311121191336 },{ 0.48397439011443294, -0.7165669224151788, 0.5022953667054891 },{ 0.36180339887498947, 0.2628655560595668, 0.8944271909999157 },{ 0.44721359549995787, 0.5257311121191336, 0.7236067977499789 },{ 0.48397439011443294, 0.7165669224151788, 0.5022953667054891 },{ 0.6381966011250104, 0.2628655560595668, 0.7236067977499789 },{ 0.6881909602355868, 0.5, 0.5257311121191336 },{ 0.8310519523121299, 0.23885564080505955, 0.5022953667054891 },{ 0.7236067977499789, -0.5257311121191336, -0.4472135954999579 },{ 0.531939329536909, -0.6817183540715489, -0.5022953667054891 },{ 0.6095482317908053, -0.4428627132664893, -0.6575131712132815 },{ 0.36180339887498947, -0.587785252292473, -0.7236067977499789 },{ 0.42532540417602, -0.3090169943749474, -0.8506508083520399 },{ 0.1381966011250105, -0.42532540417601994, -0.8944271909999157 },{ 0.20318274393026847, -0.14762090442216314, -0.9679487802288661 },{ -0.07760890225389615, -0.2388556408050596, -0.9679487802288659 },{ 0, 0, -1. },{ 0.2628655560595668, -0.8090169943749473, -0.5257311121191336 },{ 0.052786404500042065, -0.6881909602355867, -0.7236067977499789 },{ -0.16245984811645317, -0.5, -0.8506508083520399 },{ -0.02964396283142001, -0.8641878268373419, -0.5022953667054891 },{ -0.23282670676168846, -0.7165669224151787, -0.6575131712132815 },{ -0.27639320225002106, -0.8506508083520399, -0.4472135954999579 },{ 0.7236067977499789, 0.5257311121191336, -0.4472135954999579 },{ 0.8127309757210738, 0.2952418088443262, -0.5022953667054891 },{ 0.6095482317908053, 0.4428627132664893, -0.6575131712132815 },{ 0.6708203932499369, 0.16245984811645314, -0.7236067977499789 },{ 0.42532540417602, 0.3090169943749474, -0.8506508083520399 },{ 0.4472135954999579, 0, -0.8944271909999157 },{ 0.20318274393026847, 0.14762090442216314, -0.9679487802288661 },{ 0.85065080835204, 0, -0.5257311121191336 },{ 0.6708203932499369, -0.16245984811645314, -0.7236067977499789 },{ 0.8127309757210738, -0.2952418088443262, -0.5022953667054891 },{ -0.27639320225002106, 0.8506508083520399, -0.4472135954999579 },{ -0.02964396283142001, 0.8641878268373419, -0.5022953667054891 },{ -0.23282670676168846, 0.7165669224151787, -0.6575131712132815 },{ 0.052786404500042065, 0.6881909602355867, -0.7236067977499789 },{ -0.16245984811645317, 0.5, -0.8506508083520399 },{ 0.1381966011250105, 0.42532540417601994, -0.8944271909999157 },{ -0.07760890225389615, 0.2388556408050596, -0.9679487802288659 },{ 0.2628655560595668, 0.8090169943749473, -0.5257311121191336 },{ 0.36180339887498947, 0.587785252292473, -0.7236067977499789 },{ 0.531939329536909, 0.6817183540715489, -0.5022953667054891 },{ -0.8944271909999159, 0, -0.4472135954999579 },{ -0.8310519523121299, 0.23885564080505955, -0.5022953667054891 },{ -0.7534430500582336, 0, -0.6575131712132815 },{ -0.6381966011250104, 0.2628655560595668, -0.7236067977499789 },{ -0.5257311121191336, 0, -0.8506508083520399 },{ -0.36180339887498947, 0.2628655560595668, -0.8944271909999157 },{ -0.2511476833527446, 0, -0.9679487802288661 },{ -0.6881909602355868, 0.5, -0.5257311121191336 },{ -0.44721359549995787, 0.5257311121191336, -0.7236067977499789 },{ -0.48397439011443294, 0.7165669224151788, -0.5022953667054891 },{ -0.48397439011443294, -0.7165669224151788, -0.5022953667054891 },{ -0.44721359549995787, -0.5257311121191336, -0.7236067977499789 },{ -0.36180339887498947, -0.2628655560595668, -0.8944271909999157 },{ -0.6881909602355868, -0.5, -0.5257311121191336 },{ -0.6381966011250104, -0.2628655560595668, -0.7236067977499789 },{ -0.8310519523121299, -0.23885564080505955, -0.5022953667054891 },{ 0.1552178045077923, 0.9554225632202383, 0.25114768335274457 },{ -0.1381966011250105, 0.9510565162951535, 0.276393202250021 },{ -0.4472135954999579, 0.8506508083520399, 0.276393202250021 },{ -0.6871571340447014, 0.6817183540715489, 0.25114768335274457 },{ 0, 1., 0 },{ -0.3090169943749474, 0.9510565162951535, 0 },{ -0.5877852522924731, 0.8090169943749473, 0 },{ -0.1552178045077923, 0.9554225632202383, -0.25114768335274457 },{ -0.4360094506919568, 0.8641878268373419, -0.25114768335274457 },{ -0.8606959151435498, 0.4428627132664894, 0.2511476833527446 },{ -0.9472135954999581, 0.1624598481164532, 0.2763932022500211 },{ -0.9472135954999581, -0.1624598481164532, 0.2763932022500211 },{ -0.8606959151435498, -0.4428627132664894, 0.2511476833527446 },{ -0.9510565162951535, 0.3090169943749474, 0 },{ -0.9999999999999999, 0, 0 },{ -0.9510565162951535, -0.3090169943749474, 0 },{ -0.9566257939885021, 0.1476209044221631, -0.25114768335274457 },{ -0.9566257939885021, -0.1476209044221631, -0.25114768335274457 },{ -0.6871571340447014, -0.6817183540715489, 0.25114768335274457 },{ -0.4472135954999579, -0.8506508083520399, 0.276393202250021 },{ -0.1381966011250105, -0.9510565162951535, 0.276393202250021 },{ 0.1552178045077923, -0.9554225632202383, 0.25114768335274457 },{ -0.5877852522924731, -0.8090169943749473, 0 },{ -0.3090169943749474, -0.9510565162951535, 0 },{ 0, -1., 0 },{ -0.4360094506919568, -0.8641878268373419, -0.25114768335274457 },{ -0.1552178045077923, -0.9554225632202383, -0.25114768335274457 },{ 0.4360094506919568, -0.8641878268373419, 0.25114768335274457 },{ 0.6708203932499369, -0.6881909602355867, 0.276393202250021 },{ 0.8618033988749894, -0.42532540417601994, 0.276393202250021 },{ 0.9566257939885021, -0.1476209044221631, 0.25114768335274457 },{ 0.5877852522924731, -0.8090169943749473, 0 },{ 0.8090169943749475, -0.587785252292473, 0 },{ 0.9510565162951535, -0.3090169943749474, 0 },{ 0.6871571340447014, -0.6817183540715489, -0.25114768335274457 },{ 0.8606959151435498, -0.4428627132664894, -0.2511476833527446 },{ 0.9566257939885021, 0.1476209044221631, 0.25114768335274457 },{ 0.8618033988749894, 0.42532540417601994, 0.276393202250021 },{ 0.6708203932499369, 0.6881909602355867, 0.276393202250021 },{ 0.4360094506919568, 0.8641878268373419, 0.25114768335274457 },{ 0.9510565162951535, 0.3090169943749474, 0 },{ 0.8090169943749475, 0.587785252292473, 0 },{ 0.5877852522924731, 0.8090169943749473, 0 },{ 0.8606959151435498, 0.4428627132664894, -0.2511476833527446 },{ 0.6871571340447014, 0.6817183540715489, -0.25114768335274457 },{ 0.4472135954999579, -0.8506508083520399, -0.276393202250021 },{ 0.1381966011250105, -0.9510565162951535, -0.276393202250021 },{ 0.3090169943749474, -0.9510565162951535, 0 },{ 0.9472135954999581, 0.1624598481164532, -0.2763932022500211 },{ 0.9472135954999581, -0.1624598481164532, -0.2763932022500211 },{ 1., 0, 0 },{ 0.1381966011250105, 0.9510565162951535, -0.276393202250021 },{ 0.4472135954999579, 0.8506508083520399, -0.276393202250021 },{ 0.3090169943749474, 0.9510565162951535, 0 },{ -0.8618033988749894, 0.42532540417601994, -0.276393202250021 },{ -0.6708203932499369, 0.6881909602355867, -0.276393202250021 },{ -0.8090169943749475, 0.587785252292473, 0 },{ -0.6708203932499369, -0.6881909602355867, -0.276393202250021 },{ -0.8618033988749894, -0.42532540417601994, -0.276393202250021 },{ -0.8090169943749475, -0.587785252292473, 0 } };
	std::vector<std::vector<double>> const orientationsProjective = { { 0., 0., 1. },{ 0.07760890225389615, 0.2388556408050596, 0.967948780228866 },{ 0.1624598481164532, 0.5, 0.85065080835204 },{ 0.2328267067616884, 0.7165669224151787, 0.6575131712132815 },{ 0.02964396283142001, 0.864187826837342, 0.5022953667054892 },{ 0.2763932022500211, 0.85065080835204, 0.447213595499958 },{ 0.07760890225389615, -0.2388556408050596, 0.967948780228866 },{ 0.1624598481164532, -0.5, 0.85065080835204 },{ 0.2328267067616884, -0.7165669224151787, 0.6575131712132815 },{ 0.2763932022500211, -0.85065080835204, 0.447213595499958 },{ 0.02964396283142001, -0.864187826837342, 0.5022953667054892 },{ 0.2511476833527446, 0., 0.967948780228866 },{ 0.5257311121191336, 0., 0.85065080835204 },{ 0.7534430500582337, 0., 0.6575131712132815 },{ 0.894427190999916, 0., 0.447213595499958 },{ 0.3618033988749895, -0.2628655560595668, 0.894427190999916 },{ 0.6381966011250104, -0.2628655560595668, 0.723606797749979 },{ 0.83105195231213, -0.2388556408050595, 0.5022953667054892 },{ 0.4472135954999579, -0.5257311121191336, 0.723606797749979 },{ 0.6881909602355868, -0.5, 0.5257311121191336 },{ 0.4839743901144329, -0.7165669224151788, 0.5022953667054892 },{ 0.3618033988749895, 0.2628655560595668, 0.894427190999916 },{ 0.4472135954999579, 0.5257311121191336, 0.723606797749979 },{ 0.4839743901144329, 0.7165669224151788, 0.5022953667054892 },{ 0.6381966011250104, 0.2628655560595668, 0.723606797749979 },{ 0.6881909602355868, 0.5, 0.5257311121191336 },{ 0.83105195231213, 0.2388556408050595, 0.5022953667054892 },{ 0.723606797749979, -0.5257311121191336, -0.447213595499958 },{ 0.6095482317908053, -0.4428627132664893, -0.6575131712132815 },{ 0.42532540417602, -0.3090169943749474, -0.85065080835204 },{ 0.2031827439302685, -0.1476209044221631, -0.967948780228866 },{ 0.5319393295369091, -0.6817183540715489, -0.5022953667054892 },{ 0.3618033988749895, -0.587785252292473, -0.723606797749979 },{ 0.1381966011250105, -0.4253254041760199, -0.894427190999916 },{ 0.2628655560595668, -0.809016994374947, -0.5257311121191336 },{ 0.05278640450004206, -0.6881909602355867, -0.723606797749979 },{ 0.723606797749979, 0.5257311121191336, -0.447213595499958 },{ 0.6095482317908053, 0.4428627132664893, -0.6575131712132815 },{ 0.42532540417602, 0.3090169943749474, -0.85065080835204 },{ 0.2031827439302685, 0.1476209044221631, -0.967948780228866 },{ 0.812730975721074, 0.2952418088443262, -0.5022953667054892 },{ 0.6708203932499369, 0.1624598481164532, -0.723606797749979 },{ 0.447213595499958, 0., -0.894427190999916 },{ 0.85065080835204, 0., -0.5257311121191336 },{ 0.6708203932499369, -0.1624598481164532, -0.723606797749979 },{ 0.812730975721074, -0.2952418088443262, -0.5022953667054892 },{ 0.05278640450004206, 0.6881909602355867, -0.723606797749979 },{ 0.1381966011250105, 0.4253254041760199, -0.894427190999916 },{ 0.2628655560595668, 0.809016994374947, -0.5257311121191336 },{ 0.3618033988749895, 0.587785252292473, -0.723606797749979 },{ 0.5319393295369091, 0.6817183540715489, -0.5022953667054892 },{ 0.1552178045077923, 0.955422563220238, 0.2511476833527446 },{ 0., 1., 0. },{ 0.1552178045077923, -0.955422563220238, 0.2511476833527446 },{ 0.4360094506919568, -0.864187826837342, 0.2511476833527446 },{ 0.6708203932499369, -0.6881909602355867, 0.276393202250021 },{ 0.861803398874989, -0.4253254041760199, 0.276393202250021 },{ 0.956625793988502, -0.1476209044221631, 0.2511476833527446 },{ 0.5877852522924732, -0.809016994374947, 0. },{ 0.809016994374948, -0.587785252292473, 0. },{ 0.951056516295154, -0.3090169943749474, 0. },{ 0.6871571340447014, -0.6817183540715489, -0.2511476833527446 },{ 0.86069591514355, -0.4428627132664894, -0.2511476833527446 },{ 0.956625793988502, 0.1476209044221631, 0.2511476833527446 },{ 0.861803398874989, 0.4253254041760199, 0.276393202250021 },{ 0.6708203932499369, 0.6881909602355867, 0.276393202250021 },{ 0.4360094506919568, 0.864187826837342, 0.2511476833527446 },{ 0.951056516295154, 0.3090169943749474, 0. },{ 0.809016994374948, 0.587785252292473, 0. },{ 0.5877852522924732, 0.809016994374947, 0. },{ 0.86069591514355, 0.4428627132664894, -0.2511476833527446 },{ 0.6871571340447014, 0.6817183540715489, -0.2511476833527446 },{ 0.447213595499958, -0.85065080835204, -0.276393202250021 },{ 0.1381966011250105, -0.951056516295154, -0.276393202250021 },{ 0.3090169943749474, -0.951056516295154, 0. },{ 0.947213595499958, 0.1624598481164532, -0.2763932022500211 },{ 0.947213595499958, -0.1624598481164532, -0.2763932022500211 },{ 1., 0., 0. },{ 0.1381966011250105, 0.951056516295154, -0.276393202250021 },{ 0.447213595499958, 0.85065080835204, -0.276393202250021 },{ 0.3090169943749474, 0.951056516295154, 0. } };

	int* m_dims = data->GetDimensions();

	// Create an image data
	VTK_CREATE(vtkImageData, projection);
	projection->SetDimensions(m_dims);
#if VTK_MAJOR_VERSION <= 5
	projection->SetNumberOfScalarComponents(3);
	projection->SetScalarTypeToUnsignedChar();
#else
	projection->AllocateScalars(VTK_UNSIGNED_CHAR, 3);
#endif

	// Scale to 0-1 range
//    double* range = data->GetScalarRange();

	// Check lengths
	int numComponents = data->GetNumberOfScalarComponents();
	std::vector<std::vector<double>> const * workingList;
	if (orientations.size() == numComponents) {
		workingList = &orientations;
	}
	else if (orientations.size() / 2 == numComponents) {
		workingList = &orientationsProjective;
	}
	else {
		std::cout << "Incorrect orientations list, data: " << numComponents << ", orientations: "
			<< orientations.size() << std::endl;
		return data;
	}

	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {

				double* value = static_cast<double*>(data->GetScalarPointer(x, y, z));

				unsigned char* target =
					static_cast<unsigned char*>(projection->GetScalarPointer(x, y, z));

				getProjectionValueComposite(value, numComponents, workingList, target);
			}
		}
	}

	return projection;
}

SP(vtkImageData) VisualizationObject::CalculateContrast(SP(vtkImageData) data, ContrastFunction contrast) {

	std::cout << "CalculateContrast!" << std::endl;

	switch (contrast.type) {
	case ContrastFunctionType::None: {
		return data;
		break;
	}
	case ContrastFunctionType::Exponential: {
		
        // 1/(e^{ (\frac{ D }{\sigma ^ 2})^p })

        // todo: replace with filter per pixel for performance
        
        double sign = pow(-1.0, contrast.flipSign);
        double sigmaInvSquared = 1 / pow(contrast.sigma, 2.0);
        
        vtkSmartPointer<vtkImageMathematics> imageMath1 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath1->SetOperationToMultiplyByK();
        imageMath1->SetConstantK(sigmaInvSquared);
        imageMath1->SetInputData(data);

		vtkSmartPointer<vtkImageMathematics> imageMath2 =
			vtkSmartPointer<vtkImageMathematics>::New();
        imageMath2->SetOperationToLog();
		imageMath2->SetInputConnection(imageMath1->GetOutputPort());
        
        vtkSmartPointer<vtkImageMathematics> imageMath3 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath3->SetOperationToMultiplyByK();
        imageMath3->SetConstantK(contrast.p);
        imageMath3->SetInputConnection(imageMath2->GetOutputPort());
        
        vtkSmartPointer<vtkImageMathematics> imageMath4 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath4->SetOperationToExp();
        imageMath4->SetInputConnection(imageMath3->GetOutputPort());
        
        vtkSmartPointer<vtkImageMathematics> imageMath5 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath5->SetOperationToMultiplyByK();
        imageMath5->SetConstantK(sign);
        imageMath5->SetInputConnection(imageMath4->GetOutputPort());

		vtkSmartPointer<vtkImageMathematics> imageMath6 =
			vtkSmartPointer<vtkImageMathematics>::New();
		imageMath6->SetOperationToExp();
		imageMath6->SetInputConnection(imageMath5->GetOutputPort());
		imageMath6->Update();

		return imageMath6->GetOutput();
		break;
	}
	case ContrastFunctionType::Polynomal: {

        double sign = -pow(-1.0, contrast.flipSign);
        double sigmaInvSquared = 1 / pow(contrast.sigma, 2.0);

        // D / sigma^2
        vtkSmartPointer<vtkImageMathematics> imageMath1 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath1->SetOperationToMultiplyByK();
        imageMath1->SetConstantK(sigmaInvSquared);
        imageMath1->SetInputData(data);
        
        // (D / sigma^2)^p
        vtkSmartPointer<vtkImageMathematics> imageMath2 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath2->SetOperationToLog();
        imageMath2->SetInputConnection(imageMath1->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath3 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath3->SetOperationToMultiplyByK();
        imageMath3->SetConstantK(contrast.p);
        imageMath3->SetInputConnection(imageMath2->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath4 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath4->SetOperationToExp();
        imageMath4->SetInputConnection(imageMath3->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath5 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath5->SetOperationToMultiplyByK();
        imageMath5->SetConstantK(sign);
        imageMath5->SetInputConnection(imageMath4->GetOutputPort());
        
        // 1/2 (D / sigma^2)^(2p)
        
        vtkSmartPointer<vtkImageMathematics> imageMath6 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath6->SetOperationToMultiplyByK();
        imageMath6->SetConstantK(2*contrast.p);
        imageMath6->SetInputConnection(imageMath2->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath7 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath7->SetOperationToExp();
        imageMath7->SetInputConnection(imageMath6->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath8 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath8->SetOperationToMultiplyByK();
        imageMath8->SetConstantK(sign*0.5);
        imageMath8->SetInputConnection(imageMath7->GetOutputPort());
        
        // 1 + (D / sigma^2)^p + 1/2 (D / sigma^2)^(2p)
        
        vtkSmartPointer<vtkImageMathematics> imageMath9 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath9->SetOperationToAdd();
        imageMath9->SetInputConnection(0, imageMath5->GetOutputPort());
        imageMath9->SetInputConnection(1, imageMath8->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath10 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath10->SetOperationToAddConstant();
        imageMath10->SetConstantC(1.0);
        imageMath10->SetInputConnection(imageMath9->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath11 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath11->SetOperationToInvert();
        imageMath11->SetInputConnection(imageMath10->GetOutputPort());
        imageMath11->Update();

		return imageMath11->GetOutput();
		break;
	}
    case ContrastFunctionType::CostFunction: {
        
        // get maximum value
        double* valuesRange = data->GetScalarRange();
        if(valuesRange[1] <= 0.)
        {
            std::cout << "Contrast CostFunction error: maximum of dataset is negative." << std::endl;
//            return data;
        }
        
        // clip negative values
        vtkSmartPointer<vtkImageThreshold> imageThreshold =
        vtkSmartPointer<vtkImageThreshold>::New();
        imageThreshold->SetInputData(data);
        imageThreshold->ThresholdByLower(0.0);
        imageThreshold->ReplaceInOn();
        imageThreshold->SetInValue(0.0);
        imageThreshold->Update();
        
        // W / ||W+||_inf
        vtkSmartPointer<vtkImageMathematics> imageMath1 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath1->SetOperationToMultiplyByK();
        imageMath1->SetConstantK(1/valuesRange[1]);
        imageMath1->SetInputData(imageThreshold->GetOutput());
        
        // | W / ||W+||_inf |
        vtkSmartPointer<vtkImageMathematics> imageMath2 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath2->SetOperationToAbsoluteValue();
        imageMath2->SetInputConnection(imageMath1->GetOutputPort());
        
        // | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath3 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath3->SetOperationToLog();
        imageMath3->SetInputConnection(imageMath2->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath4 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath4->SetOperationToMultiplyByK();
        imageMath4->SetConstantK(contrast.p);
        imageMath4->SetInputConnection(imageMath3->GetOutputPort());
        vtkSmartPointer<vtkImageMathematics> imageMath5 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath5->SetOperationToExp();
        imageMath5->SetInputConnection(imageMath4->GetOutputPort());
        
        // sigma | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath6 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath6->SetOperationToMultiplyByK();
        imageMath6->SetConstantK(contrast.sigma);
        imageMath6->SetInputConnection(imageMath5->GetOutputPort());
        
        // 1 + sigma | W / ||W+||_inf |^p
        vtkSmartPointer<vtkImageMathematics> imageMath7 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath7->SetOperationToAddConstant();
        imageMath7->SetConstantC(1.0);
        imageMath7->SetInputConnection(imageMath6->GetOutputPort());
        
        // 1 / (1 + sigma | W / ||W+||_inf |^p)
        vtkSmartPointer<vtkImageMathematics> imageMath8 =
        vtkSmartPointer<vtkImageMathematics>::New();
        imageMath8->SetOperationToInvert();
        imageMath8->SetInputConnection(imageMath7->GetOutputPort());
        imageMath8->Update();
        
        return imageMath8->GetOutput();
        break;
    }
	default:
		return nullptr;
		break;
	}
}

double VisualizationObject::getProjectionValue(std::vector<double>* list,
											   Projection::ProjectionType type) {

	double returnValue = NAN;

	switch (type) {
	case Projection::ProjectionType::MINIMUM:

		returnValue = 1e99;
		for (size_t i = 0; i < list->size(); i++) {
			if (list->at(i) < returnValue) {
				returnValue = list->at(i);
			}
		}
		break;
	case Projection::ProjectionType::MAXIMUM:

		returnValue = -1e99;
		for (size_t i = 0; i < list->size(); i++) {
			if (returnValue < list->at(i)) {
				returnValue = list->at(i);
			}
		}
		break;
	case Projection::ProjectionType::COMPOSITE:
		// Other function

		break;
	default:
		break;
	}

	return returnValue;
}

void VisualizationObject::getProjectionValueComposite(double* values, int size,
													  const std::vector<std::vector<double>>* orientations,
													  unsigned char* target) {

	// Calculate sum
	double sum = 0;
	for (size_t i = 0; i < size; i++) {
		sum += values[i];
	}

	// Inner product
	double color[3]{ 0, 0, 0 };
	for (size_t i = 0; i < size; i++) {
		double scale = values[i] / sum;

		color[0] += qAbs(orientations->at(i)[0] * scale);
		color[1] += qAbs(orientations->at(i)[1] * scale);
		color[2] += qAbs(orientations->at(i)[2] * scale);
	}

	target[0] = qRound(255.0 * color[0]);
	target[1] = qRound(255.0 * color[1]);
	target[2] = qRound(255.0 * color[2]);
}

int VisualizationObject::SnapToExisitingOrientation(std::vector<std::vector<double>>const * orientations, double* orient) {

	double radialDistance = 1e99;

	//TODO: change to radial distance

	int location = -1;

	for (int i = 0; i < orientations->size(); i++) {

		std::vector<double> tempOrient = orientations->at(i);

		double distance = sqrt(
			(tempOrient[0] - orient[0]) * (tempOrient[0] - orient[0]) +
			(tempOrient[1] - orient[1]) * (tempOrient[1] - orient[1]) +
			(tempOrient[2] - orient[2]) * (tempOrient[2] - orient[2]));

		if (distance < radialDistance) {
			radialDistance = distance;
			location = i;
		}
	}

	return location;
}

double VisualizationObject::ApplyInverseDistanceWeighting(double* dataPos,	// position in scalar dataset
														  double* dataValues, // values in scalar dataset
														  QList<QPair<double*, int>>* orientationSourceList, // local orientation of geo
														  std::vector<std::vector<double>> const *orientationDataList) { // orientation used by scalar dataset

	const int IDW_POWER = 4;

	double totalWeight = 0.0;

	QList<int> localOrientations;
	QList<double> weights;

	// Get list of source orientations
	for (int i = 0; i < orientationSourceList->size(); i++) {

		double* position = orientationSourceList->at(i).first;
		int pos = orientationSourceList->at(i).second;
		if (pos == -1) {
			continue;
		}

		localOrientations.append(pos);

		// Calculate distance to current point, weight = distance^-IDW
		double weight = sqrt(
			(dataPos[0] - position[0]) * (dataPos[0] - position[0]) +
			(dataPos[1] - position[1]) * (dataPos[1] - position[1]) +
			(dataPos[2] - position[2]) * (dataPos[2] - position[2]));

		weight = 1.0 / pow(weight, IDW_POWER);
		weights.append(weight);

		totalWeight += weight;
	}

	double value = 0.0;

	// Weight data by orientations
	for (int i = 0; i < orientationSourceList->size(); i++) {

		int pos = localOrientations.at(i);
		double localValue = dataValues[pos];
		value += localValue * weights.at(i) / totalWeight;
	}

	return value;
}

SP(vtkImageData) VisualizationObject::CalculateProjectionGeodesic(SP(vtkImageData) data,
																  QList<QPair<double*, int>> * list) {

	std::cout << "CalculateProjectionGeodesic!" << std::endl;

	int* m_dims = data->GetDimensions();
	data->ComputeBounds();
	double* bounds = data->GetBounds();

	// Create an image data
	VTK_CREATE(vtkImageData, projection);
	projection->SetDimensions(m_dims);
#if VTK_MAJOR_VERSION <= 5
	projection->SetNumberOfScalarComponents(1);
	projection->SetScalarTypeToDouble();
#else
	projection->AllocateScalars(VTK_DOUBLE, 1);
#endif

	// Check lengths
	int numComponents = data->GetNumberOfScalarComponents();
	std::vector<std::vector<double>> const * workingList;
	if (numComponents == ORIENTATIONS_FULL.size()) {
		workingList = &ORIENTATIONS_FULL;
	}
	else if (numComponents == ORIENTATIONS_PROJECTIVE.size()) {
		workingList = &ORIENTATIONS_PROJECTIVE;
	}
	else {
		std::cout << "Incorrect orientations list, data: " << numComponents << ", orientations: "
			<< ORIENTATIONS_FULL.size() << std::endl;
		return data;
	}

	// Iterate image data
	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {

				// Get position in volume
				double pos[3] = {
					x * 1.0 / m_dims[0] * (bounds[1] - bounds[0]) + bounds[0],
					y * 1.0 / m_dims[1] * (bounds[3] - bounds[2]) + bounds[2],
					z * 1.0 / m_dims[2] * (bounds[5] - bounds[4]) + bounds[4] };

				// Get source data list
				double* localValue = static_cast<double*>(data->GetScalarPointer(x, y, z));

				// Get target data
				double* target = static_cast<double*>(projection->GetScalarPointer(x, y, z));

				target[0] = ApplyInverseDistanceWeighting(pos, localValue, list, workingList);
			}
		}
	}
	//  double* dataPos,							// position in scalar dataset
	//	QList<double>*dataValues,					// values in scalar dataset
	//	QList<QPair<double*, double*>>* ,			// local orientation of geo
	//	QList<double*> *orientationDataList) {		// orientation used by scalar dataset

	return projection;
}

SP(vtkImageData) VisualizationObject::CalculateProjectionGeodesic2(SP(vtkImageData) data,
																   QList<QPair<double*, int>> * list) {

	std::cout << "CalculateProjectionGeodesic2!" << std::endl;

	int* m_dims = data->GetDimensions();
	data->ComputeBounds();
	double* bounds = data->GetBounds();

	// Create an image data
	VTK_CREATE(vtkImageData, projection);
	projection->SetDimensions(m_dims);
#if VTK_MAJOR_VERSION <= 5
	projection->SetNumberOfScalarComponents(1);
	projection->SetScalarTypeToDouble();
#else
	projection->AllocateScalars(VTK_DOUBLE, 1);
#endif

	// Check lengths
	int numComponents = data->GetNumberOfScalarComponents();
	std::vector<std::vector<double>> const * workingList;
	if (numComponents == ORIENTATIONS_FULL.size()) {
		workingList = &ORIENTATIONS_FULL;
	}
	else if (numComponents == ORIENTATIONS_PROJECTIVE.size()) {
		workingList = &ORIENTATIONS_PROJECTIVE;
	}
	else {
		std::cout << "Incorrect orientations list, data: " << numComponents << ", orientations: "
			<< ORIENTATIONS_FULL.size() << std::endl;
		return data;
	}

	// Iterate image data
	for (int z = 0; z < m_dims[2]; z++) {
		for (int y = 0; y < m_dims[1]; y++) {
			for (int x = 0; x < m_dims[0]; x++) {

				// Get position in volume
				double pos[3] = {
					x * 1.0 / m_dims[0] * (bounds[1] - bounds[0]) + bounds[0],
					y * 1.0 / m_dims[1] * (bounds[3] - bounds[2]) + bounds[2],
					z * 1.0 / m_dims[2] * (bounds[5] - bounds[4]) + bounds[4] };

				// Get source data list
				double* localValue = static_cast<double*>(data->GetScalarPointer(x, y, z));

				// Get target data
				double* target = static_cast<double*>(projection->GetScalarPointer(x, y, z));

				target[0] = ApplyInverseDistanceWeighting2(pos, localValue, list, workingList);
			}
		}
	}
	//  double* dataPos,							// position in scalar dataset
	//	QList<double>*dataValues,					// values in scalar dataset
	//	QList<QPair<double*, double*>>* ,			// local orientation of geo
	//	QList<double*> *orientationDataList) {		// orientation used by scalar dataset

	return projection;
}

double VisualizationObject::ApplyInverseDistanceWeighting2(double* dataPos,	// position in scalar dataset
														   double* dataValues, // values in scalar dataset
														   QList<QPair<double*, int>>* orientationSourceList, // local orientation of geo
														   std::vector<std::vector<double>> const *orientationDataList) { // orientation used by scalar dataset

	const int IDW_POWER = 4;

	double totalWeight = 0.0;

	QList<double> weights;
	QList<double*> orientations;

	// Get list of source orientations
	for (int i = 0; i < orientationSourceList->size(); i++) {

		// Get averaged orientation

		double* position = orientationSourceList->at(i).first;

		// Calculate distance to current point, weight = distance^-IDW
		double weight = sqrt(
			(dataPos[0] - position[0]) * (dataPos[0] - position[0]) +
			(dataPos[1] - position[1]) * (dataPos[1] - position[1]) +
			(dataPos[2] - position[2]) * (dataPos[2] - position[2]));

		weight = 1.0 / pow(weight, IDW_POWER);
		totalWeight += weight;
		weights.append(weight);
	}

	// Weight orientations
	double averagedOrientation[3] = { 0,0,0 };
	for (int i = 0; i < orientationSourceList->size(); i++) {

		std::vector<double> orientation = orientationDataList->at(orientationSourceList->at(i).second);

		averagedOrientation[0] += weights.at(i) / totalWeight * orientation[0];
		averagedOrientation[1] += weights.at(i) / totalWeight * orientation[1];
		averagedOrientation[2] += weights.at(i) / totalWeight * orientation[2];
	}

	int pos = SnapToExisitingOrientation(orientationDataList, averagedOrientation);

	std::cout
		<< dataPos[0] << " "
		<< dataPos[1] << " "
		<< dataPos[2] << "    "
		<< averagedOrientation[0] << " "
		<< averagedOrientation[1] << " "
		<< averagedOrientation[2] << std::endl;

	return dataValues[pos];
}

QList<QPair<double*, int>> VisualizationObject::AggregateOrientationData(SP(vtkPolyData) polydata, bool projective) {

	QList<QPair<double*, int>> data;

	for (vtkIdType i = 0; i < polydata->GetNumberOfPoints(); i++) {

		// Position in dataset
		double* dataPos = new double[3]{
			polydata->GetPoint(i)[0],
			polydata->GetPoint(i)[1],
			polydata->GetPoint(i)[2] };

		// Orientation in dataset
		double* vec = new double[3]{
			polydata->GetPointData()->GetVectors()->GetTuple(i)[0],
			polydata->GetPointData()->GetVectors()->GetTuple(i)[1],
			polydata->GetPointData()->GetVectors()->GetTuple(i)[2] };

		// Snap orientation to closest point in scalarDataset
		int pos;
		if (projective) {
			pos = SnapToExisitingOrientation(&ORIENTATIONS_PROJECTIVE, vec);
		}
		else {
			pos = SnapToExisitingOrientation(&ORIENTATIONS_FULL, vec);
		}

		QPair<double*, int> pair(dataPos, pos);
		data.append(pair);
	}

	return data;
}
