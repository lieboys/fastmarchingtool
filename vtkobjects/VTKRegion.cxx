/** Includes */
#include "VTKRegion.h"

#include <QDir>

#include <vtkOutlineFilter.h>
#include <vtkTextProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkImageResliceMapper.h>
#include <vtkImageProperty.h>
#include <vtkProp.h>
#include <vtkActor2D.h>
#include <vtkActor.h>
#include <vtkImageData.h>
#include <vtkScalarBarActor.h>
#include <vtkImageSlice.h>
#include <vtkPlane.h>
#include <vtkImageMapToColors.h>
#include <vtkLookupTable.h>
#include <vtkImageMathematics.h>
#include <vtkFlyingEdges3D.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkImageGaussianSmooth.h>
#include <vtkPolyDataNormals.h>

#include <vtkFeatureEdges.h>
#include <vtkStripper.h>
#include <vtkAppendPolyData.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyDataConnectivityFilter.h>

#include "RegionSource.h"

#include "common/ColorMap.h"

// DEBUG

#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>

VTKRegion* VTKRegion::CreateWithDataObject(DataObject* DataObject) {
	VTKRegion *pRet = new (std::nothrow) VTKRegion();
	if (pRet && pRet->initWithDataObject(DataObject)) {
		return pRet;
	}
	else {
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
	return nullptr;
}

void VTKRegion::Update() {

	if (this->m_settings->positionSettings.isDirty) {
		this->setPlane(this->m_settings->positionSettings);
		this->m_settings->positionSettings.isDirty = false;
	}

	if (!this->m_settings->isDirty) {
		return;
	}

	// Update visuals
	this->m_imageSlice->SetVisibility(m_settings->visible);

	this->m_actor_original->SetVisibility(m_settings->visible &&
										  m_settings->renderSmooth != 1);

	this->m_actor_smooth->SetVisibility(m_settings->visible &&
										m_settings->renderSmooth == 1);

	this->m_imageSlice->GetProperty()->SetOpacity(m_settings->opacity);
	this->m_imageSlice->GetProperty()->SetInterpolationType(this->m_settings->interpolationOrder);

	this->m_actor_original->GetProperty()->SetOpacity(m_settings->opacity);
	this->m_actor_smooth->GetProperty()->SetOpacity(m_settings->opacity);

	this->setColor(m_settings->color);

	this->m_settings->isDirty = false;
}

bool VTKRegion::initWithDataObject(DataObject* dataObject) {
	this->m_dataObject = dataObject;

	this->type = VisualType::VTYPE_ROI;

	this->m_imageSlice = SP(vtkImageSlice)::New();
	this->m_imageSlice->SetVisibility(false);

	this->m_visuals2D = SP(vtkAssembly)::New();
	this->m_visuals2D->AddPart(m_imageSlice);

	this->m_visuals3D = SP(vtkAssembly)::New();

	this->m_colormapper = SP(vtkImageMapToColors)::New();
	this->m_plane = SP(vtkPlane)::New();

	// Default values
	this->m_settings = new VisualSettingsRegion();
	this->m_settings->label = dataObject->label;

	RegionSource* mapping = dynamic_cast <RegionSource*>(this->m_dataObject);
	this->m_dims[0] = mapping->data->GetDimensions()[0];
	this->m_dims[1] = mapping->data->GetDimensions()[1];
	this->m_dims[2] = mapping->data->GetDimensions()[2];

	this->buildSlices();
	this->buildVisualsNormal();
	this->buildVisualsSmooth();

	this->Update();

	return true;
}

void VTKRegion::ReloadFromSource() {

	RegionSource* mapping = dynamic_cast <RegionSource*>(this->m_dataObject);
	this->m_dims[0] = mapping->data->GetDimensions()[0];
	this->m_dims[1] = mapping->data->GetDimensions()[1];
	this->m_dims[2] = mapping->data->GetDimensions()[2];

	this->m_visuals2D->RemovePart(this->m_imageSlice);

	this->m_visuals3D->RemovePart(this->m_actor_smooth);
	this->m_visuals3D->RemovePart(this->m_actor_original);

	this->buildSlices();
	this->buildVisualsNormal();
	this->buildVisualsSmooth();

	this->Update();
}

void VTKRegion::setColor(QColor t_color) {

	double* range = dynamic_cast<RegionSource*> (m_dataObject)->data->GetScalarRange();

	this->m_actor_original->GetProperty()->SetColor(t_color.red() / 255.0,
													t_color.green() / 255.0,
													t_color.blue() / 255.0);
	this->m_actor_smooth->GetProperty()->SetColor(t_color.red() / 255.0,
												  t_color.green() / 255.0,
												  t_color.blue() / 255.0);

	double* color = new double[4]{
		t_color.red() / 255.0,
		t_color.green() / 255.0,
		t_color.blue() / 255.0,
		1.0 };

	VTK_CREATE(vtkLookupTable, table);
	table->SetNumberOfTableValues(2);

	table->SetTableValue(0, new double[4]{ 0, 0, 0, 0 });
	table->SetTableValue(1, color);

	table->SetNanColor(0, 0, 0, 0);

	table->SetBelowRangeColor(0, 0, 0, 0);
	table->SetUseBelowRangeColor(1);
	table->SetAboveRangeColor(color);
	table->SetUseAboveRangeColor(1);

	table->SetTableRange(range[0], range[1]);
	table->SetRampToLinear();

	this->m_colormapper->SetLookupTable(table);
	this->m_colormapper->Update();
}

void VTKRegion::setPlane(PositionSettings posSettings) {

	RegionSource* source = dynamic_cast<RegionSource*> (m_dataObject);

	int* extent = source->data->GetExtent();

	double imagePosition[3] = {
		(extent[1] - extent[0]) / 2.0,
		(extent[3] - extent[2]) / 2.0,
		(extent[5] - extent[4]) / 2.0 };

	double normals[3] = { 0, 0, 0 };
	switch (posSettings.orient) {
	case SAGITTAL:
		imagePosition[0] = posSettings.position;
		normals[0] = 1;
		break;
	case CORONAL:
		imagePosition[1] = posSettings.position;
		normals[1] = 1;
		break;
	case AXIAL:
		imagePosition[2] = posSettings.position;
		normals[2] = 1;
		break;
	default:
		break;
	}

	// Update plane position and orientation
	double* worldPosition = source->transform->TransformDoublePoint(imagePosition);

	this->m_imageSlice->SetOrigin(worldPosition);
	this->m_plane->SetOrigin(worldPosition);
	this->m_plane->SetNormal(normals);
}

SP(vtkAssembly) VTKRegion::GetVisuals(int dim) {

	if (dim == 2) {
		return this->m_visuals2D;
	}
	else if (dim == 3) {
		return this->m_visuals3D;
	}
	else {
		return nullptr;
	}
}

void VTKRegion::buildSlices() {

	vtkImageData* data = dynamic_cast <RegionSource*>(this->m_dataObject)->data;

	// Create the three orthogonal planes for 2D image merging
	double normals[3] = { 0, 0, 0 };
	switch (this->m_orientation) {
	case Orientation::SAGITTAL:
		normals[0] = 1;
		break;

	case Orientation::CORONAL:
		normals[1] = 1;
		break;

	case Orientation::AXIAL:
		normals[2] = 1;
		break;
	}

	double* center = data->GetCenter();

	this->m_plane->SetNormal(normals);
	this->m_plane->SetOrigin(center[0], center[1], center[2]);

	VTK_CREATE(vtkLookupTable, table);
	double* color = new double[4]{
		this->m_settings->color.red() / 255.0,
		this->m_settings->color.green() / 255.0,
		this->m_settings->color.blue() / 255.0,
		1.0 };

	table->SetAboveRangeColor(color);
	table->SetBelowRangeColor(new double[4]{ 0, 0, 0, 0 });

	this->m_colormapper->SetLookupTable(table);
	this->m_colormapper->PassAlphaToOutputOn();

#if VTK_MAJOR_VERSION <= 5
	this->m_colormapper->SetInput(data);
#else
	this->m_colormapper->SetInputData(data);
#endif
	this->m_colormapper->Update();

	VTK_CREATE(vtkImageResliceMapper, imageMapper);
#if VTK_MAJOR_VERSION <= 5
	scalarValuesToColors->Update();
	imageMapper->SetInput(this->m_colormapper->GetOutputPort());
#else
	imageMapper->SetInputConnection(this->m_colormapper->GetOutputPort());
#endif
	imageMapper->UpdateWholeExtent();
	imageMapper->SetSlicePlane(this->m_plane);
	imageMapper->SliceAtFocalPointOff();
	imageMapper->SliceFacesCameraOff();

	this->m_imageSlice->SetMapper(imageMapper);
	this->m_imageSlice->Update();

	this->m_imageSlice->GetProperty()->BackingOff();
	this->m_imageSlice->SetOrigin(center[0], center[1], center[2]);

	this->m_visuals2D->RemovePart(this->m_imageSlice);
	this->m_visuals2D->AddPart(this->m_imageSlice);
}

void VTKRegion::buildVisualsNormal() {

	RegionSource* source = dynamic_cast<RegionSource*> (m_dataObject);
	double mid = 0.5 * (source->data->GetScalarRange()[0] + source->data->GetScalarRange()[1]);

	// Margin cubes
	vtkSmartPointer<vtkFlyingEdges3D> surface =
		vtkSmartPointer<vtkFlyingEdges3D>::New();
#if VTK_MAJOR_VERSION <= 5
	surface->SetInput(source->data);
#else
	surface->SetInputData(source->data);
#endif
	surface->ComputeNormalsOn();
	surface->SetValue(0, mid);
	surface->Update();

	// Now extract feature edges
	vtkSmartPointer<vtkFeatureEdges> boundaryEdges =
		vtkSmartPointer<vtkFeatureEdges>::New();
#if VTK_MAJOR_VERSION <= 5
	boundaryEdges->SetInput(surface->GetOutput());
#else
	boundaryEdges->SetInputData(surface->GetOutput());
#endif
	boundaryEdges->BoundaryEdgesOn();
	boundaryEdges->FeatureEdgesOff();
	boundaryEdges->NonManifoldEdgesOff();
	boundaryEdges->ManifoldEdgesOff();

	vtkSmartPointer<vtkStripper> boundaryStrips =
		vtkSmartPointer<vtkStripper>::New();
	boundaryStrips->SetInputConnection(boundaryEdges->GetOutputPort());
	boundaryStrips->Update();

	// Change the polylines into polygons
	vtkSmartPointer<vtkPolyData> boundaryPoly =
		vtkSmartPointer<vtkPolyData>::New();
	boundaryPoly->SetPoints(boundaryStrips->GetOutput()->GetPoints());
	boundaryPoly->SetPolys(boundaryStrips->GetOutput()->GetLines());

	// Merge polydatas
	vtkSmartPointer<vtkAppendPolyData> appendFilter =
		vtkSmartPointer<vtkAppendPolyData>::New();
#if VTK_MAJOR_VERSION <= 5
	appendFilter->AddInputConnection(input1->GetProducerPort());
	appendFilter->AddInputConnection(input2->GetProducerPort());
#else
	appendFilter->AddInputData(surface->GetOutput());
	appendFilter->AddInputData(boundaryPoly);
#endif
	appendFilter->Update();

	// Remove any duplicate points.
	vtkSmartPointer<vtkCleanPolyData> cleanFilter =
		vtkSmartPointer<vtkCleanPolyData>::New();
	cleanFilter->SetInputConnection(appendFilter->GetOutputPort());
	cleanFilter->Update();

	vtkSmartPointer<vtkPolyDataConnectivityFilter> confilter =
		vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	confilter->SetInputConnection(cleanFilter->GetOutputPort());
	confilter->SetExtractionModeToLargestRegion();
	confilter->Update();

	// -- Orginal
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(confilter->GetOutput());
	mapper->ScalarVisibilityOff();

	m_actor_original = vtkSmartPointer<vtkActor>::New();
	m_actor_original->SetMapper(mapper);
	m_actor_original->SetVisibility(false);
	m_actor_original->GetProperty()->SetOpacity(this->m_settings->opacity);
	m_actor_original->GetProperty()->SetColor(this->m_settings->color.red() / 255.0,
											  this->m_settings->color.green() / 255.0,
											  this->m_settings->color.blue() / 255.0);

	//m_actor_original->GetProperty()->SetBackfaceCulling(true);

	this->m_visuals3D->RemovePart(this->m_actor_original);
	this->m_visuals3D->AddPart(this->m_actor_original);
}

void VTKRegion::buildVisualsSmooth() {

	RegionSource* source = dynamic_cast<RegionSource*> (m_dataObject);
	double mid = 0.5 * (source->data->GetScalarRange()[0] + source->data->GetScalarRange()[1]);

	vtkSmartPointer<vtkImageGaussianSmooth> smoother =
		vtkSmartPointer<vtkImageGaussianSmooth>::New();
#if VTK_MAJOR_VERSION <= 5
	smoother->SetInput(source->data);
#else
	smoother->SetInputData(source->data);
#endif
	smoother->SetStandardDeviation(1.2, 1.2, 1.2);
	smoother->Update();

	vtkSmartPointer<vtkFlyingEdges3D> surface =
		vtkSmartPointer<vtkFlyingEdges3D>::New();
#if VTK_MAJOR_VERSION <= 5
	surface2->SetInput(smoother->GetOutput());
#else
	surface->SetInputData(smoother->GetOutput());
#endif
	surface->ComputeNormalsOn();
	surface->SetValue(0, mid);
	surface->Update();

	vtkSmartPointer<vtkWindowedSincPolyDataFilter> smoothFilter =
		vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
	smoothFilter->SetInputConnection(surface->GetOutputPort());
	smoothFilter->SetNumberOfIterations(15);
	smoothFilter->BoundarySmoothingOn();
	smoothFilter->FeatureEdgeSmoothingOn();
	smoothFilter->SetFeatureAngle(60.0);
	smoothFilter->SetPassBand(.001);
	smoothFilter->NonManifoldSmoothingOn();
	smoothFilter->NormalizeCoordinatesOn();
	smoothFilter->Update();

	vtkSmartPointer<vtkPolyDataNormals> normalGenerator =
		vtkSmartPointer<vtkPolyDataNormals>::New();
	normalGenerator->SetInputConnection(smoothFilter->GetOutputPort());
	normalGenerator->ComputePointNormalsOn();
	normalGenerator->ComputeCellNormalsOn();
	normalGenerator->Update();

	// Now extract feature edges
	vtkSmartPointer<vtkFeatureEdges> boundaryEdges =
		vtkSmartPointer<vtkFeatureEdges>::New();
#if VTK_MAJOR_VERSION <= 5
	boundaryEdges->SetInput(normalGenerator->GetOutput());
#else
	boundaryEdges->SetInputData(normalGenerator->GetOutput());
#endif
	boundaryEdges->BoundaryEdgesOn();
	boundaryEdges->FeatureEdgesOff();
	boundaryEdges->NonManifoldEdgesOff();
	boundaryEdges->ManifoldEdgesOff();

	vtkSmartPointer<vtkStripper> boundaryStrips =
		vtkSmartPointer<vtkStripper>::New();
	boundaryStrips->SetInputConnection(boundaryEdges->GetOutputPort());
	boundaryStrips->Update();

	// Change the polylines into polygons
	vtkSmartPointer<vtkPolyData> boundaryPoly =
		vtkSmartPointer<vtkPolyData>::New();
	boundaryPoly->SetPoints(boundaryStrips->GetOutput()->GetPoints());
	boundaryPoly->SetPolys(boundaryStrips->GetOutput()->GetLines());

	// Merge polydatas
	vtkSmartPointer<vtkAppendPolyData> appendFilter =
		vtkSmartPointer<vtkAppendPolyData>::New();
#if VTK_MAJOR_VERSION <= 5
	appendFilter->AddInputConnection(input1->GetProducerPort());
	appendFilter->AddInputConnection(input2->GetProducerPort());
#else
	appendFilter->AddInputData(surface->GetOutput());
	appendFilter->AddInputData(boundaryPoly);
#endif
	appendFilter->Update();

	// Remove any duplicate points.
	vtkSmartPointer<vtkCleanPolyData> cleanFilter =
		vtkSmartPointer<vtkCleanPolyData>::New();
	cleanFilter->SetInputConnection(appendFilter->GetOutputPort());
	cleanFilter->Update();

	vtkSmartPointer<vtkPolyDataConnectivityFilter> confilter =
		vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
	confilter->SetInputConnection(cleanFilter->GetOutputPort());
	confilter->SetExtractionModeToLargestRegion();
	confilter->Update();

	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputData(confilter->GetOutput());
	mapper->ScalarVisibilityOff();

	m_actor_smooth = vtkSmartPointer<vtkActor>::New();
	m_actor_smooth->SetMapper(mapper);
	m_actor_smooth->SetVisibility(false);
	m_actor_smooth->GetProperty()->SetOpacity(this->m_settings->opacity);
	m_actor_smooth->GetProperty()->SetColor(this->m_settings->color.red() / 255.0,
											this->m_settings->color.green() / 255.0,
											this->m_settings->color.blue() / 255.0);

	//m_actor_smooth->GetProperty()->SetBackfaceCulling(true);

	this->m_visuals3D->RemovePart(this->m_actor_smooth);
	this->m_visuals3D->AddPart(this->m_actor_smooth);
}