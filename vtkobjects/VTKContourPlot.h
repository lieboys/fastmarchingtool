#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"
#include "common/Macros.h"

#include <vector>

#include <vtkImageData.h>
#include <vtkSmartPointer.h>

class ColorMap;

/** Includes - VTK */
class vtkScalarBarActor;
class vtkPlane;
class vtkCamera;

class VTKContourPlot : public VisualizationObject {
public:

	/** Functions */

	/** Constructor from data object. */
	static VTKContourPlot* CreateWithDataObject(DataObject* dataObject);

	SP(vtkAssembly) GetVisuals(int dim);

	void Update();
	
	VisualSettings* GetSettings() {
		return this->m_settings;
	};
	void SetSettings(VisualSettingsContourPlot* newSettings) {
		this->m_settings = newSettings;
		this->Update();
	}
	void SetSettings(VisualSettings* newSettings) {
		if (dynamic_cast<VisualSettingsContourPlot*>(newSettings)) {
			this->SetSettings(dynamic_cast<VisualSettingsContourPlot*>(newSettings));
		}
	}

	void ReloadFromSource();


	/** Variables */

	/* Object of this VisualObject's colorbar */
	SP(vtkScalarBarActor) colorbar;

	/* Connection to the current camera, for forcing correct depth peeling. */
	SP(vtkCamera) camera;

private:

	/** Functions */

	bool initWithDataObject(DataObject* dataObject);

	void setSlicePlane(PositionSettings posSettings);

	void setColorMap(ColorMap* map);

	void buildData();
	void buildContours();
	void buildContoursSmooth();

	VisualSettingsContourPlot* m_settings;

	/** Variables */

	SP(vtkImageData) m_projectionData;
	SP(vtkImageData) m_finalData;

	SP(vtkPlane) m_slicePlane;
	SP(vtkActor) m_smoothActor3D;
	SP(vtkActor) m_originalActor3D;

	SP(vtkActor) m_smoothActor2D;
	SP(vtkActor) m_actor2D;

};
