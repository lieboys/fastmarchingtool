#pragma once

/** Includes - Main Headers */
#include "VisualizationObject.h"

#include <vtkOrientationMarkerWidget.h>
#include <vtkSmartPointer.h>

#include <QList>

#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkOrientationMarkerWidget.h"

/** Includes - VTK */

/** Forward declarations */
class Controller;
class Model;

class VTKRegion;
class QListWidget;
class QComboBox;

/**
Main visual controller, that manages all visualizations and rendering.
Contains the info on how each VisualObject is visualised, as well as its navigation.
*/
class Visualization {

public:

	struct RenderProperties {
		PositionSettings positionSettings;
		int interpolationOrder = 1;

		bool isDirty = true;
	};
	
	/**
	 * Model constructor
	 * @param    argumentList   List of command-line arguments
	 * @param    controller       Reference to the controller
	 */
	Visualization();

	/**
	 * Model destructor
	 */
	~Visualization();

	void setMVC(Controller* controller, Model* model) {
		this->m_controller = controller;
		this->m_model = model;
	}

	void BindRenderWindow(vtkRenderWindow* renderWindow);
	void Setup();
	void ResetCamera();

	void AddVisualObject(VisualizationObject* object);

	void RemoveVisualObjectFrom(DataObject * object);

	void UpdateVisualObjectFrom(DataObject * object);

	void Render();

	void UpdateVisuals(VisualSettings * m_settings);

	void SetFocus(double x, double y, double z);

	void UpdateNavigation();

	void RenameObj(QString old, QString next);

	void SetObjectDimensionality(int dim);

	void SetInterpolation(int order);

	void matchOrientationData();

	bool CheckGeodesicLoaded();

	SP(vtkImageData) GetExportData(VisualType visualType, Projection::ProjectionType type);

	VisualizationObject* GetVisualObject(VisualType type, QString label);
	VisualizationObject* GetVisualObjectByType(VisualType type, QString label = "");

	RenderProperties renderProperties;

	SP(vtkRenderWindow) GetRenderWindow() {
		return this->m_renderWindow;
	}

private:

	void setupOrientationWidget();

	/** Variables */
	vtkRenderWindow* m_renderWindow;

	Controller* m_controller;
	Model* m_model;

	SP(vtkRenderer) m_renderer;

	QList<VisualizationObject*> m_visualsObjects;

	SP(vtkOrientationMarkerWidget) m_orientationWidget = nullptr;
	
	enum Dimensionality {
		DIM_2D = 2,
		DIM_3D = 3,
	};

	Dimensionality m_dimensionality;

};
