#pragma once

#include <QDialog>
#include <QList>
#include <QPair>
#include <QString>

#include "ui_FilterDialog.h"

/* Dialog for importing data from a binary file, is only used for filtering the available datasets before import.*/
class FilterDialog : public QDialog, private Ui::FilterDialog {
	Q_OBJECT

public:

	FilterDialog(QList<QPair<QString, QString>> data);

	~FilterDialog();

	QStringList selection;

private:
	void done(int r);

	Ui_FilterDialog* m_ui;

	private slots:
};
