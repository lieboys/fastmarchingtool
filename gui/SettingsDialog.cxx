#include "SettingsDialog.h"

#include "database/DataIOManager.h"

#include "processing/CodeLinkManager.h"

#include <QSettings>
#include <QFileDialog>

// Settings labels
const QString SettingsDialog::SETTINGS_CODE_PATH_LABEL = "FM_CODE_LOCAL_PATH";
const QString SettingsDialog::SETTINGS_CODE_RUN_OPTION = "FM_CODE_LOCAL_OPTION";
const QString SettingsDialog::SETTINGS_CODE_SERVER_LABEL = "FM_CODE_SERVER_PATH";
const QString SettingsDialog::SETTINGS_CODE_SERVER_USERNAME = "SETTINGS_CODE_SERVER_USERNAME";
const QString SettingsDialog::SETTINGS_CODE_SERVER_PASSWORD = "SETTINGS_CODE_SERVER_PASSWORD";
const QString SettingsDialog::SETTINGS_CODE_SERVER_DATA_DIRECTORY = "SETTINGS_CODE_SERVER_DATA_DIRECTORY";
const QString SettingsDialog::SETTINGS_CODE_SSH_LABEL = "FM_CODE_SSH";
const QString SettingsDialog::SETTINGS_CODE_PUTTY_DIRECTORY = "SETTINGS_CODE_PUTTY_DIRECTORY";

// Experiment labels
const QString SettingsDialog::SETTINGS_EXP_BUNDLE_MESH_LABEL = "SETTINGS_EXP_BUNDLE_MESH_LABEL";
const QString SettingsDialog::SETTINGS_EXP_BUNDLE_TYPE_LABEL = "SETTINGS_EXP_BUNDLE_TYPE_LABEL";
const QString SettingsDialog::SETTINGS_EXP_GRIDSCALE_LABEL = "SETTINGS_EXP_GRIDSCALE_LABEL";
const QString SettingsDialog::SETTINGS_EXP_EPS_LABEL = "SETTINGS_EXP_EPS_LABEL";
const QString SettingsDialog::SETTINGS_EXP_XI_LABEL = "SETTINGS_EXP_XI_LABEL";
const QString SettingsDialog::SETTINGS_EXP_SIGMALABEL = "SETTINGS_EXP_SIGMALABEL";
const QString SettingsDialog::SETTINGS_EXP_P_LABEL = "SETTINGS_EXP_P_LABEL";
const QString SettingsDialog::SETTINGS_EXP_APPLY_FOD_CONTRAST = "SETTINGS_EXP_APPLY_FOD_CONTRAST";
const QString SettingsDialog::SETTINGS_EXP_TRANSPOSE = "SETTINGS_EXP_TRANSPOSE";
const QString SettingsDialog::SETTINGS_EXP_STOP_TIPS_REACHED = "SETTINGS_EXP_STOP_TIPS_REACHED";

const QString SettingsDialog::SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL = "SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL";
const QString SettingsDialog::SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL = "SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL";

// ROI segmentation
const QString SettingsDialog::SETTINGS_ROI_ITKSNAP_COMMAND = "SETTINGS_ROI_ITKSNAP_COMMAND";


SettingsDialog::SettingsDialog() : QDialog() {

	this->m_ui = new Ui_SettingsDialog;
	this->m_ui->setupUi(this);


	connect(this->m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(this->m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	connect(this->m_ui->locatePathButton, SIGNAL(clicked()), this, SLOT(slotSpecifyLocalPath()));
	connect(this->m_ui->itkSnapEditButton, SIGNAL(clicked()), this, SLOT(slotSpecifyITKSnapPath()));
	connect(this->m_ui->runLocatePuttyPath, SIGNAL(clicked()), this, SLOT(slotSpecifyPuttyPath()));
    
    connect(this->m_ui->applyFODContrastCheckBox, SIGNAL(clicked(bool)), this, SLOT(slotApplyFodChanged(bool)));
	
	updateInterfaceFromSettings();
    
    this->m_ui->codeLinkGridScaleEdit->setSingleStep(0.1);
    this->m_ui->codeLinkEpsilonValueBox->setSingleStep(0.1);
    this->m_ui->codeLinkXiValueBox->setSingleStep(0.1);
    this->m_ui->codeLinkPValueBox->setSingleStep(0.1);
    this->m_ui->codeLinkSigmaValueBox->setSingleStep(0.1);
}

SettingsDialog::~SettingsDialog() {


}

void SettingsDialog::done(int r) {


	if (QDialog::Accepted == r)
    {
//        QString localPath = this->m_ui->runLocalPathEdit->text();
//        if (!QFile().exists(localPath) &&
//            this->m_ui->runLocalRadioButton->isChecked()) {
//
//            this->m_ui->statusLabel->setText("Local path to executable is incorrect!");
//
//            return;
//        }
		updateSettingsFromInterface();

		QDialog::done(r);
		return;
	}
	else if (QDialog::Rejected == r){

		QDialog::done(r);
		return;
	}

}

void SettingsDialog::slotSpecifyLocalPath() {

#ifdef _WIN32
	QString filepath = QFileDialog::getOpenFileName(this, tr("Locate local FM executable"),
													"", "Executable (*.exe);;Any files (*)");
#else

	QString filepath = QFileDialog::getOpenFileName(this, tr("Locate local FM executable"),
													"", "Any files (*)");
#endif

	if (filepath.isEmpty()) {
		return;
	}

	this->m_ui->runLocalPathEdit->setText(filepath);
	
}


void SettingsDialog::slotSpecifyITKSnapPath() {

#ifdef _WIN32
	QString filepath = QFileDialog::getOpenFileName(this, tr("Locate local ITK_SNAP executable"),
													"", "Executable (*.exe);;Any files (*)");
#else

	QString filepath = QFileDialog::getOpenFileName(this, tr("Locate local FM executable"),
													"", "Any files (*)");
#endif

	if (filepath.isEmpty()) {
		return;
	}

	this->m_ui->itkSnapCommandEdit->setText(filepath);

}

void SettingsDialog::slotSpecifyPuttyPath() {

	QString filepath = QFileDialog::getExistingDirectory(this, tr("Locate local PuTTy directory"), "",
									QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	if (filepath.isEmpty()) {
		return;
	}

	this->m_ui->runPuttyPath->setText(filepath);

}


void SettingsDialog::slotApplyFodChanged(bool value)
{
    if(value == true)
    {
        this->m_ui->codeLinkPValueBox->setEnabled(true);
        this->m_ui->codeLinkSigmaValueBox->setEnabled(true);
    }
    else
    {
        this->m_ui->codeLinkPValueBox->setEnabled(false);
        this->m_ui->codeLinkSigmaValueBox->setEnabled(false);
    }
}

void SettingsDialog::updateInterfaceFromSettings() {


	QSettings m_settings;

	// Codelink
	QString option = m_settings.value(SETTINGS_CODE_RUN_OPTION).toString();
	if (option == "LOCAL") {
		this->m_ui->runLocalRadioButton->setChecked(true);
		this->m_ui->runRemoteRadioButton->setChecked(false);
	}
	else if (option == "REMOTE") {
		this->m_ui->runLocalRadioButton->setChecked(false);
		this->m_ui->runRemoteRadioButton->setChecked(true);
	}

	this->m_ui->runLocalPathEdit->setText(
		m_settings.value(SETTINGS_CODE_PATH_LABEL).toString());

	this->m_ui->runServerEdit->setText(
		m_settings.value(SETTINGS_CODE_SERVER_LABEL).toString());
    this->m_ui->runServerUsername->setText(
        m_settings.value(SETTINGS_CODE_SERVER_USERNAME).toString());
    this->m_ui->runServerPassword->setText(
        m_settings.value(SETTINGS_CODE_SERVER_PASSWORD).toString());
    this->m_ui->dataDirectoryEdit->setText(
        m_settings.value(SETTINGS_CODE_SERVER_DATA_DIRECTORY).toString());
    this->m_ui->runSshEdit->setText(
        m_settings.value(SETTINGS_CODE_SSH_LABEL).toString());
    
    this->m_ui->sshCommandPlainText->document()->setPlainText(
        QString("%1 %2/%3_input %2/%3_output").arg(
            m_settings.value(SETTINGS_CODE_SSH_LABEL).toString(),
            m_settings.value(SETTINGS_CODE_SERVER_DATA_DIRECTORY).toString(),
            "{experimentname}"));

	// Experiment settings
	this->m_ui->codeLinkBundleMeshEdit->setCurrentText(
		m_settings.value(SETTINGS_EXP_BUNDLE_MESH_LABEL).toString());
	this->m_ui->codeLinkBundleTypeEdit->setCurrentText(
		m_settings.value(SETTINGS_EXP_BUNDLE_TYPE_LABEL).toString());

	this->m_ui->codeLinkGridScaleEdit->setValue(
		m_settings.value(SETTINGS_EXP_GRIDSCALE_LABEL).toDouble());

	this->m_ui->codeLinkEpsilonValueBox->setValue(
		m_settings.value(SETTINGS_EXP_EPS_LABEL).toDouble());
	this->m_ui->codeLinkXiValueBox->setValue(
		m_settings.value(SETTINGS_EXP_XI_LABEL).toDouble());
	this->m_ui->codeLinkSigmaValueBox->setValue(
		m_settings.value(SETTINGS_EXP_SIGMALABEL).toDouble());
	this->m_ui->codeLinkPValueBox->setValue(
		m_settings.value(SETTINGS_EXP_P_LABEL).toDouble());

	this->m_ui->codeLinkExportEuclCheck->setChecked(
		m_settings.value(SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL).toBool());
	this->m_ui->codeLinkExportLiftCheck->setChecked(
		m_settings.value(SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL).toBool());
    this->m_ui->applyFODContrastCheckBox->setChecked(
        m_settings.value(SETTINGS_EXP_APPLY_FOD_CONTRAST).toBool());
    this->m_ui->codeLinkStopTipsReachedCheck->setChecked(
        m_settings.value(SETTINGS_EXP_STOP_TIPS_REACHED).toBool());
    this->m_ui->codeLinkTransposeCheck->setChecked(
        m_settings.value(SETTINGS_EXP_TRANSPOSE).toBool());

	this->m_ui->runPuttyPath->setText(
		m_settings.value(SETTINGS_CODE_PUTTY_DIRECTORY).toString());
    
    // enable p and sigma if apply fod enabled
    slotApplyFodChanged(m_settings.value(SETTINGS_EXP_APPLY_FOD_CONTRAST).toBool());

	// Segmentation
	this->m_ui->itkSnapCommandEdit->setText(
		m_settings.value(SETTINGS_ROI_ITKSNAP_COMMAND).toString());
    
    
}

void SettingsDialog::updateSettingsFromInterface() {

	// Save values in UI to settings
	QSettings m_settings;

	// CodeLink settings
	QString option = m_settings.value(SETTINGS_CODE_PATH_LABEL).toString();
	if (this->m_ui->runLocalRadioButton->isChecked()) {
		m_settings.setValue(SETTINGS_CODE_RUN_OPTION, "LOCAL");
	}
	else if (this->m_ui->runRemoteRadioButton->isChecked()) {
		m_settings.setValue(SETTINGS_CODE_RUN_OPTION, "REMOTE");
	}

	m_settings.setValue(SETTINGS_CODE_PATH_LABEL,
					  this->m_ui->runLocalPathEdit->text());
	m_settings.setValue(SETTINGS_CODE_SERVER_LABEL,
					  this->m_ui->runServerEdit->text());
    m_settings.setValue(SETTINGS_CODE_SERVER_USERNAME,
                        this->m_ui->runServerUsername->text());
    m_settings.setValue(SETTINGS_CODE_SERVER_PASSWORD,
                        this->m_ui->runServerPassword->text());
    m_settings.setValue(SETTINGS_CODE_SERVER_DATA_DIRECTORY,
                        this->m_ui->dataDirectoryEdit->text());
	m_settings.setValue(SETTINGS_CODE_SSH_LABEL,
					  this->m_ui->runSshEdit->text());
	m_settings.setValue(SETTINGS_CODE_PUTTY_DIRECTORY,
					  this->m_ui->runPuttyPath->text());

	// CodeLink experiment settings
	m_settings.setValue(SETTINGS_EXP_BUNDLE_MESH_LABEL,
					  this->m_ui->codeLinkBundleMeshEdit->currentText());
	m_settings.setValue(SETTINGS_EXP_BUNDLE_TYPE_LABEL,
					  this->m_ui->codeLinkBundleTypeEdit->currentText());

	m_settings.setValue(SETTINGS_EXP_GRIDSCALE_LABEL,
					  this->m_ui->codeLinkGridScaleEdit->value());

	m_settings.setValue(SETTINGS_EXP_EPS_LABEL,
					  this->m_ui->codeLinkEpsilonValueBox->value());
	m_settings.setValue(SETTINGS_EXP_XI_LABEL,
					  this->m_ui->codeLinkXiValueBox->value());
	m_settings.setValue(SETTINGS_EXP_SIGMALABEL,
					  this->m_ui->codeLinkSigmaValueBox->value());
	m_settings.setValue(SETTINGS_EXP_P_LABEL,
					  this->m_ui->codeLinkPValueBox->value());

	m_settings.setValue(SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL,
					  this->m_ui->codeLinkExportEuclCheck->isChecked());
	m_settings.setValue(SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL,
					  this->m_ui->codeLinkExportLiftCheck->isChecked());
    m_settings.setValue(SETTINGS_EXP_APPLY_FOD_CONTRAST,
                        this->m_ui->applyFODContrastCheckBox->isChecked());
    m_settings.setValue(SETTINGS_EXP_TRANSPOSE,
                        this->m_ui->codeLinkTransposeCheck->isChecked());
    m_settings.setValue(SETTINGS_EXP_STOP_TIPS_REACHED,
                        this->m_ui->codeLinkStopTipsReachedCheck->isChecked());

	// Segmentation
	m_settings.setValue(SETTINGS_ROI_ITKSNAP_COMMAND,
					  this->m_ui->itkSnapCommandEdit->text());
	
}

void SettingsDialog::selectTab(int nr)
{
    this->m_ui->tabWidget->setCurrentIndex(nr);
}

CodeLinkManager::CodeRunSettings SettingsDialog::GetExpFromSettings() {

	// Save values in UI to settings
	QSettings m_settings;

	CodeLinkManager::CodeRunSettings output;

	output.params.bundleMesh =
		m_settings.value(SETTINGS_EXP_BUNDLE_MESH_LABEL).toString();

	output.params.bundleType =
		m_settings.value(SETTINGS_EXP_BUNDLE_TYPE_LABEL).toString();

	output.params.gridScale =
		m_settings.value(SETTINGS_EXP_GRIDSCALE_LABEL).toDouble();

	output.params.epsilon =
		m_settings.value(SETTINGS_EXP_EPS_LABEL).toDouble();

	output.params.xi=
		m_settings.value(SETTINGS_EXP_XI_LABEL).toDouble();

	output.params.sigma=
		m_settings.value(SETTINGS_EXP_SIGMALABEL).toDouble();

	output.params.p =
		m_settings.value(SETTINGS_EXP_P_LABEL).toDouble();

	output.params.exportValuesEucl =
		m_settings.value(SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL).toBool();

	output.params.exportValuesLift =
		m_settings.value(SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL).toBool();
    
    output.params.applyFODTransformation =
        m_settings.value(SETTINGS_EXP_APPLY_FOD_CONTRAST).toBool();
    
    output.params.stopWhenTipsReached =
        m_settings.value(SETTINGS_EXP_STOP_TIPS_REACHED).toBool();
    
    output.params.transposeCoordinates =
        m_settings.value(SETTINGS_EXP_TRANSPOSE).toBool();
    
    // remote settings
    
    output.remoteSettings.serverPath =
        m_settings.value(SETTINGS_CODE_SERVER_LABEL).toString();
    
    output.remoteSettings.dataDirectory =
        m_settings.value(SETTINGS_CODE_SERVER_DATA_DIRECTORY).toString();
    
    output.remoteSettings.username =
        m_settings.value(SETTINGS_CODE_SERVER_USERNAME).toString();
    
    output.remoteSettings.password =
        m_settings.value(SETTINGS_CODE_SERVER_PASSWORD).toString();

	return output;
}

CodeLinkManager::CodeParameters SettingsDialog::GetParameters()
{
    CodeLinkManager::CodeParameters codeParameters;
    QSettings m_settings;
    
    codeParameters.bundleMesh = m_settings.value(SETTINGS_EXP_BUNDLE_MESH_LABEL).toString();
    codeParameters.bundleType = m_settings.value(SETTINGS_EXP_BUNDLE_TYPE_LABEL).toString();
    
    codeParameters.gridScale = m_settings.value(SETTINGS_EXP_GRIDSCALE_LABEL).toDouble();
    codeParameters.epsilon = m_settings.value(SETTINGS_EXP_EPS_LABEL).toDouble();
    codeParameters.xi = m_settings.value(SETTINGS_EXP_XI_LABEL).toDouble();
    codeParameters.sigma = m_settings.value(SETTINGS_EXP_SIGMALABEL).toDouble();
    codeParameters.p = m_settings.value(SETTINGS_EXP_P_LABEL).toDouble();
    
    codeParameters.exportValuesEucl = m_settings.value(SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL).toBool();
    codeParameters.exportValuesLift = m_settings.value(SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL).toBool();
    codeParameters.applyFODTransformation = m_settings.value(SETTINGS_EXP_APPLY_FOD_CONTRAST).toBool();
    codeParameters.stopWhenTipsReached = m_settings.value(SETTINGS_EXP_STOP_TIPS_REACHED).toBool();
    codeParameters.transposeCoordinates = m_settings.value(SETTINGS_EXP_TRANSPOSE).toBool();
    
    return codeParameters;
}



