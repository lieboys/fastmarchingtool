#include "FilterDialog.h"

#include "database/DataIOManager.h"

FilterDialog::FilterDialog(QList<QPair<QString, QString>> data) : QDialog() {

	this->m_ui = new Ui_FilterDialog;
	this->m_ui->setupUi(this);

	this->m_ui->volumeListWIdget->clear();
	this->m_ui->geoListWIdget->clear();
	this->m_ui->regionListWidget->clear();

	QListIterator<QPair<QString, QString>> i(data);
	while (i.hasNext()) {

		QPair<QString, QString> pair = i.next();

		QListWidgetItem* item = new QListWidgetItem(pair.second);
		item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
		item->setCheckState(Qt::Unchecked);

		if (BinairDefinitions::BIN_NAMES_ScalarVolumes.contains(pair.second)){
			if (this->m_ui->volumeListWIdget->count() == 0) {
				item->setCheckState(Qt::Checked);
			}
			this->m_ui->volumeListWIdget->addItem(item);
		}
		else if (BinairDefinitions::BIN_NAMES_Geodesics.contains(pair.second)) {
			if (this->m_ui->geoListWIdget->count() == 0) {
				item->setCheckState(Qt::Checked);
			}
			this->m_ui->geoListWIdget->addItem(item);
		}
		else if (BinairDefinitions::BIN_NAMES_Regions.contains(pair.second)) {
			if (this->m_ui->regionListWidget->count() == 0) {
				item->setCheckState(Qt::Checked);
			}
			this->m_ui->regionListWidget->addItem(item);
		}
		else {
			//
		}
	}

	connect(this->m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(this->m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

FilterDialog::~FilterDialog() {


}

void FilterDialog::done(int r) {

	this->selection.clear();

	if (QDialog::Accepted == r) {

		// Volume items
		for (int i = 0; i < this->m_ui->volumeListWIdget->count(); i++) {
			
			QListWidgetItem* item = this->m_ui->volumeListWIdget->item(i);
			if (item->checkState() == Qt::CheckState::Checked) {
				this->selection.append(item->text());
			}
		}

		// Point items
		for (int i = 0; i < this->m_ui->geoListWIdget->count(); i++) {

			QListWidgetItem* item = this->m_ui->geoListWIdget->item(i);
			if (item->checkState() == Qt::CheckState::Checked) {
				this->selection.append(item->text());
			}
		}

		// Point items
		for (int i = 0; i < this->m_ui->regionListWidget->count(); i++) {

			QListWidgetItem* item = this->m_ui->regionListWidget->item(i);
			if (item->checkState() == Qt::CheckState::Checked) {
				this->selection.append(item->text());
			}
		}

		QDialog::done(r);
		return;
	}

	if (QDialog::Rejected == r) {

		QDialog::done(r);
		return;
	}

}
