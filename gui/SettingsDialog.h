#pragma once

#include <QDialog>
#include <QList>
#include <QPair>
#include <QString>

#include "ui_SettingsDialog.h"

#include "processing/CodeLinkManager.h"

/* Global settings dialog for the application-wide settings. Contains functionality for reading from the saved settings in the registry. */
class SettingsDialog : public QDialog, private Ui::SettingsDialog {
	Q_OBJECT

public:

	SettingsDialog();

	~SettingsDialog();

	void done(int r);

	void updateInterfaceFromSettings();
	void updateSettingsFromInterface();
    
    void selectTab(int nr);

	// Settings labels
	const static QString SETTINGS_CODE_PATH_LABEL;
	const static QString SETTINGS_CODE_RUN_OPTION;
	const static QString SETTINGS_CODE_SERVER_LABEL;
    const static QString SETTINGS_CODE_SERVER_USERNAME;
    const static QString SETTINGS_CODE_SERVER_PASSWORD;
    const static QString SETTINGS_CODE_SERVER_DATA_DIRECTORY;
	const static QString SETTINGS_CODE_SSH_LABEL;
	const static QString SETTINGS_CODE_PUTTY_DIRECTORY;

	// Experiment labels
	const static QString SETTINGS_EXP_BUNDLE_MESH_LABEL;
	const static QString SETTINGS_EXP_BUNDLE_TYPE_LABEL;

	const static QString SETTINGS_EXP_GRIDSCALE_LABEL;
	const static QString SETTINGS_EXP_EPS_LABEL;
	const static QString SETTINGS_EXP_XI_LABEL;
	const static QString SETTINGS_EXP_SIGMALABEL;
	const static QString SETTINGS_EXP_P_LABEL;
    const static QString SETTINGS_EXP_APPLY_FOD_CONTRAST;
    const static QString SETTINGS_EXP_STOP_TIPS_REACHED;
    const static QString SETTINGS_EXP_TRANSPOSE;

	const static QString SETTINGS_EXP_EXPORT_VALUES_EUCL_LABEL;
	const static QString SETTINGS_EXP_EXPORT_VALUES_LIFT_LABEL;

	// ROI segmentation
	const static QString SETTINGS_ROI_ITKSNAP_COMMAND;

	static CodeLinkManager::CodeRunSettings GetExpFromSettings();
    static CodeLinkManager::CodeParameters GetParameters();

private:

	Ui_SettingsDialog* m_ui;

	private slots:

	void slotSpecifyLocalPath();
	void slotSpecifyITKSnapPath();
	void slotSpecifyPuttyPath();
    void slotApplyFodChanged(bool value);
};
