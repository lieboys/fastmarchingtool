/** Includes */
#include "Visualization.h"

#include <cmath>

#include <QDir>
#include <QFile>

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkActor.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkAssembly.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPolyDataReader.h>
#include <vtkPointData.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkLookupTable.h>
#include <vtkInteractorStyleImage.h>
#include <vtkOBJReader.h>
#include <vtkDataSetMapper.h>

#include "VTKGlyphs.h"
#include "VTKScalarProjection.h"
#include "VTKRegion.h"
#include "VTKContourPlot.h"
#include "VTKGeodesics.h"
#include "VTKAnatomicalVolume.h"

#include "Controller.h"
#include "Model.h"

#include "common/Interactor2D.h"

//TODO: move
#include "database/GeodesicSource.h"

// OPENGL includes
#include "vtkActor.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkMath.h"
#include "vtkNew.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkPLYReader.h"
#include "vtkPolyDataNormals.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkShaderProgram.h"
#include "vtkShader.h"
#include "vtkTimerLog.h"
#include "vtkOpenGLVertexBufferObject.h"
#include "vtkOpenGLIndexBufferObject.h"
#include "vtkOpenGLVertexArrayObject.h"

#include <QListWidget>
#include <QComboBox>
#include <QPainter>

//---------------[ Constructor ]-------------------

Visualization::Visualization() {

	this->m_dimensionality = DIM_3D;
	this->m_controller = nullptr;
	this->m_model = nullptr;

	this->m_renderWindow = nullptr;
	this->m_renderer = nullptr;
}

//---------------[ Destructor ]-------------------

Visualization::~Visualization() {
}

void Visualization::BindRenderWindow(vtkRenderWindow* renderWindow) {
	this->m_renderWindow = renderWindow;
}

void Visualization::Setup() {

	m_renderer = vtkSmartPointer<vtkRenderer>::New();
	m_renderer->SetLayer(0);
	m_renderer->SetInteractive(true);
	m_renderer->GetActiveCamera()->SetParallelProjection(true);
	m_renderer->ResetCameraClippingRange();
	m_renderer->SetBackground(0, 0, 0); // Background color
	m_renderWindow->AddRenderer(m_renderer);

	VTK_CREATE(vtkRenderWindowInteractor, renderWindowInteractor);
	VTK_CREATE(vtkInteractorStyleTrackballCamera, style);
	renderWindowInteractor->SetInteractorStyle(style);

	m_renderWindow->SetInteractor(renderWindowInteractor);

	// anti-aliasing
	m_renderWindow->SetMultiSamples(12);

	Render();
	m_renderWindow->Start();
}

void Visualization::setupOrientationWidget() {

	if (this->m_orientationWidget == nullptr) {

		this->m_orientationWidget = SP(vtkOrientationMarkerWidget)::New();

		QString headPath = QDir(QCoreApplication::applicationDirPath())
			.absoluteFilePath("head.obj");

		if (!QFile(headPath).exists()) {
			VTK_CREATE(vtkAxesActor, axis);
			axis->SetXAxisLabelText("R");
			axis->SetYAxisLabelText("A");
			axis->SetZAxisLabelText("S");

			this->m_orientationWidget->SetOrientationMarker(axis);
		}
		else {

			vtkSmartPointer<vtkOrientationMarkerWidget> orientationMarker =
				vtkSmartPointer<vtkOrientationMarkerWidget>::New();
			orientationMarker->SetOutlineColor(0.9300, 0.5700, 0.1300);

			vtkSmartPointer<vtkOBJReader> reader =
				vtkSmartPointer<vtkOBJReader>::New();
			reader->SetFileName(headPath.toStdString().c_str());
			reader->Update();

			vtkSmartPointer<vtkDataSetMapper> iconMapper =
				vtkSmartPointer<vtkDataSetMapper>::New();
			iconMapper->SetInputConnection(reader->GetOutputPort());

			vtkSmartPointer<vtkActor> iconActor =
				vtkSmartPointer<vtkActor>::New();
			iconActor->SetMapper(iconMapper);

			this->m_orientationWidget->SetOrientationMarker(iconActor);
		}

		this->m_orientationWidget->SetViewport(-0.05, -0.05, 0.15, 0.15);
		this->m_orientationWidget->SetCurrentRenderer(m_renderer);
		this->m_orientationWidget->SetInteractor(m_renderWindow->GetInteractor());
		this->m_orientationWidget->SetEnabled(true);
		this->m_orientationWidget->SetInteractive(false);
	}
}

void Visualization::ResetCamera() {

	setupOrientationWidget();

	this->m_renderer->ResetCamera();

	vtkCamera* camera = this->m_renderer->GetActiveCamera();
	double dist = camera->GetDistance();
	double origin[3];
	camera->GetFocalPoint(origin);
	camera->SetParallelProjection(true);

	switch (this->renderProperties.positionSettings.orient) {
	case Orientation::AXIAL:
		camera->SetPosition(origin[0], origin[1], origin[2] + dist);
		camera->SetViewUp(0.0, 1.0, 0.0);
		break;

	case Orientation::CORONAL:
		camera->SetPosition(origin[0], origin[1] - dist, origin[2]);
		camera->SetViewUp(0.0, 0.0, 1.0);
		break;

	case Orientation::SAGITTAL:
		camera->SetPosition(origin[0] + dist, origin[1], origin[2]);
		camera->SetViewUp(0.0, 0.0, 1.0);
		break;

	default:
		camera->SetViewUp(0.0, 1.0, 0.0);
		break;
	}

	this->m_renderer->ResetCameraClippingRange();
	this->m_renderer->Render();
}

void Visualization::AddVisualObject(VisualizationObject* object) {

	// Check for existing labels
	QString newLabel = object->GetSettings()->label;
	int cnt = 1;

	while (this->GetVisualObject(object->type, newLabel) != nullptr) {
		newLabel = object->GetSettings()->label + " " + QString::number(cnt);
		cnt++;
	}
	object->GetSettings()->label = newLabel;

	std::vector<int> extent(6, 0);
	this->m_model->RequestNavigationExtent(extent);

	this->SetFocus(
		(extent[1] - extent[0]) / 2.0,
		(extent[3] - extent[2]) / 2.0,
		(extent[5] - extent[4]) / 2.0);

	vtkAssembly* visuals = object->GetVisuals(this->m_dimensionality);
	this->m_renderer->AddActor(visuals);

	this->m_visualsObjects.append(object);

	// VTYPE specific
	VTKScalarProjection* scalarMap = dynamic_cast<VTKScalarProjection*> (object);
	if (scalarMap != nullptr) {
		this->m_renderer->AddActor2D(scalarMap->colorbar);
	}
	//VTKRegion* region = dynamic_cast<VTKRegion*> (object);
	//if (region != nullptr) {
	//	AddListObjectToUI(region, this->m_controller->ui->regionListWidget);
	//	this->m_controller->slotRegionListSelectionChanged();

	//}
	VTKContourPlot* contour = dynamic_cast<VTKContourPlot*> (object);
	if (contour != nullptr) {
		this->m_renderer->AddActor2D(contour->colorbar);
		contour->camera = this->m_renderer->GetActiveCamera();
	}
	//VTKGeodesics* geo = dynamic_cast<VTKGeodesics*> (object);
	//if (geo != nullptr) {
	//	//this->m_controller->slotGeoListSelectionChanged(); TODO

	//}
	VTKGlyphs* glyphs = dynamic_cast<VTKGlyphs*> (object);
	if (glyphs != nullptr) {
		this->m_renderer->AddActor2D(glyphs->colorbar);
	}
	VTKAnatomicalVolume* vol = dynamic_cast<VTKAnatomicalVolume*> (object);
	if (vol != nullptr) {
		this->m_renderer->AddActor2D(vol->colorbar);
	}

	this->m_controller->AddVisualObjectToUi(object->type, object->GetSettings()->label);

	ResetCamera();
	Render();

	matchOrientationData();
}

void Visualization::RemoveVisualObjectFrom(DataObject* object) {

	for (int i = 0; i < this->m_visualsObjects.size(); i++) {

		VisualizationObject* visualObj = this->m_visualsObjects.at(i);
		if (visualObj->getDataObject() == object) {

			m_renderer->RemoveActor(visualObj->GetVisuals((int)Dimensionality::DIM_3D));
			m_renderer->RemoveActor(visualObj->GetVisuals((int)Dimensionality::DIM_2D));

			this->m_visualsObjects.removeAll(visualObj);

			delete visualObj;
		}
	}

	ResetCamera();
	Render();
}

void Visualization::UpdateVisualObjectFrom(DataObject* object) {

	for (int i = 0; i < this->m_visualsObjects.size(); i++) {

		VisualizationObject* visualObj = this->m_visualsObjects.at(i);
		if (visualObj->getDataObject() == object) {

			m_renderer->RemoveActor(visualObj->GetVisuals((int)Dimensionality::DIM_3D));
			m_renderer->RemoveActor(visualObj->GetVisuals((int)Dimensionality::DIM_2D));

			visualObj->ReloadFromSource();

			m_renderer->AddActor(visualObj->GetVisuals(this->m_dimensionality));
		}
	}

	Render();
}

void Visualization::Render() {
	if (m_renderWindow->IsDrawable())
		m_renderWindow->Render();
}

void Visualization::UpdateVisuals(VisualSettings* m_settings) {

	//NOT USED

	QListIterator<VisualizationObject*> i(this->m_visualsObjects);
	while (i.hasNext()) {
		VisualizationObject* obj = i.next();

		if (obj->GetSettings()->label == m_settings->label) {
			m_settings->isDirty = true;
			obj->SetSettings(m_settings);
		}
	}

	this->Render();
}

VisualizationObject * Visualization::GetVisualObject(VisualType type, QString label) {
	
	QList<VisualizationObject *> found;

	// First search by type
	QListIterator<VisualizationObject *> i(m_visualsObjects);
	while (i.hasNext()) {
		VisualizationObject *obj = i.next();
		if (obj->type == type && obj->GetSettings()->label == label) {
			return obj;
		}
	}

	return nullptr;
}

VisualizationObject * Visualization::GetVisualObjectByType(VisualType type, QString label) {

	QList<VisualizationObject *> found;

	// First search by type
	QListIterator<VisualizationObject *> i(m_visualsObjects);
	while (i.hasNext()) {
		VisualizationObject *obj = i.next();
		if (obj->type == type) {
			found.append(obj);
		}
	}

	if (found.size() == 1) {
		return found[0];
	}

	// Second search by label
	if (label != "") {
		QListIterator<VisualizationObject *> j(found);
		while (j.hasNext()) {
			VisualizationObject *obj = j.next();
			if (obj->GetSettings()->label == label) {
				return obj;
			}
		}
	}
	return nullptr;
}

void Visualization::SetFocus(double x, double y, double z) {
	double* currentFocus = this->m_renderer->GetActiveCamera()->GetFocalPoint();

	if (x == -1) {
		x = currentFocus[0];
	}
	if (y == -1) {
		y = currentFocus[1];
	}
	if (z == -1) {
		z = currentFocus[2];
	}

	this->m_renderer->GetActiveCamera()->SetFocalPoint(x, y, z);
	ResetCamera();
}

void Visualization::UpdateNavigation() {
	if (!this->renderProperties.isDirty) {
		return;
	}

	QListIterator<VisualizationObject*> i(this->m_visualsObjects);
	while (i.hasNext()) {

		VisualizationObject* obj = i.next();

		VisualSettings* m_settings = obj->GetSettings();
		m_settings->positionSettings.orient = this->renderProperties.positionSettings.orient;
		m_settings->positionSettings.position = this->renderProperties.positionSettings.position;
		m_settings->positionSettings.isDirty = true;

		obj->SetSettings(m_settings);
	}

	if (this->m_dimensionality == Dimensionality::DIM_2D) {
		ResetCamera();
	}

	this->Render();
}

void Visualization::RenameObj(QString old, QString next) {

	QListIterator<VisualizationObject*> i(this->m_visualsObjects);
	while (i.hasNext()) {
		VisualizationObject* obj = i.next();

		if (obj->GetSettings()->label == old) {
			obj->GetSettings()->label = next;
		}
	}
}

void Visualization::SetObjectDimensionality(int t_dim) {

	if (t_dim == this->m_dimensionality) {
		return;
	}

	this->m_dimensionality = Dimensionality(t_dim);

	QListIterator<VisualizationObject*> i(this->m_visualsObjects);
	while (i.hasNext()) {
		VisualizationObject* obj = i.next();

		// Remove old
		m_renderer->RemoveActor(obj->GetVisuals((int)Dimensionality::DIM_2D));
		m_renderer->RemoveActor(obj->GetVisuals((int)Dimensionality::DIM_3D));

		// Add
		m_renderer->AddActor(obj->GetVisuals((int)t_dim));
	}

	// Change interactor type and view
	if (this->m_dimensionality == Dimensionality::DIM_2D) {

		this->ResetCamera();

		VTK_CREATE(Interactor2D, style);
		this->m_renderWindow->GetInteractor()->SetInteractorStyle(style);
	}
	else if (this->m_dimensionality == Dimensionality::DIM_3D) {

		VTK_CREATE(vtkInteractorStyleTrackballCamera, style);
		this->m_renderWindow->GetInteractor()->SetInteractorStyle(style);
	}

	this->Render();
}

void Visualization::SetInterpolation(int order) {

	QListIterator<VisualizationObject*> i(this->m_visualsObjects);
	while (i.hasNext()) {

		VisualizationObject* obj = i.next();

		// VTYPE specific
		VTKScalarProjection* scalarMap = dynamic_cast<VTKScalarProjection*> (obj);
		if (scalarMap != nullptr) {
			VisualSettingsScalarProjection* m_settings =
				dynamic_cast<VisualSettingsScalarProjection*>(scalarMap->GetSettings());
			m_settings->interpolationOrder = order;
			m_settings->isDirty = true;
			scalarMap->SetSettings(m_settings);
		}

		VTKRegion* region = dynamic_cast<VTKRegion*> (obj);
		if (region != nullptr) {
			VisualSettingsRegion* m_settings =
				dynamic_cast<VisualSettingsRegion*>(region->GetSettings());
			m_settings->interpolationOrder = order;
			m_settings->isDirty = true;
			region->SetSettings(m_settings);
		}

		VTKAnatomicalVolume* vol = dynamic_cast<VTKAnatomicalVolume*> (obj);
		if (vol != nullptr) {
			VisualSettingsVolume* m_settings =
				dynamic_cast<VisualSettingsVolume*>(vol->GetSettings());
			m_settings->interpolationOrder = order;
			m_settings->isDirty = true;
			vol->SetSettings(m_settings);
		}
	}

	this->Render();
}

void Visualization::matchOrientationData() {

	VTKScalarProjection* scalars = nullptr;
	VTKGeodesics* geo = nullptr;

	for (int i = 0; i < this->m_visualsObjects.size(); i++) {

		VisualizationObject* obj = this->m_visualsObjects.at(i);

		VTKScalarProjection* tempScalars = dynamic_cast<VTKScalarProjection*>(obj);
		if (tempScalars && scalars == nullptr) {
			scalars = tempScalars;
		}

		VTKGeodesics* temGeo = dynamic_cast<VTKGeodesics*>(obj);
		if (temGeo && geo == nullptr) {
			geo = temGeo;
		}
	}

	if (scalars && geo) {
		scalars->SetOrientationData(dynamic_cast<GeodesicSource*>(geo->getDataObject())->data);
	}
}

bool Visualization::CheckGeodesicLoaded() {

	bool found = false;

	for (int i = 0; i < this->m_visualsObjects.size(); i++) {

		if (dynamic_cast<VTKGeodesics*>(this->m_visualsObjects.at(i))) {
			found = true;
		}
	}

	return found;
}

SP(vtkImageData) Visualization::GetExportData(VisualType visualType, Projection::ProjectionType type) {

	switch (visualType) {
	case VTYPE_GLYPHS:
		break;
	case VTYPE_SCALARMAP: {

		VTKScalarProjection* scalars = dynamic_cast<VTKScalarProjection*>(this->GetVisualObjectByType(visualType));
		return scalars->GetExportData(type);
		break;
	}
	case VTYPE_CONTOURPLOT:
		break;
	case VTYPE_ROI:
		break;
	case VTYPE_GEODESIC:
		break;
	default:
		break;
	}

	return nullptr;
}